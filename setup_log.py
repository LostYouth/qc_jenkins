import os, sys, json, logging, traceback
import queue
from logging.handlers import DatagramHandler
from datetime import datetime

from PyQt5 import QtCore

from __app_name__ import __app_name__
from app_setting import AppSetting


log_server_param = ('10.10.77.229', 5005)
log_level_server = logging.DEBUG


class UdpHandler(DatagramHandler):
    def __init__(self, *args, **kwargs):
        super(UdpHandler, self).__init__(*args, **kwargs)

    def emit(self, record):
        try:
            self.send(record)
        except Exception as err:
            pass

    def send(self, s):
        msg = {
            'prog': __app_name__,
            'text': "{:<28} {} {}".format(datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f"), s.levelname, s.getMessage())
        }
        if self.sock is None:
            self.sock = self.makeSocket()
        msg = json.dumps(msg)
        self.sock.sendto(msg.encode('utf8'), self.address)


# catch err
class ExceptionHandler(QtCore.QObject):
    errorSignal = QtCore.pyqtSignal(object, object, object)

    def __init__(self):
        super(ExceptionHandler, self).__init__()

    def handler(self, exctype, value, traceback):
        self.errorSignal.emit(exctype, value, traceback)
        if issubclass(exctype, KeyboardInterrupt):
            sys.__excepthook__(exctype, value, traceback)
            sys.exit(1)
        sys.__excepthook__(exctype, value, traceback)


def log_error(exctype_class, exctype_err, traceback_err):
    logger = logging.getLogger(AppSetting.LOGGER_NAME)
    logger.critical("{} {} {}".format(exctype_class, exctype_err, traceback), exc_info=(exctype_class, exctype_err, traceback_err))


def setup_logging():
    if not os.path.exists('logs'):
        os.mkdir('logs')

    logger = logging.getLogger(AppSetting.LOGGER_NAME)
    logger.setLevel(logging.DEBUG)
    logger.propagate = False

    path_log = os.path.join("logs", 'qc_log.log')

    handler_console = logging.StreamHandler(stream=sys.stdout)
    handler_console.setLevel(logging.DEBUG)
    handler_console.setFormatter(logging.Formatter('%(levelname)-10s [%(asctime)s] %(message)s'))

    que = queue.Queue(156)
    queue_handler = logging.handlers.QueueHandler(que)

    handler_log_server_upd = UdpHandler(*log_server_param)
    handler_log_server_upd.setLevel(log_level_server)

    handler_file = logging.handlers.RotatingFileHandler(path_log, maxBytes=5 * 1024 * 1024, backupCount=1, delay=0)
    handler_file.setLevel(logging.DEBUG)
    handler_file.setFormatter(logging.Formatter('%(levelname)-6s [%(asctime)s] %(message)s'))

    logger.addHandler(handler_console)
    logger.addHandler(handler_file)

    listener = logging.handlers.QueueListener(que, handler_log_server_upd)
    logger.addHandler(queue_handler)
    listener.start()
