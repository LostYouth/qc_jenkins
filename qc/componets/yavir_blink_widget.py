
from PyQt5 import QtWidgets, QtCore

from qc.componets.repair_name_obj.repair_name_obj import RepairNameObj
from qc.enum_color import ColorResult


class YavirBlinkWidget(QtWidgets.QWidget, RepairNameObj):
    def __init__(self, message_label, message_btn):
        super(YavirBlinkWidget, self).__init__()
        l = QtWidgets.QVBoxLayout()
        l.setAlignment(QtCore.Qt.AlignCenter)
        self.check_box = QtWidgets.QCheckBox(message_label)
        self.check_box.stateChanged.connect(self.change_state)
        l.addWidget(self.check_box)
        self.push_btn_changed = QtWidgets.QPushButton(message_btn)
        l.addWidget(self.push_btn_changed)
        self.setLayout(l)

    def get_state(self):
        return self.check_box.isChecked()

    def change_state(self):
        self.setStyleSheet(ColorResult.success.value if self.get_state() else ColorResult.fail.value)

    def change_data_btn(self, value):
        self.push_btn_changed.setText("{}".format(value))

