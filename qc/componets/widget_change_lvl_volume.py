import enum

from PyQt5 import QtWidgets, QtCore


class EnumVolumeLevel(enum.Enum):
    """ Enum volume level for family siren"""
    Low = 31
    MidLow = 24
    Mid = 17
    MidHigh = 10
    High = 3


class WidgetChangeLvlVolume(QtWidgets.QWidget):
    signal_change_value_volume = QtCore.pyqtSignal(str, int)        # dev id, value
    max_value = EnumVolumeLevel.__len__() - 1
    min_value = 0

    def __init__(self, dev_id: str):
        super(WidgetChangeLvlVolume, self).__init__()
        main_layout = QtWidgets.QVBoxLayout()
        self.setLayout(main_layout)

        self.label_current_value = QtWidgets.QLabel("")
        self.label_current_value.setAlignment(QtCore.Qt.AlignCenter)
        main_layout.addWidget(self.label_current_value)

        self.slider = QtWidgets.QSlider()
        self.slider.setOrientation(QtCore.Qt.Horizontal)
        self.slider.setMaximum(WidgetChangeLvlVolume.max_value)
        self.slider.setMinimum(WidgetChangeLvlVolume.min_value)
        self.slider.setPageStep(1)
        self.slider.setSingleStep(1)

        main_layout.addWidget(self.slider)

        self.dev_id = dev_id
        self._current_value = EnumVolumeLevel.Mid
        self._set_current_value_label(self._current_value)
        self.slider.setValue(2)
        self.slider.valueChanged.connect(self._hand_change_val_slider)

    def _hand_change_val_slider(self, current_index: int):
        val = list(EnumVolumeLevel)[current_index]
        self.current_value = val
        self.signal_change_value_volume.emit(self.dev_id, val.value)

    @property
    def current_value(self):
        return self._current_value

    @current_value.setter
    def current_value(self, val: EnumVolumeLevel):
        self._current_value = val
        self._set_current_value_label(val)

    def _set_current_value_label(self, val):
        desc_val = "невідомо"
        if val == EnumVolumeLevel.Low:
            desc_val = 'низька'
        elif val == EnumVolumeLevel.MidLow:
            desc_val = 'менше середнього'
        elif val == EnumVolumeLevel.Mid:
            desc_val = 'середня'
        elif val == EnumVolumeLevel.MidHigh:
            desc_val = 'вище середнього'
        elif val == EnumVolumeLevel.High:
            desc_val = 'максимальна'
        self.label_current_value.setText("Гучність: {}".format(desc_val))
