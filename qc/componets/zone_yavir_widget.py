from PyQt5 import QtWidgets, QtCore

from qc.componets.repair_name_obj.repair_name_obj import RepairNameObj
from qc.enum_color import ColorResult


class WidgetDeviceYavir(QtWidgets.QWidget, RepairNameObj):

    def __init__(self, zones_count):
        super(WidgetDeviceYavir, self).__init__()
        row_length = zones_count / 2
        main_layout_widget = QtWidgets.QGridLayout()

        self.relations_label_sensor_id = dict()

        for zone in range(zones_count):
            label = QtWidgets.QLabel('False')
            self._change_background_color_label(label, False)
            self.relations_label_sensor_id['2600000{}'.format(zone + 1)] = label
            label.setAlignment(QtCore.Qt.AlignCenter)

            row = 0 if zone < row_length else 1
            col = zone - row_length if zone >= row_length else zone
            main_layout_widget.addWidget(label, row, col)

        self.setLayout(main_layout_widget)
        self.setEnabled(False)

    def change_state_device(self, device_id: str):
        """ if have alarm with yavir hub for device
            device_id example : 26000001, 26000002, 26000003
        """
        label = self.relations_label_sensor_id.get(device_id, False)
        if label:
            self._change_background_color_label(label, True)

    def _change_background_color_label(self, label: QtWidgets.QLabel, state: bool):
        if state:
            label.setStyleSheet(ColorResult.success.value)
            label.setText("True")
        else:
            label.setStyleSheet(ColorResult.fail.value)
            label.setText("False")

    def get_state(self):
        return self.get_state_alarms_device()

    def get_state_alarms_device(self) -> bool:
        """ return true if label has green backGround else False"""
        return all([eval(label.text()) for label in self.relations_label_sensor_id.values()])
