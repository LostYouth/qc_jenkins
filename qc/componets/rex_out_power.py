from PyQt5 import QtWidgets, QtCore, QtGui

from qc.enum_color import ColorResult
from .repair_name_obj.repair_name_obj import RepairNameObj


class RexOutPower(QtWidgets.QLabel, RepairNameObj):
    def __init__(self, status='Невідомо', _expected_res=1):
        super(RexOutPower, self).__init__(status)
        self.setText(status)
        self.setAlignment(QtCore.Qt.AlignCenter)
        self._count_lost_power = 0
        self._expected_res = _expected_res
        self._status_state = {
            '': 'Невідомо',
            '00': "Відсутнє",
            '01': "Підключене"
        }

    def change_state(self, status: str):
        """ exampel status '', '00', '01'"""
        self.setText(self._status_state.get(status, 'Unknow'))
        self.check_result()

    def increment(self):
        self._count_lost_power += 1

    def get_state(self):
        return self._count_lost_power >= self._expected_res and self.text() == self._status_state['01']

    def check_result(self):
        if self.get_state():
            self.setStyleSheet(ColorResult.success.value)
        else:
            self.setStyleSheet(ColorResult.fail.value)

    def set_expected_result(self, val: int):
        self._expected_res = val
