from PyQt5 import QtWidgets

from qc.componets.repair_name_obj.repair_name_obj import RepairNameObj


class WifiSearchHubPlus(QtWidgets.QPushButton, RepairNameObj):
    def __init__(self, text_btn=''):
        super(WifiSearchHubPlus, self).__init__()

        self.setText(text_btn)
        self._state = False

    def get_state(self):
        return self._state

    def set_state(self, state: bool):
        self._state = state
