import logging
import os

from collections import defaultdict

from PyQt5 import QtWidgets, QtCore, QtGui

from app_setting import AppSetting

logger = logging.getLogger(AppSetting.LOGGER_NAME)


class WidgetMotionCamShowImages(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(WidgetMotionCamShowImages, self).__init__(parent=parent, flags=QtCore.Qt.Popup)
        self.parent = parent
        self.qr_path2images = defaultdict(list)

        l = QtWidgets.QVBoxLayout()
        self.label_qr_code = QtWidgets.QLabel('')
        self.label_qr_code.setAlignment(QtCore.Qt.AlignCenter)
        l.addWidget(self.label_qr_code)
        self.label_show_image = QtWidgets.QLabel("image")
        l.addWidget(self.label_show_image)

        # botton widget
        l_botton = QtWidgets.QHBoxLayout()
        self.btn_l_image = QtWidgets.QPushButton(" < ")
        self.btn_r_image = QtWidgets.QPushButton(" > ")
        self.btn_l_image.setMaximumHeight(20)
        self.btn_l_image.setMaximumWidth(55)
        self.btn_r_image.setMaximumHeight(20)
        self.btn_r_image.setMaximumWidth(55)

        self.label_count_images = QtWidgets.QLabel('0')
        self.label_count_images.setAlignment(QtCore.Qt.AlignCenter)

        l_botton.addWidget(self.btn_l_image)
        l_botton.addWidget(self.label_count_images)
        l_botton.addWidget(self.btn_r_image)
        l.addLayout(l_botton)
        self.setLayout(l)

        self._current_qr = None
        self._current_index = 0

        self.btn_r_image.clicked.connect(self.next_image)
        self.btn_l_image.clicked.connect(self.previous_image)

    def move_center(self):
        self.move(
            self.parent.window().frameGeometry().topLeft() + self.parent.window().rect().center() - self.rect().center()
        )

    def delete_data(self, qr):
        self._remove_images_by_qr(qr)
        self.qr_path2images.pop(qr)

    def _remove_images_by_qr(self, qr: str):
        images = self.qr_path2images[qr]
        for path_images in images:
            os.remove(path_images)
            logger.debug("Delete file {}".format(path_images))

    def set_data(self, qr, path2image):
        if path2image not in self.qr_path2images[qr]:
            self.qr_path2images[qr].append(path2image)

    @property
    def current_qr(self):
        return self._current_qr

    @current_qr.setter
    def current_qr(self, qr: str):
        self._current_qr = qr
        self.label_qr_code.setText("Device: {}".format(qr))

    @property
    def current_index(self):
        return self._current_index

    @current_index.setter
    def current_index(self, index: int):
        c_images = len(self.qr_path2images[self.current_qr])
        if not c_images:
            self.label_count_images.setText("Немає доступних фото")
        elif index not in range(0, c_images):
            pass
        else:
            self._current_index = index
            self.label_count_images.setText("{}/{}".format(
                str(index + 1),
                len(self.qr_path2images[self.current_qr])
            )
            )
            self.set_image_label(index)

    def set_image_label(self, index):
        if not len(self.qr_path2images[self.current_qr]):
            return
        image = self.qr_path2images[self.current_qr][index]
        pixmap = QtGui.QPixmap(image)
        self.label_show_image.setPixmap(pixmap)

    def next_image(self):
        c_index = self.current_index + 1
        self.current_index = c_index

    def previous_image(self):
        self.current_index -= 1

    def show_device_image(self, qr: str):
        self.move_center()
        self.current_qr = qr
        self.current_index = 0
        self.show()

    def insert_qr_images(self, qr, path2images):
        images = self.qr_path2images[qr]
        images.append(path2images)

