from PyQt5 import QtWidgets, QtCore

from qc.componets.repair_name_obj.repair_name_obj import RepairNameObj
from qc.enum_color import ColorResult


class IncrementWidget(QtWidgets.QLabel, RepairNameObj):
    def __init__(self, *args):
        super(IncrementWidget, self).__init__(*args)
        self.setText('0')
        self.setAlignment(QtCore.Qt.AlignCenter)
        self._expected_result = 0

    def set_expected_result(self, res: int):
        self._expected_result = res

    def get_state(self):
        return self.check_result()

    def get_current_val(self):
        return int(self.text())

    def increment(self):
        current_val = self.get_current_val()
        self.setText("{}".format(current_val + 1))
        self.check_result()

    def check_result(self):
        if self.get_current_val() >= self._expected_result:
            self.setStyleSheet(ColorResult.success.value)
            return True
        self.setStyleSheet(ColorResult.fail.value)
        return False
