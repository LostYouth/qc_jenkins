from PyQt5 import QtWidgets, QtCore

from qc.componets.repair_name_obj.repair_name_obj import RepairNameObj
from qc.enum_color import ColorResult


class LedsStatusWidget(QtWidgets.QWidget, RepairNameObj):
    signal_turn_leds_on = QtCore.pyqtSignal()
    signal_update_status = QtCore.pyqtSignal()

    def __init__(self, message_text):
        super(LedsStatusWidget, self).__init__()
        layout_m = QtWidgets.QVBoxLayout()
        self.turn_leds_on = QtWidgets.QPushButton("Включити підсвітку")
        self.turn_leds_on.clicked.connect(self.signal_turn_leds_on.emit)
        layout_m.addWidget(self.turn_leds_on)
        self.check_box_select = QtWidgets.QCheckBox(message_text)
        self.check_box_select.stateChanged.connect(self.check_state)
        layout_m.addWidget(self.check_box_select)
        self.setLayout(layout_m)

    def get_state(self):
        return self.check_box_select.isChecked()

    def check_state(self):
        if self.check_box_select.isChecked():
            self.setStyleSheet(ColorResult.success.value)
        else:
            self.setStyleSheet(ColorResult.fail.value)
        self.signal_update_status.emit()
