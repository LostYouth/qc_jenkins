from PyQt5 import QtWidgets, QtCore


class ChangePasswordKeypad(QtWidgets.QPushButton):
    def __init__(self, *args):
        super(ChangePasswordKeypad, self).__init__(*args)

        self._state = False

    def get_state(self):
        return self._state
