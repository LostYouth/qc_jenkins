from PyQt5 import QtWidgets, QtCore

from qc.componets.repair_name_obj.repair_name_obj import RepairNameObj
from qc.enum_color import ColorResult


class VoltageStatusWallSwitch(QtWidgets.QLabel, RepairNameObj):
    def __init__(self, min_voltage, max_voltage):
        super(VoltageStatusWallSwitch, self).__init__()

        self.setText("0")
        self.setAlignment(QtCore.Qt.AlignCenter)
        self.min_v = min_voltage
        self.max_v = max_voltage

    def get_state(self):
        return self.check_state()

    def set_value(self, val: int):
        self.setText(str(val))
        self._change_color()

    def _change_color(self):
        if self.get_state():
            self.setStyleSheet(ColorResult.success.value)
        else:
            self.setStyleSheet(ColorResult.fail.value)

    def check_state(self):
        val = int(self.text())
        if self.min_v <= val <= self.max_v:
            return True
        else:
            return False
