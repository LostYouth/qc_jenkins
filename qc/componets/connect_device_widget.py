from PyQt5 import QtWidgets, QtCore

from qc.enum_color import ColorResult


class ConnectDeviceWidget(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(ConnectDeviceWidget, self).__init__(parent=parent)
        l = QtWidgets.QVBoxLayout()
        l.setAlignment(QtCore.Qt.AlignCenter)
        self.push_btn_connect_device = QtWidgets.QPushButton('Почати')
        self.push_btn_connect_device.clicked.connect(self.change_cont_click_device)
        l.addWidget(self.push_btn_connect_device)
        self.setLayout(l)

        self._state = False

        self._count_click_connect_device = 0

    def get_state_repair(self):
        s = self.styleSheet()
        if s == ColorResult.fail.value:
            return True
        return False

    def change_cont_click_device(self):
        self._count_click_connect_device += 1

    def check_count_clicked_connect(self):
        return bool(self._count_click_connect_device)

    def get_state(self):
        return self._state

    def set_state(self, val: bool):
        if val:
            self.setEnabled(False)
        self._state = val
