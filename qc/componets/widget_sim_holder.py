from PyQt5 import QtWidgets, QtCore

from qc.componets.repair_name_obj.repair_name_obj import RepairNameObj
from qc.enum_color import ColorResult


class WidgetSimHolder(QtWidgets.QWidget, RepairNameObj):

    def __init__(self):
        super(WidgetSimHolder, self).__init__()

        self.label_sim_one = QtWidgets.QLabel("Sim1")
        self._state_sim_one = False

        self.label_second_sim = QtWidgets.QLabel("Sim2")
        self._state_sim_second = False

        main_layout = QtWidgets.QVBoxLayout()
        main_layout.setAlignment(QtCore.Qt.AlignCenter)
        main_layout.addWidget(self.label_sim_one)
        main_layout.addWidget(self.label_second_sim)

        self.setLayout(main_layout)

    def get_state(self):
        return self._state_sim_one and self._state_sim_second

    def set_state_one(self, state):
        self._change_state_sim(self.label_sim_one, state)
        self._state_sim_one = state

    def set_state_second(self, state):
        self._change_state_sim(self.label_second_sim, state)
        self._state_sim_second = state

    def _change_state_sim(self, label, state):
        c = ColorResult.success.value if state else ColorResult.fail.value
        label.setStyleSheet(c)
