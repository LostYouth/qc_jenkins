from PyQt5 import QtWidgets, QtCore

from qc.componets.repair_name_obj.repair_name_obj import RepairNameObj
from qc.enum_color import ColorResult


class AccelerometerStreetSiren(QtWidgets.QWidget, RepairNameObj):
    def __init__(self):
        super(AccelerometerStreetSiren, self).__init__()

        l = QtWidgets.QVBoxLayout()

        self.label_increment = QtWidgets.QLabel('0')
        self.label_increment.setAlignment(QtCore.Qt.AlignCenter)

        l.addWidget(self.label_increment)

        self.push_btn_state_accelerometer = QtWidgets.QPushButton("Відключити\nакселерометр")

        l.addWidget(self.push_btn_state_accelerometer)

        self.setLayout(l)

        self._expected_result = 0

    def change_state_accelerometer(self, state: str):
        if state == '00':
            self.push_btn_state_accelerometer.setText("Включити\nакселерометр")
        elif state == '01':
            self.push_btn_state_accelerometer.setText("Відключити\nакселерометр")

    def set_expected_result(self, res: int):
        self._expected_result = res

    def get_state(self):
        return self.check_result()

    def get_current_val(self):
        return int(self.label_increment.text())

    def increment(self):
        current_val = self.get_current_val()
        self.label_increment.setText("{}".format(current_val + 1))
        self.check_result()

    def check_result(self):
        if self.get_current_val() >= self._expected_result:
            self.setStyleSheet(ColorResult.success.value)
            return True
        self.setStyleSheet(ColorResult.fail.value)
        return False
