from PyQt5 import QtWidgets, QtCore


class SearchDevicePopup(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(SearchDevicePopup, self).__init__(parent=parent, flags=QtCore.Qt.Popup)
        l = QtWidgets.QVBoxLayout()
        l.setAlignment(QtCore.Qt.AlignCenter)

        self.setLayout(l)

        self.setMinimumSize(int(parent.width() * 0.3), int(parent.height() * 0.3))

        self.label_status = QtWidgets.QLabel("1")

        self.label_des = QtWidgets.QLabel("Виключіть та включить пристрій")
        self.label_des.setAlignment(QtCore.Qt.AlignCenter)

        self.label_status.setMinimumSize(int(parent.width() * 0.2), int(parent.height() * 0.2))
        self.label_status.setAlignment(QtCore.Qt.AlignCenter)
        self.push_btn_cancel_register_device = QtWidgets.QPushButton("Відмінити")
        self.timer = QtCore.QTimer()
        self.timer.setInterval(1000)
        self.timer.timeout.connect(self._change_status)
        l.addWidget(self.label_status)
        l.addWidget(self.label_des)
        l.addWidget(self.push_btn_cancel_register_device)
        self._flag_close = False

        self._counts_timeout = 32

    def move_center(self):
        if self.parent():
            self.move(self.parent().window().frameGeometry().topLeft() + self.parent().window().rect().center() - self.rect().center())

    def _change_status(self):
        count = self.label_status.text()
        count = int(count) + 1
        if count > self._counts_timeout:
            count = 1
        self.label_status.setText("{}".format(count))

    def close_timeout(self):
        self._flag_close = True
        self.close()

    def show(self):
        self._flag_close = False
        self.label_status.setText("{}".format(1))
        self.timer.start()
        self.move_center()
        super(SearchDevicePopup, self).show()

    def closeEvent(self, QCloseEvent):
        if not self._flag_close:
            QCloseEvent.ignore()
            return
        self.timer.stop()
        super(SearchDevicePopup, self).closeEvent(QCloseEvent)
