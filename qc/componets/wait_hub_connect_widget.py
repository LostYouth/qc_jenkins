from PyQt5 import QtWidgets, QtCore


class WaitHubConnect(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(WaitHubConnect, self).__init__(flags=QtCore.Qt.SplashScreen, parent=parent)
        l = QtWidgets.QVBoxLayout()
        self.setLayout(l)

        self.setMinimumSize(int(self.parent().width() * 0.1), int(self.parent().height() * 0.1))
        self.setMaximumSize(int(self.parent().width() * 0.4), int(self.parent().height() * 0.4))
        self.resize(int(self.parent().width() * 0.2), int(self.parent().height() * 0.2))
        self.label_status = QtWidgets.QLabel('Почекайте')
        self.label_status.setAlignment(QtCore.Qt.AlignCenter)
        l.addWidget(self.label_status)
        self.label_update = QtWidgets.QLabel('*')
        l.addWidget(self.label_update)
        self.label_update.setAlignment(QtCore.Qt.AlignCenter)

        self.__count = 1
        self.timer_update = QtCore.QTimer()
        self.timer_update.setInterval(500)
        self.timer_update.timeout.connect(self._update)

    def _update(self):
        if self.__count > 50:
            self.__count = 0
        self.__count += 1
        self.label_update.setText("*" * self.__count)
        self.move_center()

    def move_center(self):
        self.move(self.parent().window().frameGeometry().topLeft() + self.parent().window().rect().center() - self.rect().center())

    def show(self):
        self.timer_update.start()
        super(WaitHubConnect, self).show()
        self.move_center()
