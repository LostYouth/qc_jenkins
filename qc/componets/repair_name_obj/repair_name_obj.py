class RepairNameObj:
    def __init__(self):
        self._malfunction_name = None

    def set_malfunction_name(self, name_malf: str):
        self._malfunction_name = name_malf

    def get_malfunction_name(self):
        return self._malfunction_name
