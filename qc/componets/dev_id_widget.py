from PyQt5 import QtWidgets, QtCore


class DevIdWidget(QtWidgets.QLabel):
    def __init__(self, qr_code):
        super(DevIdWidget, self).__init__(qr_code)
        self.setAlignment(QtCore.Qt.AlignCenter)
        self._qr_code = qr_code
        self._dev_id = self._qr_code[:6]

    def get_qr_code(self):
        return self._qr_code

    def get_id(self):
        return self._dev_id

    def get_state(self):
        return bool(self.get_id())
