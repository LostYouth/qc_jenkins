from PyQt5 import QtWidgets, QtCore


class IdWidget(QtWidgets.QWidget):
    def __init__(self):
        super(IdWidget, self).__init__()
        main_widget_layout = QtWidgets.QVBoxLayout()
        main_widget_layout.setAlignment(QtCore.Qt.AlignCenter)
        self.label_id = QtWidgets.QLabel("")
        self.label_master_key = QtWidgets.QLabel("")
        self.setLayout(main_widget_layout)
        main_widget_layout.addWidget(self.label_id)
        main_widget_layout.addWidget(self.label_master_key)

    def get_state(self):
        return bool(self.label_id.text()) and bool(self.label_master_key.text())

    def set_id_hub(self, qr_code_hub):
        self._qr_code = qr_code_hub
        hub_id, master_key = qr_code_hub[:8], qr_code_hub[8:16]
        self.label_id.setText(hub_id)
        self.label_master_key.setText(master_key)

    def get_hub_id(self):
        return self.label_id.text()

    def get_master_key(self):
        return self.label_master_key.text()

    def get_qr_code(self):
        c1 = self._qr_code[:5]
        c2 = self._qr_code[5:10]
        c3 = self._qr_code[10:15]
        c4 = self._qr_code[15:20]
        return "{}-{}-{}-{}".format(c1, c2, c3, c4)
