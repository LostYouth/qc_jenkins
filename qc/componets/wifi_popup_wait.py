from PyQt5 import QtWidgets, QtCore, QtGui


class WifiPopupWait(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(WifiPopupWait, self).__init__(flags=QtCore.Qt.Popup, parent=parent)
        self._qc_main_window = parent
        self._flag_closed = False
        l = QtWidgets.QVBoxLayout()
        l.setAlignment(QtCore.Qt.AlignCenter)
        self.setLayout(l)
        self.label_wait = QtWidgets.QLabel("Пошук")
        self.__counter = 1
        self.label_status = QtWidgets.QLabel('.' * self.__counter)
        self.timer_change_status = QtCore.QTimer()
        self.timer_change_status.setInterval(1000)
        self.timer_change_status.timeout.connect(self._change_status)
        l.addWidget(self.label_wait)
        l.addWidget(self.label_status)

        self.setMinimumWidth(200)
        self.setMinimumHeight(120)

    def _change_status(self):
        self.__counter += 1
        if self.__counter > 11:
            self.__counter = 1
        self.label_status.setText("." * self.__counter)

    def move_center(self):
        self.move(self.parent().window().frameGeometry().topLeft() + self.parent().window().rect().center() - self.rect().center())

    def custom_close(self):
        self._flag_closed = True
        self.close()

    def show(self):
        self._qc_main_window.setGraphicsEffect(QtWidgets.QGraphicsBlurEffect())
        self._flag_closed = False
        self.move_center()
        self.timer_change_status.start()
        super(WifiPopupWait, self).show()

    def closeEvent(self, QCloseEvent):
        if not self._flag_closed:
            QCloseEvent.ignore()
            return
        self.timer_change_status.stop()
        self._qc_main_window.setGraphicsEffect(None)
        super(WifiPopupWait, self).closeEvent(QCloseEvent)
