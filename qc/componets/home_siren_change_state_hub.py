from PyQt5 import QtWidgets

from qc.componets.repair_name_obj.repair_name_obj import RepairNameObj
from qc.enum_color import ColorResult


class HomeSirenChangeStateHub(QtWidgets.QWidget, RepairNameObj):
    def __init__(self, message_text_box, message_text_btn):
        super(HomeSirenChangeStateHub, self).__init__()
        main_layout = QtWidgets.QVBoxLayout()

        self.setLayout(main_layout)

        self.check_box = QtWidgets.QCheckBox(message_text_box)
        self.check_box.stateChanged.connect(self.check_state)
        main_layout.addWidget(self.check_box)
        self.push_btn_change_state_hub = QtWidgets.QPushButton(message_text_btn)
        main_layout.addWidget(self.push_btn_change_state_hub)

    def change_state_btn(self, state):
        self.push_btn_change_state_hub.setEnabled(state)

    def get_state(self):
        return self.check_box.isChecked()

    def check_state(self):
        if self.get_state():
            self.setStyleSheet(ColorResult.success.value)
        else:
            self.setStyleSheet(ColorResult.fail.value)
