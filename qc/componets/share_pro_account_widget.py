from PyQt5 import QtWidgets


class ShareProAccountWidget(QtWidgets.QMessageBox):
    def __init__(self, accounts):
        super(ShareProAccountWidget, self).__init__()
        self.setText("Оберіть про аккаунт на який потрібно розшарити хаб: ")
        for val_account in accounts:
            self.addButton(QtWidgets.QPushButton(val_account), QtWidgets.QMessageBox.ActionRole)

    def closeEvent(self, QCloseEvent):
        QCloseEvent.accept()
