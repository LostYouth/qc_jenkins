from PyQt5 import QtWidgets, QtCore

from qc.componets.repair_name_obj.repair_name_obj import RepairNameObj
from qc.enum_color import ColorResult


class StatusWidget(QtWidgets.QWidget, RepairNameObj):
    def __init__(self):
        super(StatusWidget, self).__init__()
        main_widget = QtWidgets.QVBoxLayout()
        self.label_status = QtWidgets.QLabel()
        main_widget.addWidget(self.label_status)
        main_widget.setAlignment(QtCore.Qt.AlignCenter)
        self.setLayout(main_widget)
        self._expected_result = ''

    def check_state(self):
        if self.get_state():
            self.setStyleSheet(ColorResult.success.value)
        else:
            self.setStyleSheet(ColorResult.fail.value)

    def get_state(self):
        if isinstance(self._expected_result, int):
            try:
                return self._expected_result <= int(self.label_status.text(), 10)
            except (ValueError, TypeError) as t_err:
                return False
        else:
            return self._expected_result == self.label_status.text()

    def set_expected_result(self, res):
        self._expected_result = res

    def set_status(self, data: str):
        self.label_status.setText(data)
        self.check_state()
