from PyQt5 import QtWidgets, QtCore


class ErrorPopupWidget(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(ErrorPopupWidget, self).__init__(parent=parent, flags=QtCore.Qt.Popup)
        main_layout = QtWidgets.QVBoxLayout()
        main_layout.setAlignment(QtCore.Qt.AlignCenter)
        self.label_message_text = QtWidgets.QLabel()
        main_layout.addWidget(self.label_message_text)
        self.setLayout(main_layout)

    def move_center(self):
        self.move(
            self.parent().window().frameGeometry().topLeft() + self.parent().window().rect().center() - self.rect().center())

    def show(self):
        super(ErrorPopupWidget, self).show()
        self.move_center()

    def set_message_data(self, message_data):
        self.label_message_text.setText(message_data)
