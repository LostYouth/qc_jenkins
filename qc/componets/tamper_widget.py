from PyQt5 import QtWidgets, QtCore

from qc.componets.repair_name_obj.repair_name_obj import RepairNameObj
from qc.enum_color import ColorResult


class TamperWidget(QtWidgets.QWidget, RepairNameObj):
    def __init__(self):
        super(TamperWidget, self).__init__()
        main_widget_layout = QtWidgets.QVBoxLayout()
        main_widget_layout.setAlignment(QtCore.Qt.AlignCenter)
        self.label_on = QtWidgets.QLabel('ВКЛ: 0/2')
        self.label_off = QtWidgets.QLabel('ВИКЛ: 0/2')
        main_widget_layout.addWidget(self.label_on)
        main_widget_layout.addWidget(self.label_off)

        self.setLayout(main_widget_layout)

    def get_state(self):
        return self._check_success_tampered(self.label_on) and self._check_success_tampered(self.label_off)

    def _change_tamper_count(self, label):
        text_label = label.text()
        _, count_text = text_label.split(' ')
        count_str, expected_count = count_text.split('/')
        count = int(count_str) + 1
        if int(expected_count) <= count:
            label.setStyleSheet(ColorResult.success.value)
        else:
            label.setStyleSheet(ColorResult.fail.value)
        label.setText("{} {}/{}".format(_, count, expected_count))

    def _check_success_tampered(self, label) -> bool:
        _, countered = label.text().split(' ')
        curr, expected = countered.split('/')
        if int(curr) >= int(expected):
            return True
        return False

    def change_tamper_count_on(self):
        self._change_tamper_count(self.label_on)

    def change_tamper_count_off(self):
        self._change_tamper_count(self.label_off)
