from PyQt5 import QtWidgets, QtCore

from qc.componets.repair_name_obj.repair_name_obj import RepairNameObj
from qc.enum_color import ColorResult


class PowerHubWidget(QtWidgets.QWidget, RepairNameObj):
    def __init__(self):
        super(PowerHubWidget, self).__init__()
        main_layout = QtWidgets.QVBoxLayout()
        main_layout.setAlignment(QtCore.Qt.AlignCenter)
        self.label_status = QtWidgets.QLabel("")
        main_layout.addWidget(self.label_status)
        self.setLayout(main_layout)

        self.power_off_flag = False
        self.power_on_flag = False

    def check_success(self):
        if self.get_state():
            self.setStyleSheet(ColorResult.success.value)
        else:
            self.setStyleSheet(ColorResult.fail.value)

    def set_status(self, data):
        self.label_status.setText(data)
        self.check_success()

    def set_power_off_flag(self, val: bool):
        self.power_off_flag = val
        self.check_success()

    def set_power_on_flag(self, val: bool):
        self.power_on_flag = val
        self.check_success()

    def get_state(self):
        return self.power_on_flag and self.power_off_flag
