from enum import Enum

from PyQt5 import QtWidgets

from qc.componets.repair_name_obj.repair_name_obj import RepairNameObj
from qc.enum_color import ColorResult


class EnumStateImage(Enum):
    ImageInit = 'Загрузка фото'                         # default
    ImageNotFound = 'Фото не знайдено'                  # yellow
    ImageAvailable = 'Фото доступне для перегляду'      # green


class WidgetMCamShowWidget(QtWidgets.QWidget):
    def __init__(self, *args):
        super(WidgetMCamShowWidget, self).__init__(*args)
        l = QtWidgets.QVBoxLayout()
        self.setLayout(l)

        # Btn show photo make test_room
        self.btn_show_photos = QtWidgets.QPushButton("Переглянути\nфото")
        l.addWidget(self.btn_show_photos)

        # Check box state ok photo MCam
        self.check_box_status = QtWidgets.QCheckBox("Oк")
        self.check_box_status.stateChanged.connect(self.handle_photos)
        l.addWidget(self.check_box_status)

        self._state_image = EnumStateImage.ImageInit

    def handle_photos(self, state):
        if state:
            self.check_box_status.setStyleSheet(ColorResult.success.value)
        else:
            self.check_box_status.setStyleSheet(ColorResult.fail.value)

    def state_btn(self):
        return self._state_image

    def set_state_btn(self, _state):
        if _state == EnumStateImage.ImageNotFound or _state == EnumStateImage.ImageInit:
            color = ColorResult.fail.value
        elif _state == EnumStateImage.ImageAvailable:
            color = ColorResult.success.value
        else:
            color = ColorResult.unknow.value

        self.btn_show_photos.setStyleSheet(color)
        self._state_image = _state

    def get_state(self):
        s = self.state_btn()
        if EnumStateImage(s.value) == EnumStateImage.ImageAvailable and self.check_box_status.isChecked():
            self.setStyleSheet(ColorResult.success.value)
            return True
        self.setStyleSheet(ColorResult.fail.value)
        return False
