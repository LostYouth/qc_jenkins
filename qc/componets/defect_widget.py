from PyQt5 import QtWidgets, QtCore


class VisualDefectTable(QtWidgets.QWidget):
    signal_update_defects = QtCore.pyqtSignal()

    def __init__(self, defects, parent=None):
        super(VisualDefectTable, self).__init__(parent=parent)
        layout_defect = QtWidgets.QHBoxLayout()
        layout_defect.setAlignment(QtCore.Qt.AlignJustify)
        self.qc_main_widget = parent

        self.push_btn_select_defect = QtWidgets.QPushButton("Обрати")
        self.push_btn_select_defect.clicked.connect(self._show_defect_widget_popup)
        layout_defect.addWidget(self.push_btn_select_defect)

        self.check_box_grade_defect = QtWidgets.QCheckBox("Клас Б")
        self.check_box_grade_defect.stateChanged.connect(self._change_grade_defect)
        layout_defect.addWidget(self.check_box_grade_defect)

        self.defects_popup = DefectWidget(defects, parent=parent)
        self.defects_popup.signal_select_defect.connect(self._handler_defects)

        layout_main_widget = QtWidgets.QVBoxLayout()
        self.setLayout(layout_main_widget)
        layout_main_widget.addLayout(layout_defect)

        self.layout_defect = QtWidgets.QVBoxLayout()
        layout_main_widget.addLayout(self.layout_defect)
        self._result = True

    def get_grade_qc(self):
        return self.check_box_grade_defect.isChecked() and self.check_box_grade_defect.isEnabled()

    def get_defects(self):
        defects = []
        defects += self.defects_popup.get_defects_grade()
        defects += self.defects_popup.get_defects_return()
        defects += self.defects_popup.get_custom_defects()
        return defects

    def _handler_defects(self):
        self._clear_layout_defect()
        defect_grade = self.defects_popup.get_defects_grade()
        defect_return = self.defects_popup.get_defects_return()
        defects_custom = self.defects_popup.get_custom_defects()
        state_d = not bool(defect_return)
        self._result = state_d

        self.change_state_grade_class(state_d)
        for d in defect_return + defect_grade + defects_custom:
            l = QtWidgets.QLabel(str(d))
            self.layout_defect.addWidget(l, alignment=QtCore.Qt.AlignCenter)
        self.signal_update_defects.emit()

    def _clear_layout_defect(self):
        for index in range(self.layout_defect.count()):
            item = self.layout_defect.takeAt(0)
            if not item:
                continue
            self.layout_defect.removeItem(item)
            w = item.widget()
            w.deleteLater()
            self.layout_defect.removeWidget(w)

    def get_state(self):
        if not self.defects_popup.get_defects_return() and not self.defects_popup.get_custom_defects() and not self.defects_popup.get_defects_grade():
            return True

        if self.defects_popup.get_defects_return():
            return False

        if self.defects_popup.get_defects_grade() and self.check_box_grade_defect.isChecked():
            return True

        if self.defects_popup.get_custom_defects() and self.check_box_grade_defect.isChecked():
            return True

        return False

    def change_state_grade_class(self, state: bool):
        self.check_box_grade_defect.setEnabled(state)
        self.signal_update_defects.emit()
        if not state:
            self.check_box_grade_defect.setCheckState(QtCore.Qt.Unchecked)

    def _show_defect_widget_popup(self):
        self.defects_popup.show()

    def _change_grade_defect(self, state):
        if not self.get_defects():
            self.check_box_grade_defect.setCheckState(QtCore.Qt.Unchecked)
            self.qc_main_widget.show_error("Виберіть дефект!")
            return
        self.signal_update_defects.emit()
        self._result = bool(state)


class DefectWidget(QtWidgets.QWidget):
    signal_select_defect = QtCore.pyqtSignal()

    def __init__(self, defects: dict, parent=None):
        super(DefectWidget, self).__init__(flags=QtCore.Qt.Popup, parent=parent)
        self.init_ui()
        self._insert_check_box_defects(defects)

    def init_ui(self):
        self.layout_defects = QtWidgets.QHBoxLayout()
        self.layout_defects_grade = QtWidgets.QVBoxLayout()
        self.layout_defects_return_device = QtWidgets.QVBoxLayout()

        self.layout_botton = QtWidgets.QVBoxLayout()
        self.layout_defects.addLayout(self.layout_defects_grade)
        self.layout_defects.addLayout(self.layout_defects_return_device)

        self.layout_line_edit = QtWidgets.QVBoxLayout()
        self.text_edit_custom_defects = QtWidgets.QTextEdit()
        self.text_edit_custom_defects.setMinimumSize(120, 100)
        self.text_edit_custom_defects.setPlaceholderText(
            'Якщо знайденого вами дефекту немає у списку, опишіть його в цьому полі. '
            'Якщо ви знайшли декілька дефектів, яких немає у списку, використайте для їх розділення крапку з комою, '
            'наприклад: "немає такої-то деталі; така-то деталь погано закріплена"'
        )
        self.layout_line_edit.addWidget(self.text_edit_custom_defects)

        self.push_btn_select_defect = QtWidgets.QPushButton("Обрати")
        self.push_btn_select_defect.clicked.connect(self.close)
        self.layout_botton.addWidget(self.push_btn_select_defect)

        layout_widget = QtWidgets.QVBoxLayout()
        self.setLayout(layout_widget)
        layout_widget.addLayout(self.layout_defects)
        layout_widget.addLayout(self.layout_line_edit)
        layout_widget.addLayout(self.layout_botton)
        self.move_center()

    def move_center(self):
        self.move(self.parent().window().frameGeometry().topLeft() + self.parent().window().rect().center() - self.rect().center())

    def show(self):
        super(DefectWidget, self).show()

    def paintEvent(self, QPaintEvent):
        super(DefectWidget, self).paintEvent(QPaintEvent)
        self.move_center()

    def get_defects_grade(self):
        defects_grade = []
        for index_item in range(self.layout_defects_grade.count()):
            item_l = self.layout_defects_grade.itemAt(index_item)
            check_box = item_l.widget()
            if check_box.isChecked():
                defect_name = check_box.text()
                defects_grade.append(defect_name)
        return defects_grade

    def get_defects_return(self):
        defects_return = []
        for index_item in range(self.layout_defects_return_device.count()):
            item_l = self.layout_defects_return_device.itemAt(index_item)
            check_box = item_l.widget()
            if check_box.isChecked():
                defects_return.append(check_box.text())
        return defects_return

    def set_defects_last_device(self, defects_b: list, defects_return: list, defects_custom: list):
        self._set_state_defects_grade(defects_b)
        self._set_state_defects_return(defects_return)
        self._set_custom_defects(defects_custom)
        self.signal_select_defect.emit()

    def _set_state_defects_grade(self, defects_grade: list):
        """ Defects array(string)"""
        for index_item in range(self.layout_defects_grade.count()):
            item_l = self.layout_defects_grade.itemAt(index_item)
            check_box = item_l.widget()
            if check_box.text() in defects_grade:
                check_box.setChecked(True)

    def _set_state_defects_return(self, defects_return: list):
        """ Defects array(string) if defects only for return """
        for index_item in range(self.layout_defects_return_device.count()):
            item_l = self.layout_defects_return_device.itemAt(index_item)
            check_box = item_l.widget()
            if check_box.text() in defects_return:
                check_box.setChecked(True)

    def _set_custom_defects(self, defects_custom):
        self.text_edit_custom_defects.append("; ".join(defects_custom))

    def get_custom_defects(self):
        defects = self.text_edit_custom_defects.toPlainText()
        return [d for d in defects.split(";") if d]

    def _insert_check_box_defects(self, defects):
        for defect_class_b in defects.get('class_b', []):
            check_box = QtWidgets.QCheckBox(str(defect_class_b))
            self.layout_defects_grade.addWidget(check_box)

        for defect_return in defects.get("return", []):
            check_box = QtWidgets.QCheckBox(defect_return)
            self.layout_defects_return_device.addWidget(check_box)

    def closeEvent(self, QCloseEvent):
        self.signal_select_defect.emit()
        return super(DefectWidget, self).closeEvent(QCloseEvent)
