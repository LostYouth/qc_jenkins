
from PyQt5 import QtWidgets, QtCore

from qc.componets.repair_name_obj.repair_name_obj import RepairNameObj
from qc.enum_color import ColorResult


class MotionOutdoorCheckPower(QtWidgets.QWidget, RepairNameObj):
    signal_check_state = QtCore.pyqtSignal()

    def __init__(self):
        super(MotionOutdoorCheckPower, self).__init__()

        main_layout = QtWidgets.QVBoxLayout()
        self.label_status = QtWidgets.QLabel("Невідомо")
        self.label_status.setAlignment(QtCore.Qt.AlignCenter)

        self.check_radio_btn = QtWidgets.QRadioButton("Перевірити")
        self.check_radio_btn.clicked.connect(lambda: self.signal_check_state.emit())
        self.check_radio_btn.toggled.connect(lambda *args: self.signal_check_state.emit())
        self.skip_radio_btn = QtWidgets.QRadioButton("Пропустити")
        self.skip_radio_btn.clicked.connect(lambda: self.signal_check_state.emit())

        radio_btn_layout = QtWidgets.QHBoxLayout()
        radio_btn_layout.addWidget(self.skip_radio_btn)
        radio_btn_layout.addWidget(self.check_radio_btn)

        main_layout.addWidget(self.label_status)
        main_layout.addLayout(radio_btn_layout)
        self.setLayout(main_layout)
        self.check_radio_btn.toggled.connect(self._change_color)
        self.skip_radio_btn.click()

    def get_state(self):
        if self.skip_radio_btn.isChecked():
            return True
        elif self.label_status.text() == 'Підключенне' and self.check_radio_btn.isChecked():
            return True
        return False

    def change_state(self, state_powerout_mpo):
        if state_powerout_mpo == '00' or not state_powerout_mpo:
            self.label_status.setText("Відключенне")
        elif state_powerout_mpo == '01':
            self.label_status.setText("Підключенне")

        self._change_color()

    def _change_color(self):
        if self.get_state():
            self.setStyleSheet(ColorResult.success.value)
        else:
            self.setStyleSheet(ColorResult.fail.value)
