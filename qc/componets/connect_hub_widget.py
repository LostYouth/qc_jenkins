from PyQt5 import QtWidgets

from csa_client_qt.csa_client import CsaClient


class ConnectHubWidget(QtWidgets.QWidget):
    def __init__(self):
        super(ConnectHubWidget, self).__init__()
        l = QtWidgets.QHBoxLayout()
        self._count_click_start = 0

        self.push_btn_connect = QtWidgets.QPushButton("Почати")
        self.push_btn_connect.clicked.connect(self._change_count_click)

        l.addWidget(self.push_btn_connect)
        self.setLayout(l)

    def _change_count_click(self):
        self._count_click_start += 1

    def get_state(self):
        return True if self.push_btn_connect.text() == 'Додано' else False

    def check_count_clicked_connect(self):
        if self._count_click_start:
            return True
        return False

    def get_state_repair(self):
        s = self.push_btn_connect.styleSheet()
        if not s:
            return True
        return False
