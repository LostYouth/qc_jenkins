from enum import Enum
from collections import namedtuple

HOST_STORAGE_PHOTO_MCAM = '10.10.77.231'


class QrCodeLength(Enum):
    Hub = 20
    Device = 9
    ocBridge = 6


class QrDevTypes(Enum):
    Central = 'central'
    Hub = 'hub'
    Device = 'dev'
    ocBridge = 'bridge'


class Frequency(Enum):
    EU = 868500, 868300
    RU = 868800, 868950


class FrequencyString(Enum):
    EU = '868.50', '868.300'
    RU = '868.80', '868.950'


check_stages = namedtuple("stage", [
    'prog',
    'assembling',
    'calibration',
    'longtest',
    'standroom',
])


class Color(Enum):
    white = '1'
    black = '2'


StageChecksCentral = dict()
StageChecksCentral['Hub'] = check_stages(True, True, False, True, False)
StageChecksCentral['Hub2'] = check_stages(True, True, False, True, False)
StageChecksCentral['HubPlus'] = check_stages(True, True, False, True, False)
StageChecksCentral['HubYavir'] = check_stages(True, True, False, False, False)
StageChecksCentral['HubYavirPlus'] = check_stages(True, True, False, False, False)

StageChecksCentral['ocBridge'] = check_stages(True, True, False, False, False)
StageChecksCentral['uartBridge'] = check_stages(True, True, False, False, False)
# Device
StageChecksDevice = dict()
StageChecksDevice['DoorProtect'] = check_stages(True, True, False, True, False)
StageChecksDevice['DoorProtectPlus'] = check_stages(True, True, False, True, False)
StageChecksDevice['GlassProtect'] = check_stages(True, True, False, True, True)
StageChecksDevice['MotionProtect'] = check_stages(True, True, False, True, True)

StageChecksDevice['MotionCam'] = check_stages(True, True, True, True, True)
StageChecksDevice['MotionProtectPlus'] = check_stages(True, True, True, True, True)
StageChecksDevice['MotionProtectOutdoor'] = check_stages(True, True, True, True, True)
StageChecksDevice['MotionProtectCurtain'] = check_stages(True, True, False, True, True)

StageChecksDevice['CombiProtect'] = check_stages(True, True, False, True, True)
StageChecksDevice['LeaksProtect'] = check_stages(True, True, False, True, False)
StageChecksDevice['HomeSiren'] = check_stages(True, True, False, False, False)
StageChecksDevice['StreetSiren'] = check_stages(True, True, False, False, False)
StageChecksDevice['FireProtect'] = check_stages(True, True, True, True, False)
StageChecksDevice['FireProtectPlus'] = check_stages(True, True, True, True, False)
StageChecksDevice['Keypad'] = check_stages(True, True, True, True, False)
StageChecksDevice['RangeExtender'] = check_stages(True, True, False, False, False)
StageChecksDevice['WallSwitch'] = check_stages(True, True, False, False, False)
StageChecksDevice['Relay'] = check_stages(True, True, False, False, False)
StageChecksDevice['Socket'] = check_stages(True, True, False, True, False)
StageChecksDevice['SpaceControl'] = check_stages(True, True, False, False, False)
StageChecksDevice['Button'] = check_stages(True, True, False, False, False)
StageChecksDevice['Transmitter'] = check_stages(True, True, False, True, False)


class CheckFrequencyIgnore:
    ignore_hubs_types = ('HubYavir', 'HubYavirPlus',)
    ignore_device_types = ("StreetSiren", "GlassProtect", )


class CheckColorIgnore:
    ignore_hubs_types = ('HubYavir', 'HubYavirPlus',)
