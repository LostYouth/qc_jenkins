from enum import Enum
from collections import namedtuple

group_models_hubs = namedtuple("group_models_hubs", [
    'name', 'group'
])


class EnumParamWhereHub(Enum):
    Standart = group_models_hubs('Стандартний', '1715')
    Russian = group_models_hubs("В Росію", '1815')
    Italia = group_models_hubs("В Італію", '1715')
    Ireland = group_models_hubs("В Ірландію", '1718')
    ProAccount = group_models_hubs("Привязати до аккаунту", '1716')
