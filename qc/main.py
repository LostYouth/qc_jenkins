import logging

from PyQt5 import QtWidgets, QtGui, QtCore
from PyQt5.QtCore import Qt

from app_setting import AppSetting
from qc.window.login_wiget import LoginWidget
from qc.window.popup.add_hub_widget import AddHubWidget
from qc.window.popup.widget_send_report import WidgetSendReport
from qc.window.qc_main_widget import QcMainWidget

logger = logging.getLogger(AppSetting.LOGGER_NAME)


class QcMainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super(QcMainWindow, self).__init__(flags=QtCore.Qt.Window)
        self.login_widget = LoginWidget(parent=self)
        self.login_widget.signal_success_result.connect(self.change_main_widget)
        self.setCentralWidget(self.login_widget)
        self.showMaximized()

        self.qc_main_widget = None

        menu_bar = self.menuBar()
        control_app = menu_bar.addMenu("&Додаток")
        add_hub = QtWidgets.QAction("Додати Хаб", self)
        control_app.addAction(add_hub)
        add_hub.triggered.connect(self.add_hub)

        control_hub = QtWidgets.QAction("Керування хабами", self)
        control_hub.triggered.connect(self.control_hub_obj)
        control_app.addAction(control_hub)

        action_app = QtWidgets.QAction("Керування обєктами хабів", self)
        action_app.triggered.connect(self.control_object)
        control_app.addAction(action_app)

        settings_app = menu_bar.addMenu("&Налаштування")
        frequency = QtWidgets.QAction("Змінити частоту", self)
        frequency.triggered.connect(self._change_frequency)

        change_frame = QtWidgets.QAction("Змінити фрейм", self)
        change_frame.triggered.connect(self._change_frame)

        color = QtWidgets.QAction("Змінити колір", self)
        color.triggered.connect(self._change_color)

        wifi = QtWidgets.QAction("Змінити Wifi", self)
        wifi.triggered.connect(self.change_wifi)

        settings_app.addAction(frequency)
        settings_app.addAction(change_frame)
        settings_app.addAction(color)
        settings_app.addAction(wifi)

        report_problem = menu_bar.addMenu("&Повідомити про помилку")

        report = QtWidgets.QAction("повідомити про помилку", self)
        report.triggered.connect(self._report_problem)
        self.widget_report_problem = WidgetSendReport()
        report_problem.addAction(report)

        self.installEventFilter(self)
        self.timer_open_port = QtCore.QTimer()

    def closeEvent(self, QCloseEvent):
        if self.qc_main_widget and self.qc_main_widget.database_prod.connect:
            self.qc_main_widget.database_prod.connect.close()
            logger.info('DB connection was closed')
        QCloseEvent.accept()

    def control_object(self):
        if self.qc_main_widget is None:
            return
        self.qc_main_widget.show_control_obj()

    def control_hub_obj(self):
        if self.qc_main_widget is None:
            return
        self.qc_main_widget.show_control_hubs()

    def change_main_widget(self, operator_login, operator_full_name):
        self.login_widget.hide()
        self.qc_main_widget = QcMainWidget(operator_login, operator_full_name, parent=self)
        self.setCentralWidget(self.qc_main_widget)
        self.qc_main_widget.show()
        self.widget_report_problem.set_operator(operator_full_name)
        port_name = self.login_widget.scanner_login.portName()
        if port_name:
            self.qc_main_widget.comboBox_ports_scanner.addItem(port_name)
            self.qc_main_widget.comboBox_ports_scanner.setCurrentText(port_name)
            self.timer_open_port.singleShot(750, self.qc_main_widget.connect_scanner)

    def _change_frequency(self):
        if self.qc_main_widget is None:
            logger.debug("access denied user not auth in app")
            return
        self.qc_main_widget.change_frequency()

    def _change_frame(self):
        if self.qc_main_widget is None:
            logger.debug("access denied user not auth in app")
            return
        self.qc_main_widget.change_frame()

    def _change_color(self):
        if self.qc_main_widget is None:
            logger.debug("access denied user not auth in app")
            return
        self.qc_main_widget.change_color_show_widget()

    def change_wifi(self):
        if self.qc_main_widget is None:
            logger.debug("access denied user not auth in app")
            return
        self.qc_main_widget.change_wifi()

    def add_hub(self):
        if self.qc_main_widget is None:
            return
        self.widget_add_hub = AddHubWidget(self.qc_main_widget.csa_client, parent=self.qc_main_widget)
        self.widget_add_hub.show()
    
    def eventFilter(self, QObject, QEvent):
        if self.qc_main_widget is not None and (QEvent.type() == QtCore.QEvent.KeyPress or QEvent.type() == QtCore.QEvent.ShortcutOverride):
            if QEvent.key() == Qt.Key_Space:
                self.qc_main_widget.active_table.setFocus()
        return super(QcMainWindow, self).eventFilter(QObject, QEvent)

    def _report_problem(self):
        if self.qc_main_widget is None:
            logger.debug("access denied user not auth in app")
            return
        self.widget_report_problem.show()
