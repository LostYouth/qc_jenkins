import logging
from collections import namedtuple
from functools import partial

from PyQt5 import QtWidgets

from app_setting import AppSetting
from csa_client_qt.enum_obj_csa import ObjectsCsa
from qc.componets import *
from qc.enum_alarms import MotionProtectOutdoor
from .device_abstract_view import DeviceAbstractView

logger = logging.getLogger(AppSetting.LOGGER_NAME)


class MotionDeviceOutdoorView(DeviceAbstractView):

    dev_type = ObjectsCsa.MotionProtectOutdoor

    column = namedtuple('column', ['name', 'width'])

    table_head = [
        column("Qr", 120),
        column("Наявність\nдефектів", 260),
        column("Підключення", 120),
        column("Тампер", 180),
        column("Зафіксовано\nрух", 150),
        column("Рівень\nбатареї", 120),
        column("Зовнішннє\nживлення", 290),
        column("Відмінити\nтестування", 120),
        column("Зареєструвати", 100),
    ]

    def __init__(self, client_csa, parent=None):
        super(MotionDeviceOutdoorView, self).__init__(client_csa, parent=parent)
        self.row_height = 75
        self.insert_columns()

    def add_obj(self, qr_code_device):
        dev_id, dev_type, color = qr_code_device[:6], qr_code_device[6:8], qr_code_device[8]
        if self.check_exists_dev_id(dev_id):
            self.show_error("Цей девайс {} вже є в таблиці!".format(dev_id))
            return

        row = self.rowCount()
        self.insertRow(row)
        self.setRowHeight(row, self.row_height)

        widget_id = DevIdWidget(qr_code_device)
        self.setCellWidget(row, 0, widget_id)

        widget_defects = VisualDefectTable(self.defects, parent=self.qc_main_widget)
        widget_defects.signal_update_defects.connect(self.check_all_columns)
        self.setCellWidget(row, 1, widget_defects)

        widget_search_device = ConnectDeviceWidget()
        widget_search_device.push_btn_connect_device.clicked.connect(
            partial(self._start_registration_device, dev_id, dev_type, color))
        self.setCellWidget(row, 2, widget_search_device)

        widget_tamper = TamperWidget()
        widget_tamper.set_malfunction_name("Тампер")
        self.setCellWidget(row, 3, widget_tamper)

        widget_move = IncrementWidget()
        widget_move.set_malfunction_name("Піросенсор")
        widget_move.set_expected_result(2)
        self.setCellWidget(row, 4, widget_move)

        widget_battery_status = StatusWidget()
        widget_battery_status.set_malfunction_name("Рівень батареї")
        widget_battery_status.set_status("Невідомо")
        widget_battery_status.set_expected_result('100')
        self.setCellWidget(row, 5, widget_battery_status)

        widget_outpower = MotionOutdoorCheckPower()
        widget_outpower.set_malfunction_name("Зовнішнє живлення")
        widget_outpower.signal_check_state.connect(self.check_all_columns)
        self.setCellWidget(row, 6, widget_outpower)

        push_btn_cancel_test = QtWidgets.QPushButton("x")
        push_btn_cancel_test.clicked.connect(partial(self.cancel_test_device_qc, dev_type, dev_id))
        self.setCellWidget(row, 7, push_btn_cancel_test)

        push_btn_register_device = QtWidgets.QPushButton('Повернути')
        push_btn_register_device.clicked.connect(partial(self.register_device, dev_id, qr_code_device))
        self.setCellWidget(row, 8, push_btn_register_device)

    def _send_settings_device(self, dev_type, dev_id):
        hub_obj = self.get_work_hub()
        params = ['33', '01']
        a = hub_obj.write_param(dev_type, dev_id, params)
        logger.debug("Answer set settings {}".format(a))

    def _handler_update(self, device_id):
        hub_obj = self.get_work_hub()
        motion_outdoor = hub_obj.get_object(self.get_device_type(), device_id)
        row = self.get_row_by_dev_id(device_id[2:])
        # self._check_state_hub(row, hub_obj.get_parameters('08'))
        if row is None:
            return
        battery_charged = motion_outdoor.get_parameters('05')
        out_power = motion_outdoor.get_parameters('44')

        self._update_power_out(row, out_power)
        self._update_battery_charged(row, battery_charged)

        self.check_all_columns()

    def _update_power_out(self, row, out_power):
        widget_out_power = self.cellWidget(row, 6)
        if not out_power:
            return
        widget_out_power.change_state(out_power)

    def _update_battery_charged(self, row, batt_charge):
        widget = self.cellWidget(row, 5)
        if not batt_charge:
            widget.set_status("Невідомо")
            return
        battery_int = int(batt_charge, 16)
        if battery_int > 101:
            widget.set_status("Невідомо")
            return
        widget.set_status("{}".format(battery_int))

    def _handler_alarms(self, dev_id, alarm_number):
        row = self.get_row_by_dev_id(dev_id[2:])
        if row is None:
            return

        if alarm_number == MotionProtectOutdoor.TamperOpen:
            widget_tamped = self.cellWidget(row, 3)
            widget_tamped.change_tamper_count_on()
        elif alarm_number == MotionProtectOutdoor.TamperClosed:
            widget_tamped = self.cellWidget(row, 3)
            widget_tamped.change_tamper_count_off()
        elif alarm_number == MotionProtectOutdoor.Movement:
            widget_accelerometer = self.cellWidget(row, 4)
            widget_accelerometer.increment()
        self.check_all_columns()
