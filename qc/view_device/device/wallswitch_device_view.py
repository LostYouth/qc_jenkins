import logging
from collections import namedtuple
from functools import partial
from contextlib import contextmanager

from PyQt5 import QtWidgets, QtCore

from app_setting import AppSetting
from csa_client_qt.enum_obj_csa import ObjectsCsa
from qc.componets import *
from qc.enum_alarms import WallSwitch
from .device_abstract_view import DeviceAbstractView

logger = logging.getLogger(AppSetting.LOGGER_NAME)


class WallSwitchDeviceView(DeviceAbstractView):
    dev_type = ObjectsCsa.WallSwitch
    column = namedtuple('column', ['name', 'width'])

    table_head = [
        column("Qr", 120),
        column("Наявність\nдефектів", 200),
        column("Підключення", 120),
        column("Замкнути", 180),
        column("Розімункти", 150),
        column("Змінити стан", 150),
        column("Рівень\nнапруги", 120),
        column("Відмінити\nтестування", 120),
        column("Зареєструвати", 100),
    ]

    min_voltage = 21
    max_voltage = 25

    def __init__(self, client_csa, parent=None):
        super(WallSwitchDeviceView, self).__init__(client_csa, parent=parent)
        self.insert_columns()
        self.row_height = 75

    def add_obj(self, qr_code_device):
        dev_id, dev_type, color = qr_code_device[:6], qr_code_device[6:8], qr_code_device[8]
        if self.check_exists_dev_id(dev_id):
            self.show_error("Цей девайс {} же є в талиці!".format(dev_id))
            return

        row = self.rowCount()
        self.insertRow(self.rowCount())
        self.setRowHeight(row, self.row_height)

        widget_id_device = DevIdWidget(qr_code_device)
        self.setCellWidget(row, 0, widget_id_device)

        widget_defects = VisualDefectTable(self.defects, parent=self.qc_main_widget)
        widget_defects.signal_update_defects.connect(self.check_all_columns)
        self.setCellWidget(row, 1, widget_defects)

        push_button_search_device = ConnectDeviceWidget()
        push_button_search_device.push_btn_connect_device.clicked.connect(
            partial(self._start_registration_device, dev_id, dev_type, color))
        self.setCellWidget(row, 2, push_button_search_device)

        enable_wallswitch = IncrementWidget()
        enable_wallswitch.set_malfunction_name("Включення")
        enable_wallswitch.set_expected_result(1)
        self.setCellWidget(row, 3, enable_wallswitch)

        disabled_wallswitch = IncrementWidget()
        disabled_wallswitch.set_malfunction_name("Виключення")
        disabled_wallswitch.set_expected_result(1)
        self.setCellWidget(row, 4, disabled_wallswitch)

        change_state = ChangeStateWallFamily("Змінити стан")
        change_state.clicked.connect(partial(self._change_state_device, dev_id))
        change_state.setEnabled(False)
        self.setCellWidget(row, 5, change_state)

        current_voltage = VoltageStatusWallSwitch(self.min_voltage, self.max_voltage)
        current_voltage.set_malfunction_name("Рівень напруги")
        self.setCellWidget(row, 6, current_voltage)

        push_btn_cancel_test = QtWidgets.QPushButton("x")
        push_btn_cancel_test.clicked.connect(partial(self.cancel_test_device_qc, dev_type, dev_id))
        self.setCellWidget(row, 7, push_btn_cancel_test)

        push_btn_register_device = QtWidgets.QPushButton('Повернути')
        push_btn_register_device.clicked.connect(partial(self.register_device, dev_id, qr_code_device))
        self.setCellWidget(row, 8, push_btn_register_device)

    def _send_settings_device(self, dev_type, dev_id):
        pass
        # hub_obj = self.get_work_hub()
        # parameters = ['31', '00', '32', '00']
        # answer = hub_obj.write_param(self.get_device_type(), dev_id, parameters)
        # logger.info("Answer parameters {}".format(answer))

    def _special_settings_device(self, dev_type, dev_id):
        hub_obj = self.get_work_hub()
        parameters = ['31', '00', '32', '00']
        answer = hub_obj.write_param(dev_type, dev_id, parameters)
        logger.info("Answer parameters {}".format(answer))

    def _enabled_btn_change_state(self, btn, state):
        try:
            btn.setEnabled(state)
        except RuntimeError as err:
            logger.debug("WallSwitch runtime error enable btn {}".format(err))

    @contextmanager
    def disable_widget_while_block_operation(self, widget: QtWidgets.QWidget):
        widget.setEnabled(False)
        yield
        widget.setEnabled(True)

    def _change_state_device(self, dev_id: str):
        btn = self.sender()
        with self.disable_widget_while_block_operation(btn):
            hub_obj = self.get_work_hub()
            device_obj = hub_obj.get_object(self.get_device_type(), "{:0>8}".format(dev_id))
            if not device_obj:
                self.show_error("Пристій {} {} не приписаний до хабу!".format(self.get_device_type_name(), dev_id))
                return
            device_state = device_obj.get_parameters("38")
            logger.debug("State {}: {}".format(self.get_device_type_name(), device_state))
            if device_state == '00':
                command = '07'
            else:
                command = '06'
            answer_command = hub_obj.send_device_command(self.get_device_type(), "{:0>8}".format(dev_id), command)
            logger.debug("answer send command change state WF {:0>8}: {}".format(dev_id, answer_command))

    def _handler_update(self, device_id):
        hub_obj = self.get_work_hub()
        wall_switch = hub_obj.get_object(self.get_device_type(), device_id)
        row = self.get_row_by_dev_id(device_id[2:])
        if row is None:
            return
        curr_voltage = wall_switch.get_parameters('35')
        self._handler_voltage(row, curr_voltage)

        widget_voltage = self.cellWidget(row, 6)
        if (wall_switch.get_parameters('31') == '01' or wall_switch.get_parameters('32') == '01') and widget_voltage.get_state():
            self._special_settings_device(self.get_device_type(), device_id)
            widget_change_state = self.cellWidget(row, 5)
            if widget_change_state:
                widget_change_state.setEnabled(True)
        self.check_all_columns()

    def _handler_voltage(self, row, voltage):
        if not voltage:
            return
        widget = self.cellWidget(row, 6)
        voltage = int(voltage, 16)
        if voltage > 2000:
            widget.set_value(0)
            return
        widget.set_value(voltage)

    def _handler_alarms(self, device_id, alarm_number):
        row = self.get_row_by_dev_id(device_id[2:])
        if row is None:
            return

        if alarm_number == WallSwitch.Enabled:
            widget_e = self.cellWidget(row, 3)
            widget_e.increment()
        elif alarm_number == WallSwitch.Disabled:
            widget_d = self.cellWidget(row, 4)
            widget_d.increment()
        elif alarm_number == WallSwitch.NotAnswer:
            self.show_error("Розетка не відповідає!")
