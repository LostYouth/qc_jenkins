from csa_client_qt.enum_obj_csa import ObjectsCsa
from qc.view_device.device.motion_device_view import MotionDeviceView


class MotionDeviceCurtain(MotionDeviceView):
    dev_type = ObjectsCsa.MotionProtectCurtain
