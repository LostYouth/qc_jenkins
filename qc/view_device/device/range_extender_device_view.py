from collections import namedtuple
from functools import partial

from PyQt5 import QtWidgets

from csa_client_qt.enum_obj_csa import ObjectsCsa
from qc.componets import *
from qc.componets.rex_out_power import RexOutPower
from qc.enum_alarms import RangeExtender
from .device_abstract_view import DeviceAbstractView


class RangeExtenderDeviceView(DeviceAbstractView):
    dev_type = ObjectsCsa.RangeExtender
    column = namedtuple('column', ['name', 'width'])

    table_head = [
        column("Qr", 120),
        column("Наявність\nдефектів", 200),
        column("Підключення", 120),
        column("Тампер", 180),
        column("Зв'язок\nз\nхабом", 150),
        column("Зовнішнє\nживлення", 150),
        column("Рівень\nбатареї", 120),
        column("Відмінити\nтестування", 120),
        column("Зареєструвати", 100),
    ]

    def __init__(self, client_csa, parent=None):
        super(RangeExtenderDeviceView, self).__init__(client_csa, parent=parent)
        self.row_height = 75
        self.insert_columns()

    def add_obj(self, qr_code_device):
        dev_id, dev_type, color = qr_code_device[:6], qr_code_device[6:8], qr_code_device[8]
        if self.check_exists_dev_id(dev_id):
            self.show_error("Цей девайс {} же є в талиці!".format(dev_id))
            return

        row = self.rowCount()
        self.insertRow(self.rowCount())
        self.setRowHeight(row, self.row_height)

        widget_id_device = DevIdWidget(qr_code_device)
        self.setCellWidget(row, 0, widget_id_device)

        widget_defects = VisualDefectTable(self.defects, parent=self.qc_main_widget)
        widget_defects.signal_update_defects.connect(self.check_all_columns)
        self.setCellWidget(row, 1, widget_defects)

        push_button_search_device = ConnectDeviceWidget()
        push_button_search_device.push_btn_connect_device.clicked.connect(partial(self._start_registration_device, dev_id, dev_type, color))
        self.setCellWidget(row, 2, push_button_search_device)

        widget_tampered = TamperWidget()
        widget_tampered.set_malfunction_name("Тампер")
        self.setCellWidget(row, 3, widget_tampered)

        connect_hub = StatusWidget()
        connect_hub.set_malfunction_name("Звязок з цетралю")
        connect_hub.set_expected_result('01')
        self.setCellWidget(row, 4, connect_hub)

        power_range_extender = RexOutPower()
        power_range_extender.set_malfunction_name("Зовнішнє живлення")
        power_range_extender.set_expected_result(1)
        self.setCellWidget(row, 5, power_range_extender)

        battery_status = StatusWidget()
        battery_status.set_malfunction_name("Рівень батареї")
        battery_status.set_status("Невідомо")
        battery_status.set_expected_result(50)
        self.setCellWidget(row, 6, battery_status)

        push_btn_cancel_test = QtWidgets.QPushButton("x")
        push_btn_cancel_test.clicked.connect(partial(self.cancel_test_device_qc, dev_type, dev_id))
        self.setCellWidget(row, 7, push_btn_cancel_test)

        push_btn_register_device = QtWidgets.QPushButton('Повернути')
        push_btn_register_device.clicked.connect(partial(self.register_device, dev_id, qr_code_device))
        self.setCellWidget(row, 8, push_btn_register_device)

    def _handler_alarms(self, device_id, alarm_number):
        row = self.get_row_by_dev_id(device_id[2:])
        if row is None:
            return
        if alarm_number == RangeExtender.TamperOpen:
            widget_tamper = self.cellWidget(row, 3)
            widget_tamper.change_tamper_count_on()
        elif alarm_number == RangeExtender.TamperClosed:
            widget_tamper = self.cellWidget(row, 3)
            widget_tamper.change_tamper_count_off()
        elif alarm_number == RangeExtender.PowerOn or alarm_number == RangeExtender.PowerOff:
            widget_out_contact = self.cellWidget(row, 5)
            widget_out_contact.increment()
        self.check_all_columns()

    def _handler_update(self, device_id):
        hub_obj = self.get_work_hub()
        range_extender = hub_obj.get_object(self.get_device_type(), device_id)
        row = self.get_row_by_dev_id(device_id[2:])
        if row is None:
            return
        hub_connect_status = range_extender.get_parameters('0b')
        battery_status = range_extender.get_parameters('05')
        out_power_status = range_extender.get_parameters('31')

        self._handler_connect2hub(row, hub_connect_status)
        self._handler_battery_status(row, battery_status)
        self._handler_out_power_status(row, out_power_status)
        self.check_all_columns()

    def _handler_out_power_status(self, row, out_power_status):
        widget_out_power = self.cellWidget(row, 5)
        widget_out_power.change_state(out_power_status)

    def _handler_battery_status(self, row, battery_val):
        if not battery_val:
            return
        widget_battery = self.cellWidget(row, 6)
        battery_val = int(battery_val, 16)
        if battery_val > 101:
            widget_battery.set_status("Невідомо")
        else:
            widget_battery.set_status(str(battery_val))

    def _handler_connect2hub(self, row, connect_status):
        if not connect_status:
            return

        widget_connect = self.cellWidget(row, 4)
        widget_connect.set_status(connect_status)
