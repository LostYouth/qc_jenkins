from csa_client_qt.enum_obj_csa import ObjectsCsa
from .fire_device_view import FireDeviceView


class FireDevicePlusView(FireDeviceView):
    dev_type = ObjectsCsa.FireProtectPlus
