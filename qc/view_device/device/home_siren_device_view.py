import logging
from collections import namedtuple
from functools import partial
from contextlib import contextmanager

from PyQt5 import QtWidgets

from app_setting import AppSetting
from csa_client_qt.enum_obj_csa import ObjectsCsa
from csa_client_qt.enum_params import MessageType, Answer
from qc.componets.widget_change_lvl_volume import WidgetChangeLvlVolume, EnumVolumeLevel
from qc.enum_alarms import HomeSiren
from .device_abstract_view import DeviceAbstractView
from qc.componets import *

logger = logging.getLogger(AppSetting.LOGGER_NAME)


class HomeSirenDeviceView(DeviceAbstractView):
    dev_type = ObjectsCsa.HomeSiren

    column = namedtuple('column', ['name', 'width'])
    table_head = [
        column("Qr", 120),
        column("Наявність\nдефектів", 200),
        column("Підключення", 120),
        column("Тампер", 150),
        column('Блимання при \nпостановці\n під охорону', 150),
        column('Блимання при \nзнятті \nз-під охорони', 150),
        column('Тест бузера', 150),
        column("Змінити\nгучність\nсирени", 250),
        column('Рівень\n батереї', 100),
        column("Відмінити\nтестування", 120),
        column("Зареєструвати", 100),
    ]

    def __init__(self, client_csa, parent=None):
        super(HomeSirenDeviceView, self).__init__(client_csa, parent=parent)
        self.insert_columns()
        self.row_height = 75

    def add_obj(self, qr_code_device):
        dev_id, dev_type, color = qr_code_device[:6], qr_code_device[6:8], qr_code_device[8]
        if self.check_exists_dev_id(dev_id):
            self.show_error("Цей девайс {} же є в талиці!".format(dev_id))
            return

        row = self.rowCount()
        self.insertRow(row)
        self.setRowHeight(row, self.row_height)

        widget_id = DevIdWidget(qr_code_device)
        self.setCellWidget(row, 0, widget_id)

        widget_defects = VisualDefectTable(self.defects, parent=self.qc_main_widget)
        widget_defects.signal_update_defects.connect(self.check_all_columns)
        self.setCellWidget(row, 1, widget_defects)

        push_btn_search_device = ConnectDeviceWidget()
        push_btn_search_device.push_btn_connect_device.clicked.connect(partial(self._start_registration_device, dev_id, dev_type, color))
        self.setCellWidget(row, 2, push_btn_search_device)

        widget_tampered = TamperWidget()
        widget_tampered.set_malfunction_name("Тампер")
        self.setCellWidget(row, 3, widget_tampered)

        blink_if_hub_arm = HomeSirenChangeStateHub("", 'На охорону')
        blink_if_hub_arm.set_malfunction_name("Світлодіод постановки на охорону")
        blink_if_hub_arm.check_box.stateChanged.connect(self.check_all_columns)
        blink_if_hub_arm.push_btn_change_state_hub.clicked.connect(self._arm_work_hub)
        self.setCellWidget(row, 4, blink_if_hub_arm)

        blink_if_hub_disarm = HomeSirenChangeStateHub("", 'З охорони')
        blink_if_hub_disarm.set_malfunction_name("Світлодіод постановки на охорону")
        blink_if_hub_disarm.check_box.stateChanged.connect(self.check_all_columns)

        blink_if_hub_disarm.push_btn_change_state_hub.clicked.connect(self._disarm_work_hub)
        self.setCellWidget(row, 5, blink_if_hub_disarm)

        test_buzzer_siren = HomeSirenChangeStateHub("", 'Запустити\nтест\nбузера')
        test_buzzer_siren.set_malfunction_name("Бузер")
        test_buzzer_siren.check_box.stateChanged.connect(self.check_all_columns)
        test_buzzer_siren.push_btn_change_state_hub.clicked.connect(partial(self._start_test_buzzer, dev_id))
        self.setCellWidget(row, 6, test_buzzer_siren)

        widget_change_volume = WidgetChangeLvlVolume(dev_id)
        widget_change_volume.signal_change_value_volume.connect(self._hand_change_volume)
        self.setCellWidget(row, 7, widget_change_volume)

        battery_status = StatusWidget()
        battery_status.set_malfunction_name('Рівень батареї')
        battery_status.set_status("Невідомо")
        battery_status.set_expected_result('100')
        self.setCellWidget(row, 8, battery_status)

        push_btn_cancel_test = QtWidgets.QPushButton("x")
        push_btn_cancel_test.clicked.connect(partial(self.cancel_test_device_qc, dev_type, dev_id))
        self.setCellWidget(row, 9, push_btn_cancel_test)

        push_btn_register_device = QtWidgets.QPushButton('Повернути')
        push_btn_register_device.clicked.connect(partial(self.register_device, dev_id, qr_code_device))
        self.setCellWidget(row, 10, push_btn_register_device)

    def _hand_change_volume(self, dev_id: str, val: int):
        hub_obj = self.get_work_hub()
        a = hub_obj.write_param(
            self.get_device_type(),
            "{:0>8}".format(dev_id),
            ['35', '{:02x}'.format(val)]
        )
        logger.debug("Answer change volume {}".format(a))
        if a is None:
            return
        if a.message_type == MessageType.Answer.value and a.message_key == Answer.DeliveredCommandPerform.value:
            return
        else:
            hub_obj = self.get_work_hub()
            home_siren = hub_obj.get_object(self.get_device_type(), "{:0>8}".format(dev_id))
            row = self.get_row_by_dev_id(dev_id)
            if row is None or home_siren is None:
                return
            self._update_volume_dev(row, home_siren.get_parameters('35'))
            self.show_error("Помилка, не вдалось змінити гучність для сирени {}".format(dev_id))

    def register_device(self, device_id: str, device_qr: str):
        hub_obj = self.get_work_hub()
        if hub_obj.get_parameters('08') != '00':
            answer = hub_obj.disarm()
            logger.debug("Answer Disarm hub {}".format(answer))
        super(HomeSirenDeviceView, self).register_device(device_id, device_qr)

    def _arm_work_hub(self):
        hub_obj = self.get_work_hub()
        if hub_obj.get_parameters('08') == '00':
            answer = hub_obj.arm()
            logger.debug("Answer Arm hub {}".format(answer))
        else:
            self.show_error("Хаб вже на охороні!")

    def _disarm_work_hub(self):
        hub_obj = self.get_work_hub()
        if hub_obj.get_parameters('08') != '00':
            answer = hub_obj.disarm()
            logger.debug("Answer Disarm hub {}".format(answer))
        else:
            self.show_error("Хаб вже знятий з охорони!")

    @contextmanager
    def disable_widget(self, widget: QtWidgets.QWidget):
        widget.setEnabled(False)
        yield
        widget.setEnabled(True)

    def _start_test_buzzer(self, dev_id):
        with self.disable_widget(self.sender()):
            hub_obj = self.get_work_hub()
            a = hub_obj.send_device_command(self.get_device_type(), dev_id, '08')
            logger.debug("Answer send test buzzer siren({}) {}".format(dev_id, a))
            if a.message_type == MessageType.Answer.value and a.message_key == Answer.WrongState.value:
                self.show_error("Неможливо запусти тест бузера поки хаб на охороні")

    def _send_settings_device(self, dev_type, dev_id):
        hub_obj = self.get_work_hub()
        params = ['33', '01',
                  '32', '01',
                  '31', '11'
                  ]
        a = hub_obj.write_param(dev_type, dev_id, params)
        logger.debug("Answer set settings {}".format(a))

    def _handler_update(self, device_id):
        hub_obj = self.get_work_hub()
        home_siren = hub_obj.get_object(self.get_device_type(), device_id)
        row = self.get_row_by_dev_id(device_id[2:])
        if row is None:
            return
        battery_charged = home_siren.get_parameters('05')
        self._update_battery_charged(row, battery_charged)
        self.check_all_columns()

    def _update_volume_dev(self, row: int, volume: str):
        if not volume or volume == '80':
            return
        widget_volume_device = self.cellWidget(row, 7)
        try:
            current_val = int(volume, 16)
        except (TypeError, ValueError):
            return
        enum_val = EnumVolumeLevel(current_val)
        widget_volume_device.current_value = enum_val

    def _check_state_hub(self, row, state_hub):
        widget_blick_arm = self.cellWidget(row, 4)
        widget_blick_disarm = self.cellWidget(row, 5)
        if state_hub == '00':
            widget_blick_disarm.change_state_btn(False)
            widget_blick_arm.change_state_btn(True)
        elif state_hub == '01' or state_hub == '02':
            widget_blick_disarm.change_state_btn(True)
            widget_blick_arm.change_state_btn(False)

    def _update_battery_charged(self, row, batt_charge):
        widget = self.cellWidget(row, 8)
        if not batt_charge:
            widget.set_status("Невідомо")
            return
        battery_int = int(batt_charge, 16)
        if battery_int > 101:
            widget.set_status("Невідомо")
            return

        widget.set_status("{}".format(battery_int))

    def _handler_alarms(self, dev_id, alarm_number):
        row = self.get_row_by_dev_id(dev_id[2:])
        if row is None:
            return

        if alarm_number == HomeSiren.TamperOpen:
            widget_tamped = self.cellWidget(row, 3)
            widget_tamped.change_tamper_count_on()
        elif alarm_number == HomeSiren.TamperClosed:
            widget_tamped = self.cellWidget(row, 3)
            widget_tamped.change_tamper_count_off()

        self.check_all_columns()
