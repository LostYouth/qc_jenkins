import logging
from collections import namedtuple
from functools import partial
from csa_client_qt.enum_obj_csa import HubSubType
from PyQt5 import QtWidgets, QtCore

from app_setting import AppSetting
from csa_client_qt.enum_obj_csa import ObjectsCsa
from qc.componets import *
from qc.componets.widget_mcam_show_widget import EnumStateImage
from qc.componets.widget_motiocam_show_images import WidgetMotionCamShowImages
from qc.exc import WorkHubNotFound
from qc.plugins.get_mcam_image import GetMCamImage
from qc.view_device.device.device_abstract_view import DeviceAbstractView
from qc.enum_alarms import MotionCam

logger = logging.getLogger(AppSetting.LOGGER_NAME)


class MotionDeviceCamView(DeviceAbstractView):
    dev_type = ObjectsCsa.MotionCam

    column = namedtuple('column', ['name', 'width'])

    table_head = [
        column("Qr", 120),
        column("Наявність\nдефектів", 260),
        column("Підключення", 120),
        column("Тампер", 180),
        column("Зафіксовано\nрух", 150),
        column("Рівень\nбатареї", 120),
        column('Переглянути\nфото\nз тест рума', 120),
        column("Відмінити\nтестування", 120),
        column("Зареєструвати", 100),
    ]

    def __init__(self, client_csa, parent=None):
        super(MotionDeviceCamView, self).__init__(client_csa, parent=parent)
        self.row_height = 80
        self.insert_columns()

        self.widget_show_photos = WidgetMotionCamShowImages(parent=self)
        self.widget_show_photos.hide()
        self.thread_get_link_image = GetMCamImage(self.database_prod)
        self.thread_get_link_image.signal_error.connect(self._error_download_photo)
        self.thread_get_link_image.signal_path_files.connect(self._insert_image_motion_cam)
        self.thread_get_link_image.start()

    def add_obj(self, qr_code_device):
        dev_id, dev_type, color = qr_code_device[:6], qr_code_device[6:8], qr_code_device[8]
        if self.check_exists_dev_id(dev_id):
            self.show_error("Цей девайс {} вже є в таблиці!".format(dev_id))
            return

        row = self.rowCount()
        self.insertRow(row)
        self.setRowHeight(row, self.row_height)

        widget_id = DevIdWidget(qr_code_device)
        self.setCellWidget(row, 0, widget_id)

        widget_defects = VisualDefectTable(self.defects, parent=self.qc_main_widget)
        widget_defects.signal_update_defects.connect(self.check_all_columns)
        self.setCellWidget(row, 1, widget_defects)

        widget_search_device = ConnectDeviceWidget()
        widget_search_device.push_btn_connect_device.clicked.connect(
            partial(self._start_registration_device, dev_id, dev_type, color))
        self.setCellWidget(row, 2, widget_search_device)

        widget_tamper = TamperWidget()
        widget_tamper.set_malfunction_name("Тампер")
        self.setCellWidget(row, 3, widget_tamper)

        widget_move = IncrementWidget()
        widget_move.set_malfunction_name("Піросенсор")
        widget_move.set_expected_result(2)
        self.setCellWidget(row, 4, widget_move)

        widget_battery_status = StatusWidget()
        widget_battery_status.set_malfunction_name("Рівень батареї")
        widget_battery_status.set_status("Невідомо")
        widget_battery_status.set_expected_result('100')
        self.setCellWidget(row, 5, widget_battery_status)

        widget_show_cam_photos = WidgetMCamShowWidget()
        widget_show_cam_photos.setEnabled(False)
        # widget_show_cam_photos.set_malfunction_name("Невалідні фото")
        widget_show_cam_photos.btn_show_photos.clicked.connect(partial(self.show_photo_cam, qr_code_device))
        widget_show_cam_photos.check_box_status.stateChanged.connect(self.check_all_columns)
        self.setCellWidget(row, 6, widget_show_cam_photos)

        push_btn_cancel_test = QtWidgets.QPushButton("x")
        push_btn_cancel_test.clicked.connect(partial(self.cancel_test_device_qc, dev_type, dev_id))
        self.setCellWidget(row, 7, push_btn_cancel_test)

        push_btn_register_device = QtWidgets.QPushButton('Повернути')
        push_btn_register_device.clicked.connect(partial(self.register_device, dev_id, qr_code_device))
        self.setCellWidget(row, 8, push_btn_register_device)

    def get_defects_array(self, row, col=1):
        """Invalid photo it VISUAL DEFECTS"""
        d_grade, d_return, d_custom = super(MotionDeviceCamView, self).get_defects_array(row, col=col)
        widget_connect = self.cellWidget(row, 2)
        if not self.get_state_widget(row, 6) and widget_connect.get_state():    # Якщо не тикнутий комбобокс
                                                                                # фото і датчик не приписали
            d_grade.append("Невалідні фото")
        return d_grade, d_return, d_custom

    def _send_settings_device(self, dev_type, dev_id):
        hub_obj = self.get_work_hub()
        parameters = ['36', '00']
        answer = hub_obj.write_param(dev_type, dev_id, parameters)
        logger.debug("Answer set, param {}".format(answer))
        # download photo
        self.start_download_mcam_photos(dev_id)
        # change state widget show photos
        self.change_state_widget_photos(dev_id, True)

    def enable_24_mode_device(self, dev_id):
        hub_obj = self.get_work_hub()
        device_obj = hub_obj.get_object(self.get_device_type(), dev_id)
        if device_obj.get_parameters('33') != '01':
            parameters = ['33', '01']
            answer = hub_obj.write_param(self.get_device_type(), dev_id, parameters)
            logger.debug("Answer enable 24 mode {} {}".format(dev_id, answer))

    def start_download_mcam_photos(self, dev_id):
        qr_cam = self.get_qr_code_by_id(dev_id)
        self.get_photo_test_room_mcam(qr_cam)

    def _cancel_test_device_qc(self, dev_type, dev_id):
        self._remove_downloaded_photos(dev_id)
        super(MotionDeviceCamView, self)._cancel_test_device_qc(dev_type, dev_id)

    def _remove_downloaded_photos(self, dev_id):
        qr = self.get_qr_code_by_id(dev_id)
        logger.debug("Delete photo {}".format(qr))
        # Delete photo with photos motion cam
        self.widget_show_photos.delete_data(qr)

    def change_state_widget_photos(self, dev_id: str, state: bool):
        row = self.get_row_by_dev_id(dev_id)
        widget_show_photos = self.cellWidget(row, 6)
        widget_show_photos.setEnabled(state)

    def get_photo_test_room_mcam(self, qr_code_device):
        # thread get photo by qr code
        self.thread_get_link_image.add_qr_get_photo(qr_code_device)
        if not self.thread_get_link_image.isRunning():
            self.thread_get_link_image.start()

    def show_photo_cam(self, qr):
        row = self.get_row_by_dev_id(qr[:6])
        widget_show_photo = self.cellWidget(row, 6)
        if widget_show_photo.state_btn() == EnumStateImage.ImageInit:
            self.show_error("Фото для {} ще не завантажились!".format(qr))
        elif widget_show_photo.state_btn() == EnumStateImage.ImageNotFound:
            self.show_error("Фото для {} не були зробленні на етапі тест руму!".format(qr))
        else:
            self.widget_show_photos.show_device_image(qr)
            self.widget_show_photos.move_center()
            self.widget_show_photos.setFocus(QtCore.Qt.PopupFocusReason)

    def _error_download_photo(self, qr):
        self._change_state_btn_show_photo(qr, EnumStateImage.ImageNotFound)

    def _change_state_btn_show_photo(self, qr: str, state: EnumStateImage):
        row = self.get_row_by_dev_id(qr[:6])
        widget_show_photo = self.cellWidget(row, 6)
        widget_show_photo.set_state_btn(state)

    def _insert_image_motion_cam(self, qr, path_file):
        self.widget_show_photos.set_data(qr, path_file)
        self._change_state_btn_show_photo(qr, EnumStateImage.ImageAvailable)

    def _start_registration_device(self, dev_id, dev_type, dev_color):
        try:
            if not self.check_work_hub_have_data_chan():
                self.show_error("Для роботи з цим девайсом потрібно Хаб який підтримує дата канал!")
                return
        except WorkHubNotFound:
            self.show_error('Робочий хаб було видалено!')
            return
        super(MotionDeviceCamView, self)._start_registration_device(dev_id, dev_type, dev_color)

    def check_work_hub_have_data_chan(self) -> bool:
        """
            Check work hub is Hub2
            Need for device with dataChannel
        """
        hub_obj = self.get_work_hub()
        if hub_obj.sub_type_hub == HubSubType.Hub2 or hub_obj.sub_type_hub == HubSubType.Hub2Plus:
            return True
        else:
            return False

    def _handler_update(self, device_id: str):
        hub_obj = self.get_work_hub()
        motion_cam = hub_obj.get_object(self.get_device_type(), device_id)
        row = self.get_row_by_dev_id(device_id[2:])
        if row is None:
            logger.warning("Update Device {} {} not found in table".format(self.dev_type.value, device_id))
            return
        battery_charge = motion_cam.get_parameters('05')
        self.battery_change_handler(row, battery_charge)

    def battery_change_handler(self, row: int, battery_charge: str):
        widget_battery_change = self.cellWidget(row, 5)
        if not battery_charge:
            logger.debug("Battery motion cam is {}".format(repr(battery_charge)))
            widget_battery_change.set_status("Невідомо")
            return

        if int(battery_charge, 16) >= 101:
            logger.debug("Battery not correct {}".format(repr(battery_charge)))
            widget_battery_change.set_status("Невідомо")
            return
        widget_battery_change.set_status("{}".format(int(battery_charge, 16)))

    def _handler_alarms(self, dev_id: str, alarm_number: str):
        row = self.get_row_by_dev_id(dev_id[2:])
        if row is None:
            logger.warning("Alarm Device {} {} not found in table".format(self.dev_type.value, dev_id))
            return
        if alarm_number == MotionCam.TamperOpen:
            widget_tamped = self.cellWidget(row, 3)
            widget_tamped.change_tamper_count_on()
            self.enable_24_mode_device(dev_id)
        elif alarm_number == MotionCam.TamperClosed:
            widget_tamped = self.cellWidget(row, 3)
            widget_tamped.change_tamper_count_off()
            self.enable_24_mode_device(dev_id)
        elif alarm_number == MotionCam.Movement:
            widget_accelerometer = self.cellWidget(row, 4)
            widget_accelerometer.increment()

        self.check_all_columns()
