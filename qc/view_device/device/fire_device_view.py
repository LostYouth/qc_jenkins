import logging
from collections import namedtuple
from functools import partial

from PyQt5 import QtWidgets

from app_setting import AppSetting
from csa_client_qt.enum_obj_csa import ObjectsCsa
from qc.componets import *
from qc.enum_alarms import FireProtect
from .device_abstract_view import DeviceAbstractView

logger = logging.getLogger(AppSetting.LOGGER_NAME)


class FireDeviceView(DeviceAbstractView):
    dev_type = ObjectsCsa.FireProtect
    column = namedtuple('column', ['name', 'width'])
    table_head = [
        column("Qr", 120),
        column("Наявність\nдефектів", 200),
        column("Підключення", 120),
        column("Тампер", 180),
        column("Тривога", 150),
        column("Запиленість", 150),
        column("Рівень\nбатареї", 120),
        column('Рівень\nдодаткової\nбатареї', 100),
        column("Відмінити\nтестування", 120),
        column("Зареєструвати", 100),
    ]

    def __init__(self, client_csa, parent=None):
        super(FireDeviceView, self).__init__(client_csa, parent=parent)
        self.insert_columns()
        self.row_height = 75

    def add_obj(self, qr_code_device):
        dev_id, dev_type, color = qr_code_device[:6], qr_code_device[6:8], qr_code_device[8]
        if self.check_exists_dev_id(dev_id):
            self.show_error("Цей девайс {} же є в талиці!".format(dev_id))
            return

        row = self.rowCount()
        self.insertRow(self.rowCount())
        self.setRowHeight(row, self.row_height)

        widget_id_device = DevIdWidget(qr_code_device)
        self.setCellWidget(row, 0, widget_id_device)

        widget_defects = VisualDefectTable(self.defects, parent=self.qc_main_widget)
        widget_defects.signal_update_defects.connect(self.check_all_columns)
        self.setCellWidget(row, 1, widget_defects)

        push_button_search_device = ConnectDeviceWidget()
        push_button_search_device.push_btn_connect_device.clicked.connect(
            partial(self._start_registration_device, dev_id, dev_type, color)
        )
        self.setCellWidget(row, 2, push_button_search_device)

        widget_tampered = TamperWidget()
        widget_tampered.set_malfunction_name("Тампер")
        self.setCellWidget(row, 3, widget_tampered)

        widget_alarm = SelectWidget("Тривога")
        widget_alarm.set_malfunction_name("Сенсорна кнопка")
        widget_alarm.signal_update_status.connect(self.check_all_columns)
        self.setCellWidget(row, 4, widget_alarm)

        widget_durty_camera = StatusWidget()
        widget_durty_camera.set_malfunction_name("Камера, запиленість")
        widget_durty_camera.set_expected_result('Незапилена')
        self.setCellWidget(row, 5, widget_durty_camera)

        battery_status = StatusWidget()
        battery_status.set_malfunction_name("Рівень батареї")
        battery_status.set_status('Невідомо')

        battery_status.set_expected_result('100')
        self.setCellWidget(row, 6, battery_status)

        battery_status_second = StatusWidget()
        battery_status_second.set_malfunction_name("Додаткова батарея")
        battery_status_second.set_expected_result(100)
        self.setCellWidget(row, 7, battery_status_second)

        push_btn_cancel_test = QtWidgets.QPushButton("x")
        push_btn_cancel_test.clicked.connect(partial(self.cancel_test_device_qc, dev_type, dev_id))
        self.setCellWidget(row, 8, push_btn_cancel_test)

        push_btn_register_device = QtWidgets.QPushButton('Повернути')
        push_btn_register_device.clicked.connect(partial(self.register_device, dev_id, qr_code_device))
        self.setCellWidget(row, 9, push_btn_register_device)

    def _handler_update(self, device_id):
        hub_obj = self.get_work_hub()
        fire_obj = hub_obj.get_object(self.get_device_type(), device_id)
        row = self.get_row_by_dev_id(device_id[2:])
        if row is None:
            return
        battery_charge = fire_obj.get_parameters('05')
        durty_camera = fire_obj.get_parameters('38')
        status_second_battery = fire_obj.get_parameters('34')
        logger.debug("Statur backUpbattery {}".format(status_second_battery))
        self._handler_battery(row, battery_charge)
        self._handler_durty_camera(row, durty_camera)
        self._handler_status_second_batt(row, status_second_battery)

        self.check_all_columns()

    def _handler_battery(self, row, battery_charge):
        widget_batt = self.cellWidget(row, 6)
        if not battery_charge:
            widget_batt.set_status("Невідомо")
            return
        battery_charge = int(battery_charge, 16)
        if battery_charge > 101:
            widget_batt.set_status("Невідомо")
        else:
            widget_batt.set_status("{}".format(battery_charge))

    def _handler_durty_camera(self, row, status_camera):
        if not status_camera:
            return
        widget_camera = self.cellWidget(row, 5)
        if int(status_camera) == 0:
            widget_camera.set_status("Незапилена")
        elif int(status_camera) == 1:
            widget_camera.set_status("Запилена")
        else:
            widget_camera.set_status("Невідомо")

    def _handler_status_second_batt(self, row, status_batt):
        widget_batt = self.cellWidget(row, 7)
        if not status_batt or status_batt == '80':
            widget_batt.set_status("Невідомо")
            return
        widget_batt.set_status(str(int(status_batt, 16)))

    def _handler_alarms(self, device_id, alarm_number):
        row = self.get_row_by_dev_id(device_id[2:])
        if row is None:
            return

        if alarm_number == FireProtect.TamperOpen:
            widget_tamper = self.cellWidget(row, 3)
            widget_tamper.change_tamper_count_on()
        elif alarm_number == FireProtect.TamperClosed:
            widget_tamper = self.cellWidget(row, 3)
            widget_tamper.change_tamper_count_off()

        self.check_all_columns()
