from csa_client_qt.enum_obj_csa import ObjectsCsa
from .wallswitch_device_view import WallSwitchDeviceView


class RelayDeviceView(WallSwitchDeviceView):
    dev_type = ObjectsCsa.Relay

    min_voltage = 22
    max_voltage = 25

    def _handler_voltage(self, row, voltage):
        if not voltage:
            return
        voltage = int(int(voltage, 16) / 1000)
        widget = self.cellWidget(row, 6)
        widget.set_value(voltage)
