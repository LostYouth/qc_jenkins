from collections import namedtuple
from functools import partial

from PyQt5 import QtWidgets

from csa_client_qt.enum_obj_csa import ObjectsCsa
from qc.componets import DevIdWidget, VisualDefectTable, IncrementWidget, StatusWidget, ConnectDeviceWidget
from qc.enum_alarms import LeaksProtect
from .device_abstract_view import DeviceAbstractView


class LeaksDeviceView(DeviceAbstractView):

    dev_type = ObjectsCsa.LeaksProtect

    column = namedtuple('column', ['name', 'width'])

    table_head = [
        column("Qr", 120),
        column("Наявність\nдефектів", 260),
        column("Підключення", 200),
        column('Спрацювання\nпо\nвитоку\nводи', 220),
        column("Рівень\nбатареї", 180),
        column("Відмінити\nтестування", 120),
        column("Зареєструвати", 100),
    ]

    def __init__(self, client_csa, parent=None):
        super(LeaksDeviceView, self).__init__(client_csa, parent=parent)
        self.row_height = 75
        self.insert_columns()

    def add_obj(self, qr_code_device):
        dev_id, dev_type, color = qr_code_device[:6], qr_code_device[6:8], qr_code_device[8]

        if self.check_exists_dev_id(dev_id):
            self.show_error("Цей девайс {} вже є в таблиці!".format(dev_id))
            return

        row = self.rowCount()
        self.insertRow(row)
        self.setRowHeight(row, self.row_height)

        widget_id = DevIdWidget(qr_code_device)
        self.setCellWidget(row, 0, widget_id)

        widget_defects = VisualDefectTable(self.defects, parent=self.qc_main_widget)
        widget_defects.signal_update_defects.connect(self.check_all_columns)
        self.setCellWidget(row, 1, widget_defects)

        widget_search_device = ConnectDeviceWidget()
        widget_search_device.push_btn_connect_device.clicked.connect(
            partial(self._start_registration_device, dev_id, dev_type, color))
        self.setCellWidget(row, 2, widget_search_device)

        widget_leak_water = IncrementWidget()
        widget_leak_water.set_malfunction_name("Активні контакти")
        widget_leak_water.set_expected_result(8)
        self.setCellWidget(row, 3, widget_leak_water)

        widget_status_battery = StatusWidget()
        widget_status_battery.set_malfunction_name("Рівень батареї")
        widget_status_battery.set_status("Невідомо")
        widget_status_battery.set_expected_result('100')
        self.setCellWidget(row, 4, widget_status_battery)

        cancel_test_widget = QtWidgets.QPushButton("x")
        cancel_test_widget.clicked.connect(partial(self.cancel_test_device_qc, dev_type, dev_id))
        self.setCellWidget(row, 5, cancel_test_widget)

        btn_registration_device = QtWidgets.QPushButton('Повернути')
        btn_registration_device.clicked.connect(partial(self.register_device, dev_id, qr_code_device))
        self.setCellWidget(row, 6, btn_registration_device)

    def _handler_update(self, dev_id):
        hub_obj = self.get_work_hub()
        leak_obj = hub_obj.get_object(self.get_device_type(), dev_id)
        battery_charge = leak_obj.get_parameters('05')

        row = self.get_row_by_dev_id(dev_id[2:])
        if row is None:
            return
        self._update_battery_charge(row, battery_charge)

        self.check_all_columns()

    def _update_battery_charge(self, row, batt_charge):
        widget_batt = self.cellWidget(row, 4)
        if not batt_charge:
            widget_batt.set_status("Невідомо")
            return

        int_batt_charge = int(batt_charge, 16)
        if int_batt_charge > 101:
            return
        widget_batt.set_status("{}".format(int_batt_charge))

    def _handler_alarms(self, dev_id, alarm_number):
        row = self.get_row_by_dev_id(dev_id[2:])
        if row is None:
            return

        if alarm_number == LeaksProtect.LeakWater or alarm_number == LeaksProtect.LeakWaterOff:
            widget_leak_water = self.cellWidget(row, 3)
            widget_leak_water.increment()

        self.check_all_columns()
