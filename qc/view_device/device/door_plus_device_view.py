import logging
from collections import namedtuple
from functools import partial

from PyQt5 import QtWidgets

from app_setting import AppSetting
from csa_client_qt.enum_obj_csa import ObjectsCsa
from qc.componets import TamperWidget, VisualDefectTable, DevIdWidget, ConnectDeviceWidget
from qc.enum_alarms import AlarmsDoorPlusProtect
from qc.view_device.device.door_device_view import DoorDeviceView, StatusWidget, IncrementWidget

logger = logging.getLogger(AppSetting.LOGGER_NAME)


class DoorPlusDeviceView(DoorDeviceView):
    dev_type = ObjectsCsa.DoorProtectPlus
    column = namedtuple('column', ['name', 'width'])
    table_head = [
        column("Qr", 120),
        column("Наявність\nдефектів", 200),
        column("Підключення", 120),
        column("Тампер", 180),
        column("Геркон", 150),
        column("Додатковий\nконтакт", 150),
        column("Рівень\nбатареї", 120),
        column('Вібросенсор', 100),
        column("Акселерометр", 115),
        column("Відмінити\nтестування", 120),
        column("Зареєструвати", 100),
    ]

    def __init__(self, client_csa, parent=None):
        super(DoorPlusDeviceView, self).__init__(client_csa, parent=parent)

    def add_obj(self, qr_code_device):
        dev_id, dev_type, color = qr_code_device[:6], qr_code_device[6:8], qr_code_device[8]
        if self.check_exists_dev_id(dev_id):
            self.show_error("Цей девайс {} же є в талиці!".format(dev_id))
            return

        row = self.rowCount()
        self.insertRow(self.rowCount())
        self.setRowHeight(row, self.row_height)

        widget_id_device = DevIdWidget(qr_code_device)
        self.setCellWidget(row, 0, widget_id_device)

        widget_defects = VisualDefectTable(self.defects, parent=self.qc_main_widget)
        widget_defects.signal_update_defects.connect(self.check_all_columns)

        self.setCellWidget(row, 1, widget_defects)

        push_button_search_device = ConnectDeviceWidget()
        push_button_search_device.push_btn_connect_device.clicked.connect(
            partial(self._start_registration_device, dev_id, dev_type, color)
        )
        self.setCellWidget(row, 2, push_button_search_device)

        widget_tampered = TamperWidget()
        widget_tampered.set_malfunction_name("Тампер")
        self.setCellWidget(row, 3, widget_tampered)

        widget_gercon = IncrementWidget()
        widget_gercon.set_malfunction_name("Геркон")
        widget_gercon.set_expected_result(2)
        self.setCellWidget(row, 4, widget_gercon)

        widget_outcontact = IncrementWidget()
        widget_outcontact.set_malfunction_name("Додаткові контакти")
        widget_outcontact.set_expected_result(2)
        self.setCellWidget(row, 5, widget_outcontact)

        widget_battery_status = StatusWidget()
        widget_battery_status.set_malfunction_name("Рівень батареї")
        widget_battery_status.set_status('Невідомо')
        widget_battery_status.set_expected_result('100')
        self.setCellWidget(row, 6, widget_battery_status)

        widget_shock = IncrementWidget()
        widget_shock.set_malfunction_name("Вібрацію")
        widget_shock.set_expected_result(1)
        self.setCellWidget(row, 7, widget_shock)

        widget_accelerometer = IncrementWidget()
        widget_accelerometer.set_malfunction_name("Акселерометер")
        widget_accelerometer.set_expected_result(1)
        self.setCellWidget(row, 8, widget_accelerometer)

        push_btn_cancel_test = QtWidgets.QPushButton("x")
        push_btn_cancel_test.clicked.connect(partial(self.cancel_test_device_qc, dev_type, dev_id))
        self.setCellWidget(row, 9, push_btn_cancel_test)

        push_btn_register_device = QtWidgets.QPushButton('Повернути')
        push_btn_register_device.clicked.connect(partial(self.register_device, dev_id, qr_code_device))
        self.setCellWidget(row, 10, push_btn_register_device)

    def _handler_alarms(self, device_id, alarm_number):
        row = self.get_row_by_dev_id(device_id[2:])
        if alarm_number == AlarmsDoorPlusProtect.Vibration:
            widget_shock = self.cellWidget(row, 7)
            widget_shock.increment()
        elif alarm_number == AlarmsDoorPlusProtect.Incline:
            widget_accelerometer = self.cellWidget(row, 8)
            widget_accelerometer.increment()

        super(DoorPlusDeviceView, self)._handler_alarms(device_id, alarm_number)

    def _send_settings_device(self, dev_type, dev_id):
        hub_obj = self.get_work_hub()
        parameters = ['33', '01', '34', '01', '40', '01', '43', '01']
        answer = hub_obj.write_param(dev_type, dev_id, parameters)
        logger.debug("Answer set params {}".format(answer))
