import logging
from collections import namedtuple
from functools import partial

from PyQt5 import QtWidgets
from csa_client_qt.enum_obj_csa import ObjectsCsa
from qc.componets import *
from qc.enum_alarms import MotionProtect
from qc.view_device.device.motion_device_view import MotionDeviceView
from .device_abstract_view import DeviceAbstractView


class MotionDevicePlusView(MotionDeviceView):
    dev_type = ObjectsCsa.MotionProtectPlus

    column = namedtuple('column', ['name', 'width'])

    table_head = [
        column("Qr", 120),
        column("Наявність\nдефектів", 260),
        column("Підключення", 120),
        column("Тампер", 180),
        column("Зафіксовано\nрух", 150),
        column("Рівень\nбатареї", 120),
        column("Відмінити\nтестування", 120),
        column("Зареєструвати", 100),
    ]
