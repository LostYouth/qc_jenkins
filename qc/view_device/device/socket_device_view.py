from collections import namedtuple
from functools import partial

from PyQt5 import QtWidgets

from csa_client_qt.enum_obj_csa import ObjectsCsa
from qc.componets import *
from .device_abstract_view import DeviceAbstractView
from .wallswitch_device_view import WallSwitchDeviceView


class SocketDeviceView(WallSwitchDeviceView):
    dev_type = ObjectsCsa.Socket

    min_voltage = 198
    max_voltage = 245

    column = namedtuple('column', ['name', 'width'])

    table_head = [
        column("Qr", 120),
        column("Наявність\nдефектів", 200),
        column("Підключення", 120),
        column("Включення", 180),
        column("Виключення", 150),
        column("Змінити стан", 100),
        column("Рівень\nнапруги", 150),
        column("Світлодіод", 120),
        column("Відмінити\nтестування", 120),
        column("Зареєструвати", 100),
    ]

    def __init__(self, client_csa, parent=None):
        super(SocketDeviceView, self).__init__(client_csa, parent=parent)
        self.row_height = 75

    def add_obj(self, qr_code_device):
        dev_id, dev_type, color = qr_code_device[:6], qr_code_device[6:8], qr_code_device[8]
        if self.check_exists_dev_id(dev_id):
            self.show_error("Цей девайс {} же є в талиці!".format(dev_id))
            return

        row = self.rowCount()
        self.insertRow(self.rowCount())
        self.setRowHeight(row, self.row_height)

        widget_id_device = DevIdWidget(qr_code_device)
        self.setCellWidget(row, 0, widget_id_device)

        widget_defects = VisualDefectTable(self.defects, parent=self.qc_main_widget)
        widget_defects.signal_update_defects.connect(self.check_all_columns)
        self.setCellWidget(row, 1, widget_defects)

        push_button_search_device = ConnectDeviceWidget()
        push_button_search_device.push_btn_connect_device.clicked.connect(partial(self._start_registration_device, dev_id, dev_type, color))
        self.setCellWidget(row, 2, push_button_search_device)

        enable_socket = IncrementWidget()
        enable_socket.set_malfunction_name("Включення сокета")
        enable_socket.set_expected_result(1)
        self.setCellWidget(row, 3, enable_socket)

        disabled_socket = IncrementWidget()
        disabled_socket.set_malfunction_name("Включення сокета")
        disabled_socket.set_expected_result(1)
        self.setCellWidget(row, 4, disabled_socket)

        change_state = ChangeStateWallFamily("Змінити стан")
        change_state.clicked.connect(partial(self._change_state_device, dev_id))
        self.setCellWidget(row, 5, change_state)

        current_voltage = VoltageStatusWallSwitch(self.min_voltage, self.max_voltage)
        current_voltage.set_malfunction_name("Рівень напруги")
        self.setCellWidget(row, 6, current_voltage)

        blink_socket = SelectWidget('Ok')
        blink_socket.set_malfunction_name("Підсвітка")
        blink_socket.signal_update_status.connect(self.check_all_columns)
        self.setCellWidget(row, 7, blink_socket)

        push_btn_cancel_test = QtWidgets.QPushButton("x")
        push_btn_cancel_test.clicked.connect(partial(self.cancel_test_device_qc, dev_type, dev_id))
        self.setCellWidget(row, 8, push_btn_cancel_test)

        push_btn_register_device = QtWidgets.QPushButton('Повернути')

        push_btn_register_device.clicked.connect(partial(self.register_device, dev_id, qr_code_device))
        self.setCellWidget(row, 9, push_btn_register_device)

