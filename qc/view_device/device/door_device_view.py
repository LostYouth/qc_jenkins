import logging
from collections import namedtuple
from functools import partial

from PyQt5 import QtWidgets

from app_setting import AppSetting
from .device_abstract_view import DeviceAbstractView
from csa_client_qt.enum_obj_csa import ObjectsCsa
from qc.componets import *
from qc.enum_alarms import AlarmsDoorProtect

logger = logging.getLogger(AppSetting.LOGGER_NAME)


class DoorDeviceView(DeviceAbstractView):
    dev_type = ObjectsCsa.DoorProtect
    column = namedtuple('column', ['name', 'width'])

    table_head = [
        column("Qr", 120),
        column("Наявність\nдефектів", 200),
        column("Підключення", 120),
        column("Тампер", 180),
        column("Геркон", 150),
        column("Додатковий\nконтакт", 150),
        column("Рівень\nбатареї", 120),
        column("Відмінити\nтестування", 120),
        column("Зареєструвати", 100),
    ]

    def __init__(self, client_csa, parent=None):
        super(DoorDeviceView, self).__init__(client_csa, parent=parent)
        self.row_height = 75
        self.insert_columns()

    def add_obj(self, qr_code_device):
        dev_id, dev_type, color = qr_code_device[:6], qr_code_device[6:8], qr_code_device[8]
        if self.check_exists_dev_id(dev_id):
            self.show_error("Цей девайс {} же є в талиці!".format(dev_id))
            return

        row = self.rowCount()
        self.insertRow(self.rowCount())
        self.setRowHeight(row, self.row_height)

        widget_id_device = DevIdWidget(qr_code_device)
        self.setCellWidget(row, 0, widget_id_device)

        widget_defects = VisualDefectTable(self.defects, parent=self.qc_main_widget)
        widget_defects.signal_update_defects.connect(self.check_all_columns)
        self.setCellWidget(row, 1, widget_defects)

        push_button_search_device = ConnectDeviceWidget()
        push_button_search_device.push_btn_connect_device.clicked.connect(
            partial(self._start_registration_device, dev_id, dev_type, color))
        self.setCellWidget(row, 2, push_button_search_device)

        widget_tampered = TamperWidget()
        widget_tampered.set_malfunction_name("Тампер")
        self.setCellWidget(row, 3, widget_tampered)

        widget_gercon = IncrementWidget()
        widget_gercon.set_malfunction_name("Геркон")
        widget_gercon.set_expected_result(2)
        self.setCellWidget(row, 4, widget_gercon)

        widget_outcontact = IncrementWidget()
        widget_outcontact.set_malfunction_name("Додаткові контакти")
        widget_outcontact.set_expected_result(2)
        self.setCellWidget(row, 5, widget_outcontact)

        widget_battery_status = StatusWidget()
        widget_battery_status.set_malfunction_name("Рівень батареї")
        widget_battery_status.set_status('Невідомо')
        widget_battery_status.set_expected_result('100')
        self.setCellWidget(row, 6, widget_battery_status)

        push_btn_cancel_test = QtWidgets.QPushButton("x")
        push_btn_cancel_test.clicked.connect(partial(self.cancel_test_device_qc, dev_type, dev_id))
        self.setCellWidget(row, 7, push_btn_cancel_test)

        push_btn_register_device = QtWidgets.QPushButton('Повернути')
        push_btn_register_device.clicked.connect(partial(self.register_device, dev_id, qr_code_device))
        self.setCellWidget(row, 8, push_btn_register_device)

    def _send_settings_device(self, dev_type, dev_id):
        hub_obj = self.get_work_hub()
        parameters = ['33', '01', '34', '01']
        answer = hub_obj.write_param(dev_type, dev_id, parameters)
        logger.debug("Answer set params {}".format(answer))

    def _handler_alarms(self, device_id, alarm_number):
        row = self.get_row_by_dev_id(device_id[2:])
        if row is None:
            return

        if alarm_number == AlarmsDoorProtect.TamperOpen:
            widget_tamper = self.cellWidget(row, 3)
            widget_tamper.change_tamper_count_on()
        elif alarm_number == AlarmsDoorProtect.TamperClosed:
            widget_tamper = self.cellWidget(row, 3)
            widget_tamper.change_tamper_count_off()
        elif alarm_number in (AlarmsDoorProtect.ReedOpened, AlarmsDoorProtect.ReedClosed):
            widget_reed_door = self.cellWidget(row, 4)
            widget_reed_door.increment()
        elif alarm_number in (AlarmsDoorProtect.OutContactClosed, AlarmsDoorProtect.OutContactOpened):
            widget_out_contact = self.cellWidget(row, 5)
            widget_out_contact.increment()

        self.check_all_columns()
        return device_id, alarm_number

    def _handler_update(self, device_id):
        hub_obj = self.get_work_hub()
        door_obj = hub_obj.get_object(self.get_device_type(), device_id)
        row = self.get_row_by_dev_id(device_id[2:])
        if row is None:
            return
        battery_charged = door_obj.get_parameters('05')
        self._update_battery_charged(row, battery_charged)

        self.check_all_columns()

    def _update_battery_charged(self, row, value):
        widget_batt = self.cellWidget(row, 6)
        if not value:
            widget_batt.set_status("Невідомо")
            return
        battery_chage_int = int(value, 16)
        if battery_chage_int > 101:
            return

        widget_batt.set_status("{}".format(battery_chage_int))
