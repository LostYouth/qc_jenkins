import binascii
import logging
from collections import namedtuple
from functools import partial

from PyQt5 import QtWidgets

from app_setting import AppSetting
from csa_client_qt.enum_obj_csa import ObjectsCsa
from csa_client_qt.enum_params import Answer, MessageType, Updates
from qc.componets import *
from qc.enum_alarms import Keypad
from qc.enum_color import ColorResult
from .device_abstract_view import DeviceAbstractView

logger = logging.getLogger(AppSetting.LOGGER_NAME)


class KeypadDeviceView(DeviceAbstractView):
    dev_type = ObjectsCsa.Keypad
    column = namedtuple('column', ['name', 'width'])
    table_head = [
        column("Qr", 120),
        column("Наявність\nдефектів", 170),
        column("Підключення", 120),
        column("Тампер", 140),
        column("Постановка\nпід\nохорону", 120),
        column("Зняття\nз-під\nохорони", 120),
        column("Постановка\nпід\nчасткову\nохорону", 120),
        column("Спрацювання\nтривожної\nкнопки (*)", 120),
        column("Спрацювання\n кнопки\n очищення", 140),
        column('Зміна\n паролю', 130),
        column("Рівень\nбатареї", 120),
        column("Відмінити\nтестування", 120),
        column("Зареєструвати", 100),
    ]

    def __init__(self, client_csa, parent=None):
        super(KeypadDeviceView, self).__init__(client_csa, parent=parent)
        self.row_height = 75
        self.insert_columns()

    def add_obj(self, qr_code_device):
        dev_id, dev_type, color = qr_code_device[:6], qr_code_device[6:8], qr_code_device[8]
        if self.check_exists_dev_id(dev_id):
            self.show_error("Цей девайс {} же є в талиці!".format(dev_id))
            return

        row = self.rowCount()
        self.insertRow(row)
        self.setRowHeight(row, self.row_height)

        widget_id = DevIdWidget(qr_code_device)
        self.setCellWidget(row, 0, widget_id)

        widget_defects = VisualDefectTable(self.defects, parent=self.qc_main_widget)
        widget_defects.signal_update_defects.connect(self.check_all_columns)
        self.setCellWidget(row, 1, widget_defects)

        push_btn_search_device = ConnectDeviceWidget()
        push_btn_search_device.push_btn_connect_device.clicked.connect(
            partial(self._start_registration_device, dev_id, dev_type, color)
        )
        self.setCellWidget(row, 2, push_btn_search_device)

        widget_tamper = TamperWidget()
        widget_tamper.set_malfunction_name("Тампер")
        self.setCellWidget(row, 3, widget_tamper)

        arm_widget = IncrementWidget()
        arm_widget.set_malfunction_name("Сенсорна панель")
        arm_widget.set_expected_result(1)
        self.setCellWidget(row, 4, arm_widget)

        disarm_widget = IncrementWidget()
        disarm_widget.set_malfunction_name("Сенсорна панель")
        disarm_widget.set_expected_result(1)
        self.setCellWidget(row, 5, disarm_widget )

        partial_arm_widget = IncrementWidget()
        partial_arm_widget.set_malfunction_name("Сенсорна панель")
        partial_arm_widget.set_expected_result(1)
        self.setCellWidget(row, 6, partial_arm_widget)

        panic_widget = IncrementWidget()
        panic_widget.set_malfunction_name("Сенсорна панель")
        panic_widget.set_expected_result(1)
        self.setCellWidget(row, 7, panic_widget)

        clear_btn_widget = SelectWidget("Очищення (C)")
        clear_btn_widget.set_malfunction_name("Сенсорна панель")
        clear_btn_widget.signal_update_status.connect(self.check_all_columns)
        self.setCellWidget(row, 8, clear_btn_widget)

        btn_change_password = ChangePasswordKeypad("Змінити пароль\nна (567890)")
        btn_change_password.clicked.connect(partial(self._changed_password_keypad, dev_id))
        self.setCellWidget(row, 9, btn_change_password)

        battery_status_widget = StatusWidget()
        battery_status_widget.set_malfunction_name("Рівень батареї")
        battery_status_widget.set_status("Невідомо")
        battery_status_widget.set_expected_result('100')
        self.setCellWidget(row, 10, battery_status_widget)

        push_btn_cancel_test = QtWidgets.QPushButton("x")
        push_btn_cancel_test.clicked.connect(partial(self.cancel_test_device_qc, dev_type, dev_id))
        self.setCellWidget(row, 11, push_btn_cancel_test)

        push_btn_register_device = QtWidgets.QPushButton('Повернути')
        push_btn_register_device.clicked.connect(partial(self.register_device, dev_id, qr_code_device))
        self.setCellWidget(row, 12, push_btn_register_device)

    def _cancel_test_device_qc(self, dev_type, dev_id):
        hub_obj = self.get_work_hub()
        answer_disarm = hub_obj.disarm()
        logger.debug("answer disarm cancel test keypad qc {}".format(answer_disarm))
        super(KeypadDeviceView, self)._cancel_test_device_qc(dev_type, dev_id)

    def _changed_password_keypad(self, device_id):
        sender = self.sender()
        sender.setEnabled(False)
        hub_obj = self.get_work_hub()
        keypad_obj = hub_obj.get_object(self.get_device_type(), "{:0>8}".format(device_id))
        if keypad_obj is None:
            self.show_error("Девайс {} не приписаний!")
            return
        a = hub_obj.write_param(
            self.get_device_type(), "{:0>8}".format(device_id),
            payload=['31', binascii.hexlify('567890'.encode()).decode()]
        )
        logger.debug("Answer change password keypad {}".format(a))
        if a.message_type == MessageType.Answer.value and a.message_key == Answer.WrongState.value:
            self.show_error("Не можливо змінити пароль поки хаб на охороні!")
        elif (a.message_type == MessageType.Updates.value and a.message_key == Updates.UpdateSettings.value) or \
                (a.message_type == MessageType.Answer.value and a.message_key == Answer.DeliveredCommandPerform.value):
            sender.setStyleSheet(ColorResult.success.value)
            self._change_disarm_hub_default(device_id)
            sender._state = True
            return
        else:
            self.show_error("{}".format(a))
        sender.setEnabled(True)

    def _change_disarm_hub_default(self, dev_id):
        row = self.get_row_by_dev_id(dev_id)
        widget_disarm = self.cellWidget(row, 5)
        widget_disarm.setText(str(0))

    def _send_settings_device(self, dev_type, dev_id):
        hub_obj = self.get_work_hub()
        p = ['38', '01', '3b', '05']
        a = hub_obj.write_param(dev_type, dev_id, p)
        logger.debug("Answer send setting {}".format(a))

    def _handler_update(self, device_id):
        hub_obj = self.get_work_hub()
        keypad = hub_obj.get_object(self.get_device_type(), device_id)
        row = self.get_row_by_dev_id(device_id[2:])
        if row is None:
            return
        self._update_battery_charged(row, keypad.get_parameters('05'))

        self.check_all_columns()

    def _update_battery_charged(self, row, value):
        widget_batt = self.cellWidget(row, 10)
        if not value:
            widget_batt.set_status("Невідомо")
            return
        battery_chage_int = int(value, 16)
        if battery_chage_int > 101:
            widget_batt.set_status("Невідомо")
            return

        widget_batt.set_status("{}".format(battery_chage_int))

    def _handler_alarms(self, dev_id, alarm_number):
        row = self.get_row_by_dev_id(dev_id[2:])
        if row is None:
            return

        if alarm_number == Keypad.TamperClosed:
            widget_tamper = self.cellWidget(row, 3)
            widget_tamper.change_tamper_count_off()
        elif alarm_number == Keypad.TamperOpen:
            widget_tamper = self.cellWidget(row, 3)
            widget_tamper.change_tamper_count_on()
        elif alarm_number == Keypad.Arm:
            widget_arm = self.cellWidget(row, 4)
            widget_arm.increment()
        elif alarm_number == Keypad.Disarm:
            widget_disarm = self.cellWidget(row, 5)
            widget_disarm.increment()
        elif alarm_number == Keypad.PerimetralArm:
            widget_perimeter = self.cellWidget(row, 6)
            widget_perimeter.increment()
        elif alarm_number == Keypad.PanicBtn:
            widget_panic_btn = self.cellWidget(row, 7)
            widget_panic_btn.increment()

        self.check_all_columns()
