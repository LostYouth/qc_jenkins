from collections import namedtuple
from functools import partial

from PyQt5 import QtWidgets

from csa_client_qt.enum_obj_csa import ObjectsCsa
from qc.componets import *
from qc.enum_alarms import SpaceControl
from .device_abstract_view import DeviceAbstractView


class SpaceControlDeviceView(DeviceAbstractView):
    dev_type = ObjectsCsa.SpaceControl
    column = namedtuple('column', ['name', 'width'])

    table_head = [
        column("Qr", 120),
        column("Наявність\nдефектів", 200),
        column("Підключення", 120),
        column("Постановка\nпід\nохорону", 180),
        column("Зняття \nз-під\n охорони", 150),
        column("Постановка\nпід\nчасткову\nохорону", 150),
        column("Спрацювання\nтривожної\nкнопки", 150),
        column("Рівень\nбатареї", 120),
        column("Відмінити\nтестування", 120),
        column("Зареєструвати", 100),
    ]

    def __init__(self, client_csa, parent=None):
        super(SpaceControlDeviceView, self).__init__(client_csa, parent=parent)
        self.insert_columns()
        self.row_height = 75

    def add_obj(self, qr_device):
        dev_id, dev_type, color = qr_device[:6], qr_device[6:8], qr_device[8]
        if self.check_exists_dev_id(dev_id):
            self.show_error("Цей девайс {} же є в талиці!".format(dev_id))
            return

        row = self.rowCount()
        self.insertRow(self.rowCount())
        self.setRowHeight(row, self.row_height)

        widget_id_device = DevIdWidget(qr_device)
        self.setCellWidget(row, 0, widget_id_device)

        widget_defects = VisualDefectTable(self.defects, parent=self.qc_main_widget)
        widget_defects.signal_update_defects.connect(self.check_all_columns)
        self.setCellWidget(row, 1, widget_defects)

        push_button_search_device = ConnectDeviceWidget()
        push_button_search_device.push_btn_connect_device.clicked.connect(
            partial(self._start_registration_device, dev_id, dev_type, color))
        self.setCellWidget(row, 2, push_button_search_device)

        arm_widget = IncrementWidget()
        arm_widget.set_malfunction_name("Кнопка постановки на охорону")
        arm_widget.set_expected_result(1)
        self.setCellWidget(row, 3, arm_widget)

        disarm_widget = IncrementWidget()
        disarm_widget.set_malfunction_name("Кнопка постановки знятя з охорони")
        disarm_widget.set_expected_result(1)
        self.setCellWidget(row, 4, disarm_widget)

        partial_arm_widget = IncrementWidget()
        partial_arm_widget.set_malfunction_name("Кнопка постановки частокову охорону")
        partial_arm_widget.set_expected_result(1)
        self.setCellWidget(row, 5, partial_arm_widget)

        panic_btn_widget = IncrementWidget()
        panic_btn_widget.set_malfunction_name("Кнопка тривоги")
        panic_btn_widget.set_expected_result(1)
        self.setCellWidget(row, 6, panic_btn_widget)

        battery_status = StatusWidget()
        battery_status.set_malfunction_name("Рівень батареї")
        battery_status.set_status("Невідомо")
        battery_status.set_expected_result('100')
        self.setCellWidget(row, 7, battery_status)

        push_btn_cancel_test = QtWidgets.QPushButton("x")
        push_btn_cancel_test.clicked.connect(partial(self.cancel_test_device_qc, dev_type, dev_id))
        self.setCellWidget(row, 8, push_btn_cancel_test)

        push_btn_register_device = QtWidgets.QPushButton('Повернути')
        push_btn_register_device.clicked.connect(partial(self.register_device, dev_id, qr_device))
        self.setCellWidget(row, 9, push_btn_register_device)

    def register_device(self, device_id: str, device_qr: str):
        hub_obj = self.get_work_hub()
        if hub_obj.get_parameters('08') != '00':
            self.show_error("Зніміть Хаб з охорони!")
            return
        super(SpaceControlDeviceView, self).register_device(device_id, device_qr)

    def _handler_update(self, device_id):
        hub_obj = self.get_work_hub()
        space = hub_obj.get_object(self.get_device_type(), device_id)
        row = self.get_row_by_dev_id(device_id[2:])
        if row is None:
            return
        battery_charge = space.get_parameters('05')
        self._update_battery_charge(row, battery_charge)

        self.check_all_columns()

    def _update_battery_charge(self, row, battery_val):
        widget_batt = self.cellWidget(row, 7)
        if not battery_val:
            widget_batt.set_status("Невідомо")
            return
        battery_val = int(battery_val, 16)
        if battery_val > 101:
            widget_batt.set_status("Невідомо")
        else:
            widget_batt.set_status(str(battery_val))

    def _handler_alarms(self, device_id, alarm_number):
        row = self.get_row_by_dev_id(device_id[2:])
        if row is None:
            return

        if alarm_number == SpaceControl.Arm:
            widget_arm = self.cellWidget(row, 3)
            widget_arm.increment()
        elif alarm_number == SpaceControl.Disarm:
            widget_disarm = self.cellWidget(row, 4)
            widget_disarm.increment()
        elif alarm_number == SpaceControl.PerimetralArm:
            widget_par = self.cellWidget(row, 5)
            widget_par.increment()
        elif alarm_number == SpaceControl.PanicBtn:
            widget_panic = self.cellWidget(row, 6)
            widget_panic.increment()

        self.check_all_columns()
