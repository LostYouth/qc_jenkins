import logging
from functools import partial
from contextlib import contextmanager
from PyQt5 import QtWidgets, QtCore
from pymysql import DatabaseError
from __app_name__ import __app_name__ as app_name
from __version__ import __version__ as app_version
from app_setting import AppSetting
from csa_client_qt.csa_object.csa_object import CsaObjectHub
from csa_client_qt.enum_params import MessageType, DeviceRegister, Updates, PartialRegistration, Answer
from csa_client_qt.enum_obj_csa import ObjectsCsa

from qc.componets import ErrorPopupWidget
from qc.componets.search_device_start import SearchDevicePopup
from qc.database.database_production import DataBaseProduction
from qc.enum_color import ColorResult
from qc.enum_param import QrDevTypes
from qc.exc import WorkHubNotFound, NotImplement
from qc.security import KeyDatabase

logger = logging.getLogger(AppSetting.LOGGER_NAME)


class ThreadRegisterDb(QtCore.QThread):

    def __init__(self, func, *args, **kw):
        super(ThreadRegisterDb, self).__init__()
        self.func, self.args, self.kw = func, args, kw

        self.result = 'working'

    def get_result(self):
        return self.result

    def run(self):
        self.result = self.func(*self.args, **self.kw)


class DeviceAbstractView(QtWidgets.QTableWidget):
    dev_type = ObjectsCsa.DoorProtect

    table_head = []

    def __init__(self, client_csa, parent=None):
        super(DeviceAbstractView, self).__init__()
        self.client_csa = client_csa
        self.qc_main_widget = parent

        self.database_prod = DataBaseProduction(old_db_keys=KeyDatabase.get_old_keys_production(),
                                                db_keys=KeyDatabase.get_keys_production())

        self.defects = dict()
        self.defects['class_b'] = ['1', '2']
        self.defects['return'] = ['12', '32']

        self.widget_error = ErrorPopupWidget(parent=self)
        self.widget_search_device = SearchDevicePopup(parent=self.qc_main_widget)

        self.setEditTriggers(QtWidgets.QTableWidget.NoEditTriggers)
        self.setSelectionMode(QtWidgets.QTableWidget.NoSelection)
        self.horizontalHeader().setStretchLastSection(True)

    def set_defects(self, defects: dict):
        self.defects = defects

    def get_device_type_name(self):
        return self.dev_type.name

    def get_device_type(self):
        return self.dev_type.value

    def get_work_hub(self) -> CsaObjectHub:
        for hub_obj in self.client_csa._hubs:
            if hub_obj.work_hub_qc:
                return hub_obj
        raise WorkHubNotFound("Робочий Хаб не визначений!")

    def get_state_widget(self, row, col):
        widget_p = self.cellWidget(row, col)
        return widget_p.get_state()

    def insert_columns(self):
        for index, column in enumerate(self.table_head):
            self.insertColumn(index)
            self.setColumnWidth(index, column.width)
            item_column_name = QtWidgets.QTableWidgetItem(column.name)
            self.setHorizontalHeaderItem(index, item_column_name)

    def get_qr_code_by_id(self, dev_id):
        """Return qr code device if device in exists table"""
        for row in range(self.rowCount()):
            wid_id = self.cellWidget(row, 0)
            if wid_id.get_id() == dev_id:
                return wid_id.get_qr_code()
        return None

    def get_row_by_dev_id(self, dev_id):
        for row in range(self.rowCount()):
            wid_id = self.cellWidget(row, 0)
            if wid_id.get_id() == dev_id:
                return row
        return None

    def remove_row_by_device_id(self, device_id):
        row = self.get_row_by_dev_id(device_id)
        if row is not None:
            self.removeRow(row)

    def disarm_work_hub(self):
        hub_obj = self.get_work_hub()
        a = hub_obj.disarm()
        logger.info("Disarmed work hub answer: {}".format(a))

    def check_exists_dev_id(self, dev_id):
        if self.get_row_by_dev_id(dev_id) is not None:
            return True
        return False

    def _start_registration_device(self, dev_id, dev_type, dev_color):
        button_start_register_device = self.sender()
        button_start_register_device.setEnabled(False)
        button_start_register_device.startTimer(800)
        button_start_register_device.timerEvent = lambda QEvent: button_start_register_device.setEnabled(True)
        try:
            hub_obj = self.get_work_hub()
        except WorkHubNotFound as err:
            logger.error("{}".format(err))
            self.show_error("Робочий хаб було видалено!")
            return

        row = self.get_row_by_dev_id(dev_id)
        widget_defects = self.cellWidget(row, 1)
        if widget_defects.defects_popup.get_defects_return():
            self.show_error("В датчика {} вибрані дефекти 'тільки на повернення'! "
                            "Датчик не підлягає подальшому тестуванню!".format(dev_id))
            return
        if (widget_defects.defects_popup.get_defects_grade() or widget_defects.defects_popup.get_custom_defects()) and not widget_defects.check_box_grade_defect.isChecked():
            self.show_error("Якщо обрані дефекти підлягаю класу Б, відмітьте це в чекбоксі, якщо ні девайс підлягає поверненню!")
            return

        dev_obj = hub_obj.get_object(dev_type, "{:0>8}".format(dev_id))
        logger.info("Result search device in self {}".format(repr(dev_obj)))
        if dev_obj is not None:
            logger.warning("Device exists in Hub {}".format(dev_obj))
            self._success_registration_device2client(dev_id, dev_type)
            return
        rooms = hub_obj.get_objects(ObjectsCsa.Room)
        if not rooms:
            answer = hub_obj.add_room_hub("Qc Room")
            logger.debug("Answer add room {}".format(answer))
            if answer.message_type == MessageType.PartialRegistration.value and answer.message_key == PartialRegistration.ExistsRoomWithItParams.value:
                rooms = hub_obj.get_objects(ObjectsCsa.Room)
            else:
                self.show_error("Невдалось додати кімнату! Для реєстрації датчика!")
                return
        r = rooms[-1]
        answer = hub_obj.start_search_device1(dev_id, dev_type, r.id, "QC-T{}".format(dev_id), dev_color)
        if answer.message_type == MessageType.DeviceRegister.value and answer.message_key == DeviceRegister.SearchStartDevice.value:
            self.widget_search_device = SearchDevicePopup(parent=self.qc_main_widget)
            self.widget_search_device.push_btn_cancel_register_device.clicked.connect(partial(self._cancel_registration_device, answer.link))
            self.show_popup_search_device()
            self._wait_answer_registration_device(dev_id, dev_type, answer.link)
        elif answer.message_type == MessageType.Answer.value and answer.message_key == Answer.UndeliveredReceiverOffline.value:
            self.show_error("Ваш хаб в офлайні")
            hub_obj.request_full_setting()
        elif answer.message_type == MessageType.Answer.value and answer.message_key == Answer.DeliveredWasAlreadyPerform.value:
            self.show_error("Дія була виконана раніш!")
            hub_obj.request_full_setting()
        elif answer.message_type == MessageType.Answer.value and answer.message_key == Answer.WrongState.value:
            self.show_error("Хаб на охороні зніміть хаб з охорони!")
        else:
            self.show_error("Помилка {}".format(answer))

    def _wait_answer_registration_device(self, dev_id, dev_type, link):
        answer = self.client_csa._wait_message(link, timeout=32)
        self.close_popup_register_device()
        if answer.message_type == MessageType.DeviceRegister.value and answer.message_key == DeviceRegister.TimeOutRegisterDevice.value:
            logger.debug("Timeout registration device")
            self._failure_register_device(dev_id, dev_type, 'Скінчився час відведений на реєстрацію!')
        elif answer.message_type == MessageType.DeviceRegister.value and answer.message_key == DeviceRegister.ErrorWhileRegDevice.value:
            logger.error("Error register device {} dev_type {}".format(dev_id, dev_type))
            self._handler_error_register_device(answer)
        elif answer.message_type == MessageType.Updates.value and answer.message_key == Updates.ObjectAdded.value:
            logger.debug("Success device added")
            self._success_registration_device2client(dev_id, dev_type)
        elif not answer.message_key and not answer.message_type:
            logger.warning("FUCK OFF cancel task in side client or not answer")
        else:
            self._failure_register_device(dev_id, dev_type, "Помилка {}".format(answer))

    def _failure_register_device(self, dev_id, dev_type, message_text=''):
        row = self.get_row_by_dev_id(dev_id)
        widget_registration_device = self.cellWidget(row, 2)
        widget_registration_device.setStyleSheet(ColorResult.fail.value)
        if message_text:
            self.show_error(message_text)

    def _handler_error_register_device(self, answer):
        """ 05 03 answer CSA """
        dev_t, error_code = answer.payload
        if dev_t == '00' and error_code == '01':
            self.show_error("Невідомий пристрій")
        elif dev_t == '00' and error_code == '02':
            self.show_error("Пристрій не підтримується моделью Хаба!")
        elif dev_t == '00' and error_code == '03':
            self.show_error("Необхідно оновити прошивку для Хаба!")
        else:
            self.show_error("{}".format(answer))

    def show_popup_search_device(self):
        self.widget_search_device.show()

    def close_popup_register_device(self):
        self.widget_search_device.close_timeout()

    def show_error(self, message: str):
        self.widget_error.set_message_data(message)
        self.widget_error.show()

    def _cancel_registration_device(self, link):
        hub_obj = self.get_work_hub()
        answer = hub_obj.cancel_registration_device()
        logger.debug("Answer cancel registration {}".format(answer))
        r = self.client_csa._cancel_wait_message(link)
        logger.debug("Result cancel task {}".format(r))

    def _success_registration_device2client(self, dev_id, dev_type):
        row = self.get_row_by_dev_id(dev_id)
        widget_registration_device = self.cellWidget(row, 2)
        widget_registration_device.setStyleSheet(ColorResult.success.value)
        widget_registration_device.set_state(True)
        hub_obj = self.get_work_hub()
        while hub_obj.get_object(dev_type, "{:0>8}".format(dev_id)) is None:
            QtWidgets.QApplication.processEvents()
        dev_obj = hub_obj.get_object(dev_type, "{:0>8}".format(dev_id))
        self._disconnect_slots_device(dev_obj)
        dev_obj.signal_update_data.connect(self._handler_update)
        dev_obj.signal_alarm_object.connect(self._handler_alarms)
        self._send_settings_device(dev_type, dev_id)

    def _disconnect_slots_device(self, dev_obj):
        try:
            dev_obj.signal_alarm_object.disconnect(self._handler_alarms)
            dev_obj.signal_update_data.disconnect(self._handler_update)
        except Exception as err:
            logger.warning("Err disconnected signal {}".format(err))

    def _handler_update(self, *args):
        """update singal call this method"""
        pass

    def _handler_alarms(self, *args):
        """alarm signal call this method"""
        pass

    def _send_settings_device(self, dev_type, dev_id):
        """Send write param after register device"""
        pass

    def cancel_test_device_qc(self, dev_type, dev_id):
        if not self._you_sure(text="Ви впевнені що хочете відмінити тестування девайсу {} {}!".format(dev_type, dev_id)):
            logger.info("Click not sure when cancel test device {} {}".format(dev_type, dev_id))
            return
        logger.debug("id: {} Disable btn cancel".format(dev_id))
        with self.change_state_btn_cancel():
            self._cancel_test_device_qc(dev_type, dev_id)
        logger.debug("id: {} Enable btn cancel".format(dev_id))

    def _cancel_test_device_qc(self, dev_type, dev_id):
        logger.debug("id: {} Start cancel test device qc".format(dev_id, dev_type))
        if not self._disarmed_work_hub():
            return
        hub_obj = self.get_work_hub()
        if hub_obj.get_object(dev_type, "{:0>8}".format(dev_id)):
            answer = hub_obj.delete_device(dev_id)
            logger.debug("id: {} Answer delete device {}".format(dev_id, answer))

            if answer.message_type == MessageType.Answer.value and answer.message_key == Answer.WrongState.value:
                self.show_error("Неможливо видалити датчик поки Хаб на охороні!")
                return
            m_t = answer.message_type
            m_k = answer.message_key
            if m_t not in [MessageType.Answer.value, MessageType.Answer.value] and m_k not in [Answer.DeliveredCommandPerform.value, Updates.ObjectRemove.value]:
                self.show_error("Помилка під час видалення датчика!\nДеталі: {}".format(answer))
                return
        row = self.get_row_by_dev_id(dev_id)
        if row is not None:
            self.removeRow(row)
            logger.debug("id: {} Remove row with device".format(dev_id))

    def success_register_device(self, *args, **kw):
        thread_register_db = ThreadRegisterDb(self._success_register_device, *args, **kw)
        thread_register_db.start()
        loop = QtCore.QEventLoop()
        thread_register_db.finished.connect(loop.quit)
        loop.exec_()
        result = thread_register_db.get_result()
        logger.debug("id: {} Result register in db".format(args))
        return result

    def _success_register_device(self, operator, device_qr, device_id, grade_qc, defects, info):
        try:
            if self.database_prod.qr_exists(device_qr):
                login = self.qc_main_widget._operator_login
                self.database_prod.register_device_success(device_qr, login, grade_qc, defects, info)
            else:
                self.database_prod.register_device_success_old(
                    operator=operator, dev_id=device_id, grade_qc=grade_qc, defects_qc=defects
                )
        except DatabaseError as err:
            self.show_error("Не вдалось зареєструвати девайс {}!\nДеталі: {}".format(device_id, err))
            return False
        else:
            return True

    def fail_register_device(self, dev_id, dev_qr, defects, info):
        try:
            if self.database_prod.qr_exists(dev_qr):
                login = self.qc_main_widget._operator_login
                self.database_prod.register_device_fail(login, dev_qr, defects, info)
            else:
                self.database_prod.register_device_fail_old(dev_id=dev_id, defects_qc=defects)
        except DatabaseError as err:
            self.show_error("Не вдалось зареєструвати девайс {}!\nДеталі: {}".format(dev_id, err))
            return False
        return True

    def get_defects(self, device_id):
        row = self.get_row_by_dev_id(device_id)
        d_grade, d_return, d_custom = self.get_defects_array(row, 1)
        defects = d_grade + d_return + d_custom
        defects = [i for i in defects if i.strip()]
        return ';'.join(defects)

    def get_defects_array(self, row, col=1):
        widget = self.cellWidget(row, col)
        d_grade = widget.defects_popup.get_defects_grade()
        d_return = widget.defects_popup.get_defects_return()
        d_custom = widget.defects_popup.get_custom_defects()
        return d_grade, d_return, d_custom

    def get_grade_qc(self, device_id):
        row = self.get_row_by_dev_id(device_id)
        return self._get_grade_qc(row)

    def _get_grade_qc(self, row, col=1):
        widget_defects = self.cellWidget(row, col)
        return widget_defects.get_grade_qc()

    def _change_state_btn_register(self, state):
        for row in range(self.rowCount()):
            w = self.cellWidget(row, self.columnCount()-1)
            w.setEnabled(state)

    def _change_state_btn_cancel(self, state):
        for row in range(self.rowCount()):
            btn = self.cellWidget(row, self.columnCount()-2)
            btn.setEnabled(state)

    @contextmanager
    def change_state_btn_cancel(self):
        self._change_state_btn_cancel(False)
        yield
        self._change_state_btn_cancel(True)

    @contextmanager
    def _register_disable_last_btn(self):
        self._change_state_btn_register(False)
        yield
        self._change_state_btn_register(True)

    def register_device(self, device_id: str, device_qr: str):
        """register device database production"""
        button_register_text = self.sender().text()
        logger.debug("id: {} qr: {} Start register device".format(device_id, device_qr))
        row = self.get_row_by_dev_id(device_id)
        with self._register_disable_last_btn():
            logger.debug("id: {} qr: {} Disable btn register".format(device_id, device_qr))
            defects = self.get_defects(device_id)
            malfunction = '; '.join(self.get_malfunction(row))
            operator = self.qc_main_widget.get_operator_name()
            grade_qc = self.get_grade_qc(device_id)
            app = app_name + ' ' + app_version
            info = {'app': app}
            logger.debug("id: {} qr: {} Disarm work hub".format(device_id, device_qr))
            if not self._disarmed_work_hub():
                logger.debug("id: {} qr: {} Err disarm work hub".format(device_id, device_qr))
                return

            if button_register_text == "Зареєструвати":
                logger.debug('id: {} qr: {} start register in db'.format(device_id, device_qr))
                if not self.success_register_device(operator, device_qr, device_id, grade_qc, defects, info):
                    logger.debug("id: {} qr: {} Err register device in DB".format(device_id, device_qr))
                    return
                logger.debug("id: {} qr: {} Success register in db".format(device_id, device_qr))
            else:
                if not self._you_sure():
                    logger.debug("click btn not return")
                    return
                row = self.get_row_by_dev_id(device_id)
                widget_connect = self.cellWidget(row, 2)
                logger.debug("STATUS get state {}".format(widget_connect.get_state()))
                logger.debug("STATUS get state repair {}".format(widget_connect.get_state_repair()))
                logger.debug("STATUS count click search device {}".format(widget_connect.check_count_clicked_connect()))

                # датчик не спробували приписати
                if not widget_connect.check_count_clicked_connect():
                    #  немає візуальних дефектів
                    if not defects:
                        self.show_error(
                            "Щоб повернути девайс потрібно вибрати дефект або сробувати приписати девайс хоч один раз!"
                        )
                        return
                    else:
                        self.send_repair(device_id, defects)

                #  датчик не вдалось приписати
                elif not widget_connect.get_state() and widget_connect.get_state_repair():
                    malfunction = "Датчик не приписався до хабу (Не включається)"
                    self.send_repair(device_id, malfunction=malfunction)

                #  приписування датчика відмінили
                elif not widget_connect.get_state():
                    if not defects:
                        malfunction = "Датчик не приписався до хабу (Не включається)"
                        self.send_repair(device_id, malfunction=malfunction)
                        # defects = malfunction
                    else:
                        self.send_repair(device_id, malfunction=defects)

                #  датчик приписався, є візуальні або функціональні дефекти
                elif defects or malfunction:
                    defects_qc = ';'.join((defects, malfunction))
                    self.send_repair(device_id, malfunction=defects_qc)

                registered = self.fail_register_device(device_id, device_qr, defects, info)
                if not registered:
                    return
            logger.debug("id: {} qr: {} Cancel register device".format(device_id, device_qr))
            self._cancel_test_device_qc(self.get_device_type(), device_id)

    def _disarmed_work_hub(self):
        hub_obj = self.get_work_hub()
        if not hub_obj:
            self.show_error("На акаунті не знайдено робочого Хаба!")
            return False

        state_hub = hub_obj.get_parameters('08')
        logger.debug("State work hub {}: {}".format(hub_obj, state_hub))
        if state_hub != '00':
            logger.info("Disarmed work hub {}".format(hub_obj))
            answer = hub_obj.disarm()
            logger.debug("Answer disarmed workhub {} - {}".format(hub_obj, answer))
            if answer.message_type == MessageType.Answer.value and answer.message_key == Answer.DeliveredCommandPerform.value:
                return True
            elif answer.message_type == MessageType.Answer.value and answer.message_key == Answer.DeliveredWasAlreadyPerform.value:
                return True
            else:
                self.show_error("Не вдалось зняти хаб з охорони! {}".format(answer))
                return False
        return True

    @staticmethod
    def _you_sure(text="Ви впевненні, що цей пристрій необхідно повернути?"):
        reply = QtWidgets.QMessageBox()
        reply.setIcon(QtWidgets.QMessageBox.Question)
        reply.setText(text)
        reply.setStandardButtons(QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
        btn_yes = reply.button(QtWidgets.QMessageBox.Yes)
        btn_not = reply.button(QtWidgets.QMessageBox.No)
        btn_yes.setText('Так')
        btn_not.setText('Ні')
        reply.exec_()
        if reply.clickedButton() == btn_yes:
            return True
        else:
            return False

    def send_repair(self, device_id, malfunction):
        row = self.get_row_by_dev_id(device_id)
        if row is None:
            logger.error("Send repair hub not found in table")
            return
        dev_id_widget = self.cellWidget(row, 0)
        qr_code = dev_id_widget.get_qr_code()

        try:
            if self.database_prod.qr_exists(qr_code):
                login = self.qc_main_widget._operator_login
                self.database_prod.send_repair(QrDevTypes.Device.value, device_id, login, 'QC', malfunction)
            else:
                operator = self.qc_main_widget.get_operator_name()
                self.database_prod.send_repair_old(qr_code, self.get_device_type_name(), operator, "QC", malfunction)
        except DatabaseError as err:
            self.show_error("Не вдалось відправити девайс {} на ремонт!\nДеталі: {}".format(device_id, err))

    def get_malfunction(self, row):
        malfunction = []
        for col in range(self.columnCount()):
            widget = self.cellWidget(row, col)
            if hasattr(widget, '_malfunction_name') and not widget.get_state():
                m = widget.get_malfunction_name()
                malfunction.append(m)
        return list(set(malfunction))

    def change_state_btn_register(self, row, col, state):
        btn = self.cellWidget(row, col)
        if state:
            btn.setText("Зареєструвати")
            btn.setStyleSheet(ColorResult.success.value)
        else:
            btn.setText("Повернути")
            btn.setStyleSheet("background-color: None")

    def check_all_columns(self):
        for row in range(self.rowCount()):
            r = list()
            for col in range(self.columnCount() - 2):
                w = self.cellWidget(row, col)
                if hasattr(w, 'get_state'):
                    r.append(w.get_state())
            self.change_state_btn_register(row, self.columnCount() - 1, all(r))

    def press_space_start_register_device(self):
        col = 2
        if not self.rowCount():
            return
        if self.widget_search_device.isVisible():
            return
        for row in range(self.rowCount()):
            widget_connect = self.cellWidget(row, col)
            if widget_connect.isEnabled():
                widget_connect.push_btn_connect_device.click()
                break
