import logging
from collections import namedtuple
from functools import partial

from PyQt5 import QtWidgets

from app_setting import AppSetting
from csa_client_qt.enum_obj_csa import ObjectsCsa
from qc.componets import *
from qc.enum_alarms import Transmitter
from .device_abstract_view import DeviceAbstractView

logger = logging.getLogger(AppSetting.LOGGER_NAME)


class TransmitterDeviceView(DeviceAbstractView):
    dev_type = ObjectsCsa.Transmitter

    column = namedtuple('column', ['name', 'width'])

    table_head = [
        column("Qr", 120),
        column("Наявність\nдефектів", 200),
        column("Підключення", 120),
        column("Тампер", 180),
        column("Додатковий\nконтакт", 150),
        column("Акселерометр", 150),
        column("Живлення\nна\nдатчик", 150),
        column("Рівень\nбатареї", 120),
        column("Відмінити\nтестування", 120),
        column("Зареєструвати", 100),
    ]

    def __init__(self, client_csa, parent=None):
        super(TransmitterDeviceView, self).__init__(client_csa, parent=parent)

        self.insert_columns()
        self.row_height = 75

    def add_obj(self, qr_code_device):
        dev_id, dev_type, color = qr_code_device[:6], qr_code_device[6:8], qr_code_device[8]

        if self.check_exists_dev_id(dev_id):
            self.show_error("Цей девайс {} же є в талиці!".format(dev_id))
            return

        row = self.rowCount()
        self.insertRow(self.rowCount())
        self.setRowHeight(row, self.row_height)

        widget_id_device = DevIdWidget(qr_code_device)
        self.setCellWidget(row, 0, widget_id_device)

        widget_defects = VisualDefectTable(self.defects, parent=self.qc_main_widget)
        widget_defects.signal_update_defects.connect(self.check_all_columns)
        self.setCellWidget(row, 1, widget_defects)

        push_button_search_device = ConnectDeviceWidget()
        push_button_search_device.push_btn_connect_device.clicked.connect(
            partial(self._start_registration_device, dev_id, dev_type, color)
        )
        self.setCellWidget(row, 2, push_button_search_device)

        widget_tampered = TamperWidget()
        widget_tampered.set_malfunction_name("Тампер")
        self.setCellWidget(row, 3, widget_tampered)

        widget_contact = IncrementWidget()
        widget_contact.set_malfunction_name("Контакти")
        widget_contact.set_expected_result(2)
        self.setCellWidget(row, 4, widget_contact)

        widget_accelerometer = IncrementWidget()
        widget_accelerometer.set_malfunction_name("Акселерометр")
        widget_accelerometer.set_expected_result(1)
        self.setCellWidget(row, 5, widget_accelerometer)

        widget_power_for_device = SelectWidget("Діод світиться")
        widget_power_for_device.set_malfunction_name("Живлення на датчик")
        widget_power_for_device.signal_update_status.connect(self.check_all_columns)
        self.setCellWidget(row, 6, widget_power_for_device)

        widget_status_battery = StatusWidget()
        widget_status_battery.set_malfunction_name("Рівень батареї")
        widget_status_battery.set_status("Невідомо")
        widget_status_battery.set_expected_result('100')
        self.setCellWidget(row, 7, widget_status_battery)

        push_btn_cancel_test = QtWidgets.QPushButton("x")
        push_btn_cancel_test.clicked.connect(partial(self.cancel_test_device_qc, dev_type, dev_id))
        self.setCellWidget(row, 8, push_btn_cancel_test)

        push_btn_register_device = QtWidgets.QPushButton('Повернути')
        push_btn_register_device.clicked.connect(partial(self.register_device, dev_id, qr_code_device))
        self.setCellWidget(row, 9, push_btn_register_device)

    def _send_settings_device(self, dev_type, dev_id):
        hub_obj = self.get_work_hub()
        params = ['37', '01', '38', '01', '42', '03']
        answer = hub_obj.write_param(dev_type, dev_id, params)
        logger.debug("Answer send setting for device {} {} {}".format(dev_type, dev_id, answer))

    def _handler_alarms(self, device_id, alarm_number):
        row = self.get_row_by_dev_id(device_id[2:])
        if row is None:
            return

        if alarm_number == Transmitter.TamperOpen:
            widget_tamper = self.cellWidget(row, 3)
            widget_tamper.change_tamper_count_on()
        elif alarm_number == Transmitter.TamperClosed:
            widget_tamper = self.cellWidget(row, 3)
            widget_tamper.change_tamper_count_off()
        elif alarm_number in (Transmitter.ContactOff, Transmitter.ContactOn, Transmitter.ImpulseOff):
            widget_contact = self.cellWidget(row, 4)
            widget_contact.increment()
        elif alarm_number == Transmitter.Accelerometer:
            widget_accelerommetr = self.cellWidget(row, 5)
            widget_accelerommetr.increment()

        self.check_all_columns()

    def _handler_update(self, device_id):
        hub_obj = self.get_work_hub()
        transmitter = hub_obj.get_object(self.get_device_type(), device_id)
        battery_charge = transmitter.get_parameters('05')
        row = self.get_row_by_dev_id(device_id[2:])
        if row is None:
            return
        self._update_battery_charged(row, battery_charge)

        self.check_all_columns()

    def _update_battery_charged(self, row, value):
        widget_batt = self.cellWidget(row, 7)
        if not value:
            widget_batt.set_status("Невідомо")
            return
        battery_chage_int = int(value, 16)
        if battery_chage_int > 101:
            widget_batt.set_status("Невідомо")
            return

        widget_batt.set_status("{}".format(battery_chage_int))
