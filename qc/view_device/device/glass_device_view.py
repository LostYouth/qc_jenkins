import logging
from collections import namedtuple
from functools import partial

from PyQt5 import QtWidgets

from app_setting import AppSetting
from csa_client_qt.enum_obj_csa import ObjectsCsa
from qc.componets import DevIdWidget, VisualDefectTable, TamperWidget, IncrementWidget, StatusWidget, \
    ConnectDeviceWidget
from qc.enum_alarms import AlarmsGlassProtect
from .device_abstract_view import DeviceAbstractView

logger = logging.getLogger(AppSetting.LOGGER_NAME)
column = namedtuple('column', ['name', 'width'])


class GlassDeviceView(DeviceAbstractView):
    dev_type = ObjectsCsa.GlassProtect

    table_head = [
        column("Qr", 120),
        column("Наявність\nдефектів", 200),
        column("Підключення", 120),
        column("Тампер", 180),
        column("Додатковий\nконтакт", 100),
        column("Зафіксоване\nрозбиття", 150),
        column("Рівень\nбатареї", 120),
        column("Відмінити\nтестування", 120),
        column("Зареєструвати", 100),
    ]

    def __init__(self, client_csa, parent=None):
        super(GlassDeviceView, self).__init__(client_csa, parent=parent)
        self.row_height = 75
        self.insert_columns()

    def add_obj(self, qr_code_device):
        dev_id, dev_type, color = qr_code_device[:6], qr_code_device[6:8], qr_code_device[8]
        if self.check_exists_dev_id(dev_id):
            self.show_error("Цей девайс {} вже є в таблиці!".format(dev_id))
            return

        row = self.rowCount()
        self.insertRow(row)
        self.setRowHeight(row, self.row_height)

        widget_id_device = DevIdWidget(qr_code_device)
        self.setCellWidget(row, 0, widget_id_device)

        widget_defects = VisualDefectTable(self.defects, parent=self.qc_main_widget)
        widget_defects.signal_update_defects.connect(self.check_all_columns)
        self.setCellWidget(row, 1, widget_defects)

        push_btn_search_device = ConnectDeviceWidget()
        push_btn_search_device.push_btn_connect_device.clicked.connect(
            partial(self._start_registration_device, dev_id, dev_type, color)
        )
        self.setCellWidget(row, 2, push_btn_search_device)

        widget_tamper = TamperWidget()
        widget_tamper.set_malfunction_name("Тампер")
        self.setCellWidget(row, 3, widget_tamper)

        widget_contact = IncrementWidget()
        widget_contact.set_malfunction_name("Додатковий контакт")
        widget_contact.set_expected_result(2)
        self.setCellWidget(row, 4, widget_contact)

        widget_breakdown = IncrementWidget()
        widget_breakdown.set_malfunction_name("Розбиття")
        widget_breakdown.set_expected_result(1)
        self.setCellWidget(row, 5, widget_breakdown)

        widget_battery = StatusWidget()
        widget_battery.set_malfunction_name('Рівень батареї')
        widget_battery.set_status('Невідомо')
        widget_battery.set_expected_result('100')
        self.setCellWidget(row, 6, widget_battery)

        push_btn_cancel_test = QtWidgets.QPushButton("x")
        push_btn_cancel_test.clicked.connect(partial(self.cancel_test_device_qc, dev_type, dev_id))
        self.setCellWidget(row, 7, push_btn_cancel_test)

        push_btn_register_device = QtWidgets.QPushButton("Повернути")
        push_btn_register_device.clicked.connect(partial(self.register_device, dev_id, qr_code_device))
        self.setCellWidget(row, 8, push_btn_register_device)

    def _send_settings_device(self, dev_type, dev_id):
        hub_obj = self.get_work_hub()
        parameters = ['32', '01', '33', '01', '34', '01']
        answer = hub_obj.write_param(dev_type, dev_id, parameters)
        logger.debug("Answer send setting {}".format(answer))

    def _handler_update(self, device_id):
        hub_obj = self.get_work_hub()
        glass_obj = hub_obj.get_object(self.get_device_type(), device_id)
        row = self.get_row_by_dev_id(device_id[2:])
        if row is None:
            return
        battery_charge = glass_obj.get_parameters('05')
        self._update_battery_charge(row, battery_charge)

        self.check_all_columns()

    def _update_battery_charge(self, row, battery_charge):
        widget = self.cellWidget(row, 6)
        if not battery_charge:
            widget.set_status("Невідомо")
            return

        batt_charge_int = int(battery_charge, 16)
        if batt_charge_int > 101:
            widget.set_status("Невідомо")
            return

        widget.set_status("{}".format(batt_charge_int))

    def _handler_alarms(self, device_id, alarm_number):
        row = self.get_row_by_dev_id(device_id[2:])
        if row is None:
            return

        if alarm_number == AlarmsGlassProtect.BreakDown:
            widget_breakdown = self.cellWidget(row, 5)
            widget_breakdown.increment()
        elif alarm_number in (AlarmsGlassProtect.ContactClosed, AlarmsGlassProtect.ContactOpen):
            widget_contact = self.cellWidget(row, 4)
            widget_contact.increment()
        elif alarm_number == AlarmsGlassProtect.TamperOpen:
            widget_tamper = self.cellWidget(row, 3)
            widget_tamper.change_tamper_count_on()
        elif alarm_number == AlarmsGlassProtect.TamperClosed:
            widget_tamper = self.cellWidget(row, 3)
            widget_tamper.change_tamper_count_off()
        self.check_all_columns()
