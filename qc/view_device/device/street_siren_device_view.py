import logging
from collections import namedtuple
from functools import partial

from PyQt5 import QtWidgets

from app_setting import AppSetting
from csa_client_qt.enum_obj_csa import ObjectsCsa
from csa_client_qt.enum_params import MessageType, Answer
from qc.componets import *
from qc.componets.accelerometer_street_siren import AccelerometerStreetSiren
from qc.componets.widget_change_lvl_volume import WidgetChangeLvlVolume, EnumVolumeLevel
from qc.enum_alarms import StreetSiren
from .device_abstract_view import DeviceAbstractView

logger = logging.getLogger(AppSetting.LOGGER_NAME)


class StreetSirenDeviceView(DeviceAbstractView):
    dev_type = ObjectsCsa.StreetSiren
    column = namedtuple('column', ['name', 'width'])
    table_head = [
        column("Qr", 120),
        column("Наявність\nдефектів", 200),
        column("Підключення", 120),
        column("Тампер", 120),
        column("Зовнішнє\nживлення", 120),
        column("Спрацювання\nакселерометра", 120),
        column("Блимання\nпри\nпостановці\nпід\nохорону", 120),
        column("Блимання\nпри\nзнятті\nз-під\nохорони", 120),
        column("Змінити\nгучність\nсирени", 250),
        column('Тест\nбузера', 130),
        column("Рівень\nбатареї", 120),
        column("Відмінити\nтестування", 120),
        column("Зареєструвати", 100),
    ]

    def __init__(self, client_csa, parent=None):
        super(StreetSirenDeviceView, self).__init__(client_csa, parent=parent)
        self.row_height = 75
        self.insert_columns()

    def add_obj(self, qr_device):
        dev_id, dev_type, color = qr_device[:6], qr_device[6:8], qr_device[8]
        if self.check_exists_dev_id(dev_id):
            self.show_error("Цей девайс {} же є в талиці!".format(dev_id))
            return

        row = self.rowCount()
        self.insertRow(row)
        self.setRowHeight(row, self.row_height)
        widget_id = DevIdWidget(qr_device)
        self.setCellWidget(row, 0, widget_id)

        widget_defects = VisualDefectTable(self.defects, parent=self.qc_main_widget)
        widget_defects.signal_update_defects.connect(self.check_all_columns)
        self.setCellWidget(row, 1, widget_defects)

        push_btn_search_device = ConnectDeviceWidget()
        push_btn_search_device.push_btn_connect_device.clicked.connect(
            partial(self._start_registration_device, dev_id, dev_type, color))
        self.setCellWidget(row, 2, push_btn_search_device)

        widget_tamper = TamperWidget()
        widget_tamper.set_malfunction_name("Тампер")
        widget_tamper.label_on.setText('ВКЛ: 0/1')
        widget_tamper.label_off.setText('ВИКЛ: 0/1')
        self.setCellWidget(row, 3, widget_tamper)

        out_power = StatusWidget()
        out_power.set_malfunction_name("Зовнішнє живлення")
        out_power.set_expected_result('01')
        self.setCellWidget(row, 4, out_power)

        accelerometer_widget = AccelerometerStreetSiren()
        accelerometer_widget.push_btn_state_accelerometer.clicked.connect(partial(self.change_state_accelerometer, dev_id))
        accelerometer_widget.set_malfunction_name("Акселерометр")
        accelerometer_widget.set_expected_result(1)
        self.setCellWidget(row, 5, accelerometer_widget)

        blink_arm = HomeSirenChangeStateHub("Ok", "На охорону")
        blink_arm.set_malfunction_name("Світлодіод зміни стану")
        blink_arm.check_box.stateChanged.connect(self.check_all_columns)
        blink_arm.push_btn_change_state_hub.clicked.connect(self._arm_work_hub)
        self.setCellWidget(row, 6, blink_arm)

        blink_disarm = HomeSirenChangeStateHub('Ok', 'З охорони')
        blink_disarm.set_malfunction_name("Світлодіод зміни стану")
        blink_disarm.check_box.stateChanged.connect(self.check_all_columns)
        blink_disarm.push_btn_change_state_hub.clicked.connect(self._disarm_work_hub)
        self.setCellWidget(row, 7, blink_disarm)

        widget_change_volume = WidgetChangeLvlVolume(dev_id)
        widget_change_volume.signal_change_value_volume.connect(self._change_volume_device)
        self.setCellWidget(row, 8, widget_change_volume)

        test_buzzera = HomeSirenChangeStateHub('Ok', "Запустити\nтест\nбузера")
        test_buzzera.set_malfunction_name("Бузер")
        test_buzzera.check_box.stateChanged.connect(self.check_all_columns)
        test_buzzera.push_btn_change_state_hub.clicked.connect(partial(self._start_test_buzzer, dev_id))
        self.setCellWidget(row, 9, test_buzzera)

        battery_status = StatusWidget()
        battery_status.set_malfunction_name("Рівень батареї")
        battery_status.set_status("Невідомо")
        battery_status.set_expected_result('100')
        self.setCellWidget(row, 10, battery_status)

        push_btn_cancel_test = QtWidgets.QPushButton("x")
        push_btn_cancel_test.clicked.connect(partial(self.cancel_test_device_qc, dev_type, dev_id))
        self.setCellWidget(row, 11, push_btn_cancel_test)

        push_btn_register_device = QtWidgets.QPushButton('Повернути')
        push_btn_register_device.clicked.connect(partial(self.register_device, dev_id, qr_device))
        self.setCellWidget(row, 12, push_btn_register_device)

    def change_state_accelerometer(self, device_id):
        hub_obj = self.get_work_hub()
        ss = hub_obj.get_object(self.get_device_type(), "{:0>8}".format(device_id))
        if not ss:
            logger.error("device SS {} {} not found in side hub ".format(self.get_device_type(), device_id))
            return
        state_acc = ss.get_parameters('34')
        if state_acc == '01':
            p = '00'
        elif state_acc == '00':
            p = '01'
        else:
            return
        answer = hub_obj.write_param(self.get_device_type(), "{:0>8}".format(device_id), ['34', p])
        logger.debug("Answer change state accelerometer SS {}".format(answer))

    def _change_volume_device(self, dev_id: str, val: int):
        hub_obj = self.get_work_hub()
        a = hub_obj.write_param(
            self.get_device_type(),
            "{:0>8}".format(dev_id),
            ['35', '{:02x}'.format(val)]
        )
        logger.debug("Answer change volume {}".format(a))
        if a is None:
            return
        if a.message_type == MessageType.Answer.value and a.message_key == Answer.DeliveredCommandPerform.value:
            return
        else:
            hub_obj = self.get_work_hub()
            home_siren = hub_obj.get_object(self.get_device_type(), "{:0>8}".format(dev_id))
            row = self.get_row_by_dev_id(dev_id)
            if row is None or home_siren is None:
                return
            self._update_volume_dev(row, home_siren.get_parameters('35'))
            self.show_error("Помилка, не вдалось змінити гучність для сирени {}".format(dev_id))

    def _start_test_buzzer(self, dev_id):
        hub_obj = self.get_work_hub()
        if hub_obj.get_parameters('08') != '00':
            self.show_error("Неможливо запустити тест бузера поки Хаб на охороні!")
            return
        a = hub_obj.send_device_command(self.get_device_type(), dev_id, '08')
        logger.debug("Answer send test buzzer siren({}) {}".format(dev_id, a))

    def _arm_work_hub(self):
        hub_obj = self.get_work_hub()
        if hub_obj.get_parameters("08") in ("01", '02'):
            self.show_error("Хаб вже на охороні")
            return
        answer = hub_obj.arm()
        logger.debug("Answer Arm hub {}".format(answer))

    def _disarm_work_hub(self):
        hub_obj = self.get_work_hub()
        if hub_obj.get_parameters("08") == '00':
            self.show_error("Хаб вже знятий з охорони")
            return
        answer = hub_obj.disarm()
        logger.debug("Answer Disarm hub {}".format(answer))

    def _send_settings_device(self, dev_type, dev_id):
        hub_obj = self.get_work_hub()
        params = ['32', '01',
                  '33', '01',
                  '34', '01']
        a = hub_obj.write_param(dev_type, dev_id, params)
        logger.debug("Answer set settings {}".format(a))

    def _handler_update(self, device_id):
        hub_obj = self.get_work_hub()
        street_siren = hub_obj.get_object(self.get_device_type(), device_id)
        row = self.get_row_by_dev_id(device_id[2:])
        # self._check_state_hub(row, hub_obj.get_parameters('08'))
        if row is None:
            return
        battery_charged = street_siren.get_parameters('05')
        out_power = street_siren.get_parameters('36')
        state_accelerometer = street_siren.get_parameters('34')

        self._update_power_out(row, out_power)
        self._update_battery_charged(row, battery_charged)
        self._update_state_accelerometer(row, state_accelerometer)
        self.check_all_columns()

    def _update_volume_dev(self, row: int, volume: str):
        if not volume or volume == '80':
            return
        widget_volume_device = self.cellWidget(row, 8)
        try:
            current_val = int(volume, 16)
        except (TypeError, ValueError):
            return
        current_val = EnumVolumeLevel(current_val)
        widget_volume_device.current_value = current_val

    def _update_state_accelerometer(self, row, state_accelerometer):
        if not state_accelerometer:
            return
        widget_accel = self.cellWidget(row, 5)
        widget_accel.change_state_accelerometer(state_accelerometer)

    def _update_power_out(self, row, val_power_out):
        widget = self.cellWidget(row, 4)
        if not val_power_out or val_power_out == '80':
            widget.set_status("Невідомо")
            return
        if not widget.get_state():
            widget.set_status(val_power_out)

    def _update_battery_charged(self, row, batt_charge):
        widget = self.cellWidget(row, 10)
        if not batt_charge:
            widget.set_status("Невідомо")
            return
        battery_int = int(batt_charge, 16)
        if battery_int > 101:
            widget.set_status("Невідомо")
            return

        widget.set_status("{}".format(battery_int))

    def _handler_alarms(self, dev_id, alarm_number):
        row = self.get_row_by_dev_id(dev_id[2:])
        if row is None:
            return

        if alarm_number == StreetSiren.TamperOpen:
            widget_tamped = self.cellWidget(row, 3)
            widget_tamped.change_tamper_count_on()
        elif alarm_number == StreetSiren.TamperClosed:
            widget_tamped = self.cellWidget(row, 3)
            widget_tamped.change_tamper_count_off()
        elif alarm_number == StreetSiren.Accelerometer:
            widget_accelerometer = self.cellWidget(row, 5)
            widget_accelerometer.increment()
        self.check_all_columns()
