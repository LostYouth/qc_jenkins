import logging
from collections import namedtuple
from functools import partial

from PyQt5 import QtWidgets

from app_setting import AppSetting
from csa_client_qt.enum_obj_csa import ObjectsCsa
from qc.componets import *
from qc.enum_alarms import Button
from .device_abstract_view import DeviceAbstractView

logger = logging.getLogger(AppSetting.LOGGER_NAME)


class PanicButtonDeviceView(DeviceAbstractView):
    dev_type = ObjectsCsa.Button
    column = namedtuple('column', ['name', 'width'])

    table_head = [
        column("Qr", 120),
        column("Наявність\nдефектів", 200),
        column("Підключення", 120),
        column("Спрацювання\nтривожної\nкнопки", 150),
        column("Рівень\nбатареї", 120),
        column("Підсвітка", 160),
        column("Відмінити\nтестування", 120),
        column("Зареєструвати", 100),
    ]

    def __init__(self, client_csa, parent=None):
        super(PanicButtonDeviceView, self).__init__(client_csa, parent=parent)
        self.insert_columns()
        self.row_height = 75

    def add_obj(self, qr_device):
        dev_id, dev_type, color = qr_device[:6], qr_device[6:8], qr_device[8]
        if self.check_exists_dev_id(dev_id):
            self.show_error("Цей девайс {} же є в талиці!".format(dev_id))
            return

        row = self.rowCount()
        self.insertRow(self.rowCount())
        self.setRowHeight(row, self.row_height)

        widget_id_device = DevIdWidget(qr_device)
        self.setCellWidget(row, 0, widget_id_device)

        widget_defects = VisualDefectTable(self.defects, parent=self.qc_main_widget)
        widget_defects.signal_update_defects.connect(self.check_all_columns)
        self.setCellWidget(row, 1, widget_defects)

        push_button_search_device = ConnectDeviceWidget()
        push_button_search_device.push_btn_connect_device.clicked.connect(
            partial(self._start_registration_device, dev_id, dev_type, color))
        self.setCellWidget(row, 2, push_button_search_device)

        panic_btn_widget = IncrementWidget()
        panic_btn_widget.set_malfunction_name("Кнопка тривоги")
        panic_btn_widget.set_expected_result(4)
        self.setCellWidget(row, 3, panic_btn_widget)

        battery_status = StatusWidget()
        battery_status.set_malfunction_name("Рівень батареї")
        battery_status.set_status("Невідомо")
        battery_status.set_expected_result('100')
        self.setCellWidget(row, 4, battery_status)

        leds_status = LedsStatusWidget("Підсвітка")
        leds_status.set_malfunction_name("Підсвітка")
        leds_status.signal_turn_leds_on.connect(partial(self.set_leds_on, dev_id))
        leds_status.signal_update_status.connect(self.check_all_columns)
        leds_status.setEnabled(False)
        self.setCellWidget(row, 5, leds_status)

        push_btn_cancel_test = QtWidgets.QPushButton("x")
        push_btn_cancel_test.clicked.connect(partial(self.cancel_test_device_qc, dev_type, dev_id))
        self.setCellWidget(row, 6, push_btn_cancel_test)

        push_btn_register_device = QtWidgets.QPushButton('Повернути')
        push_btn_register_device.clicked.connect(partial(self.register_device, dev_id, qr_device))
        self.setCellWidget(row, 7, push_btn_register_device)

    def _send_settings_device(self, dev_type, dev_id):
        self.set_leds_state(dev_type, dev_id, state='00')

    def register_device(self, device_id: str, device_qr: str):
        hub_obj = self.get_work_hub()
        if hub_obj.get_parameters('08') != '00':
            self.show_error("Зніміть Хаб з охорони!")
            return
        super(PanicButtonDeviceView, self).register_device(device_id, device_qr)

    def set_leds_on(self, device_id):
        self.set_leds_state(self.get_device_type(), device_id, state='03')
        self.check_all_columns()

    def set_leds_state(self, dev_type, dev_id, state):
        hub_obj = self.get_work_hub()
        parameters = ['33', state]
        answer = hub_obj.write_param(dev_type, dev_id, parameters)
        logger.debug("Answer send setting {}".format(answer))

    def _handler_update(self, device_id):
        hub_obj = self.get_work_hub()
        space = hub_obj.get_object(self.get_device_type(), device_id)
        row = self.get_row_by_dev_id(device_id[2:])
        if row is None:
            return
        battery_charge = space.get_parameters('05')
        self._update_battery_charge(row, battery_charge)

        self.check_all_columns()

    def _update_battery_charge(self, row, battery_val):
        widget_batt = self.cellWidget(row, 4)
        if not battery_val:
            widget_batt.set_status("Невідомо")
            return
        battery_val = int(battery_val, 16)
        if battery_val > 101:
            widget_batt.set_status("Невідомо")
        else:
            widget_batt.set_status(str(battery_val))

    def _handler_alarms(self, device_id, alarm_number):
        row = self.get_row_by_dev_id(device_id[2:])
        if row is None:
            return

        elif alarm_number == Button.PanicBtn:
            widget_panic = self.cellWidget(row, 3)
            widget_panic.increment()
            if widget_panic.get_current_val() == 2:
                self.cellWidget(row, 5).setEnabled(True)

        self.check_all_columns()
