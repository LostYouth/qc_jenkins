import logging
from collections import namedtuple
from functools import partial

from PyQt5 import QtWidgets

from app_setting import AppSetting
from csa_client_qt.enum_obj_csa import ObjectsCsa
from qc.componets import *
from qc.enum_alarms import MotionProtect
from .device_abstract_view import DeviceAbstractView

logger = logging.getLogger(AppSetting.LOGGER_NAME)


class MotionDeviceView(DeviceAbstractView):
    dev_type = ObjectsCsa.MotionProtect

    column = namedtuple('column', ['name', 'width'])

    table_head = [
        column("Qr", 120),
        column("Наявність\nдефектів", 260),
        column("Підключення", 120),
        column("Тампер", 180),
        column("Зафіксовано\nрух", 150),
        column("Рівень\nбатареї", 120),
        column("Відмінити\nтестування", 120),
        column("Зареєструвати", 100),
    ]

    def __init__(self, client_csa, parent=None):
        super(MotionDeviceView, self).__init__(client_csa, parent=parent)
        self.row_height = 75
        self.insert_columns()

    def add_obj(self, qr_code_device):
        dev_id, dev_type, color = qr_code_device[:6], qr_code_device[6:8], qr_code_device[8]
        if self.check_exists_dev_id(dev_id):
            self.show_error("Цей девайс {} вже є в таблиці!".format(dev_id))
            return

        row = self.rowCount()
        self.insertRow(row)
        self.setRowHeight(row, self.row_height)

        widget_id = DevIdWidget(qr_code_device)
        self.setCellWidget(row, 0, widget_id)

        widget_defects = VisualDefectTable(self.defects, parent=self.qc_main_widget)
        widget_defects.signal_update_defects.connect(self.check_all_columns)
        self.setCellWidget(row, 1, widget_defects)

        widget_search_device = ConnectDeviceWidget()
        widget_search_device.push_btn_connect_device.clicked.connect(
            partial(self._start_registration_device, dev_id, dev_type, color))
        self.setCellWidget(row, 2, widget_search_device)

        widget_tampered = TamperWidget()
        widget_tampered.set_malfunction_name("Тампер")
        self.setCellWidget(row, 3, widget_tampered)

        widget_move = IncrementWidget()
        widget_move.set_malfunction_name("Піросенсор")
        widget_move.set_expected_result(2)
        self.setCellWidget(row, 4, widget_move)

        widget_battery_status = StatusWidget()
        widget_battery_status.set_malfunction_name("Рівень батареї")
        widget_battery_status.set_status("Невідомо")
        widget_battery_status.set_expected_result('100')
        self.setCellWidget(row, 5, widget_battery_status)

        cancel_test_widget = QtWidgets.QPushButton("x")
        cancel_test_widget.clicked.connect(partial(self.cancel_test_device_qc, dev_type, dev_id))
        self.setCellWidget(row, 6, cancel_test_widget)

        btn_registration_device = QtWidgets.QPushButton('Повернути')
        btn_registration_device.clicked.connect(partial(self.register_device, dev_id, qr_code_device))
        self.setCellWidget(row, 7, btn_registration_device)

    def _send_settings_device(self, dev_type, dev_id):
        hub_obj = self.get_work_hub()
        parameters = ['33', '01']
        answer = hub_obj.write_param(dev_type, dev_id, parameters)
        logger.debug("Answer set, param {}".format(answer))

    def _handler_update(self, dev_id):
        hub_obj = self.get_work_hub()
        motion_obj = hub_obj.get_object(self.get_device_type(), dev_id)
        battery_charge = motion_obj.get_parameters('05')
        row = self.get_row_by_dev_id(dev_id[2:])
        if row is None:
            return
        self._update_battery_charge(row, battery_charge)

        self.check_all_columns()

    def _update_battery_charge(self, row, batt_charge):
        widget_batt = self.cellWidget(row, 5)
        if not batt_charge:
            widget_batt.set_status("Невідомо")
            return

        int_batt_charge = int(batt_charge, 16)
        if int_batt_charge > 101:
            return
        widget_batt.set_status("{}".format(int_batt_charge))

    def _handler_alarms(self, dev_id, alarm_number):
        row = self.get_row_by_dev_id(dev_id[2:])
        if row is None:
            return

        if alarm_number == MotionProtect.Movement:
            widget_mode = self.cellWidget(row, 4)
            widget_mode.increment()
        elif alarm_number == MotionProtect.TamperClosed:
            widget_tamper = self.cellWidget(row, 3)
            widget_tamper.change_tamper_count_off()
        elif alarm_number == MotionProtect.TamperOpen:
            widget_tamper = self.cellWidget(row, 3)
            widget_tamper.change_tamper_count_on()

        self.check_all_columns()
