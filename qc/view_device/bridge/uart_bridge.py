import logging
import re
from collections import namedtuple
from functools import partial

from PyQt5 import QtWidgets

from qc.componets.search_device_start import SearchDevicePopup
from qc.enum_color import ColorResult
from qc.view_device.bridge.components.bridge_id import BridgeId
from qc.view_device.bridge.components.connect_uart_bridge import ConnectUartBridge
from qc.view_device.bridge.components.search_device_bridge import SearchDeviceBridge
from .abstract_bridge import Bridge, AbstractBridge
from qc.componets import *


class uartBridge(AbstractBridge):
    dev_type = Bridge.uartBridge

    column = namedtuple('column', ['name', 'width'])

    delay_connect_bridge = 25 * 1000

    table_head = [
        column("Qr",                    120),
        column("Наявність\nдефектів",   200),
        column("Підключення",           200),
        column("Підключення\nдатчика",  200),
        column("Тривоги\nвід\nдатчика", 200),
        column("Відмінити\nтестування", 150),
        column("Зареєструвати",         150),
    ]

    def __init__(self, parent):
        super(uartBridge, self).__init__(parent=parent)

        self.widget_search_device2bridge = SearchDevicePopup(parent=self.qc_main_widget)
        self.widget_search_device2bridge._counts_timeout = 100
        self.widget_search_device2bridge.label_des.setText("Відскануйте датчик після чого включіть його!")
        self.widget_search_device2bridge.push_btn_cancel_register_device.clicked.connect(self._cancel_search_device)

        self._qr_code_device_search = "123123321"

    def set_device_search(self, qr_code_device):
        self._qr_code_device_search = qr_code_device

    def add_obj(self, qr_device):
        if self.check_exists(qr_device):
            self.show_error("Цей пристрій {} вже є в таблиці!".format(qr_device))
            return
        row = self.rowCount()
        if row == 1:
            self.show_error("Можливо тестити декілька девайсів одночасно лише один {}!".format(self.get_device_type_name()))
            return

        self.insertRow(row)
        self.setRowHeight(row, self.row_height)

        widget_id_bridge = BridgeId(qr_device)
        widget_id_bridge.set_malfunction_name("Не відповідність Qr коду")
        self.setCellWidget(row, 0, widget_id_bridge)

        widget_defects = VisualDefectTable(self.defects, parent=self.qc_main_widget)
        widget_defects.signal_update_defects.connect(self.check_all_column)
        self.setCellWidget(row, 1, widget_defects)

        widget_connect_uart_bridge = ConnectUartBridge()
        widget_connect_uart_bridge.set_malfunction_name("Не вдалось підконектитсь до {}".format(self.get_device_type_name()))
        widget_connect_uart_bridge.set_ports_name(self._get_uart_ports_name())
        widget_connect_uart_bridge.push_btn_connect.clicked.connect(self.connect_uart_bridget)
        self.setCellWidget(row, 2, widget_connect_uart_bridge)

        widget_connect_device = SearchDeviceBridge("Почати")
        widget_connect_device.set_malfunction_name("Датчик не приписався")
        widget_connect_device.clicked.connect(self._start_search_device)
        self.setCellWidget(row, 3, widget_connect_device)

        widget_alarms_device_count = IncrementWidget()
        widget_alarms_device_count.set_malfunction_name("Тривоги від датчика")
        widget_alarms_device_count.set_expected_result(2)
        self.setCellWidget(row, 4, widget_alarms_device_count)

        push_btn_cancel_test_bridge = QtWidgets.QPushButton("x")
        push_btn_cancel_test_bridge.clicked.connect(partial(self.cancel_test_bridge, qr_device))
        self.setCellWidget(row, 5, push_btn_cancel_test_bridge)

        push_btn_register_bridge = QtWidgets.QPushButton("Повернути")
        push_btn_register_bridge.clicked.connect(partial(self.register_database_bridge, qr_device))
        self.setCellWidget(row, 6, push_btn_register_bridge)

    def connect_uart_bridget(self, bridge_id):
        if self._check_defects(bridge_id) is False:
            return

        widget_connect = self.cellWidget(0, 2)
        port_name = widget_connect.get_port_name()
        if widget_connect.push_btn_connect.text() == "Підключитися":
            self.set_port_name(port_name)
            if self.open_port():
                widget_connect.push_btn_connect.setStyleSheet(ColorResult.success.value)
                widget_connect.push_btn_connect.setText("Відключитися")
                self.get_version()
        else:
            widget_connect.push_btn_connect.setStyleSheet(ColorResult.fail.value)
            widget_connect.push_btn_connect.setText("Підключитися")
            self.close_port()

    def _start_search_device(self):
        if not self.test_bridge.isOpen():
            self.show_error("Щоб включити пошук девайсу потрібно підключитись до Бріджа!")
        self.start_add_device()

    def _timeout_search_device(self):
        self.cancel_search_device()

    def _cancel_search_device(self):
        self.cancel_search_device()
        self.widget_search_device2bridge.close_timeout()

    def _handler_message(self, message: str):
        event_search_device = re.search("EVENT;SYSTEM;SCH=(\d+);", message)

        _event_reg_device = re.search("EVENT;([0-9A-F]{6});TYP=([0-9A-F]{1,4});NEW=([0-9A-F]{1});STR=([0-9A-F]{1,2});SLT=([0-9A-F]{2,3});VER=.+;", message)

        oc_bridge_version = re.search("RSTATE;([0-9A-f]{6});VER=uartBridge.+", message)

        event_alarm_device = re.search("ALARM;(\d+);([A-Z0-9]{6});(\d+);PRT=(\d+);", message)

        confirm_reg_device = re.search("EVENT;([0-9A-F]{6});TYP=([0-9A-F]{1,4});NEW=([0-9A-F]{1});.+;WFA=Y/N;", message)

        if event_search_device:
            state_search_device = event_search_device.group(1)
            self._handler_search_device(state_search_device)
        elif confirm_reg_device:
            dev_id = confirm_reg_device.group(1)
            if dev_id.lower() == self._qr_code_device_search[:6].lower():
                self.test_bridge.write(b'Y\r\n')
            else:
                self.test_bridge.write(b'N\r\n')
        elif _event_reg_device:
            dev_id = _event_reg_device.group(1)
            self._handler_device_regiter(dev_id)
            self.start_work()
        elif event_alarm_device:
            device_id = event_alarm_device.group(2)
            self._handler_alarms_device(device_id)

        elif oc_bridge_version:
            bridge_id = oc_bridge_version.group(1)
            self._handler_bridge_id(bridge_id)

        self.check_all_column()

    def _handler_bridge_id(self, bridge_id):
        widget_id = self.cellWidget(0, 0)
        widget_id.set_bridge_id(bridge_id.lower())

    def _handler_alarms_device(self, dev_id):
        if self._qr_code_device_search[:6].lower() == dev_id.lower():
            widget_alarms = self.cellWidget(0, 4)
            widget_alarms.increment()

    def _handler_device_regiter(self, dev_id):
        if self._qr_code_device_search[:6].lower() == dev_id.lower():
            widget_connect_device = self.cellWidget(0, 3)
            widget_connect_device.set_state(True)
            self.widget_search_device2bridge.close_timeout()

    def _handler_search_device(self, state_search_device):
        if state_search_device == '1':
            self.widget_search_device2bridge.show()
        elif state_search_device == '0':
            widget_connect = self.cellWidget(0, 3)
            widget_connect.setStyleSheet(ColorResult.fail.value)
            self.widget_search_device2bridge.close_timeout()

    def press_space_start_register_device(self):
        pass