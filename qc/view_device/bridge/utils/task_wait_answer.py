from PyQt5 import QtCore


class TaskWaitAnswer:
    def __init__(self, timeout=2):
        self._result = None
        self.timer_timeout = QtCore.QTimer()
        self.timer_timeout.timeout.connect(self._cancel_timeout)
        self.timer_timeout.singleShot(timeout * 600, self._cancel_timeout)

    def done(self):
        if self._result is None:
            return False
        return self._result

    def get_result(self):
        return self._result

    def set_result(self, result):
        self._result = result

    def _cancel_timeout(self):
        self._result = "Timeout"
