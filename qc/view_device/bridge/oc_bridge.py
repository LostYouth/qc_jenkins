import logging
import re

from functools import partial
from PyQt5 import QtWidgets

from app_setting import AppSetting
from qc.componets.search_device_start import SearchDevicePopup
from qc.enum_color import ColorResult
from qc.view_device.bridge.components.bridge_id import BridgeId
from qc.view_device.bridge.components.search_device_bridge import SearchDeviceBridge
from .abstract_bridge import Bridge, AbstractBridge
from qc.componets import *

logger = logging.getLogger(AppSetting.LOGGER_NAME)


class OcBridge(AbstractBridge):
    dev_type = Bridge.ocBridge

    delay_connect_bridge = 25 * 1000

    def __init__(self, parent):
        super(OcBridge, self).__init__(parent=parent)

        self.widget_search_device2bridge = SearchDevicePopup(parent=self.qc_main_widget)
        self.widget_search_device2bridge.label_des.setText("Відскануйте датчик після чого включіть його!")
        self.widget_search_device2bridge.push_btn_cancel_register_device.clicked.connect(self._cancel_search_device)

        self._device_search = "123123321"

    def set_device_search(self, qr_code_device):
        logger.debug("Set device qr search {}".format(qr_code_device))
        self._device_search = qr_code_device

    def add_obj(self, qr_device: str):
        if self.check_exists(qr_device):
            self.show_error("Цей пристрій {} вже є в таблиці!".format(qr_device))
            return
        row = self.rowCount()
        if row == 1:
            self.show_error("Можливо тестити декілька девайсів одночасно лише один {}!".format(self.get_device_type_name()))
            return

        self.insertRow(row)
        self.setRowHeight(row, self.row_height)

        widget_id_bridge = BridgeId(qr_device)
        widget_id_bridge.set_malfunction_name("Не відповідність Qr коду")
        self.setCellWidget(row, 0, widget_id_bridge)

        widget_defects = VisualDefectTable(self.defects, parent=self.qc_main_widget)
        widget_defects.signal_update_defects.connect(self.check_all_column)
        self.setCellWidget(row, 1, widget_defects)

        widget_connect_bridge = ConnectDeviceWidget()
        widget_connect_bridge.push_btn_connect_device.clicked.connect(partial(self._search_bridge, qr_device))
        self.setCellWidget(row, 2, widget_connect_bridge)

        widget_status_com_port = StatusWidget()
        widget_status_com_port.set_malfunction_name("")
        widget_status_com_port.get_state = lambda: True
        self.setCellWidget(row, 3, widget_status_com_port)

        widget_active_contact = TamperWidget()
        widget_active_contact.set_malfunction_name("Зони спрацювання")
        self.setCellWidget(row, 4, widget_active_contact)

        widget_connect_device = SearchDeviceBridge("Почати")
        widget_connect_device.set_malfunction_name("Не приписався датчик")
        widget_connect_device.clicked.connect(self._start_search_device)
        self.setCellWidget(row, 5, widget_connect_device)

        widget_alarm_device = IncrementWidget()
        widget_alarm_device.set_malfunction_name("Спрацювання датчика (Антена)")
        widget_alarm_device.set_expected_result(2)
        self.setCellWidget(row, 6, widget_alarm_device)

        widget_saved_settings = SelectWidget("Збереження\nналаштувань")
        widget_saved_settings.set_malfunction_name("Відсутння батарейка (Світлодіод індикації збереження налаштувань)")
        widget_saved_settings.check_box_select.stateChanged.connect(self.check_all_column)
        self.setCellWidget(row, 7, widget_saved_settings)

        widget_cancel_test = QtWidgets.QPushButton("x")
        widget_cancel_test.clicked.connect(partial(self.cancel_test_bridge, qr_device))
        self.setCellWidget(row, 8, widget_cancel_test)

        widget_register = QtWidgets.QPushButton("Повернути")
        widget_register.clicked.connect(partial(self.register_database_bridge, qr_device))
        self.setCellWidget(row, 9, widget_register)

    def close_popup_search(self):
        logger.debug("Close popup search device for ocBridge")
        self.widget_search_device2bridge.close_timeout()

    def _start_search_device(self):
        if not self.test_bridge.isOpen():
            self.show_error("Щоб включити пошук девайсу потрібно підключитись до Бріджа!")
            return
        btn_search_device = self.sender()
        btn_search_device.setEnabled(False)
        self.start_add_device()

    def _cancel_search_device(self):
        logger.debug("Cancel search device")
        self.cancel_search_device()
        self.widget_search_device2bridge.close_timeout()

        btn_connect = self.cellWidget(0, 5)
        btn_connect.set_state(False)

    def _timeout_search_device(self):

        self._cancel_search_device()

    def _handler_message(self, message: str):
        oc_bridge_version = re.search(r"RSTATE;([0-9A-f]{6});VER=ocBridge.+", message)

        confirm_reg_device = re.search(r"EVENT;([0-9A-F]{6});TYP=([0-9A-F]{1,4});NEW=([0-9A-F]{1});.+;WFA=Y/N;", message)

        _event_reg_device = re.search(r"EVENT;([0-9A-F]{6});TYP=([0-9A-F]{1,4});NEW=([0-9A-F]{1});STR=([0-9A-F]{1,2});SLT=([0-9A-F]{2,3});VER=.+;", message)

        event_search_device = re.search(r"EVENT;SYSTEM;SCH=(\d+);", message)

        change_state_power = re.search(r"RSTATE;([A-F0-9]+);PRT=(\d{1});", message)

        event_alarm_device = re.search(r"ALARM;(\d+);([A-Z0-9]{6});(\d+);PRT=(\d+);", message)

        if change_state_power:
            bridge_qr = change_state_power.group(1).lower()
            state = change_state_power.group(2)
            self.handler_change_active_contact(bridge_qr, state)
            self.get_version()
        elif event_search_device:
            state_search_device = event_search_device.group(1)
            self.handler_state_search_device(state_search_device)
        elif confirm_reg_device:
            device_id = confirm_reg_device.group(1)
            self._handler_comfirm_reg_device(device_id)
        elif _event_reg_device:
            device_id = _event_reg_device.group(1)
            self.handler_add_device(device_id)
        elif event_alarm_device:
            device_id = event_alarm_device.group(2)
            self.handler_alarm_device(device_id)
        elif oc_bridge_version:
            bridge_qr = oc_bridge_version.group(1)
            self._handler_check_qr_code_bridge(bridge_qr)

        self.check_all_column()

    def _handler_check_qr_code_bridge(self, bridge_id):
        widget_id_bridge = self.cellWidget(0, 0)
        widget_id_bridge.set_bridge_id(bridge_id)

    def handler_alarm_device(self, device_id):
        if device_id.lower() == self._device_search[:6].lower():
            widget_alarms_device = self.cellWidget(0, 6)
            widget_alarms_device.increment()

    def handler_change_active_contact(self, bridge_id, state):
        row = self.get_row_by_id(bridge_id)
        if row is None:
            return

        widget_contact = self.cellWidget(row, 4)
        if state == '1':
            widget_contact.change_tamper_count_on()
        elif state == '0':
            widget_contact.change_tamper_count_off()

    def handler_state_search_device(self, state):
        if state == '1':
            self.widget_search_device2bridge.show()
        elif state == '0':
            self._timeout_search_device()

    def _handler_comfirm_reg_device(self, device_id):
        if self._device_search[:6] == device_id.lower():
            self.test_bridge.write(b"Y\r\n")
        else:
            self.test_bridge.write(b"N\r\n")

    def handler_add_device(self, device_id):
        logger.debug("Event device connect to bridge {}".format(device_id))
        if device_id.lower() == self._device_search[:6].lower():
            widget_start = self.cellWidget(0, 5)
            widget_start.set_state(True)
            widget_start.setStyleSheet(ColorResult.success.value)
            self.start_work()
            logger.debug("Close")
            self.close_popup_search()
