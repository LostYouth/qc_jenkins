from qc.componets import DevIdWidget
from qc.enum_color import ColorResult


class BridgeId(DevIdWidget):

    def __init__(self, *args):
        super(BridgeId, self).__init__(*args)

        self._bridge_id_uart = ''

        self._malfunction_name = ''

    def set_malfunction_name(self, name_malf):
        self._malfunction_name = name_malf

    def get_malfunction_name(self):
        return self._malfunction_name

    def set_bridge_id(self, bridge_id: str):
        self._bridge_id_uart = bridge_id.lower()
        self.check_eq_qr()

    def get_state(self):
        return self.get_id() == self._bridge_id_uart

    def check_eq_qr(self):
        if self.get_state():
            self.setStyleSheet(ColorResult.success.value)
        else:
            self.setStyleSheet(ColorResult.fail.value)
