from PyQt5 import QtWidgets, QtCore


class BridgeWaitWidget(QtWidgets.QWidget):
    def __init__(self, msg_text, parent=None):
        super(BridgeWaitWidget, self).__init__(parent=parent, flags=QtCore.Qt.Popup)

        self.setMinimumWidth(230)
        self.setMinimumHeight(100)
        l = QtWidgets.QVBoxLayout()
        self.setLayout(l)

        self.label_status = QtWidgets.QLabel()
        self.label_status.setAlignment(QtCore.Qt.AlignCenter)
        self.label_message = QtWidgets.QLabel(msg_text)
        self.label_message.setAlignment(QtCore.Qt.AlignCenter)

        l.addWidget(self.label_message)
        l.addWidget(self.label_status)

        self.timer_update = QtCore.QTimer()
        self.timer_update.setInterval(1000)
        self.timer_update.timeout.connect(self.update_status)

        self._count = 0
        self.flag_close = False

    def move_center(self):
        self.move(
            self.parent().window().frameGeometry().topLeft() + self.parent().window().rect().center() - self.rect().center())

    def update_status(self):
        self._count += 1
        self.label_status.setText("{}".format(self._count))

    def show(self):
        self._count = 0
        self.flag_close = False
        self.move_center()
        self.timer_update.start()
        super(BridgeWaitWidget, self).show()
        
    def close_widget(self):
        self.flag_close = True
        self.timer_update.stop()
        self.close()

    def closeEvent(self, QCloseEvent):
        if not self.flag_close:
            QCloseEvent.ignore()
        else:
            super(BridgeWaitWidget, self).closeEvent(QCloseEvent)
