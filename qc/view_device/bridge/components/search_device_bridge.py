from PyQt5 import QtWidgets

from qc.enum_color import ColorResult


class SearchDeviceBridge(QtWidgets.QPushButton):
    def __init__(self, *args):
        super(SearchDeviceBridge, self).__init__(*args)

        self._state = False

        self._malfunction_name = None

    def set_malfunction_name(self, malf_name):
        self._malfunction_name = malf_name

    def get_malfunction_name(self):
        return self._malfunction_name

    def check_state(self):
        self.setEnabled(not self._state)
        self.setStyleSheet(ColorResult.success.value) if self._state else self.setStyleSheet(ColorResult.fail.value)

    def set_state(self, state: bool):
        self._state = state
        self.check_state()

    def get_state(self):
        return self._state
