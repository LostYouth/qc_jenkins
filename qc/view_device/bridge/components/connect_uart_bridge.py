from PyQt5 import QtWidgets, QtCore
from PyQt5.QtSerialPort import QSerialPortInfo

from qc.enum_color import ColorResult


class ConnectUartBridge(QtWidgets.QWidget):
    def __init__(self):
        super(ConnectUartBridge, self).__init__()

        l = QtWidgets.QVBoxLayout()

        self.combo_box_ports = QtWidgets.QComboBox()

        self.push_btn_connect = QtWidgets.QPushButton("Підключитися")
        self.push_btn_connect.clicked.connect(self._increment_count_click)

        l.addWidget(self.combo_box_ports)
        l.addWidget(self.push_btn_connect)
        self.installEventFilter(self)
        self.setLayout(l)

        self._state = False

        self._count_click_connect_device = 0

        self._malfunction_name = None

    def set_malfunction_name(self, malf_name):
        self._malfunction_name = malf_name

    def get_malfunction_name(self):
        return self._malfunction_name

    def get_state_repair(self):
        s = self.styleSheet()
        if s == ColorResult.fail.value:
            return True
        return False

    def _increment_count_click(self):
        self._count_click_connect_device += 1

    def check_count_clicked_connect(self):
        return bool(self._count_click_connect_device)

    def set_state(self, state: bool):
        self._state = state
        self.setEnabled(not state)

    def get_state(self):
        return self._state

    def set_ports_name(self, ports: list):
        self.combo_box_ports.clear()
        self.combo_box_ports.addItems(ports)

    def get_port_name(self):
        return self.combo_box_ports.currentText()

    def eventFilter(self, QObject, QEvent):
        if QObject is self.combo_box_ports:
            if QEvent.key() == QtCore.QEvent.MouseButtonPress:
                ports = [port.portName() for port in QSerialPortInfo.availablePorts() if not port.isBusy()]
                self.set_ports_name(ports)
        return super(ConnectUartBridge, self).eventFilter(QObject, QEvent)
