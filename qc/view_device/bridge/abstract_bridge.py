import logging
import re
from collections import namedtuple, deque
from enum import Enum

from __app_name__ import __app_name__ as app_name
from __version__ import __version__ as app_version

from PyQt5 import QtWidgets, QtCore
from PyQt5.QtSerialPort import QSerialPort, QSerialPortInfo
from pymysql import DatabaseError

from app_setting import AppSetting
from qc.componets import ErrorPopupWidget
from qc.componets.search_device_start import SearchDevicePopup
from qc.database.database_production import DataBaseProduction
from qc.enum_color import ColorResult
from qc.enum_param import QrDevTypes
from qc.security import KeyDatabase
from qc.view_device.bridge.components.bridge_wait_widget import BridgeWaitWidget
from qc.view_device.bridge.utils.task_wait_answer import TaskWaitAnswer

logger = logging.getLogger(AppSetting.LOGGER_NAME)


class Bridge(Enum):
    ocBridge = 'fe'
    uartBridge = 'ff'


class CommandBridge(Enum):
    Version = 'ver'
    Clear = 'cln'
    Stop = 'stop'
    Work = 'wrk'
    StartSearchDevice = 'add'
    CancelSearchDevice = 'stt1'


RegularExpAnswer = 'RESULT;([A-z]+);\d+;'


class AbstractBridge(QtWidgets.QTableWidget):
    dev_type = None

    column = namedtuple('column', ['name', 'width'])

    delay_connect_bridge = 25 * 1000

    table_head = [

        column("Qr", 70),
        column("Наявність\nдефектів", 200),
        column("Підключення", 100),
        column("COM-порт", 100),
        column("Активні контакти", 150),
        column("Підключення\nдатчика", 120),
        column("Тривоги\nвід\nдатчика", 130),
        column("Збереження\nналаштування", 140),
        column("Відмінити\nтестування", 120),
        column("Зареєструвати", 100),
    ]

    def __init__(self, parent):
        super(AbstractBridge, self).__init__()
        self.insert_columns()
        self.qc_main_widget = parent

        self.setEditTriggers(QtWidgets.QTableWidget.NoEditTriggers)
        self.setSelectionMode(QtWidgets.QTableWidget.NoSelection)
        self.horizontalHeader().setStretchLastSection(True)

        self.defects = dict()
        self.defects['class_b'] = ['1', '2']
        self.defects['return'] = ['12', '32']

        self.row_height = 80

        self.data_base_production = DataBaseProduction(**KeyDatabase.get_keys_production())

        self.test_bridge = QSerialPort()
        self.test_bridge.setBaudRate(QSerialPort.Baud57600)
        self.test_bridge.readyRead.connect(self._handler_read)

        self.widget_error = ErrorPopupWidget(parent=parent)

        self.widget_search_bridge = SearchDevicePopup(parent=self.qc_main_widget)
        self.widget_search_bridge.push_btn_cancel_register_device.clicked.connect(self._cancel_search_bridge)

        self.timer_update_check_port = QtCore.QTimer()
        self.timer_update_check_port.setInterval(500)
        self.timer_update_check_port.timeout.connect(self._search_bridge_port)
        self._avalible_ports = []
        self.current_task = deque(maxlen=5)

        self.widget_wait_connect = BridgeWaitWidget("Коннект до пристрою.", parent=self.qc_main_widget)

    def set_defects(self, defect):
        self.defects = defect

    def show_error(self, message: str):
        logger.debug("show error {}".format(message))
        self.widget_error.set_message_data(message)
        self.widget_error.show()

    def insert_columns(self):
        for i, column_o in enumerate(self.table_head):
            self.insertColumn(i)
            self.setColumnWidth(i, column_o.width)
            item = QtWidgets.QTableWidgetItem(column_o.name)
            self.setHorizontalHeaderItem(i, item)

    def check_exists(self, bridge_id):
        if self.get_row_by_id(bridge_id) is None:
            return False
        return True

    def get_row_by_id(self, bridge_id):
        for row in range(self.rowCount()):
            widget_id = self.cellWidget(row, 0)
            if widget_id.get_id() == bridge_id:
                return row
        return None

    def check_all_column(self):
        for row in range(self.rowCount()):
            c = []
            for col in range(self.columnCount()):
                widget = self.cellWidget(row, col)
                if hasattr(widget, 'get_state'):
                    c.append(widget.get_state())
            self.change_btn_register_state(row, c)

    def register_database_bridge(self, bridge_id):

        button_register = self.sender()
        button_register.setEnabled(False)
        text_btn_register = button_register.text()

        serv_id = bridge_id
        operator = self.qc_main_widget.get_operator_name()
        grage_qc = self.get_grade_bridge(bridge_id)
        defects = self.get_defects_bridge(bridge_id)
        app = app_name + ' ' + app_version
        info = {'app': app}

        if text_btn_register == 'Зареєструвати':
            if not self.success_register_database(serv_id, operator, grage_qc, defects, info):
                button_register.setEnabled(True)
                return
        else:
            if not self._you_sure():
                button_register.setEnabled(True)
                return

            row = self.get_row_by_id(bridge_id)
            malfunction = self.get_malfunction_central(row)
            malfunction = '; '.join(malfunction)
            widget_connect = self.cellWidget(row, 2)

            # брідж не спробували приписати
            if not widget_connect.check_count_clicked_connect():
                #  немає візуальних дефектів
                if not defects:
                    self.show_error("Щоб повернути цетраль потрібно вибрати дефект або приписати цетраль на акаунт!")
                    button_register.setEnabled(True)
                    return
                else:
                    self.send_repair(bridge_id, malfunction=defects)

            #  брідж не вдалось приписати
            elif not widget_connect.get_state() and widget_connect.get_state_repair():
                malfunction = "Не вдалось підключитися"
                self.send_repair(bridge_id, malfunction=malfunction)
                defects = ';'.join((defects, malfunction))

            #  приписування бріджа відмінили
            elif not widget_connect.get_state():
                if not defects:
                    malfunction = "Не вдалось підключитися"
                    self.send_repair(bridge_id, malfunction=malfunction)
                else:
                    self.send_repair(bridge_id, defects)

            #  брідж приписався, є візуальні або функціональні дефекти
            elif defects or malfunction:
                self.send_repair(bridge_id, malfunction=defects)
                defects = ';'.join((defects, malfunction))

            registered = self.register_fail_bridge_qc(bridge_id, defects, info)
            if not registered:
                button_register.setEnabled(True)
                return
        self.cancel_test_bridge(bridge_id)

    def register_fail_bridge_qc(self, bridge_id, defects, info):
        try:
            if self.data_base_production.id_exists(QrDevTypes.Central.value, bridge_id):
                login = self.qc_main_widget._operator_login
                self.data_base_production.register_central_fail(login, bridge_id, defects, info)
            else:
                self.data_base_production.register_bridge_fail_old(bridge_id, defects)
        except DatabaseError:
            logger.error("Error while register failure {} {}".format(self.get_device_type_name(), bridge_id))
            return False
        return True

    def send_repair(self, bridge_id, malfunction):
        row = self.get_row_by_id(bridge_id)
        if row is None:
            logger.error("Send repair hub not found in table")
            return
        widget_id = self.cellWidget(row, 0)
        qr_code = widget_id.get_qr_code()

        operator = self.qc_main_widget.get_operator_name()
        try:
            if self.data_base_production.qr_exists(qr_code):
                login = self.qc_main_widget._operator_login
                self.data_base_production.send_repair(QrDevTypes.Central.value, qr_code, login, 'QC', malfunction)
            else:
                self.data_base_production.send_repair_old(qr_code, self.get_device_type_name(), operator, "QC", malfunction)
        except DatabaseError:
            return

    def get_malfunction_central(self, row):
        malfunction = []
        for col in range(self.columnCount()):
            widget = self.cellWidget(row, col)
            if hasattr(widget, '_malfunction_name') and not widget.get_state():
                m = widget.get_malfunction_name()
                malfunction.append(m)
        return malfunction

    def success_register_database(self, serv_id, operator, grade_qc, defects, info):
        try:
            if self.data_base_production.id_exists(QrDevTypes.Central.value, serv_id):
                login = self.qc_main_widget._operator_login
                self.data_base_production.register_central_success(serv_id, login, grade_qc, defects, info)
            else:
                self.data_base_production.register_bridge_success_old(serv_id, operator, grade_qc,
                                                                      defects, '')
        except DatabaseError as error:
            self.show_error("{}".format(error))
            return False
        else:
            return True

    def get_grade_bridge(self, bridge_id):
        row = self.get_row_by_id(bridge_id)
        widget_defects = self.cellWidget(row, 1)
        return widget_defects.get_grade_qc()

    def get_defects_bridge(self, bridge_id):
        row = self.get_row_by_id(bridge_id)
        widget_defects = self.cellWidget(row, 1)
        return ';'.join(widget_defects.get_defects())

    def change_btn_register_state(self, row, c: list):
        push_btn = self.cellWidget(row, self.columnCount() - 1)
        if all(c):
            push_btn.setText("Зареєструвати")
            push_btn.setStyleSheet(ColorResult.success.value)
        else:
            push_btn.setText("Повернути")
            push_btn.setStyleSheet('')

    def set_port_name(self, port_name):
        logger.debug("Set port name {}".format(port_name))
        self.test_bridge.setPortName(port_name)

    def open_port(self):
        self.widget_wait_connect.close_widget()
        logger.debug("Open port uart")
        widget_connect_bridge = self.cellWidget(0, 2)

        if not self.test_bridge.open(QtCore.QIODevice.ReadWrite):
            self.show_error("Не вдалось підконектитись до ком порту {}".format(self.test_bridge.portName()))
            widget_connect_bridge.setStyleSheet(ColorResult.fail.value)
            return False
        else:
            widget_connect_bridge.set_state(True)
            self.test_bridge.write(b'ver\r\n')
            return True

    def close_port(self):
        if self.test_bridge.isOpen():
            self.test_bridge.close()

    def get_device_type_name(self):
        return self.dev_type.name

    def get_device_type(self):
        return self.dev_type.value

    def add_obj(self, qr_device: str):
        raise NotImplemented()

    def cancel_test_bridge(self, bridge_qr):
        self.cancel_search_device()
        self.stop()
        self.cln()
        row = self.get_row_by_id(bridge_qr)
        logger.debug("Cancel test bridge {}".format(bridge_qr))
        if row is None:
            logger.warning("Row not found while cancel test")
            return
        else:
            self.close_port()
            self.removeRow(row)

    def _handler_read(self):
        while self.test_bridge.canReadLine():
            try:
                line = self.test_bridge.readLine()
                line_string = line.data().decode()
            except Exception as err:
                logger.error("{}".format(err))
            else:
                line_data = line_string.strip()
                logger.debug("bridge line: {}".format(line_data))
                answer = re.search(RegularExpAnswer, line_data)
                if answer and self.current_task:
                    task = self.current_task.popleft()
                    task.set_result(line_data)
                else:
                    self._handler_message(line_data)

    def cln(self):
        self._send_command(CommandBridge.Clear)

    def stop(self):
        return self._send_command(CommandBridge.Stop)

    def cancel_search_device(self):
        self._send_command(CommandBridge.CancelSearchDevice)

    def get_version(self):
        cmd_version = "{}\r\n".format(CommandBridge.Version.value)
        self.test_bridge.write(cmd_version.encode())

    def start_add_device(self):
        self.cancel_search_device()
        self.stop()
        self.cln()
        self._send_command(CommandBridge.StartSearchDevice)

    def start_work(self):
        self.test_bridge.write("{}\r\n".format(CommandBridge.Work.value).encode())

    def _send_command(self, command):
        if self.test_bridge.isOpen():
            command = "{}\r\n".format(command.value).encode()
            logger.debug("Send command bridge: {}".format(command))
            self.test_bridge.write(command)
            task = TaskWaitAnswer()
            self.current_task.append(task)
            return self._wait_answer(task)

    def _wait_answer(self, task):
        while not task.done():
            QtWidgets.qApp.processEvents()
        return task.get_result()

    def _handler_message(self, message: str):
        pass

    def show_widget_search(self):
        logger.debug("Show widget search")
        self.widget_search_bridge.show()

    def close_popup_register_device(self):
        logger.debug("Close popup register device")
        if self.widget_search_bridge.isVisible():
            self.widget_search_bridge.close_timeout()
        self.timer_update_check_port.stop()

    def _check_defects(self, bridge_id):
        widget_defects = self.cellWidget(0, 1)
        if widget_defects.defects_popup.get_defects_return():
            self.show_error("В датчика {} вибрані дефекти 'тільки на повернення'! "
                            "Датчик не підлягає подальшому тестуванню!".format(bridge_id))
            return False
        if (
                widget_defects.defects_popup.get_defects_grade() or widget_defects.defects_popup.get_custom_defects()) and not widget_defects.check_box_grade_defect.isChecked():
            self.show_error(
                "Якщо обрані дефекти підлягаю класу Б, відмітьте це в чекбоксі, якщо ні девайс підлягає поверненню!")
            return False
        return True

    def _search_bridge(self, bridge_qr):
        if self._check_defects(bridge_qr) is False:
            return
        logger.debug("Start search bridge {}".format(bridge_qr))
        self.show_widget_search()

        self.widget_search_bridge.startTimer(30 * 1000)

        self._avalible_ports = self._get_uart_ports_name()
        self.timer_update_check_port.start()

    def _cancel_search_bridge(self):
        logger.debug("Cancel search bridge")
        self.timer_update_check_port.stop()
        self.widget_search_bridge.close_timeout()

    def _get_uart_ports_name(self):
        return [port.portName() for port in QSerialPortInfo.availablePorts() if not port.isBusy()]

    def _search_bridge_port(self):
        ports = set(self._get_uart_ports_name())
        avalible_ports = set(self._avalible_ports)
        port = ports.difference(avalible_ports)
        logger.debug("Search port bridge avalible_ports:{} ports:{}".format(avalible_ports, ports))
        if port and avalible_ports < ports:
            logger.debug("Port Bridge {}".format(port))
            port_uart_bridget = port.pop()
            self._handler_bridge_port_search(port_uart_bridget)
            self.close_popup_register_device()
            self._change_state_btn_search(0, 2, False)

        if int(self.widget_search_bridge.label_status.text()) > 30:
            self.close_popup_register_device()
            self._change_state_btn_search(0, 2, True, color=ColorResult.fail.value)

        self._avalible_ports = ports

    def _change_state_btn_search(self, row, col, state, color=None):
        widget_search_bridge = self.cellWidget(row, col)
        widget_search_bridge.setEnabled(state)

        if color:
            widget_search_bridge.setStyleSheet(color)

    def _handler_bridge_port_search(self, port_name):
        self.widget_wait_connect.show()
        widget_status_com_port = self.cellWidget(0, 3)
        widget_status_com_port.set_status(port_name)
        self.set_port_name(port_name)
        self.timer_update_check_port.singleShot(self.delay_connect_bridge, self.open_port)

    def press_space_start_register_device(self):
        col = 2
        if not self.rowCount():
            return
        for row in range(self.rowCount()):
            widget_connect = self.cellWidget(row, col)
            if widget_connect.isEnabled():
                widget_connect.push_btn_connect_device.click()
                break

    @staticmethod
    def _you_sure():
        reply = QtWidgets.QMessageBox()
        reply.setIcon(QtWidgets.QMessageBox.Question)
        reply.setText("Ви впевненні, що цей пристрій необхідно повернути?")
        reply.setStandardButtons(QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
        btn_yes = reply.button(QtWidgets.QMessageBox.Yes)
        btn_not = reply.button(QtWidgets.QMessageBox.No)
        btn_yes.setText('Так, повернути')
        btn_not.setText('Ні, не повертати')
        reply.exec_()
        if reply.clickedButton() == btn_yes:
            return True
        else:
            return False