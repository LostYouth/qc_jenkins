import binascii
import logging
from collections import namedtuple
from functools import partial

from PyQt5 import QtWidgets

from app_setting import AppSetting
from csa_client_qt.csa_client import CsaClient
from csa_client_qt.csa_object.update_parse import update_parse
from csa_client_qt.enum_obj_csa import HubSubType
from csa_client_qt.enum_params import MessageType, Wifi, Answer
from qc.componets import IdWidget, VisualDefectTable, ConnectHubWidget, StatusWidget, TamperWidget
from qc.componets.power_hub_widget import PowerHubWidget
from qc.componets.widget_sim_holder import WidgetSimHolder
from qc.componets.wifi_popup_wait import WifiPopupWait
from qc.componets.wifi_search_hub_plus import WifiSearchHubPlus
from qc.enum_color import ColorResult
from .central_hub_view import CentralHubView

logger = logging.getLogger(AppSetting.LOGGER_NAME)
column = namedtuple('column', ['name', 'width'])


class CentralHubPlusView(CentralHubView):
    dev_type = HubSubType.HubPlus
    wifi_name = "Ajax_2G"
    table_head = [
        column("Qr", 120),
        column("Наявність\nдефектів", 200),
        column("Підключення", 100),
        column("Колір", 80),
        column('Тампер', 150),
        column("Зовнішнє\nживлення", 100),
        column("Версія ПЗ", 120),
        column("Канали\nзвязку", 120),
        column("Рівень\nбатареї", 120),
        column("Сім\nхолдери", 100),
        column("WiFi", 100),
        column("Відмінити\nтестування", 120),
        column("Зареєструвати", 100),
    ]

    def __init__(self, client_csa: CsaClient, parent=None):
        super(CentralHubPlusView, self).__init__(client_csa, parent=parent)
        self.wait_wifi_network_answer = WifiPopupWait(parent=parent)

    def clear_apn(self, hub_id):
        if self.clear_apn_sim_slot(hub_id):
            return True
        return False

    def clear_apn_sim_slot(self, hub_id):
        hub_obj = self.client_csa.get_hub_obj(hub_id)
        answer = hub_obj.send_command2console("set {} 20  ".format(hub_obj.id))
        logger.debug("Answer clear apn {} slot 1".format(answer))
        answer = hub_obj.send_command2console("set {} 7f  ".format(hub_obj.id))
        logger.debug("Answer clear apn {} slot 2".format(answer))
        return True

    def add_obj(self, hub_qr_code: str):
        hub_id = hub_qr_code[:8]
        if self.check_exists_hub(hub_id):
            self.show_error("Цей хаб {} вже є в талиці!".format(hub_id))
            return

        row = self.rowCount()
        self.insertRow(row)
        self.setRowHeight(row, self.row_height)

        widget_id = IdWidget()
        widget_id.set_id_hub(hub_qr_code)
        self.setCellWidget(row, 0, widget_id)

        widget_defects = VisualDefectTable(self.defects, parent=self.qc_main_widget)
        widget_defects.signal_update_defects.connect(self.check_all_column)
        self.setCellWidget(row, 1, widget_defects)

        widget_connect_hub2acc = ConnectHubWidget()
        widget_connect_hub2acc.push_btn_connect.clicked.connect(partial(self.add_hub2account, hub_qr_code[:8], hub_qr_code[8:16]))
        self.setCellWidget(row, 2, widget_connect_hub2acc)

        widget_color = StatusWidget()
        widget_color.set_expected_result('white' if hub_qr_code[16] == '1' else 'black')
        widget_color.set_malfunction_name("Колір цетралі не співпадає")
        self.setCellWidget(row, 3, widget_color)

        widget_tamper = TamperWidget()
        widget_tamper.set_malfunction_name("Тампер")
        self.setCellWidget(row, 4, widget_tamper)

        widget_power_hub = PowerHubWidget()
        widget_power_hub.set_malfunction_name("Живлення 220")
        self.setCellWidget(row, 5, widget_power_hub)

        widget_version_fw = StatusWidget()
        widget_version_fw.set_malfunction_name("Версія ПЗ")
        widget_version_fw.set_expected_result('')
        self.setCellWidget(row, 6, widget_version_fw)

        widget_connection_channel = StatusWidget()
        widget_connection_channel.set_malfunction_name("Канали звязку")
        widget_connection_channel.set_expected_result("Eth/Gsm")
        self.setCellWidget(row, 7, widget_connection_channel)

        widget_battery_charge = StatusWidget()
        widget_battery_charge.set_malfunction_name("Рівень батареї")
        widget_battery_charge.set_expected_result(95)
        self.setCellWidget(row, 8, widget_battery_charge)

        widget_sim_holder = WidgetSimHolder()
        widget_sim_holder.set_malfunction_name("Сім холдери")
        self.setCellWidget(row, 9, widget_sim_holder)

        widget_wifi_search = WifiSearchHubPlus("Пошук")
        widget_wifi_search.set_malfunction_name("Wifi")
        widget_wifi_search.clicked.connect(partial(self._search_wifi, hub_qr_code[:8]))
        self.setCellWidget(row, 10, widget_wifi_search)

        push_btn_cancel_test_hub = QtWidgets.QPushButton('X')
        push_btn_cancel_test_hub.clicked.connect(partial(self.cancel_test_central, hub_qr_code[:8]))
        self.setCellWidget(row, 11, push_btn_cancel_test_hub)

        push_btn_register_hub = QtWidgets.QPushButton("Повернути")
        push_btn_register_hub.clicked.connect(partial(self.register_central_register_database, hub_id))
        self.setCellWidget(row, 12, push_btn_register_hub)

    def _handler_update_data(self, hub_id):
        super(CentralHubPlusView, self)._handler_update_data(hub_id)
        hub_obj = self.client_csa.get_hub_obj(hub_id)
        row = self.get_row_by_serv_id(hub_id)
        if row is None:
            logger.info("Hub {} not found in table {}".format(hub_id, self.__class__.__name__))
            return
        self._update_sim_holder(row,
                                hub_obj.get_parameters('5f'),
                                hub_obj.get_parameters('85'),
                                hub_obj.get_parameters('7a'))

        self.check_all_column()

    def _update_sim_holder(self, row, sim_cart_state, sim_holder, mode_gsm):
        widget_sim_holder = self.cellWidget(row, 9)
        if not sim_cart_state or not sim_holder or not mode_gsm:
            return
        sim_cart_state = int(sim_cart_state, 16)
        sim_holder = int(sim_holder, 16)
        if sim_cart_state == 0 and sim_holder == 0 and mode_gsm != '00':
            widget_sim_holder.set_state_one(True)
        elif sim_cart_state == 0 and sim_holder == 1 and mode_gsm != '00':
            widget_sim_holder.set_state_second(True)

    def _search_wifi(self, hub_id):
        hub_obj = self.client_csa.get_hub_obj(hub_id)
        if hub_obj is None:
            return
        self.wait_wifi_network_answer.show()
        btn_search_wifi = self.sender()
        btn_search_wifi.setEnabled(False)
        answer = hub_obj.scan_wifi_network()
        btn_search_wifi.setEnabled(True)
        self.wait_wifi_network_answer.custom_close()
        if not answer.message_type:
            self.show_error("Не було відповіді")
        elif answer.message_type == MessageType.Wifi.value and answer.message_key == Wifi.Networks.value:
            logger.debug("Parse answer {}".format(answer))
            for wifi_name, *_ in update_parse(answer.payload):
                if self.wifi_name == binascii.unhexlify(wifi_name.encode()).decode():
                    btn_search_wifi.setStyleSheet(ColorResult.success.value)
                    btn_search_wifi.setEnabled(False)
                    btn_search_wifi.set_state(True)
                    self.check_all_column()
                    break
            else:
                self.show_error("Wifi мережу не знайдено!")

        else:
            self.show_error(str(answer))
