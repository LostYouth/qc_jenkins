import logging
from contextlib import contextmanager
from pymysql.err import DatabaseError

from __app_name__ import __app_name__ as app_name
from __version__ import __version__ as app_version

from PyQt5 import QtWidgets

from app_setting import AppSetting
from csa_client_qt.csa_client import CsaClient
from csa_client_qt.enum_params import MessageType, Answer
from qc.componets import ErrorPopupWidget, WaitHubConnect
from qc.componets.share_pro_account_widget import ShareProAccountWidget
from qc.database.database_production import DataBaseProduction
from qc.database.database_model_csa import DataBaseCsa
from qc.enum_color import ColorResult
from qc.security import KeyDatabase
from qc.enum_param_where_hub import EnumParamWhereHub
from qc.enum_param import QrDevTypes

logger = logging.getLogger(AppSetting.LOGGER_NAME)


class CentralAbstractView(QtWidgets.QTableWidget):
    def __init__(self, client_csa: CsaClient, parent=None):
        super(CentralAbstractView, self).__init__()
        self.qc_main_widget = parent
        self.client_csa = client_csa

        self.data_base_production = DataBaseProduction(old_db_keys=KeyDatabase.get_old_keys_production(),
                                                       db_keys=KeyDatabase.get_keys_production())
        self.data_base_model_csa = DataBaseCsa(**KeyDatabase.get_keys_model_csa())

        self.defects = dict()
        self.defects['class_b'] = ['1', '2']
        self.defects['return'] = ['12', '32']

        self.error_window = ErrorPopupWidget(parent=self)

    def show_error(self, message_data):
        self.error_window.set_message_data(message_data)
        self.error_window.show()

    def set_defects(self, defects: dict):
        self.defects = defects

    def get_row_by_serv_id(self, hub_id):
        for row in range(self.rowCount()):
            widget = self.cellWidget(row, 0)
            if widget.get_hub_id() == hub_id:
                return row

    def check_exists_hub(self, hub_id):
        if self.get_row_by_serv_id(hub_id) is not None:
            return True
        return False

    def get_device_type_name(self):
        return self.dev_type.name

    def get_device_type(self):
        return self.dev_type.value

    def success_register_hub(self, hub_id, operator, grade_qc, defects, info, country=None, pro_acc=None):
        try:
            if self.data_base_production.id_exists(QrDevTypes.Central.value, hub_id):
                login = self.qc_main_widget._operator_login
                self.data_base_production.register_central_success(hub_id, login, grade_qc, defects, info,
                                                                   country=country, pro_acc=pro_acc)
            else:
                self.data_base_production.register_central_success_old(hub_id, operator, grade_qc,
                                                                       defects, info['description'])
        except DatabaseError as err:
            self.show_error("Не вдалось зареєструвати хаб в базу production\nДеталі: {}".format(err))
            return False
        else:
            return True

    def register_models_csa_database(self, hub_id: str, firm_version: int, group: int):
        try:
            self.data_base_model_csa.set_fw_and_group_hub(hub_id, firm_version, group)
        except DatabaseError as err:
            self.show_error("Не вдалось оновити групу або версію для Хабу {} в базі\nДеталі: {}".format(hub_id, err))
            return False
        else:
            return True

    def get_grade_qc(self, hub_id):
        row = self.get_row_by_serv_id(hub_id)
        return self._get_grade_qc(row)

    def _get_grade_qc(self, row, col=1):
        widget_defects = self.cellWidget(row, col)
        return widget_defects.get_grade_qc()

    def get_defects(self, hub_id):
        row = self.get_row_by_serv_id(hub_id)
        d_g, d_r, d_c = self.get_defects_array(row)
        return ';'.join(d_g + d_r + d_c)

    def get_defects_array(self, row, col=1):
        widget = self.cellWidget(row, col)
        d_grade = widget.defects_popup.get_defects_grade()
        d_return = widget.defects_popup.get_defects_return()
        d_custom = widget.defects_popup.get_custom_defects()
        return d_grade, d_return, d_custom

    def _wait_hub_connect2server(self, hub_id):
        raise NotImplemented("not implemented")

    def _change_state_btn_register(self, state):
        for row in range(self.rowCount()):
            w = self.cellWidget(row, self.columnCount() - 1)
            w.setEnabled(state)

    def _change_state_btn_cancel(self, state):
        for row in range(self.rowCount()):
            btn = self.cellWidget(row, self.columnCount() - 2)
            btn.setEnabled(state)

    @contextmanager
    def change_state_btn_cancel(self):
        self._change_state_btn_cancel(False)
        yield
        self._change_state_btn_cancel(True)

    @contextmanager
    def _register_disable_btn(self):
        self._change_state_btn_register(False)
        yield
        self._change_state_btn_register(True)

    def register_central_register_database(self, hub_id):
        state_hub = self.sender().text()

        logger.info("sim slot pull with sim holder {}".format(hub_id))

        logger.info("Return hub {} {}".format(hub_id, state_hub))

        mode = self.qc_main_widget.get_mode_hub()
        logger.info("Where HUB: {} {}".format(hub_id, mode))
        operator = self.qc_main_widget.get_operator_name()
        logger.info("Operator: {} {}".format(hub_id, operator))
        group = mode.value.group
        logger.info("Group hub: {} {}".format(hub_id, group))
        description = mode.value.name
        logger.info("Desc: {} {}".format(hub_id, description))
        grade_qc = self.get_grade_qc(hub_id)
        defects = self.get_defects(hub_id)

        app = app_name + ' ' + app_version
        info = {'app': app,
                'description': description}
        with self._register_disable_btn():
            if state_hub == "Зареєструвати":
                country = None
                pro_acc = None

                hub_obj = self.client_csa.get_hub_obj(hub_id)
                logger.info('Register hub {}'.format(hub_obj))
                if hub_obj is None:
                    self.show_error("Хаб не приписаний до аккаунту!")
                    logger.info("Hub not found in side client")
                    return

                if hub_obj is not None:
                    if hub_obj.get_parameters('08') != '00':
                        logger.info('Hub is state ARM')
                        self.show_error("Зніміть хаб з охорони!")
                        return

                if not self._drop_logs(hub_id):
                    logger.info("Cant drop logs in hub {}".format(hub_obj))
                    self.show_error("Не вдалось скинути logs для Хабу!")
                    return

                if mode == EnumParamWhereHub.Italia:
                    if not self._factory_reset_hub(hub_id):
                        return
                    w = WaitHubConnect(parent=self.qc_main_widget)
                    w.show()
                    w.label_status.setText('Скидування налаштувань')

                    self._wait_hub_connect2server(hub_id)
                    w.label_status.setText('Очищення АПН')
                    logger.info("Clear APN {}".format(hub_obj))
                    if not self.clear_apn(hub_id):
                        w.close()
                        logger.info("Cant clear APN {}".format(hub_obj))
                        return
                    w.close()
                    country = 'Italy'

                if mode == EnumParamWhereHub.Ireland:
                    if not self._factory_reset_hub(hub_id):
                        return
                    w = WaitHubConnect(parent=self.qc_main_widget)
                    w.show()
                    w.label_status.setText('Скидування налаштувань')

                    self._wait_hub_connect2server(hub_id)
                    w.label_status.setText('Очищення АПН')
                    logger.info("Clear APN {}".format(hub_obj))
                    if not self.clear_apn(hub_id):
                        w.close()
                        logger.info("Cant clear APN {}".format(hub_obj))
                        return
                    if not self.set_roaming(hub_id):
                        w.close()
                        logger.info('Can`t set roaming')
                        return
                    w.close()
                    country = 'Ireland'

                if mode == EnumParamWhereHub.ProAccount:
                    result_share_hub = self.share_hub(hub_id)
                    logger.info("Share hub in pro account")
                    if not result_share_hub:
                        return
                    group, pro_acc = result_share_hub
                    info['description'] = 'For pro account {}'.format(pro_acc)

                if mode == EnumParamWhereHub.Standart:
                    logger.info("Hub standart")

                if mode == EnumParamWhereHub.Russian:
                    logger.info("Hub russian")
                    country = 'Russian Federation'


                # register hub in production database
                if not self.success_register_hub(hub_id, operator, grade_qc, defects, info,
                                                 country=country, pro_acc=pro_acc):
                    return

                # Береться версія прошивки яку потрібно назначити хабу, щоб
                # всі хаби які пройшли прод випускались з останьою прошивкою
                firm_version = self.get_firm_version_future_hub(hub_obj, mode)
                if firm_version is None:
                    return

                # register hub in models csa database server
                if not self.register_models_csa_database(hub_id, int(firm_version), group):
                    return

                self.show_notification_sim_slot()

                # factory reset if hub not Italia
                if (mode != EnumParamWhereHub.Italia and
                        mode != EnumParamWhereHub.Ireland and
                        not self._factory_reset_hub(hub_id)):
                    return

                # Check fwGroup
                if not self.check_fw_group(hub_id, group):
                    return

                # delete hub in side client and remove row
                self._cancel_test_central(hub_id)

            else:
                if not self._you_sure():
                    logger.debug("click not sure in back central {}".format(hub_id))
                    return

                row = self.get_row_by_serv_id(hub_id)
                malfunction = self.get_malfunction_central(row)
                malfunction = '; '.join((m for m in malfunction if m))
                widget_connect = self.cellWidget(row, 2)

                # хаб не спробували приписати
                if not widget_connect.check_count_clicked_connect():
                    #  немає візуальних дефектів
                    if not defects:
                        self.show_error("Щоб повернути цетраль потрібно вибрати дефект або приписати цетраль на акаунт!")
                        return
                    else:
                        self.send_repair(hub_id, defects)
                #  хаб не вдалось приписати
                elif not widget_connect.get_state() and widget_connect.get_state_repair():
                    malfunction = "Централь не вдалось приписати"
                    self.send_repair(hub_id, malfunction=malfunction)
                    # defects = ';'.join((defects, malfunction))

                #  приписування хаба відмінили
                elif not widget_connect.get_state():
                    if not defects:
                        malfunction = "Централь не вдалось приписати"
                        self.send_repair(hub_id, malfunction=malfunction)
                        # defects = malfunction
                    else:
                        self.send_repair(hub_id, malfunction=defects)

                #  хаб приписався, є візуальні або функціональні дефекти
                elif defects or malfunction:
                    defects_qc = ';'.join((defects, malfunction))
                    self.send_repair(hub_id, malfunction=defects_qc)

                registered = self._register_central_failure_qc(hub_id, defects, info)
                if not registered:
                    return

                self._cancel_test_central(hub_id)

    @staticmethod
    def show_notification_sim_slot():
        reply_pull_out_sim = QtWidgets.QMessageBox()
        reply_pull_out_sim.setIcon(QtWidgets.QMessageBox.Warning)
        reply_pull_out_sim.setText("дістаньте Сім-карту")
        reply_pull_out_sim.setStandardButtons(QtWidgets.QMessageBox.Ok)
        reply_pull_out_sim.exec_()

    @staticmethod
    def _you_sure(text="Ви впевненні, що цей пристрій необхідно повернути?"):
        reply = QtWidgets.QMessageBox()
        reply.setIcon(QtWidgets.QMessageBox.Question)
        reply.setText(text)
        reply.setStandardButtons(QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
        btn_yes = reply.button(QtWidgets.QMessageBox.Yes)
        btn_not = reply.button(QtWidgets.QMessageBox.No)
        btn_yes.setText('Так')
        btn_not.setText('Ні')
        reply.exec_()
        if reply.clickedButton() == btn_yes:
            return True
        else:
            return False

    def get_firm_version_future_hub(self, hub_object, mode: EnumParamWhereHub):
        sub_type_hub = hub_object.sub_type_hub
        if mode != EnumParamWhereHub.Russian:
            _name_soft_verion_sub_type_future_version = "future{}Version".format(sub_type_hub.name)
        else:
            # Якщо хаб на росію потрібно брати futureRuHubVersion
            _name_soft_verion_sub_type_future_version = "future{}{}Version".format('Ru', sub_type_hub.name)

        try:
            firm_hub_object = self.data_base_production.get_future_firm_version_hub(_name_soft_verion_sub_type_future_version)
        except Exception as err:
            self.show_error("Неможливо отримати futureVersion, щоб назначити хабу {}! {}".format(hub_object.id, err))
            return
        return firm_hub_object['version']

    def check_fw_group(self, hub_id: str, fwGroup: str):
        try:
            fw_g = self.data_base_model_csa.get_group_hub(hub_id)
            logger.info("Group get db eq with group {} == {}".format(repr(fw_g), repr(fwGroup)))
            assert str(fw_g) == fwGroup, "Групи не співпадають з назначеною!"
        except Exception as err:
            self.show_error("Група не назначена! {}".format(err))
            return False
        else:
            return True

    def _register_central_failure_qc(self, hub_id, defects, info):
        try:
            if self.data_base_production.id_exists(QrDevTypes.Central.value, hub_id):
                login = self.qc_main_widget._operator_login
                self.data_base_production.register_central_fail(login, hub_id, defects, info)
            else:
                self.data_base_production.register_central_fail_old(hub_id, defects)
        except DatabaseError as err:
            logger.error("{}".format(err))
            return False
        return True

    def send_repair(self, hub_id, malfunction):
        row = self.get_row_by_serv_id(hub_id)
        if row is None:
            logger.error("Send repair hub not found in table")
            return
        widget_connect = self.cellWidget(row, 2)
        if widget_connect.check_count_clicked_connect():
            widget_id = self.cellWidget(row, 0)
            qr_code = widget_id.get_qr_code()
            operator = self.qc_main_widget.get_operator_name()

            try:
                if self.data_base_production.id_exists(QrDevTypes.Central.value, hub_id):
                    login = self.qc_main_widget._operator_login
                    self.data_base_production.send_repair(QrDevTypes.Central.value, hub_id, login, 'QC', malfunction)
                else:
                    self.data_base_production.send_repair_old(qr_code, self.get_device_type_name(), operator, "QC", malfunction)
            except DatabaseError as err:
                return

    def get_malfunction_central(self, row):
        malfunction = []
        for col in range(self.columnCount()):
            widget = self.cellWidget(row, col)
            if hasattr(widget, '_malfunction_name') and not widget.get_state():
                m = widget.get_malfunction_name()
                malfunction.append(m)
        return malfunction

    def _cancel_test_central(self, hub_id):
        raise NotImplemented("pls wrote good code")

    def clear_apn(self, hub_id):
        logger.error("Not implement for hub sub type {} id {}".format(self.get_device_type(), hub_id))
        return True

    def set_roaming(self, hub_id):
        logger.error("Not implement for hub sub type {} id {}".format(self.get_device_type(), hub_id))
        return True

    def share_hub(self, hub_id):
        if not self.qc_main_widget.get_pro_accounts():
            self.show_error("Не визначенні про аккаунти!")
            return False
        widget = ShareProAccountWidget(self.qc_main_widget.get_pro_accounts())
        widget.exec_()
        select_button = widget.clickedButton()
        if select_button is None:
            logger.info("Close widget share pro accounts")
            return False
        else:
            pro_email_account, group = select_button.text().split(':')
            if not self._share_hub_on_pro_account(hub_id, pro_email_account):
                return False
            return group, pro_email_account

    def _share_hub_on_pro_account(self, hub_id, email_pro):
        hub_obj = self.client_csa.get_hub_obj(hub_id)
        logger.debug("Share hub {} on pro account {}".format(hub_id, email_pro))
        a = hub_obj.share_hub([email_pro], is_pro_user=True)
        logger.debug("Answer share hub on pro acc {}".format(a))
        if a.message_type == MessageType.Answer.value and a.message_key == Answer.DeliveredCommandPerform.value:
            return True
        self.show_error("Не вдалось розшарити хаб на про юзера {}".format(email_pro))
        return False

    def _factory_reset_hub(self, hub_id):
        hub_obj = self.client_csa.get_hub_obj(hub_id)
        row = self.get_row_by_serv_id(hub_id)
        if row is None:
            logger.error("While drop settings hub widget hub qr_code not found!")
            return False

        widget_hub_id = self.cellWidget(row, 0)
        if widget_hub_id is None:
            logger.error("While drop settings hub widget hub qr_code not found!")
            return False
        master_key = widget_hub_id.get_master_key()
        answer = hub_obj.factory_reset(master_key)
        logger.info("Factory reset hub {}; Answer: {}".format(hub_id, answer))
        if answer.message_type == MessageType.Answer.value and answer.message_key == Answer.DeliveredCommandPerform.value:
            return True
        self.show_error("Невдала спроба скинути налаштування на Хабі {}!".format(hub_id))
        return False

    def _drop_logs(self, hub_id):
        hub_obj = self.client_csa.get_hub_obj(hub_id)
        answer = hub_obj.drop_logs()
        logger.info("Reset {} logs answer: {}".format(hub_id, answer))
        if not answer.message_type and not answer.message_key:
            return False

        return True

    def change_state_register_button(self, row, state):
        button = self.cellWidget(row, self.columnCount() - 1)
        if state:
            button.setText("Зареєструвати")
            button.setStyleSheet(ColorResult.success.value)
        else:
            button.setText("Повернути")
            button.setStyleSheet("background-color: None")

    def check_all_column(self):
        for row in range(self.rowCount()):
            r = []
            for col in range(self.columnCount() - 2):
                w = self.cellWidget(row, col)
                r.append(w.get_state())
            self.change_state_register_button(row, all(r))

    def press_space_start_register_device(self):
        col = 2
        if not self.rowCount():
            return
        for row in range(self.rowCount()):
            widget_connect = self.cellWidget(row, col)
            if widget_connect.isEnabled():
                widget_connect.push_btn_connect.click()
                break
