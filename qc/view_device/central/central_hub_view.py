import logging
import time
import queue
from collections import namedtuple
from functools import partial

from PyQt5 import QtWidgets, QtCore
from PyQt5.QtWidgets import QApplication

from app_setting import AppSetting
from csa_client_qt.csa_client import CsaClient
from csa_client_qt.csa_message import CsaMessage
from csa_client_qt.enum_obj_csa import HubSubType
from qc.componets.power_hub_widget import PowerHubWidget
from qc.database.database_model_csa import DataBaseCsa
from .central_abstract_view import CentralAbstractView

from qc.componets import IdWidget, VisualDefectTable, ConnectHubWidget, TamperWidget, StatusWidget
from qc.enum_alarms import AlarmsNumberHub
from qc.enum_param import Color
from qc.enum_color import ColorResult

logger = logging.getLogger(AppSetting.LOGGER_NAME)


class ThreadCheckVersion(QtCore.QThread):
    signal_result = QtCore.pyqtSignal(str, str)

    def __init__(self, database: DataBaseCsa, queue_hubs: queue.Queue):
        super(ThreadCheckVersion, self).__init__()
        self.database = database
        self.queue_hubs = queue_hubs
        self.is_run = True

    def run(self):
        while self.is_run:
            hub_id = self.queue_hubs.get()
            try:
                logger.info("Get fw version {}".format(hub_id))
                fw_version, *_ = self.database.get_hub_firmware_version(hub_id)
                self.signal_result.emit(hub_id, str(fw_version))
            except Exception as err:
                logger.warning("ERR while check version hub {} - {}".format(hub_id, err))


class CentralHubView(CentralAbstractView):
    dev_type = HubSubType.Hub
    column = namedtuple('column', ['name', 'width'])

    table_head = [
        column("Qr", 120),
        column("Наявність\nдефектів", 200),
        column("Підключення", 100),
        column("Колір", 80),
        column('Тампер', 150),
        column("Зовнішнє\nживлення", 100),
        column("Версія ПЗ", 120),
        column("Канали\nзвязку", 120),
        column("Рівень\nбатареї", 120),
        column("Відмінити\nтестування", 120),
        column("Зареєструвати", 100),
    ]

    def __init__(self, client_csa: CsaClient, parent=None):
        super(CentralHubView, self).__init__(client_csa, parent=parent)
        self.row_height = 100
        self.insert_columns()

        self.setEditTriggers(QtWidgets.QTableWidget.NoEditTriggers)
        self.setSelectionMode(QtWidgets.QTableWidget.NoSelection)
        self.horizontalHeader().setStretchLastSection(True)

        self.queue_check_version = queue.Queue()
        self.thread_check_version = ThreadCheckVersion(self.data_base_model_csa, self.queue_check_version)
        self.thread_check_version.signal_result.connect(self.handler_result)

    def insert_columns(self):
        for index, column in enumerate(self.table_head):
            self.insertColumn(index)
            self.setColumnWidth(index, column.width)
            item_name_column = QtWidgets.QTableWidgetItem(column.name)
            self.setHorizontalHeaderItem(index, item_name_column)

    def add_obj(self, hub_qr_code: str):
        hub_id = hub_qr_code[:8]
        if self.check_exists_hub(hub_id):
            self.show_error("Цей хаб {} вже є в таблиці!".format(hub_id))
            return

        row = self.rowCount()
        self.insertRow(row)
        self.setRowHeight(row, self.row_height)

        widget_id = IdWidget()
        widget_id.set_id_hub(hub_qr_code)
        self.setCellWidget(row, 0, widget_id)

        widget_defects = VisualDefectTable(self.defects, parent=self.qc_main_widget)
        widget_defects.signal_update_defects.connect(self.check_all_column)
        self.setCellWidget(row, 1, widget_defects)

        widget_connect_hub2acc = ConnectHubWidget()
        widget_connect_hub2acc.push_btn_connect.clicked.connect(partial(self.add_hub2account, hub_qr_code[:8], hub_qr_code[8:16]))
        self.setCellWidget(row, 2, widget_connect_hub2acc)

        widget_color = StatusWidget()
        widget_color.set_malfunction_name("Колір цетралі не співпадає")
        widget_color.set_expected_result('white' if hub_qr_code[16] == '1' else 'black')
        self.setCellWidget(row, 3, widget_color)

        widget_tamper = TamperWidget()
        widget_tamper.set_malfunction_name("Тампер")
        self.setCellWidget(row, 4, widget_tamper)

        widget_power_hub = PowerHubWidget()
        widget_power_hub.set_malfunction_name("Живлення 220В")
        self.setCellWidget(row, 5, widget_power_hub)

        widget_version_fw = StatusWidget()
        widget_version_fw.set_malfunction_name("Версія ПЗ")
        widget_version_fw.set_expected_result('')
        self.setCellWidget(row, 6, widget_version_fw)

        widget_connection_channel = StatusWidget()
        widget_connection_channel.set_malfunction_name("Канали звязку")
        widget_connection_channel.set_expected_result("Eth/Gsm")
        self.setCellWidget(row, 7, widget_connection_channel)

        widget_battery_charge = StatusWidget()
        widget_battery_charge.set_malfunction_name("Рівень батареї")
        widget_battery_charge.set_expected_result('100')
        self.setCellWidget(row, 8, widget_battery_charge)

        push_btn_cancel_test_hub = QtWidgets.QPushButton('X')
        push_btn_cancel_test_hub.clicked.connect(partial(self.cancel_test_central, hub_qr_code[:8]))
        self.setCellWidget(row, 9, push_btn_cancel_test_hub)

        push_btn_register_hub = QtWidgets.QPushButton("Повернути")
        push_btn_register_hub.clicked.connect(partial(self.register_central_register_database, hub_id))
        self.setCellWidget(row, 10, push_btn_register_hub)

        if not self.thread_check_version.isRunning():
            self.thread_check_version.start()

    def add_hub2account(self, hub_id, master_key):
        button = self.sender()
        button.setEnabled(False)

        row = self.get_row_by_serv_id(hub_id)
        widget_defects = self.cellWidget(row, 1)
        if widget_defects.defects_popup.get_defects_return():
            self.show_error("В централі {} вибрані дефекти 'тільки на повернення'! "
                            "Датчик не підлягає подальшому тестуванню!".format(hub_id))
            button.setEnabled(True)
            return CsaMessage()
        if (widget_defects.defects_popup.get_defects_grade() or widget_defects.defects_popup.get_custom_defects()) and not widget_defects.check_box_grade_defect.isChecked():
            self.show_error("Якщо обрані дефекти підлягаю класу Б, "
                            "відмітьте це в чекбоксі, якщо ні девайс підлягає поверненню!")
            button.setEnabled(True)
            return CsaMessage()

        hub_obj = self.client_csa.get_hub_obj(hub_id)
        if hub_obj is not None:
            btn_connect = self.sender()
            btn_connect.setStyleSheet(ColorResult.success.value)
            btn_connect.setText("Додано")
            hub_obj = self.client_csa.get_hub_obj(hub_id)
            hub_obj.signal_update_data.connect(self._handler_update_data)
            hub_obj.signal_alarm_object.connect(self._handler_alarm_event)
            return CsaMessage()

        answer = self.client_csa.add_hub2client(hub_id, master_key, "QC_test")
        if not answer.message_type and not answer.message_key:
            self.show_error("Не було відбовіді від сервера!")
        elif answer.message_type == '16' and answer.message_key == '07':
            self.show_error("Хаб ({}) в офлайні".format(hub_id))
        elif answer.message_type == '11' and answer.message_key == '0c':
            btn_connect = self.sender()
            btn_connect.setStyleSheet(ColorResult.success.value)
            btn_connect.setText("Додано")
            while not self.client_csa.get_hub_obj(hub_id):
                QtWidgets.QApplication.processEvents(QtCore.QEventLoop.AllEvents, 1000)
            hub_obj = self.client_csa.get_hub_obj(hub_id)
            hub_obj.signal_update_data.connect(self._handler_update_data)
            hub_obj.signal_alarm_object.connect(self._handler_alarm_event)
            return answer
        elif answer.message_type == '11' and answer.message_key == '13':
            self.show_error("Цей хаб ({}) знаходиться на іншому обліковому записі!".format(hub_id))
        else:
            self.show_error(str(answer))

        button.setEnabled(True)
        return answer

    def cancel_test_central(self, hub_id):
        if not self._you_sure("Ви впевнені що хочете відмінити тестування централі {}".format(hub_id)):
            logger.info("cancel test central while click cancel test {}".format(hub_id))
            return
        self._cancel_test_central(hub_id)

    def _cancel_test_central(self, hub_id):
        with self.change_state_btn_cancel():
            logger.info("Cancel test central {}".format(hub_id))
            hub_obj = self.client_csa.get_hub_obj(hub_id)
            if hub_obj is None:
                logger.info("Hub not found while cancel_test")
            else:
                self.delete_hub2account(hub_id)
            row = self.get_row_by_serv_id(hub_id)
            if row is not None:
                self.removeRow(row)

    def delete_hub2account(self, hub_id):
        answer = self.client_csa.del_hub2client(hub_id)
        logger.info("Delete hub without acc {}".format(answer))

    def _wait_hub_connect2server(self, hub_id):
        hub_obj = self.client_csa.get_hub_obj(hub_id)
        hub_obj.set_parameters('48', '00')

        for _ in range(1000):
            time.sleep(0.01)
            QApplication.processEvents()

        while hub_obj.get_parameters('48') == '00':
            QApplication.processEvents()

        for _ in range(1500):
            time.sleep(0.01)
            QApplication.processEvents()

    def _handler_update_data(self, hub_id):
        hub_obj = self.client_csa.get_hub_obj(hub_id)
        row = self.get_row_by_serv_id(hub_id)
        if row is None:
            return
        self._check_fw_version_expected_result(row, hub_id)
        self._update_color_hub(row, hub_obj.get_parameters('49'))
        self._update_battery_charge(row, hub_obj.get_parameters('01'))
        self._update_active_channel(row, hub_obj.get_parameters('48'), hub_obj.get_parameters('7a'))
        self._update_firmware_version(row, hub_obj.get_parameters('37'))
        self._update_power_hub(row, hub_obj.get_parameters('03'))
        self.check_all_column()

    def _update_power_hub(self, row, hub_powered):
        widget_hub_power = self.cellWidget(row, 5)
        if not hub_powered:
            widget_hub_power.set_status("Невідомо")
            return
        if int(hub_powered, 16):
            widget_hub_power.set_status("Підключене")
            widget_hub_power.set_power_off_flag(True)
        else:
            widget_hub_power.set_status("Відключене")
            widget_hub_power.set_power_on_flag(True)

    def _update_color_hub(self, row, hub_color):
        widget_color = self.cellWidget(row, 3)
        colors_e = {'01': Color.white, '02': Color.black}
        c = colors_e.get(hub_color)
        if not c:
            widget_color.set_status("Невідомо")
        else:
            widget_color.set_status(c.name)

    def _update_firmware_version(self, row, fw_hub):
        widget_firmware_version = self.cellWidget(row, 6)
        widget_firmware_version.set_status(fw_hub)
        if not fw_hub:
            widget_firmware_version.set_status("Невідомо")
        else:
            fw_int_version = int(fw_hub, 16)
            widget_firmware_version.set_status("{}".format(fw_int_version))

    def _update_active_channel(self, row, active_channel, mode_work_gsm):
        widget_status_active_chanell = self.cellWidget(row, 7)
        if not active_channel:
            active_channel = 'Невідомо'
            widget_status_active_chanell.set_status(active_channel)
            return
        active_channel_l = []
        int_chanell = int(active_channel, 16)
        if int_chanell & 0b1:
            active_channel_l.append("Eth")

        if int_chanell & 0b10:
            active_channel_l.append("Wifi")

        if int_chanell & 0b100 and mode_work_gsm != '00' and mode_work_gsm:
            active_channel_l.append("Gsm")

        if active_channel_l:
            widget_status_active_chanell.set_status('/'.join(active_channel_l))
        else:
            widget_status_active_chanell.set_status("Офлайн")

    def _update_battery_charge(self, row, status_battery):
        widget_battery_charge = self.cellWidget(row, 8)
        if not status_battery:
            widget_battery_charge.set_status('Невідомо')
            return
        int_batt_charge = int(status_battery, 16)
        if int_batt_charge > 100:
            widget_battery_charge.set_status("Невстановлено")
            return
        widget_battery_charge.set_status("{}".format(int_batt_charge))

    def _handler_alarm_event(self, hub_id, alarm_number):
        row = self.get_row_by_serv_id(hub_id)
        if alarm_number == AlarmsNumberHub.TamperOff.value:
            widget_tamper = self.cellWidget(row, 4)
            widget_tamper.change_tamper_count_on()
        elif alarm_number == AlarmsNumberHub.TamperOn.value:
            widget_tamper = self.cellWidget(row, 4)
            widget_tamper.change_tamper_count_off()
        elif alarm_number == AlarmsNumberHub.PowerOff.value:
            widget_power_hub = self.cellWidget(row, 5)
            widget_power_hub.set_power_off_flag(True)
        elif alarm_number == AlarmsNumberHub.PowerOn.value:
            widget_power_hub = self.cellWidget(row, 5)
            widget_power_hub.set_power_on_flag(True)

        self.check_all_column()

    def clear_apn(self, hub_id):
        hub_obj = self.client_csa.get_hub_obj(hub_id)
        answer = hub_obj.send_command2console("set {} 20  ".format(hub_obj.id))
        logger.info("Answer clear apn hub {}".format(answer))
        return True

    def set_roaming(self, hub_id):
        hub_obj = self.client_csa.get_hub_obj(hub_id)
        answer = hub_obj.send_command2console("set {} 73 1".format(hub_obj.id))
        logger.info("Answer set hub roaming {}".format(answer))
        return True

    def _check_fw_version_expected_result(self, row, hub_id):
        widget_fw = self.cellWidget(row, 6)
        if widget_fw is None:
            logger.debug("Widget not found")
            return

        # if result exits we not get fw version in database
        if widget_fw._expected_result:
            logger.debug("result fw exists {}".format(hub_id))
            return

        else:
            self.queue_check_version.put(hub_id)
            # run thread if it not running
            if not self.thread_check_version.isRunning():
                self.thread_check_version.start()

    def handler_result(self, hub_id: str, fw_version: str, col=6):
        logger.debug("Handler result fw version {} {}".format(hub_id, fw_version))
        row = self.get_row_by_serv_id(hub_id)
        if row is None:
            logger.warning("Hub not found in table while handler fw version {}".format(hub_id))
            return
        widget_fw = self.cellWidget(row, col)
        widget_fw.set_expected_result(fw_version)
        widget_fw.check_state()
