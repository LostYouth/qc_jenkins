import logging
from collections import namedtuple
from functools import partial

from PyQt5 import QtWidgets

from app_setting import AppSetting
from csa_client_qt.csa_object.csa_object import CsaObjectHub
from csa_client_qt.enum_obj_csa import HubSubType, ObjectsCsa
from qc.componets import *
from qc.componets.widget_sim_holder import WidgetSimHolder
from qc.componets.yavir_blink_widget import YavirBlinkWidget
from qc.enum_alarms import AlarmsNumberHub, WireInput
from .central_hub_view import CentralHubView

logger = logging.getLogger(AppSetting.LOGGER_NAME)


class CentralYavirHubView(CentralHubView):
    dev_type = HubSubType.HubYavir
    zones_count = 4

    column = namedtuple('column', ['name', 'width'])

    table_head = [
        column("Qr", 120),
        column("Наявність\nдефектів", 200),
        column("Підключення", 100),
        column("Колір", 80),
        column('Тампер', 150),
        column("Канали\nзвязку", 120),
        column("Зони", 190),
        column("Перевірка\nсім\nхолдерів", 120),
        column("Cвітлодіод", 150),
        column("Зона\nсирени", 100),
        column("RS485", 100),
        column("Відмінити\nтестування", 120),
        column("Зареєструвати", 100),
    ]

    def add_obj(self, hub_qr_code: str):
        hub_id = hub_qr_code[:8]
        if self.check_exists_hub(hub_id):
            self.show_error("Цей хаб {} вже є в таблиці!".format(hub_id))
            return

        row = self.rowCount()
        self.insertRow(row)
        self.setRowHeight(row, self.row_height)

        widget_id = IdWidget()
        widget_id.set_id_hub(hub_qr_code)
        self.setCellWidget(row, 0, widget_id)

        widget_defects = VisualDefectTable(self.defects, parent=self.qc_main_widget)
        widget_defects.signal_update_defects.connect(self.check_all_column)
        self.setCellWidget(row, 1, widget_defects)

        widget_connect_hub2acc = ConnectHubWidget()
        widget_connect_hub2acc.push_btn_connect.clicked.connect(partial(self.add_hub2account, hub_qr_code[:8], hub_qr_code[8:16]))
        self.setCellWidget(row, 2, widget_connect_hub2acc)

        widget_color = StatusWidget()
        widget_color.set_malfunction_name("Колір")
        widget_color.set_expected_result('white' if hub_qr_code[16] == '1' else 'black')
        self.setCellWidget(row, 3, widget_color)

        widget_tamper = TamperWidget()
        widget_tamper.set_malfunction_name("Тампер")
        self.setCellWidget(row, 4, widget_tamper)

        widget_connection_channel = StatusWidget()
        widget_connection_channel.set_malfunction_name("Канали звязку")
        widget_connection_channel.set_expected_result("Eth/Gsm")
        self.setCellWidget(row, 5, widget_connection_channel)

        widget_zone_yavir = WidgetDeviceYavir(self.zones_count)
        widget_zone_yavir.set_malfunction_name("Активні зони")
        self.setCellWidget(row, 6, widget_zone_yavir)

        widget_sim_holder = WidgetSimHolder()
        widget_sim_holder.set_malfunction_name("Сім холдер")
        self.setCellWidget(row, 7, widget_sim_holder)

        widget_blink = YavirBlinkWidget("Світлодіод", "На охорону")
        widget_blink.set_malfunction_name("Світлодіод статусу хаба")
        widget_blink.check_box.stateChanged.connect(self.check_all_column)
        widget_blink.push_btn_changed.clicked.connect(partial(self._change_hub_state, hub_id))
        self.setCellWidget(row, 8, widget_blink)

        widget_siren = SelectWidget("Сирена")
        widget_siren.set_malfunction_name("Світлодіод роботи сирени")
        widget_siren.signal_update_status.connect(self.check_all_column)
        self.setCellWidget(row, 9, widget_siren)

        widget_rs = SelectWidget("RS485")
        widget_rs.set_malfunction_name("RS485")
        widget_rs.signal_update_status.connect(self.check_all_column)
        self.setCellWidget(row, 10, widget_rs)

        push_btn_cancel_test_hub = QtWidgets.QPushButton('X')
        push_btn_cancel_test_hub.clicked.connect(partial(self.cancel_test_central, hub_qr_code[:8]))
        self.setCellWidget(row, 11, push_btn_cancel_test_hub)

        push_btn_register_hub = QtWidgets.QPushButton("Повернути")
        push_btn_register_hub.clicked.connect(partial(self.register_central_register_database, hub_id))
        self.setCellWidget(row, 12, push_btn_register_hub)

    def _change_hub_state(self, hub_id):
        hub_obj = self.client_csa.get_hub_obj(hub_id)
        if hub_obj is None:
            self.show_error("Хаб {} не приписаний до клієнта!".format(hub_id))
            return

        btn = self.sender()
        if btn.text() == 'На охорону':
            answer = hub_obj.arm()

        else:
            answer = hub_obj.disarm()

        logger.debug("Answer change state {}".format(answer))

    def _update_state_hub(self, row, state_hub):
        widget = self.cellWidget(row, 8)
        if state_hub == '00':
            widget.change_data_btn("На охорону")
        else:
            widget.change_data_btn("З-під охорони")

    def add_group_in_hub(self, hub_obj):
        a = hub_obj.add_group("QC")
        logger.debug("Answer add group in Yavir {}".format(a))

    def add_hub2account(self, hub_id, master_key):
        answer = super(CentralYavirHubView, self).add_hub2account(hub_id, master_key)
        if answer.message_type == '11' and answer.message_key == '0c':
            hub_obj = self.client_csa.get_hub_obj(hub_id)
            if not hub_obj.get_objects(ObjectsCsa.Group):
                hub_obj.add_group("QC")
            hub_obj.write_param('27', "27{:06x}".format(1), ['06', '01'])
            for i in range(self.zones_count):
                hub_obj.write_param('26', "26{:06x}".format(i + 1),
                                    ['0a', '02', '0c', '01', '04', '{:08x}'.format(1)])
            self._subscible_yavir_device(hub_obj)

    def _subscible_yavir_device(self, hub_obj: CsaObjectHub):
        for obj in hub_obj.get_objects(ObjectsCsa.WireInput):
            obj.signal_alarm_object.connect(partial(self._handler_alarms_zone, hub_obj.id))
            logger.info("Connect alarm for WireInput {}".format(obj))

    def _handler_alarms_zone(self, hub_id, obj_id, alarm_number):
        if alarm_number == WireInput.ImpulseFailure:
            row = self.get_row_by_serv_id(hub_id)
            widget_zone = self.cellWidget(row, 6)
            widget_zone.change_state_device(obj_id)
        self.check_all_column()

    def _handler_alarm_event(self, hub_id, alarm_number):
        row = self.get_row_by_serv_id(hub_id)
        if alarm_number == AlarmsNumberHub.TamperOff.value:
            widget_tamper = self.cellWidget(row, 4)
            widget_tamper.change_tamper_count_on()
        elif alarm_number == AlarmsNumberHub.TamperOn.value:
            widget_tamper = self.cellWidget(row, 4)
            widget_tamper.change_tamper_count_off()
        self.check_all_column()

    def _handler_update_data(self, hub_id):
        hub_obj = self.client_csa.get_hub_obj(hub_id)
        row = self.get_row_by_serv_id(hub_id)
        if row is None:
            logger.info("Hub {} not found in table {}".format(hub_id, self.__class__.__name__))
            return
        self._update_color_hub(row, hub_obj.get_parameters('49'))
        self._update_active_channel(row, hub_obj.get_parameters('48'), hub_obj.get_parameters('7a'))
        self._update_sim_holder(
            row,
            hub_obj.get_parameters('5f'),
            hub_obj.get_parameters('85'),
            hub_obj.get_parameters('7a')
        )
        self._update_state_hub(row, hub_obj.get_parameters('08'))
        self.check_all_column()

    def _update_sim_holder(self, row, sim_cart_state, sim_holder, mode_gsm):
        widget_sim_holder = self.cellWidget(row, 7)
        if not sim_cart_state or not sim_holder or not mode_gsm:
            return
        sim_cart_state = int(sim_cart_state, 16)
        sim_holder = int(sim_holder, 16)
        if sim_cart_state == 0 and sim_holder == 0 and mode_gsm != '00':
            widget_sim_holder.set_state_one(True)
        elif sim_cart_state == 0 and sim_holder == 1 and mode_gsm != '00':
            widget_sim_holder.set_state_second(True)

    def _update_active_channel(self, row, active_channel, mode_gsm):
        widget_status_active_chanell = self.cellWidget(row, 5)
        if not active_channel:
            active_channel = 'Невідомо'
            widget_status_active_chanell.set_status(active_channel)
            return
        active_channel_l = []
        int_chanell = int(active_channel, 16)
        if int_chanell & 0b1:
            active_channel_l.append("Eth")

        if int_chanell & 0b10:
            active_channel_l.append("Wifi")

        if int_chanell & 0b100 and mode_gsm != '00' and mode_gsm:
            active_channel_l.append("Gsm")

        if active_channel_l:
            widget_status_active_chanell.set_status('/'.join(active_channel_l))
        else:
            widget_status_active_chanell.set_status("Офлайн")
