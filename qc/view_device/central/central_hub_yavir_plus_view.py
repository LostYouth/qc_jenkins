from csa_client_qt.enum_obj_csa import HubSubType, ObjectsCsa

from .central_yavir_hub_view import CentralYavirHubView


class CentralHubYavirPlusView(CentralYavirHubView):
    dev_type = HubSubType.HubYavirPlus
    zones_count = 8

    def add_hub2account(self, hub_id, master_key):
        answer = super(CentralYavirHubView, self).add_hub2account(hub_id, master_key)
        if answer.message_type == '11' and answer.message_key == '0c':
            hub_obj = self.client_csa.get_hub_obj(hub_id)
            if not hub_obj.get_objects(ObjectsCsa.Group):
                hub_obj.add_group("QC")
            hub_obj.write_param('27', "27{:06x}".format(1), ['06', '01'])
            # needed for RS485 test
            hub_obj.write_param('28', "28{:06x}".format(1), ['0f', '01', '10', '01'])
            for i in range(self.zones_count):
                hub_obj.write_param('26', "26{:06x}".format(i + 1),
                                    ['0a', '02', '0c', '01', '04', '{:08x}'.format(1)])
            self._subscible_yavir_device(hub_obj)
