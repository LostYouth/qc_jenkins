# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'qc_login_widget.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_LoginWidget(object):
    def setupUi(self, LoginWidget):
        LoginWidget.setObjectName("LoginWidget")
        LoginWidget.resize(598, 117)
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout(LoginWidget)
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_4.addItem(spacerItem)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.line_edit_login_name = QtWidgets.QLineEdit(LoginWidget)
        self.line_edit_login_name.setObjectName("line_edit_login_name")
        self.gridLayout.addWidget(self.line_edit_login_name, 0, 0, 1, 1)
        self.line_edit_login_password = QtWidgets.QLineEdit(LoginWidget)
        self.line_edit_login_password.setObjectName("line_edit_login_password")
        self.gridLayout.addWidget(self.line_edit_login_password, 1, 0, 1, 1)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem1)
        self.push_btn_login = QtWidgets.QPushButton(LoginWidget)
        self.push_btn_login.setObjectName("push_btn_login")
        self.horizontalLayout_2.addWidget(self.push_btn_login)
        self.gridLayout.addLayout(self.horizontalLayout_2, 2, 0, 1, 1)
        self.horizontalLayout.addLayout(self.gridLayout)
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem2)
        self.gridLayout_2 = QtWidgets.QGridLayout()
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.combo_box_ports = QtWidgets.QComboBox(LoginWidget)
        self.combo_box_ports.setObjectName("combo_box_ports")
        self.gridLayout_2.addWidget(self.combo_box_ports, 0, 0, 1, 1)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        spacerItem3 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem3)
        self.push_btn_connect_port = QtWidgets.QPushButton(LoginWidget)
        self.push_btn_connect_port.setObjectName("push_btn_connect_port")
        self.horizontalLayout_3.addWidget(self.push_btn_connect_port)
        self.gridLayout_2.addLayout(self.horizontalLayout_3, 1, 0, 1, 1)
        self.horizontalLayout.addLayout(self.gridLayout_2)
        self.horizontalLayout_4.addLayout(self.horizontalLayout)
        spacerItem4 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_4.addItem(spacerItem4)

        self.retranslateUi(LoginWidget)
        QtCore.QMetaObject.connectSlotsByName(LoginWidget)

    def retranslateUi(self, LoginWidget):
        _translate = QtCore.QCoreApplication.translate
        LoginWidget.setWindowTitle(_translate("LoginWidget", "Form"))
        self.push_btn_login.setText(_translate("LoginWidget", "login"))
        self.push_btn_connect_port.setText(_translate("LoginWidget", "PushButton"))
