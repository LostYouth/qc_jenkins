import logging
import os
import pymysql
import string
import sys

from collections import OrderedDict
from pymysql import DatabaseError
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtCore import QEvent, Qt
from PyQt5.QtSerialPort import QSerialPort, QSerialPortInfo

from ajax.db.exc import NoResultFound
from app_setting import AppSetting
from qc.componets import ErrorPopupWidget
from qc.database.database_production import DataBaseProduction
from qc.enum_param import FrequencyString
from qc.enum_param_where_hub import EnumParamWhereHub

from qc.plugins.check_last_stages import CheckLastStage
from qc.plugins.thread_get_stat_devices import ThreadGetStatDevices
from qc.security import KeyDatabase
from qc.view_device.bridge.oc_bridge import OcBridge
from qc.view_device.bridge.uart_bridge import uartBridge
from qc.view_device.central.central_hub_view import CentralHubView
from qc.view_device.central.central_hub_2_view import CentralHub2View
from qc.view_device.central.central_hub_plus_view import CentralHubPlusView
from qc.view_device.central.central_yavir_hub_view import CentralYavirHubView
from qc.view_device.central.central_hub_yavir_plus_view import CentralHubYavirPlusView
from qc.view_device.device.combi_device_view import CombiDeviceView
from qc.view_device.device.door_device_view import DoorDeviceView
from qc.view_device.device.door_plus_device_view import DoorPlusDeviceView
from qc.view_device.device.fire_device_plus_view import FireDevicePlusView
from qc.view_device.device.fire_device_view import FireDeviceView
from qc.view_device.device.glass_device_view import GlassDeviceView
from qc.view_device.device.home_siren_device_view import HomeSirenDeviceView
from qc.view_device.device.keypad_device_view import KeypadDeviceView
from qc.view_device.device.leaks_device_view import LeaksDeviceView
from qc.view_device.device.motion_device_cam_view import MotionDeviceCamView
from qc.view_device.device.motion_device_curtaine import MotionDeviceCurtain
from qc.view_device.device.motion_device_outdoor_view import MotionDeviceOutdoorView
from qc.view_device.device.motion_device_view import MotionDeviceView
from qc.view_device.device.panic_button_device_view import PanicButtonDeviceView
from qc.view_device.device.motion_device_plus_view import MotionDevicePlusView
from qc.view_device.device.range_extender_device_view import RangeExtenderDeviceView
from qc.view_device.device.relay_device_view import RelayDeviceView
from qc.view_device.device.socket_device_view import SocketDeviceView
from qc.view_device.device.space_control_device_view import SpaceControlDeviceView
from qc.view_device.device.street_siren_device_view import StreetSirenDeviceView
from qc.view_device.device.transmitter_device_view import TransmitterDeviceView
from qc.view_device.device.wallswitch_device_view import WallSwitchDeviceView
from qc.window.available_update_app import AvailableUpdateWidget
from qc.window.popup.changed_color import ChangedColor
from qc.window.popup.changed_frequency import ChangedFrequency
from qc.window.popup.manager_csa_obj.change_wifi import ChangeWifiName
from qc.window.popup.manager_csa_obj.control_hub.control_hub import ControlHub
from qc.window.popup.manager_csa_obj.control_object.control_object import ControlObject
from qc.window.popup.reconnect_widget import ReconnectWidget
from qc.window.popup.widget_change_frame_interval import WidgetChangeFrameInterval
from qc.window.ui.qc_main_widget import Ui_main_qc_widget
from qc.window.popup.manager_csa_obj.manager_csa_obj import ManagerCsaObj
from csa_client_qt.csa_client import CsaClient

logger = logging.getLogger(AppSetting.LOGGER_NAME)


DEFAULT_PASSWORD_QC_CLIENT = '123'


class QcMainWidget(QtWidgets.QWidget, Ui_main_qc_widget):
    def __init__(self, operator_login: str, operator_full_name: str, parent=None):
        super(QcMainWidget, self).__init__(parent=parent)
        self.setupUi(self)
        self.setEnabled(False)

        self.label_operator_qc.setText(operator_full_name)

        self._operator_login = operator_login

        self.database_prod = DataBaseProduction(old_db_keys=KeyDatabase.get_old_keys_production(),
                                                db_keys=KeyDatabase.get_keys_production())

        # Connect Scanner
        self.scanner_qr_code = QSerialPort()
        self.scanner_qr_code.setBaudRate(QSerialPort.Baud115200)
        self.scanner_qr_code.readyRead.connect(self._handler_scanner_qr_code)
        self.pushButton_connect_scanner.clicked.connect(self.connect_scanner)
        self.init_scanner_ports()
        self.comboBox_ports_scanner.installEventFilter(self)

        #
        self.check_last_stage = CheckLastStage()
        self.label_color_device.setText(self.check_last_stage.current_color().name)

        # Error Popup
        self.err_widget = ErrorPopupWidget(parent=self)

        self._options_qc = self.get_options_qc()
        user_option = self._get_option_for_operator(self._operator_login)

        if not user_option:
            sys.exit(1)

        client_email, client_id, master_key = user_option.split('=')

        # create client csa
        self.csa_client = self.create_client_csa(client_email)
        self.signal_client_csa()

        # All device test at QC
        self.view_obj = [
            CentralHubView(self.csa_client, parent=self),
            CentralHubPlusView(self.csa_client, parent=self),
            CentralYavirHubView(self.csa_client, parent=self),
            CentralHubYavirPlusView(self.csa_client, parent=self),
            CentralHub2View(self.csa_client, parent=self),
            DoorDeviceView(self.csa_client, parent=self),
            DoorPlusDeviceView(self.csa_client, parent=self),
            GlassDeviceView(self.csa_client, parent=self),
            CombiDeviceView(self.csa_client, parent=self),
            MotionDeviceView(self.csa_client, parent=self),
            MotionDeviceCamView(self.csa_client, parent=self),
            MotionDevicePlusView(self.csa_client, parent=self),
            MotionDeviceCurtain(self.csa_client, parent=self),
            MotionDeviceOutdoorView(self.csa_client, parent=self),
            LeaksDeviceView(self.csa_client, parent=self),
            HomeSirenDeviceView(self.csa_client, parent=self),
            KeypadDeviceView(self.csa_client, parent=self),
            StreetSirenDeviceView(self.csa_client, parent=self),
            FireDevicePlusView(self.csa_client, parent=self),
            FireDeviceView(self.csa_client, parent=self),
            RangeExtenderDeviceView(self.csa_client, parent=self),
            WallSwitchDeviceView(self.csa_client, parent=self),
            RelayDeviceView(self.csa_client, parent=self),
            SpaceControlDeviceView(self.csa_client, parent=self),
            PanicButtonDeviceView(self.csa_client, parent=self),
            SocketDeviceView(self.csa_client, parent=self),
            TransmitterDeviceView(self.csa_client, parent=self),
        ]
        # Special ocBridge
        self._oc_bridge_view = OcBridge(parent=self)
        self.view_obj.append(self._oc_bridge_view)
        # Special uartBridge
        self._uart_bridge_view = uartBridge(parent=self)
        self.view_obj.append(self._uart_bridge_view)

        self.reconnect_widget = ReconnectWidget(parent=self)

        # Where hub select country or pro account
        self.hubs_destinations = {}
        layout_where_centra = self.groupBox_hubs_central.layout()
        for index, hub in enumerate(EnumParamWhereHub):
            radio_btn = QtWidgets.QRadioButton(hub.value.name)
            radio_btn.toggled.connect(self._where_central)
            layout_where_centra.addWidget(radio_btn)
            radio_btn.click()
            if index == 0:
                radio_btn.setChecked(True)
            self.hubs_destinations[hub] = radio_btn

        # Timer reconnect
        self.__time_reconnect = 2
        self.timer_reconnect = QtCore.QTimer()
        self.timer_reconnect.setInterval(self.__time_reconnect * 1000)
        self.timer_reconnect.timeout.connect(self.reconnect_csa_client)
        self.timer_reconnect.singleShot(2000, lambda: self.widget_changed_frequency.buttons_freq[0].click())

        # Manager Object if need del obj on work hub
        self.manager_obj = ManagerCsaObj(self.csa_client, parent=self)

        self.current_frequency_name = 'EU'
        # Change frequency if device RU
        self.widget_changed_frequency = ChangedFrequency(self.csa_client, parent=self)
        self.widget_changed_frequency.set_master_key(master_key)
        self.widget_changed_frequency.signal_change_frequency.connect(self._change_frequency_ui)
        self.widget_changed_frequency.signal_error_change_freq.connect(self._error_change_frequency)

        # Change color
        self.widget_changed_color = ChangedColor(parent=self)
        self.widget_changed_color.signal_change_color.connect(self._change_check_color)

        # Change Wifi name if test HUBPlus
        self.widget_change_wifiname = ChangeWifiName(parent=self)
        self.widget_change_wifiname.signal_change_wifi_name.connect(self._change_wifi_name)
        self._change_wifi_name('AjaxGuest_2G')

        # EVENT FILTER for all view devices
        self.event_filter_view = EventFilterTable()
        for view in self.view_obj:
            self.groupBox_view_device.layout().addWidget(view)
            view.setVisible(False)
            view.installEventFilter(self.event_filter_view)

        # This variable - active table on main window
        self.active_table = self.view_obj[-1]
        self.change_view_obj_testing(self.view_obj[0].dev_type.name)

        defects = self.get_defects_devices()
        if not defects:
            return
        self.defects = {key.replace(' ', ''): val for key, val in defects.items()}

        # @! @! @! @!@! @!@! @!@! @!@! @!@! @!@! @!@! @!@! @!@! @!@! @!@! @!@! @!@! @!@! @!@! @!@! @!@! @!
        self._pro_accounts = self.get_pro_account_db()
        # @! @! @! @!@! @!@! @!@! @!@! @!@! @!@! @!@! @!@! @!@! @!@! @!@! @!@! @!@! @!@! @!@! @!@! @!@! @!
        logger.info("PRO accounts {}".format(repr(self._pro_accounts)))
        if AppSetting.DEBUG:
            self._pro_accounts = ["nedobytko.r@ajax.systems:1818"] + self._pro_accounts

        for view in self.view_obj:
            try:
                self._set_defects_view(view)
            except KeyError as err:
                logger.error("Key error set defects: {}".format(err))
                continue

        self._where_hub = EnumParamWhereHub.Standart

        self.control_hubs = ControlHub(self.csa_client, parent=self)
        self.control_object = ControlObject(self.csa_client, parent=self)

        # This timer started thread update stat if thread does not start
        self.timer_update_stat_qc = QtCore.QTimer()
        self.timer_update_stat_qc.setInterval(150 * 1000)
        self.timer_update_stat_qc.timeout.connect(self._update_stat_devices)
        self.timer_update_stat_qc.start()

        # Thread update stat on main window
        self._thread_get_stat_devices = ThreadGetStatDevices(self.get_operator_name(), self._operator_login)
        self._thread_get_stat_devices.signal_result.connect(self._handl_result_count_devices_for_stat)
        self._thread_get_stat_devices.start()
        self.pushButton_refresh_stat.clicked.connect(self._update_stat_devices)

        self._update_level_rssi_work_hub()

        # Timer update lvl RSSI on work hub
        self.timer_update_rssi = QtCore.QTimer()
        self.timer_update_rssi.setInterval(2 * 1000)
        self.timer_update_rssi.timeout.connect(self._update_level_rssi_work_hub)
        self.timer_update_rssi.start()

        # Change frame
        self.widget_change_frame_work_hub = WidgetChangeFrameInterval()

    @staticmethod
    def create_client_csa(client_email, client_password=DEFAULT_PASSWORD_QC_CLIENT):
        c = CsaClient(client_email=client_email, client_pass=client_password)
        return c

    def signal_client_csa(self):
        self.csa_client.signal_status_client.connect(self._change_status_csa_client)
        self.csa_client.signal_error_connect_socket.connect(self._handler_error_open_socket)

    def _update_stat_devices(self):
        """Update counter devices in head window"""
        self._thread_get_stat_devices.start()

    def _handl_result_count_devices_for_stat(self, r):
        stat = OrderedDict()
        for view in self.view_obj:
            stat[view.get_device_type_name()] = 0
        for type_dev in r:
            if type_dev in stat.keys():
                stat[type_dev] += 1
        logger.debug("Stat {}".format(stat))
        general_count = sum([c for c in stat.values()])
        data_text = "Загальна к-ть: {} \t\t".format(general_count) + \
                    "   ".join(["{}: {}".format(self._short_name_obj_type(dev_type), count) for dev_type, count in
                                stat.items()])
        self.label_stat.setText(data_text)
        self.label_stat.setToolTip(
            "\n".join(["{}:\t {}".format(self._short_name_obj_type(dev_type), dev_type) for dev_type in stat.keys()])
        )

    def _update_level_rssi_work_hub(self):
        work_hubs = [hub for hub in self.csa_client._hubs if hub.work_hub_qc]
        if work_hubs:
            hub_work, *_ = work_hubs
            rssi1, rssi2 = hub_work.get_parameters('34'), hub_work.get_parameters('4f')
            if rssi1 and rssi2:
                self.label_status_rssi.setText("Рівень шуму на робочому хабі: {}/{}".format(
                    int(rssi1, 16) - 65534, int(rssi2, 16) - 65534
                ))
        else:
            self.label_status_rssi.setText("Невідомий рівень шуму!")

    @staticmethod
    def _short_name_obj_type(type_name: str):
        s = ''
        for i in type_name:
            if i in string.ascii_uppercase:
                s += i
        return s

    def show_control_obj(self):
        self.control_object.show()

    def show_control_hubs(self):
        self.control_hubs.show()

    def get_pro_accounts(self):
        return self._pro_accounts

    def _where_central(self):
        current_mode_hub = self.sender().text()
        for hub_w in EnumParamWhereHub:
            if hub_w.value.name == current_mode_hub:
                self._where_hub = hub_w
                logger.info("Change mode central on {}".format(hub_w))
                break
        else:
            logger.info("Cant change find mode in {}".format(EnumParamWhereHub))

    def get_mode_hub(self):
        return self._where_hub

    def _set_defects_view(self, view_device):
        defects_device = dict()
        defects_device["class_b"] = self.defects['general']['class B'] + self.defects[view_device.get_device_type_name()]['class B']
        defects_device['return'] = self.defects['general']['return'] + self.defects[view_device.get_device_type_name()]['return']
        logger.info("Set defects {}".format(view_device.get_device_type_name()))
        view_device.set_defects(defects_device)

    def change_wifi(self):
        self.widget_change_wifiname.show()

    def _change_wifi_name(self, new_wifi_name):
        view_hub_plus = self.view_obj[1]
        view_hub_plus.wifi_name = new_wifi_name
        self.label_wifi_hub_plus.setText(new_wifi_name)

    def get_options_qc(self):
        try:
            options_qc = self.database_prod.get_option_qc()
            assert options_qc
        except DatabaseError as err:
            QtWidgets.QMessageBox.warning(self, "Помилка", "Перевірте доступ до інтернету!\n{}".format(err))
            self.parent().close()
            return
        except NoResultFound:
            logger.error('Prod options for app "QC" were not found')
            self.parent().close()
        except AssertionError:
            QtWidgets.QMessageBox.information(self, "Помилка", "Невизначений робочий аккаунт для вас!")
            self.parent().close()
            return
        else:
            return options_qc

    def _get_option_for_operator(self, operator_login):
        acc = self._options_qc['accounts'].get(operator_login, None)
        if acc is None:
            QtWidgets.QMessageBox.information(self, "Помилка", "Невизначений робочий аккаунт для вас!")
            self.parent().close()
        return acc

    def get_defects_devices(self):
        defects = self._options_qc.get('defects', None)
        if not defects:
            logger.error("Not found defects for QC application")
            QtWidgets.QMessageBox.information(self, "Помилка", 'Невизначені параметри "дефектів" для девайсів!')
            return dict()
        return defects

    def get_pro_account_db(self):
        acc = self._options_qc.get('share_pro_acc_hub', None)
        if not acc:
            QtWidgets.QMessageBox.warning(self, 'Помилка', 'Невизначенні про аккаунти для Хабів!')
            self.parent().close()
            return []
        return acc

    def show_error(self, message: str):
        self.err_widget.set_message_data(str(message))
        self.err_widget.show()

    def get_operator_name(self):
        return self.label_operator_qc.text()

    def change_frequency(self):
        logger.info("Change frequency")
        self.widget_changed_frequency.show()

    def change_frame(self):
        logger.info("Change frame")
        self.widget_change_frame_work_hub.show()

    def change_color_show_widget(self):
        self.widget_changed_color.show()

    def _change_check_color(self, color):
        self.widget_changed_color.close()
        self.label_color_device.setText(color.name)
        self.check_last_stage.change_current_color(color)

    def enable_destinations(self):
        if not self.current_frequency_name:
            return

        if 'Hub' not in self.active_table.get_device_type_name():
            self.disable_all_destinations()
        elif self.current_frequency_name == 'RU':
            self.enable_ru_destination()
        else:
            self.enable_eu_destinations()

    def disable_all_destinations(self):
        for dest in self.hubs_destinations:
            self.hubs_destinations[dest].setEnabled(False)

    def enable_ru_destination(self):
        for dest in self.hubs_destinations:
            if dest == EnumParamWhereHub.Russian:
                self.hubs_destinations[dest].setEnabled(True)
                self.hubs_destinations[dest].setChecked(True)
            else:
                self.hubs_destinations[dest].setEnabled(False)

    def enable_eu_destinations(self):
        for dest in self.hubs_destinations:
            if dest != EnumParamWhereHub.Russian:
                self.hubs_destinations[dest].setEnabled(True)
            else:
                self.hubs_destinations[dest].setEnabled(False)
        if not any([r.isChecked() for r in self.hubs_destinations.values() if r.isEnabled()]):
            self.hubs_destinations[EnumParamWhereHub.Standart].setChecked(True)

    def _change_frequency_ui(self, name):
        self.current_frequency_name = name
        freq = FrequencyString.__getattr__(name)
        self.check_last_stage.change_current_frequency(freq)
        self.label_frequency_device.setText("{} {} {}".format(freq.name, *freq.value))
        self._update_level_rssi_work_hub()
        # set work hub for change len frame on work hub
        self._set_work_hub_widget_change_frame()
        self.enable_destinations()

    def _set_work_hub_widget_change_frame(self):
        for hub in self.csa_client._hubs:
            if hub.work_hub_qc:
                self.widget_change_frame_work_hub.set_work_qc_hub(hub)
                break
        else:
            logger.warning("Work hub QC not found!")

    def _error_change_frequency(self, data: list):
        if not data:
            self.err_widget.set_message_data("На аккаунті не знайдено ні одного Хаба! Додайте свій робочий Хаб!")
        else:
            self.err_widget.set_message_data("Невдалось встановити робочу частоту для Хаба!\nДеталі: {}".format(
                "\n".join(["hub_id: {} answer: {} {}".format(hub_id, answer.message_type, answer.message_key) for hub_id, answer in data])
            ))
        self.err_widget.show()

    def get_current_color(self):
        return self.check_last_stage.current_color()

    def set_operator_name(self, operator: str):
        self.label_operator_qc.setText(operator)

    def _change_status_csa_client(self, state):
        self.setEnabled(state)
        if state:
            self.reconnect_widget.close()
            if self.timer_reconnect.isActive():
                self.timer_reconnect.stop()
        else:
            self.reconnect_widget.show()

    def _handler_error_open_socket(self):
        self.timer_reconnect.setInterval(10 * 1000)
        self.timer_reconnect.start()

    def reconnect_csa_client(self):
        self.csa_client.reconnect()

    def eventFilter(self, QObject, QEvent):
        if QObject is self.comboBox_ports_scanner:
            if QEvent.type() == QtCore.QEvent.MouseButtonPress:
                self.init_scanner_ports()
        return super(QcMainWidget, self).eventFilter(QObject, QEvent)

    def init_scanner_ports(self):
        self.comboBox_ports_scanner.clear()
        ports = QSerialPortInfo.availablePorts()
        for p in ports:
            if not p.isBusy():
                self.comboBox_ports_scanner.addItem(p.portName())

    def connect_scanner(self):
        text = self.pushButton_connect_scanner.text()
        if text == "Підключитися":
            self.comboBox_ports_scanner.setEnabled(False)
            if self.open_scanner_qr():
                self.pushButton_connect_scanner.setText("Відключитися")
        else:
            self.pushButton_connect_scanner.setText("Підключитися")
            self.comboBox_ports_scanner.setEnabled(True)
            self.close_scanner_qr()

    def open_scanner_qr(self):
        current_port = self.comboBox_ports_scanner.currentText()
        self.scanner_qr_code.setPortName(current_port)
        result_open = self.scanner_qr_code.open(QtCore.QIODevice.ReadOnly)
        if not result_open:
            self.show_error("Невдалось відкрити порт сканнера!")
            logger.warning("Fail open scanner {}".format(current_port))
            return False
        return True

    def change_view_obj_testing(self, obj_type):
        if self.active_table.get_device_type_name() == obj_type:
            return True

        if self.active_table.rowCount() != 0:
            logger.debug("Row count in table not eq zero!")
            return False

        for view in self.view_obj:
            if view.get_device_type_name() == obj_type:
                self.active_table.setVisible(False)
                view.setVisible(True)
                self.active_table = view
                self.groupBox_view_device.setTitle(obj_type)
                self.enable_destinations()
                break
        else:
            logger.error("Not found view for {}".format(obj_type))
            return False
        return True

    def close_scanner_qr(self):
        if self.scanner_qr_code.isOpen():
            self.scanner_qr_code.close()

    def _handler_scanner_qr_code(self):
        if self.scanner_qr_code.canReadLine():
            try:
                line_data = self.scanner_qr_code.readLine()
                self.scanner_qr_code.readAll()
                line = str(line_data.data(), encoding="utf8")
                qr_code = line.strip()
            except Exception as err:
                logger.error("{}".format(err))
            else:
                logger.debug("scanned qr code {}".format(qr_code))

                self.handler_qr_code(qr_code)

    def handler_qr_code(self, qr_code: str):
        # Check start search bridge device
        if self._oc_bridge_view.widget_search_device2bridge.isVisible():
            self._oc_bridge_view.set_device_search(qr_code)
            return
        # Check start search uartBridge device
        if self._uart_bridge_view.widget_search_device2bridge.isVisible():
            self._uart_bridge_view.set_device_search(qr_code)
            return

        if hasattr(self.active_table, 'widget_search_device') and self.active_table.widget_search_device.isVisible():
            logger.warning("QR code ignore because open search device popup {}".format(qr_code))
            return

        try:
            obj_type = self.check_last_stage.check_last_stage(qr_code)
            logger.info("Object type: {}".format(obj_type))
        except AssertionError as err:
            self.show_error(str(err))
        except pymysql.err.DatabaseError as err:
            self.show_error(str(err))
        except Exception as err:
            logger.error("Error while scan device {} {}".format(qr_code, err))
        else:
            if 'Hub' in obj_type:
                if (self.get_mode_hub() == EnumParamWhereHub.Russian and self.current_frequency_name != 'RU') or \
                        (self.get_mode_hub() != EnumParamWhereHub.Russian and self.current_frequency_name == 'RU'):
                    self.show_error('Частота не співпадає з країною, в яку виготовляється хаб')
                    logger.error('Frequency {} is wrong for country {}'.format(self.current_frequency_name,
                                                                               self.get_mode_hub()))
                    return

            if self.active_table.rowCount() > 10:
                self.show_error("Тестити можна лише по 10 девайсів")

            elif self.change_view_obj_testing(obj_type):
                if hasattr(self.active_table, 'get_work_hub'):
                    try:
                        self.active_table.get_work_hub()
                    except Exception as hub_not_found:
                        self.show_error('Робочий хаб не знайдено для тестування девайсів!')
                        return
                qr_code = qr_code.replace('-', '')
                self.active_table.add_obj(qr_code)
            else:
                self.show_error("Тип датчика не співпадає з вже відсканованими девайсами!")


class EventFilterTable(QtCore.QObject):
    def __init__(self, *args):
        super(EventFilterTable, self).__init__(*args)

    def eventFilter(self, obj, event):
        if event.type() == QEvent.KeyPress:
            if event.key() == Qt.Key_Space:
                obj.press_space_start_register_device()
        return super(EventFilterTable, self).eventFilter(obj, event)
