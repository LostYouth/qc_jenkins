import logging
from functools import partial

from PyQt5 import QtWidgets, QtCore

from app_setting import AppSetting
from qc.enum_param import FrequencyString, Frequency
from csa_client_qt.csa_client import CsaClient

logger = logging.getLogger(AppSetting.LOGGER_NAME)


class ChangedFrequency(QtWidgets.QWidget):
    signal_change_frequency = QtCore.pyqtSignal(str)
    signal_error_change_freq = QtCore.pyqtSignal(list)

    def __init__(self, client_csa: CsaClient, parent=None):
        super(ChangedFrequency, self).__init__(parent=parent, flags=QtCore.Qt.Dialog)

        l = QtWidgets.QVBoxLayout()
        self.setLayout(l)

        self.setMaximumSize(300, 150)

        self.client_csa = client_csa

        self._master_key = 'e313350c'

        self.buttons_freq = []

        for freq in FrequencyString:
            btn = QtWidgets.QPushButton("{} {} {}".format(freq.name, *freq.value))
            l.addWidget(btn)
            self.buttons_freq.append(btn)
            btn.clicked.connect(partial(self.send_change_freq, freq.name))

    def set_default_frequency(self):
        btn = self.buttons_freq[0]
        btn.click()

    def send_change_freq(self, name):
        self.close()
        local_freq, *_ = self.sender().text().split(' ')
        freq_enum = Frequency.__getattr__(name)
        answers = []
        for hub_obj in self.client_csa._hubs:
            answer = hub_obj.change_frequency2(self._master_key, *freq_enum.value)
            if answer is not None and answer.message_type == '16' and answer.message_key == '02':
                hub_obj.work_hub_qc = True
                self.signal_change_frequency.emit(name)
                break
            else:
                answers.append((hub_obj.id, answer))
        else:
            self.signal_error_change_freq.emit(answers)

        [logger.debug("Answers change freq Hub_id {}: {}".format(hub_id, ans)) for hub_id, ans in answers]

    def set_master_key(self, master_key_work_hub: str):
        self._master_key = master_key_work_hub

    def move_center(self):
        self.move(
            self.parent().window().frameGeometry().topLeft() + self.parent().window().rect().center() - self.rect().center())

    def show(self):
        super(ChangedFrequency, self).show()
        self.move_center()
