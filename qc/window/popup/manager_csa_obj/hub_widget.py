from PyQt5 import QtWidgets


from csa_client_qt.csa_client import CsaObjectHub


class HubWidget(QtWidgets.QWidget):
    def __init__(self, hub_obj: CsaObjectHub, parent=None):
        super(HubWidget, self).__init__(parent=parent)
        self.hub_obj = hub_obj
        l = QtWidgets.QVBoxLayout()
        self.setLayout(l)

        self.label_hub = QtWidgets.QLabel(repr(hub_obj))
        l.addWidget(self.label_hub)
