import logging

from PyQt5 import QtWidgets, QtCore

from app_setting import AppSetting
from csa_client_qt.csa_object.csa_object import CsaObjectHub

logger = logging.getLogger(AppSetting.LOGGER_NAME)


class WidgetHub(QtWidgets.QWidget):
    singnal_delete_hub = QtCore.pyqtSignal(str)

    def __init__(self, hub_obj: CsaObjectHub):
        super(WidgetHub, self).__init__()
        self._hub_obj = hub_obj

        main_layout = QtWidgets.QHBoxLayout()
        self.setLayout(main_layout)

        self.label_name_hub = QtWidgets.QLabel("{}".format(self._hub_obj))
        self.label_status_security_hub = QtWidgets.QLabel()

        self.push_btn_change_state = QtWidgets.QPushButton("")
        self.push_btn_change_state.clicked.connect(self._change_state_hub)
        main_layout.addWidget(self.label_name_hub)
        main_layout.addWidget(self.label_status_security_hub)
        main_layout.addWidget(self.push_btn_change_state)

        self.push_btn_delete_hub = QtWidgets.QPushButton("Видалити хаб")
        main_layout.addWidget(self.push_btn_delete_hub)
        self.push_btn_delete_hub.clicked.connect(self._delete_hub)

        self._hub_obj.signal_update_data.connect(self._handler_hub_status)

        self._handler_hub_status()

    def _delete_hub(self):
        self.singnal_delete_hub.emit(self._hub_obj.id)

    def get_hub_id(self):
        return self._hub_obj.id

    def _handler_hub_status(self):
        state_hub = self._hub_obj.get_parameters("08")
        self.push_btn_change_state.setText("Невідомо")
        if state_hub == '00':
            self.label_status_security_hub.setText("не на охороні")
            self.push_btn_change_state.setText("поставити на охорону")
        elif state_hub == '01':
            self.label_status_security_hub.setText("на охороні")
            self.push_btn_change_state.setText("зняти з охорони")
        elif state_hub == '02':
            self.label_status_security_hub.setText("на частковий охороні")
            self.push_btn_change_state.setText("зняти з охорони")
        elif state_hub == '03':
            self.label_status_security_hub.setText("хаб перешивається")
            self.push_btn_change_state.setText("хаб перешивається")

    def _change_state_hub(self):
        state_hub = self._hub_obj.get_parameters('08')
        if state_hub == '00':
            answer = self._hub_obj.arm()
        elif state_hub in ("01", '02'):
            answer = self._hub_obj.disarm()
        else:
            return
        logger.info("Change state answer {}".format(answer))
