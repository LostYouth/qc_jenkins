import logging

from PyQt5 import QtWidgets, QtCore

from app_setting import AppSetting
from csa_client_qt.csa_client import CsaClient
from qc.componets import ErrorPopupWidget
from qc.window.popup.manager_csa_obj.control_hub.widget_hub import WidgetHub

logger = logging.getLogger(AppSetting.LOGGER_NAME)


class ControlHub(QtWidgets.QWidget):
    def __init__(self, client_csa: CsaClient, parent=None):
        super(ControlHub, self).__init__(parent=parent, flags=QtCore.Qt.Popup)

        self._error_widget = ErrorPopupWidget(parent=parent)

        self.client_csa = client_csa

        self.client_csa.signal_added_hub.connect(self.handler_added_hub)
        self.client_csa.signal_delete_hub.connect(self.handler_remove_hub)

        self.main_layout = QtWidgets.QVBoxLayout()
        self.main_layout.setAlignment(QtCore.Qt.AlignCenter)

        self.setLayout(self.main_layout)

        for hub_obj in self.client_csa._hubs:
            l = WidgetHub(hub_obj)
            self.main_layout.addWidget(l)

    def handler_added_hub(self, hub_obj):
        widget_hub = WidgetHub(hub_obj)
        widget_hub.singnal_delete_hub.connect(self._delete_hub)
        self.main_layout.addWidget(widget_hub)

    def handler_remove_hub(self, hub_id):
        for i in range(self.main_layout.count()):
            item = self.main_layout.itemAt(i)
            widget = item.widget()
            if widget is not None:
                hub = widget.get_hub_id()
                if hub == hub_id:
                    widget.setParent(None)
                    break
        if not self.main_layout.count():
            self.close()

    def _delete_hub(self, hub_id: str):
        hub_obj = self.client_csa.get_hub_obj(hub_id)
        if hub_obj.work_hub_qc:
            self.show_error("Неможливо видалити робочий хаб! Звернисть за допомогою до майстра участка!")
            return
        a = self.client_csa.del_hub2client(hub_id)
        logger.debug("Delete hub with account answer {}".format(a))

    def show_error(self, msg):
        self._error_widget.set_message_data(str(msg))
        self._error_widget.show()

    def show(self):
        if not self.main_layout.count():
            self.show_error("На акаунті не знайдено ні одного хаба!")
            return
        super(ControlHub, self).show()

    def move_center(self):
        self.move(self.parent().window().frameGeometry().topLeft() + self.parent().window().rect().center() - self.rect().center())

    def paintEvent(self, QPaintEvent):
        self.move_center()
        super(ControlHub, self).paintEvent(QPaintEvent)
