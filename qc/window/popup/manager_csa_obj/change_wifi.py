from PyQt5 import QtWidgets, QtCore


class ChangeWifiName(QtWidgets.QWidget):
    signal_change_wifi_name = QtCore.pyqtSignal(str)

    def __init__(self, parent=None):
        super(ChangeWifiName, self).__init__(parent=parent, flags=QtCore.Qt.Dialog)

        self.setMaximumSize(parent.width() * 0.4,  parent.height() * 0.4)

        l = QtWidgets.QVBoxLayout()

        self.line_edit_name = QtWidgets.QLineEdit()
        self.line_edit_name.setPlaceholderText("ім'я Wifi мережі")
        l.addWidget(self.line_edit_name)

        self.push_btn_accept = QtWidgets.QPushButton("Змінити")
        l.addWidget(self.push_btn_accept)
        self.setLayout(l)

        self.push_btn_accept.clicked.connect(self.accept_wifi_name)

    def accept_wifi_name(self):
        self.close()
        new_wifi_name = self.line_edit_name.text()
        self.signal_change_wifi_name.emit(new_wifi_name)

