from PyQt5 import QtWidgets, QtCore, QtGui

from csa_client_qt.csa_client import CsaClient
from csa_client_qt.csa_object.csa_object import CsaObjectHub
from qc.window.popup.manager_csa_obj.control_object.tab_widget_hub import TabWidgetHub


class ControlObject(QtWidgets.QWidget):
    def __init__(self, csa_client: CsaClient, parent=None):
        super(ControlObject, self).__init__(parent=parent, flags=QtCore.Qt.Dialog)
        self.setWindowModality(QtCore.Qt.ApplicationModal)
        self.csa_client = csa_client
        self.csa_client.signal_added_hub.connect(self.add_hub_widget)
        self.csa_client.signal_delete_hub.connect(self.remove_hub_widget)
        self.tab_widgets_hubs = QtWidgets.QTabWidget()
        l = QtWidgets.QVBoxLayout()
        l.addWidget(self.tab_widgets_hubs)
        self.setLayout(l)

    def move_center(self):
        self.move(self.parent().window().frameGeometry().topLeft() +
                  self.parent().window().rect().center() - self.rect().center())

    def paintEvent(self, QPaintEvent):
        self.move_center()
        super(ControlObject, self).paintEvent(QPaintEvent)

    def add_hub_widget(self, hub_obj: CsaObjectHub):
        self.tab_widgets_hubs.addTab(TabWidgetHub(hub_obj), "{}".format(hub_obj))

    def remove_hub_widget(self, hub_id):
        for tab_index in range(self.tab_widgets_hubs.count()):
            if self.tab_widgets_hubs.tabText(tab_index).split(' ')[-1] == hub_id:
                self.tab_widgets_hubs.removeTab(tab_index)
                break
