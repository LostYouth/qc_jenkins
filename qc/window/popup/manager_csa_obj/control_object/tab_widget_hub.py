import logging
import sip
from functools import partial

from PyQt5 import QtWidgets

from app_setting import AppSetting
from csa_client_qt.csa_object.csa_object import CsaObjectHub, CsaObject
from csa_client_qt.enum_obj_csa import ObjectsCsa
from csa_client_qt.enum_params import MessageType, Answer
from qc.componets import ErrorPopupWidget

logger = logging.getLogger(AppSetting.LOGGER_NAME)


class TabWidgetHub(QtWidgets.QWidget):
    def __init__(self, hub_obj: CsaObjectHub):
        super(TabWidgetHub, self).__init__()

        l = QtWidgets.QVBoxLayout()
        self.setLayout(l)
        self.hub_obj = hub_obj
        self.hub_obj.signal_append_object.connect(self._append_obj)
        self.hub_obj.signal_remove_object.connect(self._remove_obj)
        self.widget_error = ErrorPopupWidget(parent=self)
        for obj in self.hub_obj._csa_object[:]:
            w = WidgetCsaObject(obj)
            l.addWidget(w)

    def _append_obj(self, csa_obj):
        widget_obj = WidgetCsaObject(csa_obj, parent=self)
        widget_obj.push_btn_delete_obj.clicked.connect(partial(self._delete_obj, csa_obj.type, csa_obj.id))
        self.layout().addWidget(widget_obj)

    def _remove_obj(self, obj_id):
        for index in range(self.layout().count()):
            item = self.layout().itemAt(index)
            if item is None:
                continue
            widget = item.widget()
            if widget is not None and hasattr(widget, 'get_obj_id') and widget.get_obj_id() == obj_id:
                widget.deleteLater()
                break

    def show_error(self, message_text: str):
        self.widget_error.set_message_data(message_text)
        self.widget_error.show()

    def _delete_obj(self, type_obj, id_obj):
        logger.info("DELETE t: {} id: {}".format(type_obj, id_obj))
        logger.debug(type_obj in ObjectsCsa)
        if type_obj == ObjectsCsa.Room.value:
            answer = self.hub_obj.remove_room_hub(id_obj)
            logger.debug("{}".format(answer))
            if answer.message_type == MessageType.Answer.value and answer.message_key == Answer.FailedNotEmpty.value:
                self.show_error("Не можліво видалити кімнату поки в ній є девайси!")
            elif answer.message_type == MessageType.Answer.value and answer.message_key == Answer.HubError.value:
                self.show_error("Помилка під час виконання!")
            return answer
        else:
            answer = self.hub_obj.delete_device(id_obj)
            if answer.message_type == MessageType.Answer.value and answer.message_key == Answer.HubError.value:
                self.show_error("Помилка під час виконання!")

            return answer


class WidgetCsaObject(QtWidgets.QWidget):
    def __init__(self, object_csa: CsaObject, parent=None):
        super(WidgetCsaObject, self).__init__(parent=parent)
        self.csa_object = object_csa
        main_l = QtWidgets.QHBoxLayout()
        self.setLayout(main_l)
        self.label_status_object = QtWidgets.QLabel("{}".format(object_csa))
        main_l.addWidget(self.label_status_object)
        self.push_btn_delete_obj = QtWidgets.QPushButton("Видалити")
        main_l.addWidget(self.push_btn_delete_obj)

    def get_obj_id(self):
        return self.csa_object.id
