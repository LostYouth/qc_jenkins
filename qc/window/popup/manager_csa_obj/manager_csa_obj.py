import logging

from PyQt5 import QtWidgets, QtCore

from app_setting import AppSetting
from csa_client_qt.csa_client import CsaClient

logger = logging.getLogger(AppSetting.LOGGER_NAME)


class ManagerCsaObj(QtWidgets.QWidget):
    def __init__(self, client_csa: CsaClient, parent=None):
        super(ManagerCsaObj, self).__init__(parent=parent, flags=QtCore.Qt.Popup)

        main_layout = QtWidgets.QHBoxLayout()

        self.client_csa = client_csa
        self.client_csa.signal_added_hub.connect(self._added_hub)
        self.setLayout(main_layout)

    def _added_hub(self):
        logger.info("Signal update")
