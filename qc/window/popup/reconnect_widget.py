from PyQt5 import QtWidgets, QtCore, QtGui


class ReconnectWidget(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(ReconnectWidget, self).__init__(flags=QtCore.Qt.SplashScreen, parent=parent)
        self.label_status = QtWidgets.QLabel("Перевірьте доступ до інтернету!")
        self._count = 1
        self.label_time = QtWidgets.QLabel("*" * self._count)
        self.label_time.setAlignment(QtCore.Qt.AlignCenter)
        l = QtWidgets.QVBoxLayout()
        l.setAlignment(QtCore.Qt.AlignCenter)
        l.addWidget(self.label_status)
        l.addWidget(self.label_time)
        self.setLayout(l)
        self.timer_update = QtCore.QTimer()
        self.timer_update.setInterval(1000)
        self.timer_update.timeout.connect(self._update_gui)

    def _update_gui(self):
        if self._count >= 5:
            self._count = 0
        self._count += 1
        self.label_time.setText("*" * self._count)

    def move_center(self):
        self.move(self.parent().window().frameGeometry().topLeft() + self.parent().window().rect().center() - self.rect().center())

    def paintEvent(self, QPaintEvent):
        super(ReconnectWidget, self).paintEvent(QPaintEvent)
        self.move_center()

    def show(self):
        self.timer_update.start()
        super(ReconnectWidget, self).show()

    def closeEvent(self, QCloseEvent):
        self.timer_update.stop()
        super(ReconnectWidget, self).closeEvent(QCloseEvent)
