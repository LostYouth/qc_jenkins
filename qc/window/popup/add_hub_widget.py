import logging
import string

from PyQt5 import QtWidgets, QtCore

from app_setting import AppSetting
from csa_client_qt.csa_client import CsaClient
from qc.componets import ErrorPopupWidget

logger = logging.getLogger(AppSetting.LOGGER_NAME)


class AddHubWidget(QtWidgets.QWidget):
    def __init__(self, client_csa: CsaClient, parent=None):
        super(AddHubWidget, self).__init__(parent=parent, flags=QtCore.Qt.Popup)

        self.client_csa = client_csa
        l = QtWidgets.QVBoxLayout()
        l.setAlignment(QtCore.Qt.AlignCenter)

        self.line_edit_hub_qr = QtWidgets.QLineEdit()
        self.line_edit_hub_qr.setPlaceholderText("qr code - хаба")
        self.line_edit_hub_qr.returnPressed.connect(self._add_hub2side_client)

        l.addWidget(self.line_edit_hub_qr)
        self.push_btn_add = QtWidgets.QPushButton("Додати")

        l.addWidget(self.push_btn_add)

        self.setLayout(l)
        self.error_window = ErrorPopupWidget(parent=self)

        self.push_btn_add.clicked.connect(self._add_hub2side_client)

    def move_center(self):
        self.move(
            self.parent().window().frameGeometry().topLeft() + self.parent().window().rect().center() - self.rect().center())

    def show(self):
        self.move_center()
        super(AddHubWidget, self).show()

    def show_error(self, message_data):
        self.error_window.set_message_data(message_data)
        self.error_window.show()

    def _add_hub2side_client(self):
        qr_code = self.line_edit_hub_qr.text()
        qr_code = qr_code.replace('-', '')
        if len(qr_code) != 20:
            logger.info("not correct qr hub while add hub")
            self.show_error("Не корекний qr code!")
            return

        if not all([chr in string.hexdigits for chr in qr_code.lower()]):
            self.show_error("qr code - лише хексові символи!")
            return

        hub_id, master_key = qr_code[:8], qr_code[8:16]
        hub_obj = self.client_csa.get_hub_obj(hub_id)
        if hub_obj is not None:
            self.show_error("Цей хаб вже закріплений за вашим обліковим записом")
            return

        answer = self.client_csa.add_hub2client(hub_id, master_key, "QC_WORK_HUB")
        logger.debug("Answer while add hub {}".format(answer))
        if not answer.message_type and not answer.message_key:
            self.show_error("Не було відповіді від сервера!")
        elif answer.message_type == '16' and answer.message_key == '07':
            self.show_error("Хаб ({}) в офлайні".format(hub_id))
        elif answer.message_type == '11' and answer.message_key == '13':
            self.show_error("Хаб ({}) вже приписаний до іншого користувача!".format(hub_id))
        elif answer.message_type == '11' and answer.message_key == '12':
            self.show_error("Неправильний мастеркей!")
            self.line_edit_hub_qr.clear()
        elif answer.message_type == '11' and answer.message_key == '0c':
            self.close()
            return
        else:
            self.show_error(str(answer))
