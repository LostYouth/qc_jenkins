import logging

from PyQt5 import QtWidgets, QtCore

from app_setting import AppSetting
from csa_client_qt.csa_object.csa_object import CsaObjectHub

logger = logging.getLogger(AppSetting.LOGGER_NAME)


class WidgetChangeFrameInterval(QtWidgets.QWidget):

    def __init__(self):
        super(WidgetChangeFrameInterval, self).__init__(flags=QtCore.Qt.Dialog)
        self.work_hub_qc = None
        self.setWindowModality(QtCore.Qt.ApplicationModal)
        # self.setWindowFlag(QtCore.Qt.Dialog)

        layout_main = QtWidgets.QVBoxLayout()
        self.label_desc = QtWidgets.QLabel("Змінити інтервал опитування девайсів (фрейм): ")
        layout_main.addWidget(self.label_desc)

        self.setLayout(layout_main)
        self.slider_change_frame = QtWidgets.QSlider()
        self.slider_change_frame.setOrientation(QtCore.Qt.Horizontal)
        layout_main.addWidget(self.slider_change_frame, QtCore.Qt.AlignCenter)
        self.slider_change_frame.setSingleStep(1)
        self.slider_change_frame.setTickInterval(1)
        self.slider_change_frame.setPageStep(1)
        self.slider_change_frame.setTickPosition(QtWidgets.QSlider.TicksAbove)
        self.slider_change_frame.setMinimum(1)
        self.slider_change_frame.setMaximum(25)
        self.slider_change_frame.valueChanged.connect(self.change_frame_hub_qc)

        self._current_frame_value = 12
        self.label_current_frame = QtWidgets.QLabel(str(self._current_frame_value))
        self.label_current_frame.setAlignment(QtCore.Qt.AlignCenter)
        layout_main.addWidget(self.label_current_frame)

        self.btn_save_settings = QtWidgets.QPushButton("Зберегти")
        self.btn_save_settings.clicked.connect(self.send_changed_frame)
        layout_main.addWidget(self.btn_save_settings)

    def show(self):
        if self.work_hub_qc is None:
            QtWidgets.QMessageBox.warning(self, "Помилка", "Робочий хаб не визначений!")
            self.hide()
            return
        self.handler_update_work_hub('')
        super(WidgetChangeFrameInterval, self).show()

    def set_work_qc_hub(self, hub_obj: CsaObjectHub):
        if self.work_hub_qc is not None:

            self.work_hub_qc.disconnect()
        self.work_hub_qc = hub_obj
        self.send_changed_frame()
        self.work_hub_qc.signal_update_data.connect(self.handler_update_work_hub)

    def handler_update_work_hub(self, obj_id):
        len_frame_hex_str = self.work_hub_qc.get_parameters('38')
        try:
            len_frame_int = int(len_frame_hex_str, 16)
            if len_frame_int > 300:
                # if frame is undefined
                return
        except ValueError as err:
            logger.debug("Error {} frame is not correct".format(err))
        else:
            self.slider_change_frame.setValue(len_frame_int / 12)
            self.label_current_frame.setText(str(len_frame_int))

    def change_frame_hub_qc(self, val: int):
        self.set_current_frame(val * 12)
        self.label_current_frame.setText("{}".format(self.get_current_frame()))

    def set_current_frame(self, curr_frame: int):
        self._current_frame_value = curr_frame

    def get_current_frame(self) -> int:
        return self._current_frame_value

    def send_changed_frame(self):
        if self.work_hub_qc is None:
            self.hide()
            QtWidgets.QMessageBox.warning(self, "Помилка", "Робочий хаб для вашого аккаунту не визначений!")
            return
        self.hide()
        answer = self.work_hub_qc.write_param(
            self.work_hub_qc.type,
            self.work_hub_qc.id,
            ['38', '{:04x}'.format(self.get_current_frame())]
        )
        logger.debug("Answer change frame: {}".format(answer))
