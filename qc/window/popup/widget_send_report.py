import time
import queue
import logging
from PyQt5 import QtWidgets, QtCore

from telegram_api.telegram_api import TelegramBot

from app_setting import AppSetting

logger = logging.getLogger(AppSetting.LOGGER_NAME)


class ThreadSendReport(QtCore.QThread):
    def __init__(self):
        super(ThreadSendReport, self).__init__()
        self.is_run = True

        self.queue_message = queue.Queue()
        self.queue_files = queue.Queue()

    def run(self):
        while self.is_run:
            try:
                self.send_report()
                time.sleep(3)
            except Exception as err:
                logger.error("{}".format(err))

    def send_report(self):
        m = self.get_message_if_available()
        path_file = self.get_file_if_available()
        if m:
            TelegramBot.send_message(m)
        if path_file:
            TelegramBot.send_file(path_file)

    def get_message_if_available(self):
        try:
            message_text = self.queue_message.get(block=False)
            logger.info("Send message >> {}".format(message_text))
            return message_text
        except queue.Empty:
            return ''

    def get_file_if_available(self):
        try:
            path_file = self.queue_files.get(block=False)
            logger.info("Send file >> {}".format(path_file))
            return path_file
        except queue.Empty:
            return ""


class WidgetSendReport(QtWidgets.QWidget):

    def __init__(self):
        super(WidgetSendReport, self).__init__(flags=QtCore.Qt.Popup)

        l_v = QtWidgets.QVBoxLayout()
        self.setLayout(l_v)
        self.line_edit_title = QtWidgets.QLineEdit()
        self.line_edit_title.setPlaceholderText('Заголовок')
        l_v.addWidget(self.line_edit_title)

        self.text_browser_problem = QtWidgets.QTextEdit()
        self.text_browser_problem.setPlaceholderText("Короткий опис проблеми (device id, ....)")
        l_v.addWidget(self.text_browser_problem)

        self.push_btn_send = QtWidgets.QPushButton("Відправити")
        # self.push_btn_send.setMaximumHeight(50)
        # self.push_btn_send.setMaximumWidth(130)
        l_v.addWidget(self.push_btn_send)
        self.move_center()

        self.thread_send_message = ThreadSendReport()
        self.thread_send_message.start()

        self.push_btn_send.clicked.connect(self.send_problem)

        self.operator_name = "Unknown operator name"

        self.setMinimumHeight(450)
        self.setMinimumWidth(800)

    def set_operator(self, n: str):
        self.operator_name = n

    def get_title(self):
        return self.line_edit_title.text()

    def get_text_reason_problem(self):
        return self.text_browser_problem.toPlainText()

    def get_path_log_file(self):
        for h in logger.handlers:
            if hasattr(h, "baseFilename"):
                return h.baseFilename

    def send_problem(self):
        self.thread_send_message.queue_message.put(self.operator_name)
        t = self.get_title()
        logger.debug("Title put queue {}".format(repr(t)))
        self.thread_send_message.queue_message.put(t)
        self.thread_send_message.queue_message.put(self.get_text_reason_problem())

        path_f = self.get_path_log_file()
        if path_f is not None:
            self.thread_send_message.queue_files.put(path_f)

        self.close()

    def move_center(self):
        fg = self.frameGeometry()
        cp = QtWidgets.QDesktopWidget().availableGeometry().center()
        fg.moveCenter(cp)
        self.move(fg.topLeft())

    def show(self):
        self.move_center()
        super(WidgetSendReport, self).show()

    def close(self):
        self.thread_send_message.start()
        self.line_edit_title.clear()
        self.text_browser_problem.clear()
        super(WidgetSendReport, self).close()
