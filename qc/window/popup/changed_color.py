from functools import partial

from PyQt5 import QtWidgets, QtCore
from qc.enum_param import Color


class ChangedColor(QtWidgets.QWidget):
    signal_change_color = QtCore.pyqtSignal(object)

    def __init__(self, parent=None):
        super(ChangedColor, self).__init__(parent=parent, flags=QtCore.Qt.Dialog)

        self.setMaximumSize(int(self.parent().width() * 0.2), int(self.parent().height() * 0.2))

        l = QtWidgets.QHBoxLayout()
        for color in Color:
            btn = QtWidgets.QPushButton("{}".format(color.name))
            l.addWidget(btn)
            btn.clicked.connect(partial(self.signal_change_color.emit, color))
        self.setLayout(l)
