import logging
from functools import partial

from PyQt5 import QtWidgets, QtCore
from PyQt5.QtSerialPort import QSerialPortInfo, QSerialPort
from pymysql.err import OperationalError

from app_setting import AppSetting
from qc.window.ui.qc_login_widget import Ui_LoginWidget
from qc.componets import ErrorPopupWidget
from qc.database.database_production import DataBaseProduction
from qc.security import KeyDatabase

logger = logging.getLogger(AppSetting.LOGGER_NAME)


class ThreadAuthUser(QtCore.QThread):
    signal_user_obj = QtCore.pyqtSignal(object)
    signal_failure_auth = QtCore.pyqtSignal(str)

    def __init__(self, db):
        super(ThreadAuthUser, self).__init__()

        self.db = db
        self.login = None
        self.password = None

    def auth(self, login: str, password: str):
        self.login = login
        self.password = password
        self.start()

    def run(self):
        try:
            user = self.db.authorize(self.login, self.password)
        except OperationalError:
            self.signal_failure_auth.emit("Перевірьте доступ до інтернету!")
        else:
            self.signal_user_obj.emit(user)


class LoginWidget(QtWidgets.QWidget, Ui_LoginWidget):
    signal_success_result = QtCore.pyqtSignal(str, str)

    def __init__(self, parent=None):
        super(LoginWidget, self).__init__(parent=parent, flags=QtCore.Qt.Widget)
        self.setupUi(self)
        self.layout().setAlignment(QtCore.Qt.AlignCenter)
        self.line_edit_login_name.setPlaceholderText("login")
        self.line_edit_login_password.setEchoMode(QtWidgets.QLineEdit.Password)
        self.line_edit_login_password.setPlaceholderText("password")

        self.line_edit_login_name.returnPressed.connect(self.login_button_clicked)
        self.line_edit_login_password.returnPressed.connect(self.login_button_clicked)
        self.push_btn_login.clicked.connect(self.login_button_clicked)

        self.database_prod = DataBaseProduction(old_db_keys=KeyDatabase.get_old_keys_production(),
                                                db_keys=KeyDatabase.get_keys_production())
        self.err_widget = ErrorPopupWidget(parent=self)
        self.err_widget.setMinimumHeight(int(self.parent().height() * 0.3))
        self.err_widget.setMinimumWidth(int(self.parent().width() * 0.45))

        self.thread_authorize = ThreadAuthUser(self.database_prod)
        self.thread_authorize.finished.connect(partial(self.change_state_widget, True))
        self.thread_authorize.signal_failure_auth.connect(self.show_err)
        self.thread_authorize.signal_user_obj.connect(self.handler_result_login)

        self.combo_box_ports.installEventFilter(self)
        self.push_btn_connect_port.setText('Підключитися')
        self.push_btn_connect_port.clicked.connect(self.handler_connect_port)

        self.update_ports_scanner()
        self.scanner_login = QSerialPort()
        self.scanner_login.setBaudRate(QSerialPort.Baud115200)
        self.scanner_login.readyRead.connect(self._handler_data_scanner_port)

    def eventFilter(self, QObject, QEvent):
        if QObject is self.combo_box_ports:
            if QEvent.type() == QtCore.QEvent.MouseButtonPress:
                self.update_ports_scanner()
        return super(LoginWidget, self).eventFilter(QObject, QEvent)

    def update_ports_scanner(self):
        if not self.combo_box_ports.isEnabled():
            logger.debug("Cant update items, Combo Box is disabled")
            return
        ports = sorted([port.portName() for port in QSerialPortInfo.availablePorts()])
        self.combo_box_ports.clear()
        self.combo_box_ports.addItems(ports)

    def _handler_data_scanner_port(self):
        try:
            if self.scanner_login.canReadLine():
                line_q_bytes = self.scanner_login.readLine()
                line = str(line_q_bytes.data(), encoding='utf-8').strip()
                logger.debug("Login widget read scanner data {}".format(line))
                self._search_login_operator_qr_code(line)
        except Exception as e:
            logger.error("Error while read data scanner")
            logger.error(e)

    def _search_login_operator_qr_code(self, line: str):
        try:
            login_user, password_user = line.split(" ")
            self._try_authorize_operator(login_user, password_user)
        except ValueError:
            logger.info("Not corrent qr code when login by qr code")
            self.show_err("QrCode не відповідає вимогам (Логін оператора -> пробіл -> Пароль оператора)")

    def handler_connect_port(self):
        _state = self.push_btn_connect_port.text()
        if _state == "Підключитися":
            self._connect_uart_port()
        elif _state == "Відключитися":
            self._disconnect_port()

    def _connect_uart_port(self):
        port_name = self.combo_box_ports.currentText()
        logger.debug("Login widget, try connect {}".format(port_name))
        self.scanner_login.setPortName(port_name)
        if not self.scanner_login.open(QtCore.QIODevice.ReadOnly):
            logger.warning("Fail connect port {}".format(port_name))
            return
        logger.debug("Success connect port {}".format(port_name))
        self._success_connect_port()

    def _success_connect_port(self):
        self.combo_box_ports.setEnabled(False)
        self.push_btn_connect_port.setText("Відключитися")

    def _disconnect_port(self):
        self.update_ports_scanner()
        if self.scanner_login.isOpen():
            logger.debug("Close serial port login")
            self.scanner_login.close()
        self.combo_box_ports.setEnabled(True)
        self.push_btn_connect_port.setText("Підключитися")

    def show_err(self, value: str):
        self.err_widget.set_message_data(value)
        self.err_widget.show()

    def handler_result_login(self, user_obj):
        if not user_obj:
            self.show_err("Логін або пароль - некоректні!")
        else:
            self._disconnect_port()
            self.signal_success_result.emit(*user_obj)

    def change_state_widget(self, state: bool):
        self.push_btn_login.setEnabled(state)
        self.line_edit_login_name.setEnabled(state)
        self.line_edit_login_password.setEnabled(state)
        self.combo_box_ports.setEnabled(state)
        self.push_btn_connect_port.setEnabled(state)

    def login_button_clicked(self):
        login = self.line_edit_login_name.text()
        if not login:
            self.show_err('Введіть логін')
            return
        password = self.line_edit_login_password.text()
        if not password:
            self.show_err('Введіть пароль')
            return
        self._try_authorize_operator(login, password)

    def _try_authorize_operator(self, login, password):
        self.change_state_widget(False)
        if not self.thread_authorize.isRunning():
            logger.info("Try login on app - {}".format(login))
            self.thread_authorize.auth(login, password)
        else:
            logger.warning("Thread authorize isRun")
