from PyQt5 import QtWidgets, QtCore, QtGui


class AvailableUpdateWidget(QtWidgets.QDialog):

    signal_update_app = QtCore.pyqtSignal()
    signal_remind_later = QtCore.pyqtSignal()

    def __init__(self, parent, text='Доступна нова версія програми!'):

        super(AvailableUpdateWidget, self).__init__(flags=QtCore.Qt.Popup)
        self._text_data = text
        self.parent = parent
        self.__init_ui__()
        self._init_signal()

    def __init_ui__(self):
        m_layout = QtWidgets.QVBoxLayout()
        self.setLayout(m_layout)
        self.label_text = QtWidgets.QLabel(self._text_data)
        self.label_text.setAlignment(QtCore.Qt.AlignCenter)
        self.label_text.setMinimumWidth(int(self.parent.width() * 0.2))
        self.label_text.setMinimumHeight(int(self.parent.height() * 0.08))
        font_text = QtGui.QFont()
        font_text.setPointSize(14)
        # font_text.setBold(True)
        self.label_text.setFont(font_text)
        m_layout.addWidget(self.label_text)

        botton_layout = QtWidgets.QHBoxLayout()
        m_layout.addLayout(botton_layout)
        self.btn_remind_later = QtWidgets.QPushButton('Пізніш')
        botton_layout.addWidget(self.btn_remind_later)

        self.btn_update_app = QtWidgets.QPushButton("Оновити програму")
        botton_layout.addWidget(self.btn_update_app)

    def _init_signal(self):
        self.btn_remind_later.clicked.connect(lambda *args: self.signal_remind_later.emit())
        self.btn_update_app.clicked.connect(lambda *args: self.signal_update_app.emit())
