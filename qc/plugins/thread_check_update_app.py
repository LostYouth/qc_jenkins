import logging
import time

from pyupdater.client import Client
from PyQt5 import QtCore

from __version__ import __version__
from app_setting import AppSetting
from client_config import ClientConfig

logger = logging.getLogger(AppSetting.LOGGER_NAME)


class ThreadCheckUpdateApp(QtCore.QThread):
    signal_update = QtCore.pyqtSignal()

    def __init__(self):
        super(ThreadCheckUpdateApp, self).__init__()

        self.client_update_app = None

        self._run = True

        self.app_update_obj = None

    def run(self):
        while self._run:
            try:
                self.client_update_app = Client(
                    ClientConfig,
                    refresh=True
                )
                self.app_update_obj = self.client_update_app.update_check(
                    ClientConfig.APP_NAME,
                    __version__,
                    channel=AppSetting.UPDATE_CHANNEL)
                logger.debug("Update: {}".format(repr(self.app_update_obj)))
                if self.app_update_obj:
                    result_download = self.app_update_obj.download()
                    if result_download:
                        self.signal_update.emit()
                        time.sleep(5 * 60)
                    else:
                        logger.warning("Fail result download a update for app!")
                else:
                    time.sleep(10)
            except Exception as e:
                logger.error("Error while check a update on server!")
                logger.error(e)
                time.sleep(5*60)
