import queue
import os
import logging
import ftplib

from ajax.db.utils import parse_qr_code
from PyQt5 import QtCore

from app_setting import AppSetting
from qc.enum_param import HOST_STORAGE_PHOTO_MCAM
from qc.database.database_production import DataBaseProduction

logger = logging.getLogger(AppSetting.LOGGER_NAME)


class GetMCamImage(QtCore.QThread):
    signal_error = QtCore.pyqtSignal(str)
    signal_path_files = QtCore.pyqtSignal(str, str)

    host_photos_storage = HOST_STORAGE_PHOTO_MCAM
    _usr_ftp, _password_ftp = '', ''

    def __init__(self, db_obj: DataBaseProduction):
        super(GetMCamImage, self).__init__()

        self._is_run = True

        self.db_obj = db_obj

        self._queue_qr = queue.LifoQueue()

        self.ftp_obj = ftplib.FTP()

    def add_qr_get_photo(self, qr: str):
        self._queue_qr.put(qr)

    @staticmethod
    def _parse_id_in_qr_code(qr_code: str):
        obj_parse = parse_qr_code(qr_code)
        return obj_parse['id_code']

    def get_qr(self):
        qr = self._queue_qr.get()
        return qr

    def get_url_device_storage(self, qr):
        device_id = self._parse_id_in_qr_code(qr)
        result_device = self.db_obj.get_device_by_id(device_id)
        link_test_room = result_device['info_test_room']            # id record
        stand_room_obj = self.db_obj.get_stand_room_device(qr, link_test_room)
        photos_info = stand_room_obj['info'].get('photo_info', list())
        urls_file = [photo['link'] for photo in photos_info if photo.get('link', False)]
        return urls_file

    def _connect_ftp(self):
        self.ftp_obj.connect(host=self.host_photos_storage, timeout=5)
        self.ftp_obj.login(self._usr_ftp, self._password_ftp)

    def get_image_from_ftp(self, url_file):
        self._connect_ftp()
        file_name = os.path.split(url_file)[-1]
        dir_name = 'photos_motion_cam'
        self._check_exists_dir_image(dir_name)
        p_file_image = os.path.join(dir_name, file_name)
        with open(p_file_image, 'wb') as f:
            self.ftp_obj.retrbinary(
                "RETR " + '{}/{}'.format('mcam_photos', file_name),
                f.write
            )
        return p_file_image

    def _check_exists_dir_image(self, dir_name: str):
        if not os.path.exists(dir_name):
            os.mkdir(dir_name)

    def run(self):
        while self._is_run:
            qr = self.get_qr()
            logger.debug('try download photo {}'.format(qr))
            try:
                urls = self.get_url_device_storage(qr)
                logger.debug("urls photo {} {}".format(qr, urls))
                for url in urls:
                    path_file = self.get_image_from_ftp(url)
                    logger.debug('emit photo {} {}'.format(qr, path_file))
                    self.signal_path_files.emit(qr, path_file)
            except Exception as e:
                logger.error(e)
                self.signal_error.emit(qr)
