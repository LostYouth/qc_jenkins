import logging

from PyQt5 import QtCore, QtWidgets
from pymysql import DatabaseError

from app_setting import AppSetting
from qc.database.database_production import DataBaseProduction
from qc.security import KeyDatabase

logger = logging.getLogger(AppSetting.LOGGER_NAME)


class ThreadGetStatDevices(QtCore.QThread):

    signal_result = QtCore.pyqtSignal(object)

    def __init__(self, operator_name, operator_login):
        super(ThreadGetStatDevices, self).__init__()

        self.operator_name = operator_name
        self.operator_login = operator_login

        self.db = DataBaseProduction(
            old_db_keys=KeyDatabase.get_old_keys_production(),
            db_keys=KeyDatabase.get_keys_production()
        )

    def run(self):
        try:
            r = self.db.get_type_central_devices_by_operator(self.operator_name, self.operator_login)
            logger.debug("Result get count devices\n{}".format(r))
            self.signal_result.emit(r)
        except DatabaseError as e:
            logger.error("Error when get stat devices {}".format(self.operator_name))
            logger.error("{}".format(e))
        except Exception as e:
            logger.error("Error {}".format(e))
