from PyQt5 import QtWidgets

from ajax.db.utils import parse_qr_code

from qc.exc import QcExists
from qc.security import KeyDatabase
from qc.enum_param import StageChecksCentral, QrDevTypes, Color, CheckFrequencyIgnore, \
    StageChecksDevice, CheckColorIgnore
from qc.database.database_production import DataBaseProduction
from qc.enum_device import DevicesQrCode


class CheckLastStage:
    def __init__(self):
        self.database = DataBaseProduction(old_db_keys=KeyDatabase.get_old_keys_production(),
                                           db_keys=KeyDatabase.get_keys_production())
        self._current_color = Color.white
        self._current_frequency = None

    def current_frequency(self):
        return self._current_frequency.value

    def change_current_frequency(self, freq):
        self._current_frequency = freq

    def current_color(self):
        return self._current_color

    def change_current_color(self, color: Color):
        self._current_color = color

    def check_last_stage(self, qr_code_device):
        assert self._current_frequency, "Частота не визначена для подальшого тестування девайсу!"
        assert self._current_color, 'Колір не визначений для подальшого тестування девайсу!'

        qr_code = parse_qr_code(qr_code_device)

        if qr_code['specific_type'] == QrDevTypes.Device.value:
            return self.check_last_stage_device(qr_code['id_code'], qr_code['color'], qr_code['type'])
        elif qr_code['specific_type'] == QrDevTypes.Hub.value:
            return self.check_last_state_central(qr_code['id_code'])
        elif qr_code['specific_type'] == QrDevTypes.ocBridge.value:
            return self.check_last_stage_bridge(qr_code_device)

    def check_last_stage_device(self, dev_id, color, dev_type_qr):
        #  new db

        result = self.database.get_device_by_id(dev_id)
        if result:
            dev = result['dev_name']
            assert result['color'] == self.current_color().name, \
                "Колір відсканованого девайсу не співпадає з обаним кольором!"

            check_last_stange_device = StageChecksDevice.get(dev, None)
            assert check_last_stange_device, "В базі для датчика {} неправильно вказаний тип!".format(dev_id)

            if dev not in CheckFrequencyIgnore.ignore_device_types:
                assert self._current_frequency.name == result['region'], \
                    "Частота пристрою не співпадає з обраною в налаштуваннях!"

            if check_last_stange_device.prog:
                assert result['info_prog'], "Етап програмування та первинного тестування не пройдений, " \
                                            "поверніть його на відповідний етап!"

            if check_last_stange_device.assembling:
                assert result['info_assembling'], "Етап зборки не пройдений, поверніть пристрій на відповідний етап!"

            if check_last_stange_device.calibration:
                assert result['info_calibration'], "Етап калібровки не пройдений," \
                                                   " поверніть пристрій на відповідний етап!"
                calibration = self.database.get_device_calibration(result['info_calibration'])
                assert calibration['success'], "Етап калібровки пройдений неуспішно," \
                                               " поверніть пристрій на відповідний етап!"

            if check_last_stange_device.longtest:
                assert result['info_long_test'], "Етап лонг теста не пройдений, поверніть пристрій на відповідний етап!"
                long_test = self.database.get_device_long_test(result['info_long_test'])
                assert long_test['success'], "Етап лонг теста пройдений неуспішно," \
                                             " поверніть пристрій на відповідний етап!"

            if check_last_stange_device.standroom:
                assert result['info_test_room'], "Етап тест кімнати не пройдений, " \
                                                 "поверніть пристрій на відповідний етап!"
                test_room = self.database.get_device_test_room(result['info_test_room'])
                assert test_room['success'], "Етап тест кімнати пройдений неуспішно," \
                                             " поверніть пристрій на відповідний етап!"

            if result['info_qc']:
                qc = self.database.get_device_qc(result['info_qc'])
                if qc['success']:
                    operator_name = self.database.get_name_by_login(qc['operator'])
                    if not self.retry_test_device_qc(dev_id, operator_name, qc['time']):
                        raise QcExists("Девайс пройшов етап QC!")

            return dev

        # old db, would delete this part in future
        result = self.database.get_device_by_id_old(dev_id)
        assert result, "Девайс {} не знайдено в базі".format(dev_id)

        if color == '1':
            assert self.current_color() == Color.white, "Колір відсканованого девайсу не співпадає з обаним кольором!"
        elif color == '2':
            assert self.current_color() == Color.black, "Колір відсканованого девайсу не співпадає з обаним кольором!"

        prog_operator, color, dev_type, assembler, stand_operator, longtest_operator, qc, qc_time, frequency = self._prepare_value(result)

        self.check_color(color)
        check_last_stange_device = StageChecksDevice.get(DevicesQrCode(dev_type_qr).name, None)
        assert check_last_stange_device, "В базі для датчика {} неправильно вказаний тип!".format(dev_id)

        if dev_type not in CheckFrequencyIgnore.ignore_device_types:
            self.check_frequency(frequency)

        if check_last_stange_device.prog:
            assert prog_operator, "Етап програмування та первинного тестування не пройдений, " \
                                  "поверніть його на відповідний етап!"

        if check_last_stange_device.assembling:
            assert assembler, "Етап зборки не пройдений, поверніть пристрій на відповідний етап!"

        if check_last_stange_device.standroom:
            assert stand_operator, "Етап тест кімнати не пройдений, поверніть пристрій на відповідний етап!"

        if check_last_stange_device.longtest:
            assert longtest_operator, "Етап лонг теста не пройдений, поверніть пристрій на відповідний етап!"

        if qc:
            if not self.retry_test_device_qc(dev_id, qc, qc_time):
                raise QcExists("Девайс пройшов етап QC!")
        return DevicesQrCode(dev_type_qr).name

    def _prepare_value(self, result):
        _result = list()
        for val in result:
            if isinstance(val, str):
                v = val.strip()
                _result.append(v)
            elif val is None:
                _result.append('')
            else:
                _result.append(val)

        return _result

    def check_last_state_central(self, hub_id):
        result = self.database.get_central_by_id(hub_id)
        if result:
            if result['hub_subtype_name'] not in CheckColorIgnore.ignore_hubs_types:
                assert result['color'] == self.current_color().name, \
                    "Колір відсканованого девайсу не співпадає з обаним кольором!"

            if result['hub_subtype_name'] not in CheckFrequencyIgnore.ignore_hubs_types:
                assert self._current_frequency.name == result['region'], \
                    "Частота пристрою не співпадає з обраною в налаштуваннях!"

            check_last_stage_central = StageChecksCentral.get(result['hub_subtype_name'])
            assert check_last_stage_central, 'Для типу - {} центарлі; не визначені параментри ' \
                                             'попередніх етапів!'.format(result['hub_subtype_name'])

            if check_last_stage_central.prog:
                assert result['info_prog'], "Етап програмування та первиного тестування не пройдений, " \
                                            "поверніть його на відповідний етап!"

            if check_last_stage_central.assembling:
                assert result['info_assembling'], "Етап зборки непройдений, поверніть пристрій на відповідний етап!"

            if check_last_stage_central.longtest:
                assert result['info_long_test'], "Етап лонг теста не пройдений, поверніть пристрій на відповідний етап!"
                long_test = self.database.get_hub_long_test(result['info_long_test'])
                assert long_test['success'], "Етап лонг теста пройдений неуспішно, " \
                                             "поверніть пристрій на відповідний етап!"

            if result['info_qc']:
                qc = self.database.get_hub_qc(result['info_qc'])
                if qc['success']:
                    operator_name = self.database.get_name_by_login(qc['operator'])
                    if not self.retry_test_device_qc(hub_id, operator_name, qc['time']):
                        raise QcExists("Девайс пройшов етап QC!")

            return result['hub_subtype_name']

        result = self.database.get_hub_by_id_old(hub_id)
        assert result, "Хаб {} не знайдено в базі".format(hub_id)

        hub_type, color, prog_operator, assembler, QC, QC_time, description, frequency = self._prepare_value(result)
        self.check_color(color)
        if hub_type not in CheckFrequencyIgnore.ignore_hubs_types:
            self.check_frequency(frequency)
        check_last_stage_central = StageChecksCentral.get(hub_type)

        assert check_last_stage_central, 'Для типу - {} центарлі; не визначені параментри попередніх етапів!'.format(hub_type)

        if check_last_stage_central.prog:
            assert bool(prog_operator), "Етап програмування та первиного тестування не пройдений, " \
                                        "поверніть його на відповідний етап!"

        if check_last_stage_central.assembling:
            assert bool(assembler), "Етап зборки непройдений, поверніть пристрій на відповідний етап!"

        if check_last_stage_central.longtest:
            result_long_test_db = self.database.get_longtest_hub_by_id(hub_id)
            assert result_long_test_db is not None, "Хаб {} не пройшов єтап лонг тесту " \
                                                    "(Незнайдено запис про проходження лонг тесту)".format(hub_id)

            status_long_test_hub, *_ = result_long_test_db
            res_long_test_central = status_long_test_hub == b'\x01'
            assert bool(res_long_test_central), "Етап лонг теста не пройдений, поверніть пристрій на відповідний етап!"

        if QC:
            if not self.retry_test_device_qc(hub_id, QC, QC_time):
                raise QcExists("Девайс пройшов етап QC!")
        return hub_type

    def check_color(self, color_dev: str):
        assert color_dev.lower() == self.current_color().name, "Колір девайсу не співпадає з обраним в налаштуваннях!"

    def check_frequency(self, frequency):
        assert frequency in self.current_frequency(), "Частота пристрої не співпадає з обраною в налаштуваннях!"

    def check_last_stage_bridge(self, qr_code):
        result = self.database.get_central_by_id(qr_code)
        if result:
            assert self._current_frequency.name == result['region'], \
                "Частота пристрою не співпадає з обраною в налаштуваннях!"
            parameter_last_stage_bridge = StageChecksCentral.get(result['hub_subtype_name'], None)
            assert parameter_last_stage_bridge, 'Для типу - {} центарлі; не визначені ' \
                                                'параментри попередніх етапів!'.format(result['dev_name'])

            if parameter_last_stage_bridge.prog:
                assert result['info_prog'], "Етап програмування та первиного тестування не пройдений, " \
                                            "поверніть його на відповідний етап!"

            if parameter_last_stage_bridge.assembling:
                assert result['info_assembling'], "Етап зборки не пройдений, поверніть пристрій на відповідний етап!"

            if result['info_qc']:
                qc = self.database.get_hub_qc(result['info_qc'])
                if qc['success']:
                    operator_name = self.database.get_name_by_login(qc['operator'])
                    if not self.retry_test_device_qc(qr_code, operator_name, qc['time']):
                        raise QcExists("Девайс пройшов етап QC!")
            return result['dev_name']
        qr_bridge = qr_code

        result = self.database.get_bridge_by_id(qr_bridge)
        assert result, "Девайс {} не знайдено в базі!".format(qr_code)

        bridge_type, color, prog_operator, assembler, qc, qc_time, description, frequency = self._prepare_value(result)

        self.check_frequency(frequency)

        parameter_last_stage_bridge = StageChecksCentral.get(bridge_type, None)
        assert parameter_last_stage_bridge, 'Для типу - {} центарлі; не визначені параментри попередніх етапів!'.format(bridge_type)

        if parameter_last_stage_bridge.prog:
            assert bool(prog_operator), "Етап програмування та первиного тестування не пройдений, " \
                                        "поверніть його на відповідний етап!"

        if parameter_last_stage_bridge.assembling:
            assert bool(assembler), "Етап зборки не пройдений, поверніть пристрій на відповідний етап!"

        if qc:
            if not self.retry_test_device_qc(qr_code, qc, qc_time):
                raise QcExists("Девайс пройшов етап QC!")
        return bridge_type

    @staticmethod
    def retry_test_device_qc(dev_id: str, qc_operator: str, qc_time: str):
        """ Question operator retry test device! """
        reply = QtWidgets.QMessageBox()
        reply.setIcon(QtWidgets.QMessageBox.Question)
        reply.setText("Цей пристрій {} вже успішно пройшов етап {} ({})!".format(dev_id, qc_operator, qc_time))
        reply.setStandardButtons(QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
        btn_yes = reply.button(QtWidgets.QMessageBox.Yes)
        btn_not = reply.button(QtWidgets.QMessageBox.No)
        btn_yes.setText('Перетестувати')
        btn_not.setText('ОК')
        reply.exec_()
        if reply.clickedButton() == btn_yes:
            return True
        else:
            return False

