try:
    from .security import KeyDatabase
except ImportError:
    from .example_security import KeyDatabase
