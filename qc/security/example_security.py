class Keys:
    HOST = 'name host'
    USER = "user name"
    PASSWORD = 'pass'
    DB_NAME = "db name"
    DEBUG_DB_NAME = "debug db name"

    MODEL_CSA = "model csa table"
    DEBUG_MODEL_CSA = "debug model csa table"


class KeyDatabase(Keys):
    @classmethod
    def get_keys_production(cls):
        return {
            'host': cls.HOST,
            "user": cls.USER,
            "password": cls.PASSWORD,
            "db": cls.DB_NAME
        }

    @classmethod
    def get_keys_model_csa(cls):
        return {
            'host': cls.HOST,
            "user": cls.USER,
            "password": cls.PASSWORD,
            "db": cls.MODEL_CSA
        }
