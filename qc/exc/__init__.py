

class WorkHubNotFound(Exception):
    pass


class NotImplement(Exception):
    pass


class QcExists(Exception):
    pass