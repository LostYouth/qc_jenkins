from enum import Enum


class AlarmsNumberHub(Enum):
    PowerOff = '00'
    PowerOn = '01'
    LowBattery = '02'
    OkBattery = '03'
    TamperOn = '04'
    TamperOff = '05'
    GsmNotOk = '06'
    GsmOk = '07'
    LevelRssiNotOk = '08'
    LevelRssiOk = '09'
    HubOffline = '0a'
    HubOnline = '0b'
    HubPowerOff = '0c'
    FirmwareUpdate = '10'
    FinalFirmwareUpdate = '11'
    MalfunctionHub = '12'


class AlarmsNumberDevice:
    TamperOpen = '00'
    TamperClosed = '01'
    BatteryChargeOk = '02'
    BatteryChangeNotOk = '03'
    DeviceLostConnect = '04'
    DeviceRecoverConnect = '05'
    SynchConnectNotOk = '06'
    SynchConnectOk = '07'
    ObjectAdded = '08'
    ObjectRemove = '09'
    MalfunctionDevice = '12'
    PowerOffBtn = '13'


class AlarmsDoorProtect(AlarmsNumberDevice):
    ReedOpened = '20'
    ReedClosed = '21'
    OutContactOpened = '22'
    OutContactClosed = '23'
    AlarmRollerShutter = '24'
    DisconnectRollerShutter = '25'
    RecoverRollerShutter = '26'


class AlarmsDoorPlusProtect(AlarmsDoorProtect):
    Vibration = '30'
    Incline = '31'


class AlarmsGlassProtect(AlarmsNumberDevice):
    BreakDown = '20'
    ContactOpen = '22'
    ContactClosed = '23'


class MotionProtect(AlarmsNumberDevice):
    Movement = '20'


class MotionCam(MotionProtect):
    LostConnectDataChannel = '30'
    RecoveryConnectDataChannel = '31'
    TakePhotoForce = '32'
    FailTakePhotoForce = '33'


class MotionProtectOutdoor(MotionProtect):
    MaskingDetect = '22'
    MaskingNotDetect = '23'
    OutpowerOff = '24'
    OutpowerOn = '25'


class CombiProtect(AlarmsNumberDevice):
    Movement = '20'
    BreakDown = '21'


class LeaksProtect(AlarmsNumberDevice):
    LeakWater = '20'
    LeakWaterOff = '21'
    Accelerometer = '22'


class RangeExtender(AlarmsNumberDevice):
    PowerOn = '21'
    PowerOff = '20'


class HomeSiren(AlarmsNumberDevice):
    pass


class FireProtect(AlarmsNumberDevice):
    pass


class StreetSiren(HomeSiren):
    Accelerometer = '20'
    OffPower = '21'
    OnPower = '22'


class Keypad(AlarmsNumberDevice):
    Disarm = '20'
    Arm = '21'
    PerimetralArm = '22'
    PanicBtn = '23'


class WallSwitch(AlarmsNumberDevice):
    DisableTemperature = '20'
    TemperatureOk = '21'
    Enabled = '22'
    Disabled = '23'
    ContactSticking = '24'
    DisabledCurrentNotOk = '25'
    DisabledCurrentUserNotOk = '26'
    CurrentOk = '27'
    NotAnswer = '2c'


class SpaceControl:
    Disarm = '20'
    Arm = '21'
    PerimetralArm = '22'
    PanicBtn = '23'


class Button:
    PanicBtn = '20'


class WireInput:
    TamperOpen = '00'
    TamperClose = '01'
    OutContactFailure = '20'
    OutContactRecover = '21'
    ImpulseFailure = '22'
    ImpulseRecover = '23'


class Transmitter(AlarmsNumberDevice):
    ContactOff = '20'
    ContactOn = '21'
    ImpulseOff = '22'
    Accelerometer = '26'