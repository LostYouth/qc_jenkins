import datetime
import logging
import pymysql
from contextlib import contextmanager
from pymysql.err import DatabaseError

from ajax.db import Connector, Operators, ProdOptions, Register, Meta, Hubs, TestRoom, SoftVersion
from ajax.db.exc import NoResultFound

from app_setting import AppSetting
from qc.enum_param import QrDevTypes

logger = logging.getLogger(AppSetting.LOGGER_NAME)


def singleton_decorate(object_class):
    object_database = None

    def create_data_base(*args, **kwargs):
        nonlocal object_database
        if object_database is None:
            object_database = object_class(*args, **kwargs)
        return object_database
    return create_data_base


@singleton_decorate
class DataBaseProduction:
    def __init__(self, old_db_keys, db_keys):
        self.old_db_keys = old_db_keys
        self.db_keys = db_keys
        self.old_connect = None
        self.connect = None

    def __enter__(self):
        if self.old_connect is None:
            self.old_connect = pymysql.Connect(**self.old_db_keys, connect_timeout=2, read_timeout=2)
        self.old_connect.ping()
        if self.connect is None or not self.connect.established:
            self.connect = Connector()
            self.connect.init(**self.db_keys)
        return self.old_connect.cursor(), self.connect

    def __exit__(self, exc_type, exc_val, exc_tb):
        logger.debug("database__EXIT__ {} {} {}".format(exc_type, exc_val, exc_tb))

    @contextmanager
    def second_connect(self):
        connect_old = pymysql.Connect(**self.old_db_keys, connect_timeout=2, read_timeout=2)
        if self.connect is None or not self.connect.established:
            self.connect = Connector()
            self.connect.init(**self.db_keys)
        yield connect_old, self.connect
        connect_old.close()

    def authorize(self, login, password):
        with self as (_, conn):
            op = Operators(conn)
            if op.login(login, password):
                user = op.by_login(login)
                return user['login'], ' '.join((user['name'], user['surname']))
            return None

    def get_name_by_login(self, login):
        with self as (_, conn):
            op = Operators(conn)
            try:
                user = op.by_login(login)
                return ' '.join((user['name'], user['surname']))
            except NoResultFound:
                return None

    def get_future_firm_version_hub(self, hub_type: str):
        with self as (_, conn):
            soft_ver = SoftVersion(conn)
            return soft_ver.get(hub_type)

    def get_option_qc(self):
        with self as (_, conn):
            opt = ProdOptions(conn)
            options = opt.get_option('QC')
            return options['options']

    def client_option(self, operator_login: str):
        with self as (_, conn):
            opt = ProdOptions(conn)
            options = opt.get_option('QC')
            return options['options']['accounts'].get(operator_login, None)

    def qr_exists(self, qr):
        with self as (_, conn):
            reg = Register(conn)
            return reg.probe(qr)

    def id_exists(self, root_type, id):
        with self as (_, conn):
            reg = Register(conn)
            try:
                reg.by_id(root_type, id)
                return True
            except NoResultFound:
                return False

    def get_central_by_id(self, hub_id):
        with self as (_, conn):
            reg = Register(conn)
            meta = Meta(conn)
            hubs = Hubs(conn)
            try:
                hub = reg.by_id(QrDevTypes.Central.value, hub_id)
                hub['dev_name'] = meta.by_index(hub['dev_type'])['name_form']
                hub['hub_subtype_name'] = hubs.by_index(hub['hub_subtype'])['name_full']
                return hub
            except NoResultFound:
                return

    def get_hub_qc(self, qc_id):
        with self as (_, conn):
            reg = Register(conn)
            return reg.qc.get_model(QrDevTypes.Central.value).by_index(qc_id)

    def get_hub_long_test(self, long_test_id):
        with self as (_, conn):
            reg = Register(conn)
            return reg.long_test.get_model(QrDevTypes.Central.value).by_index(long_test_id)

    def get_hub_by_id_old(self, hub_id):
        query = "SELECT dev_type, color, prog_operator, assembler, QC, QC_time, description, frequency " \
                "FROM central_register " \
                "WHERE serv_id=%s " \
                "LIMIT 1"
        with self as (cursor, conn):
            cursor.execute(query, hub_id)
            result = cursor.fetchone()
            self.old_connect.commit()
        return result

    def get_bridge_by_id(self, bridge_id):
        query = "SELECT dev_type, color, prog_operator, assembler, QC, QC_time, description, frequency " \
                "FROM central_register " \
                "WHERE qr_code=%s " \
                "LIMIT 1"
        with self as (cursor, _):
            cursor.execute(query, bridge_id)
            result = cursor.fetchone()
            self.old_connect.commit()
        return result

    def get_defects_devices(self):
        with self as (cursor, conn):
            opt = ProdOptions(conn)
            options = opt.get_option('QC')
            return options['options']['defects']

    def get_pro_acc(self):
        with self as (cursor, conn):
            opt = ProdOptions(conn)
            options = opt.get_option('QC')
            return options['options']['share_pro_acc_hub']

    def get_longtest_hub_by_id(self, hub_id):
        self.old_connect.ping()
        query = "SELECT result " \
                "FROM test_results " \
                "WHERE hub_id='{}' " \
                "ORDER BY id DESC LIMIT 1".format(hub_id)
        with self as (cursor, conn):
            cursor.execute(query)
            self.old_connect.commit()
            return cursor.fetchone()

    def get_device_by_id_old(self, dev_id):
        query = "SELECT prog_operator, color, dev_type, assembler, stand_operator, longtest_operator, QC, QC_time, frequency " \
                "FROM device_register " \
                "WHERE dev_id=%s " \
                "LIMIT 1"
        with self as (cursor, conn):
            cursor.execute(query, dev_id)
            self.old_connect.commit()
            return cursor.fetchone()

    def get_device_by_id(self, dev_id):
        with self as (cursor, conn):
            reg = Register(conn)
            meta = Meta(conn)
            try:
                dev = reg.by_id(QrDevTypes.Device.value, dev_id)
                dev['dev_name'] = meta.by_index(dev['dev_type'])['name_form']
                return dev
            except NoResultFound:
                return

    def get_device_qc(self, qc_id):
        with self as (_, conn):
            reg = Register(conn)
            return reg.qc.get_model(QrDevTypes.Device.value).by_index(qc_id)

    def get_device_calibration(self, calibration_id):
        with self as (_, conn):
            reg = Register(conn)
            return reg.calibration.get_model(QrDevTypes.Device.value).by_index(calibration_id)

    def get_device_test_room(self, test_room_id):
        with self as (_, conn):
            reg = Register(conn)
            return reg.test_room.get_model(QrDevTypes.Device.value).by_index(test_room_id)

    def get_device_long_test(self, long_test_id):
        with self as (_, conn):
            reg = Register(conn)
            return reg.long_test.get_model(QrDevTypes.Device.value).by_index(long_test_id)

    def register_device_success(self, qr, operator, grade, defects, info):
        with self as (_, conn):
            reg = Register(conn)
            reg.qc.add(reg.by_qr(qr), operator=operator, info=info, grade=grade, defects=defects)

    def register_device_success_old(self, operator, dev_id, grade_qc, defects_qc):
        query = "UPDATE device_register " \
                "SET QC='{}', QC_time=now(), class_b={}, QC_defects='{}' " \
                "WHERE dev_id='{}' " \
                "LIMIT 1".format(operator, grade_qc, defects_qc, dev_id)
        with self as (cursor, _):
            cursor.execute(query)
            if cursor.rowcount == 1:
                self.old_connect.commit()
            else:
                self.old_connect.rollback()
                raise DatabaseError("Count update rows > 1 or 0")

    def register_device_fail(self, operator, dev_qr, defects, info):
        with self as (_, conn):
            reg = Register(conn)
            reg.qc.add(reg.by_qr(dev_qr), operator=operator, info=info, defects=defects, success=False)

    def register_device_fail_old(self, dev_id, defects_qc):
        query = "UPDATE device_register " \
                "SET QC_defects='{}', QC=null, QC_time=null, class_b=null " \
                "WHERE dev_id='{}' " \
                "LIMIT 1".format(defects_qc, dev_id)
        with self as (cursor, conn):
            cursor.execute(query)
            self.old_connect.commit()

    def register_central_success(self, id, operator, grade, defects, info, country=None, pro_acc=None):
        with self as (_, conn):
            reg = Register(conn)
            hub = reg.by_id(QrDevTypes.Central.value, id)
            reg.qc.add(hub, operator=operator, info=info,
                       grade=grade, defects=defects)
            reg.update(hub, country=country, pro_account_email=pro_acc)

    def register_central_fail(self, operator, central_id, defects, info):
        with self as (_, conn):
            reg = Register(conn)
            reg.qc.add(reg.by_id(QrDevTypes.Central.value, central_id), operator=operator,
                       info=info, defects=defects, success=False)

    def get_stand_room_device(self, qr: str, id_record: int):
        with self as (_, conn):
            stand_room = TestRoom(conn)
            try:
                dev_res = list(stand_room.select({"qr": qr}, root_type='dev', id=id_record))
            except NoResultFound:
                return dict()
            else:
                return dev_res[-1] if dev_res else dict()

    def register_central_success_old(self, serv_id, operator, grade_qc, defects_qc, description):
        query = "UPDATE central_register " \
                "SET QC='{}', QC_time=now(), class_b={}, QC_defects='{}', description='{}'" \
                "WHERE serv_id='{}' " \
                "LIMIT 1".format(
            operator, grade_qc, defects_qc, description, serv_id
        )

        with self as (cursor, conn):
            cursor.execute(query)
            if cursor.rowcount == 1:
                self.old_connect.commit()
            else:
                self.old_connect.rollback()
                raise DatabaseError("Count update rows > 1 or 0")

    def register_bridge_success_old(self, serv_id, operator, grade_qc, defects_qc, description):
        query = "UPDATE central_register " \
                "SET QC='{}', QC_time=now(), class_b={}, QC_defects='{}', description='{}'" \
                "WHERE qr_code='{}' " \
                "LIMIT 1".format(
            operator, grade_qc, defects_qc, description, serv_id
        )

        with self as (cursor, conn):
            cursor.execute(query)
            if cursor.rowcount == 1:
                self.old_connect.commit()
            else:
                self.old_connect.rollback()
                raise DatabaseError("Count update rows > 1 or 0")

    def register_central_fail_old(self, serv_id, defects_qc):
        query = "UPDATE central_register SET QC=null, QC_time=null, class_b=null, QC_defects='{}' " \
                "WHERE serv_id='{}' LIMIT 1".format(defects_qc, serv_id)
        with self as (cursor, conn):
            cursor.execute(query)
            if cursor.rowcount == 1:
                self.old_connect.commit()
            else:
                self.old_connect.rollback()

    def register_bridge_fail_old(self, bridge_id, defects_qc):
        query = "UPDATE central_register SET QC=null, QC_time=null, class_b=null, QC_defects='{}' " \
                "WHERE qr_code='{}' LIMIT 1".format(defects_qc, bridge_id)
        with self as (cursor, conn):
            cursor.execute(query)
            if cursor.rowcount == 1:
                self.old_connect.commit()
            else:
                self.old_connect.rollback()

    def send_repair(self, root_type, dev_id, operator, from_stage, reason):
        with self as (_, conn):
            reg = Register(conn)
            reg.repair.add(reg.by_id(root_type, dev_id), income_operator=operator,
                           from_stage=from_stage, reason=reason, status='Відправлено у ремонт')

    def send_repair_old(self, qr_code, dev_type, operator, from_stage, malfunction):
        query = "INSERT INTO repair (qr_code, dev_type, from_stage, who_sent, when_sent, repair_reason) " \
                "VALUES ('{}', '{}', '{}', '{}', '{}', '{}')".format(qr_code, dev_type, from_stage, operator,
                                                                     datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                                                                     malfunction)
        with self as (cursor, conn):
            cursor.execute(query)
            self.old_connect.commit()

    def get_type_central_devices_by_operator(self, operator_full_name, operator_login):
        time_start = datetime.datetime.now().strftime("%Y-%m-%d 00:00:01")
        time_finish = datetime.datetime.now().strftime("%Y-%m-%d 23:59:59")
        query_cental = "SELECT dev_type FROM central_register WHERE QC='{}' and QC_time >= '{}' and QC_time <= '{}'".format(
            operator_full_name, time_start, time_finish
        )
        query_device = "SELECT dev_type FROM device_register WHERE QC='{}' and QC_time > '{}' and QC_time <= '{}'".format(
            operator_full_name, time_start, time_finish
        )
        result = []

        with self.second_connect() as (conn_old, conn):
            cursor = conn_old.cursor()
            reg = Register(conn)
            meta = Meta(conn)
            hubs = Hubs(conn)
            dev_names = {dev['type_index']: dev['name_form'] for dev in meta.select()}
            hub_names = {hub['subtype_index']: hub['name_form'] for hub in hubs.select()}
            qrs = set()
            for qc in reg.qc.select_all(operator=operator_login, success=True,
                                        from_time=datetime.datetime.now().date()):
                qrs.add(qc['qr'])

            if qrs:
                for dev in reg.select_all(qrs=qrs):
                    if 'hub_subtype' in dev:
                        result.append(hub_names[dev['hub_subtype']])
                    else:
                        result.append(dev_names[dev['dev_type']])

            cursor.execute(query_cental)
            result_central = cursor.fetchall()
            [result.append(dev_type) for dev_type, *_ in result_central]
            cursor.execute(query_device)
            result_device = cursor.fetchall()
            [result.append(dev_type) for dev_type, *_ in result_device]
            self.old_connect.commit()
        return result
