import pymysql
import logging

from pymysql import DatabaseError

from app_setting import AppSetting
logger = logging.getLogger(AppSetting.LOGGER_NAME)


def singleton_decorate(object_class):
    object_database = None

    def create_data_base(*args, **kwargs):
        nonlocal object_database
        if object_database is None:
            object_database = object_class(*args, **kwargs)
        return object_database
    return create_data_base


@singleton_decorate
class DataBaseCsa:
    def __init__(self, **kw):
        self.keys_database = kw
        self.connect = None

    def __enter__(self):
        if self.connect is None:
            self.connect = pymysql.Connect(**self.keys_database, connect_timeout=2, read_timeout=2)
        self.connect.ping()
        return self.connect.cursor()

    def __exit__(self, exc_type, exc_val, exc_tb):
        logger.debug("database__EXIT__ {} {} {}".format(exc_type, exc_val, exc_tb))

    def get_hub_firmware_version(self, hub_id):
        query = "SELECT firmVersion FROM hubs WHERE HubId=%s"
        with self as cursor:
            cursor.execute(query, hub_id)
        self.connect.commit()
        return cursor.fetchone()

    def set_fw_and_group_hub(self, hub_id: str, hub_fw: int, group: int):
        query = "UPDATE hubs SET fwGroup={}, firmVersion={} WHERE HubId='{}' LIMIT 1".format(group, hub_fw, hub_id)
        with self as cursor:
            cursor.execute(query)
            if cursor.rowcount <= 1:
                self.connect.commit()
            else:
                self.connect.rollback()
                raise DatabaseError("FwGroup, firmVersion for hub {} not update".format(hub_id))

    def set_group_hub(self, hub_id: str, group: int):
        query = "UPDATE hubs SET fwGroup={} WHERE HubId='{}' LIMIT 1".format(group, hub_id)
        with self as cursor:
            cursor.execute(query)
            if cursor.rowcount <= 1:
                self.connect.commit()
            else:
                self.connect.rollback()
                raise DatabaseError("FwGroup for hub {} not updated".format(hub_id))

    def get_group_hub(self, hub_id):
        query = "SELECT fwGroup FROM hubs WHERE HubId='{}' LIMIT 1".format(hub_id)
        with self as cursor:
            cursor.execute(query)
            self.connect.commit()
            fwGroup, *_ = cursor.fetchone()
            return fwGroup
