from enum import Enum



class ColorResult(Enum):
    success = 'background-color: rgb(33, 232, 10)'
    fail = "background-color: rgb(255, 222, 0)"
    unknow = 'background-color: yellow;'
