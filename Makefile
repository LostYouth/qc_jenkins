BRANCH := $(shell git rev-parse --abbrev-ref HEAD)
APP_NAME := New_QC
VERSION := $(shell python3 -c "from __version__ import __version__; print(__version__);")
platform := linux
BUILD_NAME := $(APP_NAME)_v.$(VERSION)

buildName:
	@echo $(BUILD_NAME)

version:
	python3 __main__.py -v

venv: requirements.txt
	source /Users/yurasenchakov/PycharmProjects/qc_jenkins/test_qtt/bin/activate;\
	pip install -Ur requirements.txt;\
	cp /Users/yurasenchakov/PycharmProjects/qc_jenkins/csa_client_qt/security /Users/yurasenchakov/workspace/Jenkins_test_repo/csa_client_qt;\
	cp /Users/yurasenchakov/PycharmProjects/qc_jenkins/qc/security/security.py /Users/yurasenchakov/workspace/Jenkins_test_repo/qc/security;\
	cp /Users/yurasenchakov/PycharmProjects/qc_jenkins/csa_client_qt/security /Users/yurasenchakov/workspace/Jenkins_test_repo@2/csa_client_qt;\
	cp /Users/yurasenchakov/PycharmProjects/qc_jenkins/qc/security/security.py /Users/yurasenchakov/workspace/Jenkins_test_repo@2/qc/security

clear:
	if [ -d "./build/$(BUILD_NAME)" ]; then rm -r "./build/$(BUILD_NAME)"; fi;
	if [ -c "./dist/$(BUILD_NAME)" ]; then rm -f "./dist/$(BUILD_NAME)"; fi;


build:
	source /Users/yurasenchakov/PycharmProjects/qc_jenkins/test_qtt/bin/activate;\
	python --version;\
	python -m PyInstaller __main__.py -Fn $(BUILD_NAME) --specpath spec --hidden-import='distutils' -p /usr/lib/python3.6/

zip:
	cd ./dist && zip -r $(BUILD_NAME)_$(platform).zip ./$(BUILD_NAME)
