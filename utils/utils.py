import datetime
import time

import pymysql
from ajax.db import ProdOptions, Connector, Operators
from passlib.hash import sha256_crypt


def try_connect_bd(**kw):
    for i in range(60):
        try:
            pymysql.Connect(**kw)
        except Exception as e:
            time.sleep(1)
        else:
            break


def create_production_db(**kw):
    conn = pymysql.Connect(
        **kw
    )
    with conn as cursor:
        cursor.execute("CREATE DATABASE IF NOT EXISTS debug_production;")
        conn.commit()


def create_prod_table_new_db(**kw):
    conn = pymysql.Connect(**kw)
    with conn as cursor:
        cursor.execute(
            """create table if not exists prod_operators 
            (
                ud INT(11) NOT NULL A
            )"""
        )

        cursor.execute(
            """create table if not exists prod_options 
            (
                id INT(11) NOT NULL AUTO_INCREMENT,
                app_name varchar(100) NOT NULL,
                options JSON NOT NULL,
                time DATETIME NOT NULL,
                operator VARCHAR(32) NOT NULL,
                PRIMARY KEY(id)
                
            )
            """
        )


def create_prod_table_db_old(**kw):
    conn = pymysql.Connect(**kw)
    with conn as cursor:
        # central_register
        cursor.execute(
            """create table if not exists central_register (
                id INT(10) NOT NULL AUTO_INCREMENT,
                central_id VARCHAR(150),
                qr_code VARCHAR(150),
                dev_type VARCHAR(150),
                color VARCHAR(150),
                serv_id VARCHAR(150),
                mac_eth VARCHAR(150), 
                mac_wifi VARCHAR(150), 
                hardware_ver VARCHAR(150), 
                firmware_ver VARCHAR(150), 
                IMEI VARCHAR(100),
                prog_operator VARCHAR(150),
                prog_time VARCHAR(150),
                assembler VARCHAR(150),
                assembler_time VARCHAR(150),
                longtest_operator VARCHAR(150),
                longtest_time VARCHAR(150),
                QC VARCHAR(150),
                QC_time VARCHAR(150),
                class_b BIT(1),
                QC_defects VARCHAR(150),
                packer VARCHAR(150),
                packer_time VARCHAR(150),
                restored_count INT(11),
                frequency VARCHAR(255),
                description VARCHAR(400),
                PRIMARY KEY (id),
                UNIQUE KEY (central_id, serv_id, mac_eth, mac_wifi)
            )
            """
        )
        # device register
        cursor.execute(
            """create table if not exists device_register (
                id INT(10) NOT NULL AUTO_INCREMENT,
                dev_id VARCHAR(8) NOT NULL,
                qr_code VARCHAR(25) NOT NULL,
                dev_type VARCHAR(20),
                color VARCHAR(10),
                firmware_version VARCHAR(10) NOT NULL,
                prog_operator VARCHAR(40),
                prog_time VARCHAR(150),
                prog_test_params VARCHAR(409),
                assembler VARCHAR(40),
                assembler_time VARCHAR(150),
                stand_operator VARCHAR(40),
                stand_time VARCHAR(150),
                longtest_operator VARCHAR(40),
                longtest_reg_time VARCHAR(150),
                longtest_time VARCHAR(100),
                long_test_params VARCHAR(409),
                QC VARCHAR(40),
                QC_time VARCHAR(150),
                class_b BIT(1),
                QC_defects VARCHAR(400),
                packer VARCHAR(150),
                packer_time VARCHAR(150),
                restored_count INT(11),
                frequency VARCHAR(255),
                description VARCHAR(400),
                PRIMARY KEY(id)
            )
            """)
        # qc parameters
        cursor.execute(
            """create table if not exists qc_dev_parameters (
                id INT(11) NOT NULL AUTO_INCREMENT,
                device_type VARCHAR(150) NOT NULL,
                qc_parameter VARCHAR(150) NOT NULL,
                parameter_value VARCHAR(400) NOT NULL,
                PRIMARY KEY(id)
            );
            """
        )


def create_user(conn: Connector, login: str, name: str, surname: str, password: str, role: dict, active=1):
    hash_password = sha256_crypt.hash(password)
    op = Operators(conn)
    op.insert(
        login=login,
        name=name,
        surname=surname,
        password=hash_password,
        role=role,
        active=active
    )


def create_options(connector: Connector, app_name: str, options: dict, operator: str):
    o = ProdOptions(connector)
    o.insert(
        app_name=app_name,
        options=options,
        operator=operator,
        time=datetime.datetime.now()
    )
