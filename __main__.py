# Need for PyInstaller
import distutils
import pkg_resources.py2_warn  # <-- need if PyInstaller highest 3.2 run this cmd `pip install setuptools==45.0.0`
from passlib import handlers
from passlib.handlers import sha2_crypt
import PyQt5.sip

import os
import logging
import sys
from PyQt5 import QtWidgets
from qc.security import KeyDatabase
from app_setting import AppSetting
from qc.main import QcMainWindow
import setup_log
import argparse
from telegram_api.setup_telegrambot import setup_telegram
from __version__ import __version__
from __app_name__ import __app_name__


app_name = __app_name__
app_version = __version__

bot_token = '827080207:AAFfwMFXTSpW4InVR9kNn692hXX2wokyJQM'
chat_id = -329486689

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-k", '--key', help="launch key")
    parser.add_argument("-d", '--debug', help="debug mode", action="store_true")
    parser.add_argument("-v", '--version', help="get version app", action="store_true")
    parser.add_argument("-mv", '--my_version', help="get version special make file", action="store_true")
    argv = parser.parse_args()
    if argv.version:
        print("Version {} {}".format(app_name, app_version))
        sys.exit(0)
    if argv.my_version:
        print("{}_v.{}".format(app_name, app_version))
        sys.exit(0)

    setup_log.setup_logging()
    logger = logging.getLogger(AppSetting.LOGGER_NAME)

    app = QtWidgets.QApplication(sys.argv)
    path_dir_app = os.path.dirname(app.applicationDirPath())

    # needed for PyUpdater
    # if os.path.split(path_dir_app)[-1] != 'venv':
    #     os.chdir(path_dir_app)
    # logger.info("Move dir application {}".format(path_dir_app))

    if argv.debug:
        AppSetting.DEBUG = True
        KeyDatabase.DB_NAME_OLD = KeyDatabase.DEBUG_DB_NAME_OLD
        KeyDatabase.DB_NAME = KeyDatabase.DEBUG_DB_NAME
        KeyDatabase.MODEL_CSA = KeyDatabase.DEBUG_MODEL_CSA

    setup_telegram(bot_token, chat_id)
    app.setApplicationName(app_name)
    app.setApplicationVersion(app_version)
    w = QcMainWindow()
    w.setWindowTitle("{} {}".format(app_name, app_version))
    w.show()

    if not argv.debug:
        exceptionHandler = setup_log.ExceptionHandler()
        sys.excepthook = exceptionHandler.handler
        exceptionHandler.errorSignal.connect(setup_log.log_error)
    app.exec_()
