import logging
import re
import time
from abc import ABC, abstractmethod
from PyQt5 import QtCore
from app_setting import AppSetting
from tests.utils import db_handler

logger = logging.getLogger(AppSetting.LOGGER_NAME)


class AbstractTestView(ABC):
    dev_type = None
    dev_id = None
    dev_type_num = None

    @abstractmethod
    def test_success_device_reg(self, *args):
        pass

    @abstractmethod
    def test_scan_device_without_prog(self, *args):
        pass

    @abstractmethod
    def test_scan_device_without_assembling(self, *args):
        pass

    @abstractmethod
    def test_device_with_qc_passed(self, *args):
        pass

    @abstractmethod
    def test_success_device_reg_with_grade_defects(self, *args):
        pass

    @abstractmethod
    def test_all_func_fail(self, *args):
        pass

    @staticmethod
    def add_device_last_steps(database, programming=False,
                                        assembling=False,
                                        calibration=False,
                                        long_test=False,
                                        test_room=False,
                                        qc=False):

        database.delete_device_from_db()
        database.insert_device_in_db()
        if programming:
            database.insert_device_prog()
        if assembling:
            database.insert_device_assembler()
        if calibration:
            database.insert_device_calibration()
        if long_test:
            database.insert_device_long_test()
        if test_room:
            database.insert_device_test_room()
        if qc:
            database.insert_device_qc()

    @staticmethod
    def wait_freq_apply(qtbot, main_instance):
        with qtbot.waitSignal(signal=main_instance.qc_main_widget.widget_changed_frequency.signal_change_frequency,
                              timeout=5 * 1000):
            assert True
        assert re.match(r'EU\s\d{3}\.\d{2,3}\s\d{3}\.\d{2,3}',
                        main_instance.qc_main_widget.label_frequency_device.text()).group(0)

    def scan_and_test_default_setup(self, main_instance, view_obj_num):
        main_instance.qc_main_widget.handler_qr_code(db_handler.devices[self.dev_type][0])
        assert main_instance.qc_main_widget.view_obj[view_obj_num].cellWidget(0, 0).text() == db_handler.devices[self.dev_type][0]
        assert not main_instance.qc_main_widget.view_obj[view_obj_num].cellWidget(0, 1).check_box_grade_defect.isChecked()
        assert main_instance.qc_main_widget.view_obj[view_obj_num].cellWidget(0, 1).push_btn_select_defect.isEnabled()
        assert main_instance.qc_main_widget.view_obj[view_obj_num].cellWidget(0, 1).defects_popup.get_defects_return() == []

    def scan_without_prog(self, qtbot, mock_socket_csa, database):
        time.sleep(1)
        main_w, *_ = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_device_last_steps(database=database)

        self.wait_freq_apply(qtbot, main_w)

        main_w.qc_main_widget.handler_qr_code(db_handler.devices[self.dev_type][0])
        assert main_w.qc_main_widget.err_widget.label_message_text.text() == "Етап програмування та первинного " \
                                                                             "тестування не пройдений, поверніть його " \
                                                                             "на відповідний етап!"

    def scan_without_assembling(self, qtbot, mock_socket_csa, database):
        main_w, *_ = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_device_last_steps(database=database, programming=True)

        self.wait_freq_apply(qtbot, main_w)

        main_w.qc_main_widget.handler_qr_code(db_handler.devices[self.dev_type][0])
        assert main_w.qc_main_widget.err_widget.label_message_text.text() == "Етап зборки не пройдений, " \
                                                                             "поверніть пристрій на відповідний етап!"

    def scan_without_calibration(self, qtbot, mock_socket_csa, database):
        main_w, *_ = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_device_last_steps(database=database, programming=True, assembling=True)

        self.wait_freq_apply(qtbot, main_w)

        main_w.qc_main_widget.handler_qr_code(db_handler.devices[self.dev_type][0])
        assert main_w.qc_main_widget.err_widget.label_message_text.text() == "Етап калібровки не пройдений," \
                                                                            " поверніть пристрій на відповідний етап!"

    def scan_without_long_test(self, qtbot, mock_socket_csa, database):
        main_w, *_ = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_device_last_steps(database=database, programming=True, assembling=True, calibration=True)

        self.wait_freq_apply(qtbot, main_w)

        main_w.qc_main_widget.handler_qr_code(db_handler.devices[self.dev_type][0])
        assert main_w.qc_main_widget.err_widget.label_message_text.text() == "Етап лонг теста не пройдений, " \
                                                                             "поверніть пристрій на відповідний етап!"

    def scan_without_test_room(self, qtbot, mock_socket_csa, database):
        main_w, *_ = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_device_last_steps(database=database, programming=True, assembling=True, calibration=True, long_test=True)

        self.wait_freq_apply(qtbot, main_w)

        main_w.qc_main_widget.handler_qr_code(db_handler.devices[self.dev_type][0])
        assert main_w.qc_main_widget.err_widget.label_message_text.text() == "Етап тест кімнати не пройдений, " \
                                                                             "поверніть пристрій на відповідний етап!"

    def scan_device_with_qc_passed(self, qtbot, main_w, database):
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_device_last_steps(database=database, programming=True,
                                                      assembling=True,
                                                      calibration=True,
                                                      long_test=True,
                                                      test_room=True,
                                                      qc=True)

        self.wait_freq_apply(qtbot, main_w)

        main_w.qc_main_widget.handler_qr_code(db_handler.devices[self.dev_type][0])

    @staticmethod
    def add_grade_defects(qtbot, main_w, view_obj_num):
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[view_obj_num].cellWidget(0, 1).push_btn_select_defect,
                         QtCore.Qt.LeftButton)
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[view_obj_num].cellWidget(0, 1).defects_popup.push_btn_select_defect,
                         QtCore.Qt.LeftButton)
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[view_obj_num].cellWidget(0, 1).check_box_grade_defect,
                         QtCore.Qt.LeftButton)


