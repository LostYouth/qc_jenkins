import re
import pytest
from PyQt5 import QtCore
from app_setting import AppSetting
from qc.security.security import KeyDatabase
from qc.enum_color import ColorResult
from tests.utils import db_handler
from __version__ import __version__


@pytest.mark.last
class TestGeneral:
    AppSetting.DEBUG = True

    def test_success_multiply_device_reg(self, qtbot, mock_main_w_scanner_ports, mock_socket_csa, mock_qr_scanner_main_w):
        dev_type = "door"
        dev_id = db_handler.devices[dev_type][0][:6]
        dev_type_num = db_handler.devices[dev_type][0][-3:-1]
        dev_id1 = "213456"
        main_w, server, socket = mock_socket_csa
        socket.dev = (dev_id, dev_type_num)
        qtbot.addWidget(main_w.qc_main_widget)

        # add device in DB
        database = db_handler.DBHandler(KeyDatabase.get_keys_production(), dev_type)
        database.delete_device_from_db()
        database.insert_device_in_db()
        database.insert_device_prog()
        database.insert_device_assembler()
        database.insert_device_long_test()

        db_handler.devices["door"] = ("213456011", "1")
        # add device2 in DB
        database = db_handler.DBHandler(KeyDatabase.get_keys_production(), dev_type)
        database.delete_device_from_db()
        database.insert_device_in_db()
        database.insert_device_prog()
        database.insert_device_assembler()
        database.insert_device_long_test()

        db_handler.devices["door"] = ("444321011", "1")

        qtbot.mouseClick(main_w.qc_main_widget.pushButton_connect_scanner, QtCore.Qt.LeftButton)
        assert not main_w.qc_main_widget.comboBox_ports_scanner.isEnabled()

        # test frequency apply
        with qtbot.waitSignal(signal=main_w.qc_main_widget.widget_changed_frequency.signal_change_frequency,
                              timeout=3 * 1000):
            assert True
        assert re.match(r'EU\s\d{3}\.\d{2,3}\s\d{3}\.\d{2,3}',
                        main_w.qc_main_widget.label_frequency_device.text()).group(0)

        # scan device with qr scanner && test default setup table
        main_w.qc_main_widget.handler_qr_code(db_handler.devices[dev_type][0])
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 0).text() == db_handler.devices[dev_type][0]
        assert not main_w.qc_main_widget.view_obj[5].cellWidget(0, 1).check_box_grade_defect.isChecked()
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 1).push_btn_select_defect.isEnabled()
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 1).defects_popup.get_defects_return() == []

        # scan device2 with qr scanner && test default setup table
        main_w.qc_main_widget.handler_qr_code("213456011")
        assert main_w.qc_main_widget.view_obj[5].cellWidget(1, 0).text() == "213456011"
        assert not main_w.qc_main_widget.view_obj[5].cellWidget(1, 1).check_box_grade_defect.isChecked()
        assert main_w.qc_main_widget.view_obj[5].cellWidget(1, 1).push_btn_select_defect.isEnabled()
        assert main_w.qc_main_widget.view_obj[5].cellWidget(1, 1).defects_popup.get_defects_return() == []

        # test click button start reg process1
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[5].cellWidget(0, 2).push_btn_connect_device,
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='05', message_key='00')

        socket.messages_heap.append(server.send_alarm("09", "08", (dev_id, dev_type_num)))
        assert not main_w.qc_main_widget.view_obj[5].cellWidget(0, 2).push_btn_connect_device.isEnabled()
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 2).styleSheet() == ColorResult.success.value
        assert socket.check_message_exists(message_type='07', message_key=None,
                                           message_payload=[dev_type_num, f'00{dev_id}', '33', '01', '34', '01'])

        # test check tamper alarms1
        assert not main_w.qc_main_widget.view_obj[5].cellWidget(0, 3).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm(
                "07", "01", (dev_id, dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 07" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 3).label_off.text() == f'ВИКЛ: {i + 1}/2'

            socket.messages_heap.append(server.send_alarm(
                "01", "00", (dev_id, dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 01" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 3).label_on.text() == f'ВКЛ: {i + 1}/2'

        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 3).label_on.styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 3).label_off.styleSheet() == ColorResult.success.value

        # test check reed switch alarms1
        assert not main_w.qc_main_widget.view_obj[5].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm(
                "04", "21", (dev_id, dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 04" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 4).text() == f'{i + i + 1}'

            socket.messages_heap.append(server.send_alarm(
                "00", "20", (dev_id, dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 00" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 4).text() == f'{i + i + 2}'

            if int(main_w.qc_main_widget.view_obj[5].cellWidget(0, 4).text()) >= 2:
                assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        # test check extra contact alarms1
        assert not main_w.qc_main_widget.view_obj[5].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm(
                "04", "23", (dev_id, dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 04" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 5).text() == f'{i + i + 1}'

            socket.messages_heap.append(server.send_alarm(
                "00", "22", (dev_id, dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 00" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 5).text() == f'{i + i + 2}'

            if int(main_w.qc_main_widget.view_obj[5].cellWidget(0, 5).text()) >= 2:
                assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        # test check batt lvl update1
        assert not main_w.qc_main_widget.view_obj[5].cellWidget(0, 6).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update(
            (dev_id, dev_type_num), ("05", "0064")
        ))
        # time.sleep(1)
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 6).label_status.text() == '100'
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 6).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 7).isEnabled()
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 8).isEnabled()
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 8).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 8).text() == 'Зареєструвати'
        # time.sleep(2)

        socket.dev = (dev_id1, dev_type_num)

        # test click button start reg process2
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[5].cellWidget(1, 2).push_btn_connect_device,
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='05', message_key='00')

        socket.messages_heap.append(server.send_alarm("09", "08", (dev_id1, dev_type_num)))
        assert not main_w.qc_main_widget.view_obj[5].cellWidget(1, 2).push_btn_connect_device.isEnabled()
        assert main_w.qc_main_widget.view_obj[5].cellWidget(1, 2).styleSheet() == ColorResult.success.value
        assert socket.check_message_exists(message_type='07', message_key=None,
                                           message_payload=[dev_type_num, f'00{dev_id1}', '33', '01', '34', '01'])

        # test check tamper alarms2
        assert not main_w.qc_main_widget.view_obj[5].cellWidget(1, 3).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm(
                "07", "01", (dev_id1, dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 07" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[5].cellWidget(1, 3).label_off.text() == f'ВИКЛ: {i + 1}/2'

            socket.messages_heap.append(server.send_alarm(
                "01", "00", (dev_id1, dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 01" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[5].cellWidget(1, 3).label_on.text() == f'ВКЛ: {i + 1}/2'

        assert main_w.qc_main_widget.view_obj[5].cellWidget(1, 3).label_on.styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[5].cellWidget(1, 3).label_off.styleSheet() == ColorResult.success.value

        # test check reed switch alarms2
        assert not main_w.qc_main_widget.view_obj[5].cellWidget(1, 4).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm(
                "04", "21", (dev_id1, dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 04" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[5].cellWidget(1, 4).text() == f'{i + i + 1}'

            socket.messages_heap.append(server.send_alarm(
                "00", "20", (dev_id1, dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 00" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[5].cellWidget(1, 4).text() == f'{i + i + 2}'

            if int(main_w.qc_main_widget.view_obj[5].cellWidget(1, 4).text()) >= 2:
                assert main_w.qc_main_widget.view_obj[5].cellWidget(1, 4).styleSheet() == ColorResult.success.value

        # test check extra contact alarms2
        assert not main_w.qc_main_widget.view_obj[5].cellWidget(1, 5).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm(
                "04", "23", (dev_id1, dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 04" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[5].cellWidget(1, 5).text() == f'{i + i + 1}'

            socket.messages_heap.append(server.send_alarm(
                "00", "22", (dev_id1, dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 00" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[5].cellWidget(1, 5).text() == f'{i + i + 2}'

            if int(main_w.qc_main_widget.view_obj[5].cellWidget(1, 5).text()) >= 2:
                assert main_w.qc_main_widget.view_obj[5].cellWidget(1, 5).styleSheet() == ColorResult.success.value

        # test check batt lvl update2
        assert not main_w.qc_main_widget.view_obj[5].cellWidget(1, 6).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update(
            (dev_id1, dev_type_num), ("05", "0064")
        ))
        # time.sleep(1)
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[5].cellWidget(1, 6).label_status.text() == '100'
        assert main_w.qc_main_widget.view_obj[5].cellWidget(1, 6).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[5].cellWidget(1, 7).isEnabled()
        assert main_w.qc_main_widget.view_obj[5].cellWidget(1, 8).isEnabled()
        assert main_w.qc_main_widget.view_obj[5].cellWidget(1, 8).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[5].cellWidget(1, 8).text() == 'Зареєструвати'
        # time.sleep(2)

        # test success register device2
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[5].cellWidget(1, 8),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='05', message_key='05')

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[5].cellWidget(1, 8) is None, timeout=5 * 1000)
        socket.messages_heap.append(server.send_alarm(
            "09", "09", (dev_id1, dev_type_num)
        ))
        socket.messages_heap.append(server.update_delete_device(
            (dev_id1, dev_type_num)
        ))
        qtbot.waitUntil(lambda: "19 04" not in str(socket.messages_heap), timeout=5 * 1000)
        _, grade, _, info, defects, time_reg, success, operator, qr = \
            database.get_stat_device_after_success_reg(qr="213456011")

        assert grade == '0'
        assert __version__ in info
        assert defects == '""'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 1
        assert operator == 'armen'
        assert qr == "213456011"

        # test success register device1
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[5].cellWidget(0, 8),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='05', message_key='05')

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[5].cellWidget(0, 8) is None, timeout=5 * 1000)
        socket.messages_heap.append(server.send_alarm(
            "09", "09", (dev_id, dev_type_num)
        ))
        socket.messages_heap.append(server.update_delete_device(
            (dev_id, dev_type_num)
        ))
        qtbot.waitUntil(lambda: "19 04" not in str(socket.messages_heap), timeout=5 * 1000)
        _, grade, _, info, defects, time_reg, success, operator, qr = \
            database.get_stat_device_after_success_reg(qr=db_handler.devices[dev_type][0])

        assert grade == '0'
        assert __version__ in info
        assert defects == '""'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 1
        assert operator == 'armen'
        assert qr == db_handler.devices[dev_type][0]

        database.delete_device_from_db()
        db_handler.devices["door"] = ("213456011", "1")
        database.delete_device_from_db()

    def test_add_10_devices(self, qtbot, mock_main_w_scanner_ports, mock_socket_csa):
        dev_qrs = ['987654011', '987653011', '987652011', '987651011', '987650011',
                   '987659011', '987658011', '987657011', '987656011', '987655011']
        main_w, server, socket = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)
        database = db_handler.DBHandler(KeyDatabase.get_keys_production(), '')

        # test frequency apply
        with qtbot.waitSignal(signal=main_w.qc_main_widget.widget_changed_frequency.signal_change_frequency,
                              timeout=3 * 1000):
            assert True

        for row, qr in enumerate(dev_qrs):
            database.add_device_last_steps_by_qr(qr, "door", programming=True, assembling=True, long_test=True)

            main_w.qc_main_widget.handler_qr_code(qr)
            assert main_w.qc_main_widget.view_obj[5].cellWidget(row, 0).text() == qr
            assert not main_w.qc_main_widget.view_obj[5].cellWidget(row, 1).check_box_grade_defect.isChecked()
            assert main_w.qc_main_widget.view_obj[5].cellWidget(row, 1).push_btn_select_defect.isEnabled()
            assert main_w.qc_main_widget.view_obj[5].cellWidget(row, 1).defects_popup.get_defects_return() == []
            #time.sleep(0.5)

        for row, qr in enumerate(dev_qrs):
            #time.sleep(0.5)
            dev_id = qr[:6]
            dev_type_num = '01'
            socket.dev = (dev_id, dev_type_num)

            # test click button start reg process1
            qtbot.mouseClick(main_w.qc_main_widget.view_obj[5].cellWidget(row, 2).push_btn_connect_device,
                             QtCore.Qt.LeftButton)
            assert socket.check_message_exists(message_type='05', message_key='00')

            socket.messages_heap.append(server.send_alarm("09", "08", (dev_id, dev_type_num)))
            assert not main_w.qc_main_widget.view_obj[5].cellWidget(row, 2).push_btn_connect_device.isEnabled()
            assert main_w.qc_main_widget.view_obj[5].cellWidget(row, 2).styleSheet() == ColorResult.success.value
            assert socket.check_message_exists(message_type='07', message_key=None,
                                               message_payload=[dev_type_num, f'00{dev_id}', '33', '01', '34', '01'])

            # test check tamper alarms
            assert not main_w.qc_main_widget.view_obj[5].cellWidget(row, 3).styleSheet() == ColorResult.success.value

            for i in range(2):
                socket.messages_heap.append(server.send_alarm(
                    "07", "01", (dev_id, dev_type_num)
                ))
                # time.sleep(1)
                qtbot.waitUntil(lambda: "08 07" not in str(socket.messages_heap), timeout=5 * 1000)
                assert main_w.qc_main_widget.view_obj[5].cellWidget(row, 3).label_off.text() == f'ВИКЛ: {i + 1}/2'

                socket.messages_heap.append(server.send_alarm(
                    "01", "00", (dev_id, dev_type_num)
                ))
                # time.sleep(1)
                qtbot.waitUntil(lambda: "08 01" not in str(socket.messages_heap), timeout=5 * 1000)
                assert main_w.qc_main_widget.view_obj[5].cellWidget(row, 3).label_on.text() == f'ВКЛ: {i + 1}/2'

            assert main_w.qc_main_widget.view_obj[5].cellWidget(row, 3).label_on.styleSheet() == ColorResult.success.value
            assert main_w.qc_main_widget.view_obj[5].cellWidget(row, 3).label_off.styleSheet() == ColorResult.success.value

            # test check reed switch alarms
            assert not main_w.qc_main_widget.view_obj[5].cellWidget(row, 4).styleSheet() == ColorResult.success.value

            for i in range(2):
                socket.messages_heap.append(server.send_alarm(
                "04", "21", (dev_id, dev_type_num)
            ))
                # time.sleep(1)
                qtbot.waitUntil(lambda: "08 04" not in str(socket.messages_heap), timeout=5 * 1000)
                assert main_w.qc_main_widget.view_obj[5].cellWidget(row, 4).text() == f'{i + i + 1}'

                socket.messages_heap.append(server.send_alarm(
                "00", "20", (dev_id, dev_type_num)
            ))
                # time.sleep(1)
                qtbot.waitUntil(lambda: "08 00" not in str(socket.messages_heap), timeout=5 * 1000)
                assert main_w.qc_main_widget.view_obj[5].cellWidget(row, 4).text() == f'{i + i + 2}'

                if int(main_w.qc_main_widget.view_obj[5].cellWidget(row, 4).text()) >= 2:
                    assert main_w.qc_main_widget.view_obj[5].cellWidget(row, 4).styleSheet() == ColorResult.success.value

            # test check extra contact alarms
            assert not main_w.qc_main_widget.view_obj[5].cellWidget(row, 5).styleSheet() == ColorResult.success.value

            for i in range(2):
                socket.messages_heap.append(server.send_alarm(
                "04", "23", (dev_id, dev_type_num)
            ))
                # time.sleep(1)
                qtbot.waitUntil(lambda: "08 04" not in str(socket.messages_heap), timeout=5 * 1000)
                assert main_w.qc_main_widget.view_obj[5].cellWidget(row, 5).text() == f'{i + i + 1}'

                socket.messages_heap.append(server.send_alarm(
                "00", "22", (dev_id, dev_type_num)
            ))
                # time.sleep(1)
                qtbot.waitUntil(lambda: "08 00" not in str(socket.messages_heap), timeout=5 * 1000)
                assert main_w.qc_main_widget.view_obj[5].cellWidget(row, 5).text() == f'{i + i + 2}'

                if int(main_w.qc_main_widget.view_obj[5].cellWidget(row, 5).text()) >= 2:
                    assert main_w.qc_main_widget.view_obj[5].cellWidget(row, 5).styleSheet() == ColorResult.success.value

            # test check batt lvl update
            assert not main_w.qc_main_widget.view_obj[5].cellWidget(row, 6).styleSheet() == ColorResult.success.value
            socket.messages_heap.append(server.send_update(
            (dev_id, dev_type_num), ("05", "0064")
        ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[5].cellWidget(row, 6).label_status.text() == '100'
            assert main_w.qc_main_widget.view_obj[5].cellWidget(row, 6).styleSheet() == ColorResult.success.value
            assert main_w.qc_main_widget.view_obj[5].cellWidget(row, 7).isEnabled()
            assert main_w.qc_main_widget.view_obj[5].cellWidget(row, 8).styleSheet() == ColorResult.success.value
            assert main_w.qc_main_widget.view_obj[5].cellWidget(row, 8).text() == 'Зареєструвати'

        for qr in dev_qrs:
            #time.sleep(0.5)
            dev_id = qr[:6]
            dev_type_num = '01'

            # test success register device
            qtbot.mouseClick(main_w.qc_main_widget.view_obj[5].cellWidget(0, 8),
                             QtCore.Qt.LeftButton)
            assert socket.check_message_exists(message_type='05', message_key='05')

            socket.messages_heap.append(server.send_alarm(
            "09", "09", (dev_id, dev_type_num)
        ))
            socket.messages_heap.append(server.update_delete_device(
                (dev_id, dev_type_num)
            ))
            qtbot.waitUntil(lambda: "19 04" not in str(socket.messages_heap), timeout=5 * 1000)
            _, grade, _, info, defects, time_reg, success, operator, qr_db = \
                database.get_stat_device_after_success_reg(qr=qr)

            assert grade == '0'
            assert __version__ in info
            assert defects == '""'
            assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                             time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
            assert success == 1
            assert operator == 'armen'
            assert qr_db == qr

            database.delete_device_by_qr(qr)

    def test_device_with_qc_passed(self, qtbot, mock_main_w_scanner_ports, mock_socket_csa, mock_qr_scanner_main_w, mock_retry_test_popup_true):
        dev_type = "door_plus"
        main_w, _, _ = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        # add device in DB
        database = db_handler.DBHandler(KeyDatabase.get_keys_production(), dev_type)
        database.delete_device_from_db()
        database.insert_device_in_db()
        database.insert_device_prog()
        database.insert_device_assembler()
        database.insert_device_long_test()
        database.insert_device_qc()

        # test frequency apply
        with qtbot.waitSignal(signal=main_w.qc_main_widget.widget_changed_frequency.signal_change_frequency,
                              timeout=3 * 1000):
            assert True

        main_w.qc_main_widget.handler_qr_code(db_handler.devices[dev_type][0])
        assert main_w.qc_main_widget.view_obj[6].cellWidget(0, 1) is not None
        database.delete_device_from_db()

    def test_device_with_qc_passed_cancel(self, qtbot, mock_main_w_scanner_ports, mock_socket_csa, mock_qr_scanner_main_w, mock_retry_test_popup_false):
        dev_type = "door"
        main_w, _, _ = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        # add device in DB
        database = db_handler.DBHandler(KeyDatabase.get_keys_production(), dev_type)
        database.delete_device_from_db()
        database.insert_device_in_db()
        database.insert_device_prog()
        database.insert_device_assembler()
        database.insert_device_long_test()
        database.insert_device_qc()

        # test frequency apply
        with qtbot.waitSignal(signal=main_w.qc_main_widget.widget_changed_frequency.signal_change_frequency,
                              timeout=3 * 1000):
            assert True

        main_w.qc_main_widget.handler_qr_code(db_handler.devices[dev_type][0])
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 1) is None
        database.delete_device_from_db()

    def test_device_with_same_qr(self, qtbot, mock_main_w_scanner_ports, mock_socket_csa, mock_qr_scanner_main_w):
        dev_type = "door"
        main_w, _, _ = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        # add device in DB
        database = db_handler.DBHandler(KeyDatabase.get_keys_production(), dev_type)
        database.delete_device_from_db()
        database.insert_device_in_db()
        database.insert_device_prog()
        database.insert_device_assembler()
        database.insert_device_long_test()

        # test frequency apply
        with qtbot.waitSignal(signal=main_w.qc_main_widget.widget_changed_frequency.signal_change_frequency,
                              timeout=3 * 1000):
            assert True

        main_w.qc_main_widget.handler_qr_code(db_handler.devices[dev_type][0])
        assert not main_w.qc_main_widget.view_obj[5].cellWidget(0, 1) is None
        main_w.qc_main_widget.handler_qr_code(db_handler.devices[dev_type][0])
        assert main_w.qc_main_widget.view_obj[5].widget_error.label_message_text.text() == f"Цей девайс " \
                                                                f"{db_handler.devices[dev_type][0][:6]} же є в талиці!"
        database.delete_device_from_db()

    def test_device_reg_without_testing(self, qtbot, mock_socket_csa, mock_qr_scanner_main_w, mock_you_sure):
        dev_type = "door"
        main_w, server, socket = mock_socket_csa
        server.obj_settings_answer = server.object_add_timeout_answer
        qtbot.addWidget(main_w.qc_main_widget)

        # add device in DB
        database = db_handler.DBHandler(KeyDatabase.get_keys_production(), dev_type)
        database.delete_device_from_db()
        database.insert_device_in_db()
        database.insert_device_prog()
        database.insert_device_assembler()
        database.insert_device_long_test()

        # test frequency apply
        with qtbot.waitSignal(signal=main_w.qc_main_widget.widget_changed_frequency.signal_change_frequency,
                              timeout=3 * 1000):
            assert True

        # scan device with qr scanner
        main_w.qc_main_widget.handler_qr_code(db_handler.devices[dev_type][0])

        # add return defects
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[5].cellWidget(0, 1).push_btn_select_defect,
                         QtCore.Qt.LeftButton)
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[5].cellWidget(0, 1).defects_popup.push_btn_select_defect,
                         QtCore.Qt.LeftButton)

        # try send device to repair
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[5].cellWidget(0, 8),
                         QtCore.Qt.LeftButton)

        assert main_w.qc_main_widget.view_obj[5].widget_error.label_message_text.text() == "Щоб повернути девайс " \
                                                                                           "потрібно вибрати дефект або " \
                                                                                           "сробувати приписати девайс " \
                                                                                           "хоч один раз!"

    def test_add_device_fail(self, qtbot, mock_main_w_scanner_ports, mock_socket_csa, mock_qr_scanner_main_w, mock_you_sure):
        dev_type = "door"
        dev_id = db_handler.devices[dev_type][0][:6]
        dev_type_num = db_handler.devices[dev_type][0][-3:-1]
        main_w, server, socket = mock_socket_csa
        server.obj_settings_answer = server.object_add_timeout_answer
        socket.dev = (dev_id, dev_type_num)
        qtbot.addWidget(main_w.qc_main_widget)

        # add device in DB
        database = db_handler.DBHandler(KeyDatabase.get_keys_production(), dev_type)
        database.delete_device_from_db()
        database.insert_device_in_db()
        database.insert_device_prog()
        database.insert_device_assembler()
        database.insert_device_long_test()

        qtbot.mouseClick(main_w.qc_main_widget.pushButton_connect_scanner, QtCore.Qt.LeftButton)
        assert not main_w.qc_main_widget.comboBox_ports_scanner.isEnabled()

        # test frequency apply
        with qtbot.waitSignal(signal=main_w.qc_main_widget.widget_changed_frequency.signal_change_frequency,
                              timeout=3 * 1000):
            assert True

        # scan device with qr scanner && test default setup table
        main_w.qc_main_widget.handler_qr_code(db_handler.devices[dev_type][0])
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 0).text() == db_handler.devices[dev_type][0]
        assert not main_w.qc_main_widget.view_obj[5].cellWidget(0, 1).check_box_grade_defect.isChecked()
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 1).push_btn_select_defect.isEnabled()
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 1).defects_popup.get_defects_return() == []

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[5].cellWidget(0, 2).push_btn_connect_device,
                         QtCore.Qt.LeftButton)

        socket.messages_heap.append(server.send_alarm("09", "08", (dev_id, dev_type_num)))
        qtbot.waitUntil(lambda: "05 03" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[5].widget_error.label_message_text.text() == 'Скінчився час відведений ' \
                                                                                              'на реєстрацію!'
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 2).styleSheet() == ColorResult.fail.value
        #assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 2).push_btn_connect_device.isEnabled()

        # try send device to repair
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[5].cellWidget(0, 8),
                         QtCore.Qt.LeftButton)

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[5].cellWidget(0, 8) is None, timeout=5 * 1000)
        socket.messages_heap.append(server.send_alarm(
            "09", "09", (dev_id, dev_type_num)
        ))
        socket.messages_heap.append(server.update_delete_device(
            (dev_id, dev_type_num)
        ))
        qtbot.waitUntil(lambda: "19 04" not in str(socket.messages_heap), timeout=5 * 1000)

        info_qc, info_repair = database.get_stat_device_after_fail_reg(qr=db_handler.devices[dev_type][0])
        _, grade, _, info, defects, time_reg, success, operator, qr = info_qc
        _, from_stage, reason, income_time, initial_problem, what_done, comment, outcome_time, \
            status, dev_type_db, income_operator, outcome_operator, qr_repair, repair_type, *_ = info_repair

        # check info in dev_qc
        assert grade is None
        assert __version__ in info
        assert defects == '""'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 0
        assert operator == 'armen'
        assert qr == db_handler.devices[dev_type][0]

        # check info in dev_repair
        assert from_stage == "QC"
        assert reason == "Датчик не приписався до хабу (Не включається)"
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         income_time.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert initial_problem is None
        assert what_done is None
        assert comment is None
        assert status == 'Відправлено у ремонт'
        assert dev_type_db == 1
        assert income_operator == 'armen'
        assert outcome_operator is None
        assert qr_repair == db_handler.devices[dev_type][0]
        assert repair_type is None

        database.delete_all_tables_info()

    @pytest.mark.skip
    def test_update_device_count_thread(self, qtbot, mock_main_w_scanner_ports, mock_socket_csa, mock_qr_scanner_main_w):
        dev_type = "door"
        dev_id = db_handler.devices[dev_type][0][:6]
        dev_type_num = db_handler.devices[dev_type][0][-3:-1]
        main_w, server, socket = mock_socket_csa
        socket.dev = (dev_id, dev_type_num)
        qtbot.addWidget(main_w.qc_main_widget)

        # add device in DB
        database = db_handler.DBHandler(KeyDatabase.get_keys_production(), dev_type)
        database.delete_all_tables_info()
        database.insert_device_in_db()
        database.insert_device_prog()
        database.insert_device_assembler()
        database.insert_device_long_test()

        qtbot.mouseClick(main_w.qc_main_widget.pushButton_connect_scanner, QtCore.Qt.LeftButton)
        assert not main_w.qc_main_widget.comboBox_ports_scanner.isEnabled()

        # test frequency apply
        with qtbot.waitSignal(signal=main_w.qc_main_widget.widget_changed_frequency.signal_change_frequency,
                              timeout=3 * 1000):
            assert True
        assert re.match(r'EU\s\d{3}\.\d{2,3}\s\d{3}\.\d{2,3}',
                        main_w.qc_main_widget.label_frequency_device.text()).group(0)

        # scan device with qr scanner && test default setup table
        main_w.qc_main_widget.handler_qr_code(db_handler.devices[dev_type][0])
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 0).text() == db_handler.devices[dev_type][0]
        assert not main_w.qc_main_widget.view_obj[5].cellWidget(0, 1).check_box_grade_defect.isChecked()
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 1).push_btn_select_defect.isEnabled()
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 1).defects_popup.get_defects_return() == []

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[5].cellWidget(0, 2).push_btn_connect_device,
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='05', message_key='00')

        socket.messages_heap.append(server.send_alarm("09", "08", (dev_id, dev_type_num)))
        assert not main_w.qc_main_widget.view_obj[5].cellWidget(0, 2).push_btn_connect_device.isEnabled()
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 2).styleSheet() == ColorResult.success.value
        assert socket.check_message_exists(message_type='07', message_key=None,
                                           message_payload=[dev_type_num, f'00{dev_id}', '33', '01', '34', '01'])

        # test check tamper alarms
        assert not main_w.qc_main_widget.view_obj[5].cellWidget(0, 3).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm(
                "07", "01", (dev_id, dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 07" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 3).label_off.text() == f'ВИКЛ: {i + 1}/2'

            socket.messages_heap.append(server.send_alarm(
                "01", "00", (dev_id, dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 01" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 3).label_on.text() == f'ВКЛ: {i + 1}/2'

        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 3).label_on.styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 3).label_off.styleSheet() == ColorResult.success.value

        # test check reed switch alarms
        assert not main_w.qc_main_widget.view_obj[5].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm(
                "04", "21", (dev_id, dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 04" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 4).text() == f'{i + i + 1}'

            socket.messages_heap.append(server.send_alarm(
                "00", "20", (dev_id, dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 00" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 4).text() == f'{i + i + 2}'

            if int(main_w.qc_main_widget.view_obj[5].cellWidget(0, 4).text()) >= 2:
                assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        # test check extra contact alarms
        assert not main_w.qc_main_widget.view_obj[5].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm(
                "04", "23", (dev_id, dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 04" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 5).text() == f'{i + i + 1}'

            socket.messages_heap.append(server.send_alarm(
                "00", "22", (dev_id, dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 00" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 5).text() == f'{i + i + 2}'

            if int(main_w.qc_main_widget.view_obj[5].cellWidget(0, 5).text()) >= 2:
                assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        # test check batt lvl update
        assert not main_w.qc_main_widget.view_obj[5].cellWidget(0, 6).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update(
            (dev_id, dev_type_num), ("05", "0064")
        ))
        # time.sleep(1)
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 6).label_status.text() == '100'
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 6).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 7).isEnabled()
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 8).isEnabled()
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 8).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 8).text() == 'Зареєструвати'
        # time.sleep(2)

        # test success register device
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[5].cellWidget(0, 8),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='05', message_key='05')

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[5].cellWidget(0, 8) is None, timeout=5 * 1000)
        print(main_w.qc_main_widget.label_stat.text())
        qtbot.waitUntil(lambda: 'Загальна к-ть: 1' in main_w.qc_main_widget.label_stat.text(), timeout=260 * 1000)
        qtbot.waitUntil(lambda: 'DP: 1' in main_w.qc_main_widget.label_stat.text(), timeout=3 * 1000)

        database.delete_all_tables_info()

    @pytest.mark.skip
    def test_update_device_count_manual(self, qtbot, mock_main_w_scanner_ports, mock_socket_csa, mock_qr_scanner_main_w):
        dev_type = "door"
        dev_id = db_handler.devices[dev_type][0][:6]
        dev_type_num = db_handler.devices[dev_type][0][-3:-1]
        main_w, server, socket = mock_socket_csa
        socket.dev = (dev_id, dev_type_num)
        qtbot.addWidget(main_w.qc_main_widget)

        # add device in DB
        database = db_handler.DBHandler(KeyDatabase.get_keys_production(), dev_type)
        database.delete_device_from_db()
        database.insert_device_in_db()
        database.insert_device_prog()
        database.insert_device_assembler()
        database.insert_device_long_test()

        qtbot.mouseClick(main_w.qc_main_widget.pushButton_connect_scanner, QtCore.Qt.LeftButton)
        assert not main_w.qc_main_widget.comboBox_ports_scanner.isEnabled()

        # test frequency apply
        with qtbot.waitSignal(signal=main_w.qc_main_widget.widget_changed_frequency.signal_change_frequency,
                              timeout=3 * 1000):
            assert True
        assert re.match(r'EU\s\d{3}\.\d{2,3}\s\d{3}\.\d{2,3}',
                        main_w.qc_main_widget.label_frequency_device.text()).group(0)

        # scan device with qr scanner && test default setup table
        main_w.qc_main_widget.handler_qr_code(db_handler.devices[dev_type][0])
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 0).text() == db_handler.devices[dev_type][0]
        assert not main_w.qc_main_widget.view_obj[5].cellWidget(0, 1).check_box_grade_defect.isChecked()
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 1).push_btn_select_defect.isEnabled()
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 1).defects_popup.get_defects_return() == []

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[5].cellWidget(0, 2).push_btn_connect_device,
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='05', message_key='00')

        socket.messages_heap.append(server.send_alarm("09", "08", (dev_id, dev_type_num)))
        assert not main_w.qc_main_widget.view_obj[5].cellWidget(0, 2).push_btn_connect_device.isEnabled()
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 2).styleSheet() == ColorResult.success.value
        assert socket.check_message_exists(message_type='07', message_key=None,
                                           message_payload=[dev_type_num, f'00{dev_id}', '33', '01', '34', '01'])

        # test check tamper alarms
        assert not main_w.qc_main_widget.view_obj[5].cellWidget(0, 3).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm(
                "07", "01", (dev_id, dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 07" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 3).label_off.text() == f'ВИКЛ: {i + 1}/2'

            socket.messages_heap.append(server.send_alarm(
                "01", "00", (dev_id, dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 01" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 3).label_on.text() == f'ВКЛ: {i + 1}/2'

        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 3).label_on.styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 3).label_off.styleSheet() == ColorResult.success.value

        # test check reed switch alarms
        assert not main_w.qc_main_widget.view_obj[5].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm(
                "04", "21", (dev_id, dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 04" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 4).text() == f'{i + i + 1}'

            socket.messages_heap.append(server.send_alarm(
                "00", "20", (dev_id, dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 00" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 4).text() == f'{i + i + 2}'

            if int(main_w.qc_main_widget.view_obj[5].cellWidget(0, 4).text()) >= 2:
                assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        # test check extra contact alarms
        assert not main_w.qc_main_widget.view_obj[5].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm(
                "04", "23", (dev_id, dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 04" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 5).text() == f'{i + i + 1}'

            socket.messages_heap.append(server.send_alarm(
                "00", "22", (dev_id, dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 00" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 5).text() == f'{i + i + 2}'

            if int(main_w.qc_main_widget.view_obj[5].cellWidget(0, 5).text()) >= 2:
                assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        # test check batt lvl update
        assert not main_w.qc_main_widget.view_obj[5].cellWidget(0, 6).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update(
            (dev_id, dev_type_num), ("05", "0064")
        ))
        # time.sleep(1)
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 6).label_status.text() == '100'
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 6).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 7).isEnabled()
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 8).isEnabled()
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 8).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 8).text() == 'Зареєструвати'
        # time.sleep(2)

        # test success register device
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[5].cellWidget(0, 8),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='05', message_key='05')

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[5].cellWidget(0, 8) is None, timeout=5 * 1000)
        qtbot.mouseClick(main_w.qc_main_widget.pushButton_refresh_stat,
                         QtCore.Qt.LeftButton)
        qtbot.waitUntil(lambda: 'Загальна к-ть: 1' in main_w.qc_main_widget.label_stat.text(), timeout=260 * 1000)
        qtbot.waitUntil(lambda: 'DP: 1' in main_w.qc_main_widget.label_stat.text(), timeout=3 * 1000)


    # def test_click_register_when_hub_arm(self, qtbot, mock_main_w_scanner_ports, mock_socket_csa, mock_qr_scanner_main_w):
    #     dev_type = "door"
    #     dev_id = DBHandler.devices[dev_type][0][:6]
    #     dev_type_num = DBHandler.devices[dev_type][0][-3:-1]
    #     main_w, server, socket = mock_socket_csa
    #     socket.dev = (dev_id, dev_type_num)
    #     qtbot.addWidget(main_w.qc_main_widget)
    #
    #     # add device in DB
    #     database = DBHandler.DBHandler(KeyDatabase.get_keys_production(), dev_type)
    #     database.delete_device_from_db()
    #     database.insert_device_in_db()
    #     database.insert_device_prog()
    #     database.insert_device_assembler()
    #     database.insert_device_long_test()
    #
    #     qtbot.mouseClick(main_w.qc_main_widget.pushButton_connect_scanner, QtCore.Qt.LeftButton)
    #     assert not main_w.qc_main_widget.comboBox_ports_scanner.isEnabled()
    #
    #     # test frequency apply
    #     with qtbot.waitSignal(signal=main_w.qc_main_widget.widget_changed_frequency.signal_change_frequency,
    #                           timeout=3 * 1000):
    #         assert True
    #     assert re.match(r'EU\s\d{3}\.\d{2,3}\s\d{3}\.\d{2,3}',
    #                     main_w.qc_main_widget.label_frequency_device.text()).group(0)
    #
    #     # scan device with qr scanner && test default setup table
    #     main_w.qc_main_widget.handler_qr_code(DBHandler.devices[dev_type][0])
    #     assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 0).text() == DBHandler.devices[dev_type][0]
    #     assert not main_w.qc_main_widget.view_obj[5].cellWidget(0, 1).check_box_grade_defect.isChecked()
    #     assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 1).push_btn_select_defect.isEnabled()
    #     assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 1).defects_popup.get_defects_return() == []
    #
    #     # test click button start reg process
    #     qtbot.mouseClick(main_w.qc_main_widget.view_obj[5].cellWidget(0, 2).push_btn_connect_device,
    #                      QtCore.Qt.LeftButton)
    #     assert socket.check_message_exists(message_type='05', message_key='00')
    #
    #     socket.messages_heap.append(server.send_alarm("09", "08", (dev_id, dev_type_num)))
    #     assert not main_w.qc_main_widget.view_obj[5].cellWidget(0, 2).push_btn_connect_device.isEnabled()
    #     assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 2).styleSheet() == ColorResult.success.value
    #     assert socket.check_message_exists(message_type='07', message_key=None,
    #                                        message_payload=[dev_type_num, f'00{dev_id}', '33', '01', '34', '01'])
    #
    #     # test check tamper alarms
    #     assert not main_w.qc_main_widget.view_obj[5].cellWidget(0, 3).styleSheet() == ColorResult.success.value
    #
    #     for i in range(2):
    #         socket.messages_heap.append(server.reed_closed_alarm(
    #             (dev_id, dev_type_num)
    #         ))
    #         # time.sleep(1)
    #         qtbot.waitUntil(lambda: "08 07" not in str(socket.messages_heap), timeout=5 * 1000)
    #         assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 3).label_off.text() == f'ВИКЛ: {i + 1}/2'
    #
    #         socket.messages_heap.append(server.reed_open_alarm(
    #             (dev_id, dev_type_num)
    #         ))
    #         # time.sleep(1)
    #         qtbot.waitUntil(lambda: "08 01" not in str(socket.messages_heap), timeout=5 * 1000)
    #         assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 3).label_on.text() == f'ВКЛ: {i + 1}/2'
    #
    #     assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 3).label_on.styleSheet() == ColorResult.success.value
    #     assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 3).label_off.styleSheet() == ColorResult.success.value
    #
    #     # test check reed switch alarms
    #     assert not main_w.qc_main_widget.view_obj[5].cellWidget(0, 4).styleSheet() == ColorResult.success.value
    #
    #     for i in range(2):
    #         socket.messages_heap.append(server.reed_switch_alarm1(
    #             (dev_id, dev_type_num)
    #         ))
    #         # time.sleep(1)
    #         qtbot.waitUntil(lambda: "08 04" not in str(socket.messages_heap), timeout=5 * 1000)
    #         assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 4).text() == f'{i + i + 1}'
    #
    #         socket.messages_heap.append(server.reed_switch_alarm2(
    #             (dev_id, dev_type_num)
    #         ))
    #         # time.sleep(1)
    #         qtbot.waitUntil(lambda: "08 00" not in str(socket.messages_heap), timeout=5 * 1000)
    #         assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 4).text() == f'{i + i + 2}'
    #
    #         if int(main_w.qc_main_widget.view_obj[5].cellWidget(0, 4).text()) >= 2:
    #             assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 4).styleSheet() == ColorResult.success.value
    #
    #     # test check extra contact alarms
    #     assert not main_w.qc_main_widget.view_obj[5].cellWidget(0, 5).styleSheet() == ColorResult.success.value
    #
    #     for i in range(2):
    #         socket.messages_heap.append(server.extra_contact_close_alarm(
    #             (dev_id, dev_type_num)
    #         ))
    #         # time.sleep(1)
    #         qtbot.waitUntil(lambda: "08 04" not in str(socket.messages_heap), timeout=5 * 1000)
    #         assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 5).text() == f'{i + i + 1}'
    #
    #         socket.messages_heap.append(server.extra_contact_open_alarm(
    #             (dev_id, dev_type_num)
    #         ))
    #         # time.sleep(1)
    #         qtbot.waitUntil(lambda: "08 00" not in str(socket.messages_heap), timeout=5 * 1000)
    #         assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 5).text() == f'{i + i + 2}'
    #
    #         if int(main_w.qc_main_widget.view_obj[5].cellWidget(0, 5).text()) >= 2:
    #             assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 5).styleSheet() == ColorResult.success.value
    #
    #     # test check batt lvl update
    #     assert not main_w.qc_main_widget.view_obj[5].cellWidget(0, 6).styleSheet() == ColorResult.success.value
    #     socket.messages_heap.append(server.update_batt_lvl_100(
    #         (dev_id, dev_type_num)
    #     ))
    #     # time.sleep(1)
    #     qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
    #     assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 6).label_status.text() == '100'
    #     assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 6).styleSheet() == ColorResult.success.value
    #     assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 7).isEnabled()
    #     assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 8).isEnabled()
    #     assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 8).styleSheet() == ColorResult.success.value
    #     assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 8).text() == 'Зареєструвати'
    #     # time.sleep(2)
    #
    #     socket.messages_heap.append(server.update_state_hub_arm())
    #     for hub in main_w.qc_main_widget.csa_client._hubs:
    #         if hub._work_hub_for_qc:
    #             qtbot.waitUntil(lambda: hub.get_parameters("08") == '01', timeout=5 * 1000)
    #
    #     # test success register device
    #     qtbot.mouseClick(main_w.qc_main_widget.view_obj[5].cellWidget(0, 8),
    #                      QtCore.Qt.LeftButton)
    #     assert socket.check_message_exists(message_type='05', message_key='05')
    #     assert main_w.qc_main_widget.view_obj[5].widget_error.label_message_text.text() == "sda"
    #
    #     database.delete_device_from_db()

    def test_device_scan_before_work_hub_change_freq(self, qtbot, mock_socket_csa, mock_qr_scanner_main_w):
        dev_type = "door"
        main_w, _, _ = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        main_w.qc_main_widget.handler_qr_code(db_handler.devices[dev_type][0])
        assert main_w.qc_main_widget.err_widget.label_message_text.text() == "Частота не визначена для " \
                                                                             "подальшого тестування девайсу!"
