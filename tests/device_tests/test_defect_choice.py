import re
import pytest
from PyQt5 import QtCore
from app_setting import AppSetting
from qc.security.security import KeyDatabase
from qc.enum_color import ColorResult
from tests.utils import db_handler
from __version__ import __version__


@pytest.mark.last
class TestDefectChoice:
    AppSetting.DEBUG = True

    def test_add_class_b_without_defects(self, qtbot, mock_main_w_scanner_ports, mock_socket_csa, mock_qr_scanner_main_w):
        dev_type = "door"
        main_w, server, socket = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        # add device in DB
        database = db_handler.DBHandler(KeyDatabase.get_keys_production(), dev_type)
        database.delete_device_from_db()
        database.insert_device_in_db()
        database.insert_device_prog()
        database.insert_device_assembler()
        database.insert_device_long_test()

        # test frequency apply
        with qtbot.waitSignal(signal=main_w.qc_main_widget.widget_changed_frequency.signal_change_frequency,
                              timeout=3 * 1000):
            assert True

        # scan device with qr scanner && test default setup table
        main_w.qc_main_widget.handler_qr_code(db_handler.devices[dev_type][0])
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[5].cellWidget(0, 1).check_box_grade_defect, QtCore.Qt.LeftButton)
        assert main_w.qc_main_widget.err_widget.label_message_text.text() == "Виберіть дефект!"

        database.delete_device_from_db()

    def test_add_class_b_with_return_defects(self, qtbot, mock_socket_csa, mock_qr_scanner_main_w, mock_defects_return):
        dev_type = "door"
        main_w, server, socket = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        # add device in DB
        database = db_handler.DBHandler(KeyDatabase.get_keys_production(), dev_type)
        database.delete_device_from_db()
        database.insert_device_in_db()
        database.insert_device_prog()
        database.insert_device_assembler()
        database.insert_device_long_test()

        # test frequency apply
        with qtbot.waitSignal(signal=main_w.qc_main_widget.widget_changed_frequency.signal_change_frequency,
                              timeout=3 * 1000):
            assert True

        # scan device with qr scanner
        main_w.qc_main_widget.handler_qr_code(db_handler.devices[dev_type][0])

        # add return defects
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[5].cellWidget(0, 1).push_btn_select_defect,
                         QtCore.Qt.LeftButton)
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[5].cellWidget(0, 1).defects_popup.push_btn_select_defect,
                         QtCore.Qt.LeftButton)

        assert not main_w.qc_main_widget.view_obj[5].cellWidget(0, 1).check_box_grade_defect.isEnabled()
        assert not main_w.qc_main_widget.view_obj[5].cellWidget(0, 1).check_box_grade_defect.isChecked()

        database.delete_device_from_db()

    def test_add_device_with_return_defects(self, qtbot, mock_socket_csa, mock_qr_scanner_main_w, mock_defects_return):
        dev_type = "door"
        dev_id = db_handler.devices[dev_type][0][:6]
        main_w, server, socket = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        # add device in DB
        database = db_handler.DBHandler(KeyDatabase.get_keys_production(), dev_type)
        database.delete_device_from_db()
        database.insert_device_in_db()
        database.insert_device_prog()
        database.insert_device_assembler()
        database.insert_device_long_test()

        # test frequency apply
        with qtbot.waitSignal(signal=main_w.qc_main_widget.widget_changed_frequency.signal_change_frequency,
                              timeout=3 * 1000):
            assert True

        # scan device with qr scanner
        main_w.qc_main_widget.handler_qr_code(db_handler.devices[dev_type][0])

        # add return defects
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[5].cellWidget(0, 1).push_btn_select_defect,
                         QtCore.Qt.LeftButton)
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[5].cellWidget(0, 1).defects_popup.push_btn_select_defect,
                         QtCore.Qt.LeftButton)

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[5].cellWidget(0, 2).push_btn_connect_device,
                         QtCore.Qt.LeftButton)

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[5].widget_error.isVisible(), timeout=3 * 1000)
        assert main_w.qc_main_widget.view_obj[5].widget_error.label_message_text.text() == f"В датчика {dev_id} вибрані дефекти " \
                                                                            f"'тільки на повернення'! " \
                                                                            f"Датчик не підлягає подальшому тестуванню!"

        database.delete_device_from_db()

    def test_add_device_with_grade_defect_when_cb_not_checked(self, qtbot, mock_socket_csa, mock_qr_scanner_main_w, mock_defects_grade):
        dev_type = "door"
        main_w, server, socket = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        # add device in DB
        database = db_handler.DBHandler(KeyDatabase.get_keys_production(), dev_type)
        database.delete_device_from_db()
        database.insert_device_in_db()
        database.insert_device_prog()
        database.insert_device_assembler()
        database.insert_device_long_test()

        # test frequency apply
        with qtbot.waitSignal(signal=main_w.qc_main_widget.widget_changed_frequency.signal_change_frequency,
                              timeout=3 * 1000):
            assert True

        # scan device with qr scanner
        main_w.qc_main_widget.handler_qr_code(db_handler.devices[dev_type][0])

        # add grade defects
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[5].cellWidget(0, 1).push_btn_select_defect,
                         QtCore.Qt.LeftButton)
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[5].cellWidget(0, 1).defects_popup.push_btn_select_defect,
                         QtCore.Qt.LeftButton)

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[5].cellWidget(0, 2).push_btn_connect_device,
                         QtCore.Qt.LeftButton)

        assert main_w.qc_main_widget.view_obj[5].widget_error.label_message_text.text() == "Якщо обрані дефекти підлягаю" \
                                                                                           " класу Б, відмітьте це в чек" \
                                                                                           "боксі, якщо ні девайс підляг" \
                                                                                           "ає поверненню!"

    def test_success_device_reg_with_grade_defects(self, qtbot, mock_main_w_scanner_ports, mock_socket_csa, mock_qr_scanner_main_w, mock_defects_grade):
        dev_type = "door"
        dev_id = db_handler.devices[dev_type][0][:6]
        dev_type_num = db_handler.devices[dev_type][0][-3:-1]
        main_w, server, socket = mock_socket_csa
        socket.dev = (dev_id, dev_type_num)
        qtbot.addWidget(main_w.qc_main_widget)

        # add device in DB
        database = db_handler.DBHandler(KeyDatabase.get_keys_production(), dev_type)
        database.delete_device_from_db()
        database.insert_device_in_db()
        database.insert_device_prog()
        database.insert_device_assembler()
        database.insert_device_long_test()

        qtbot.mouseClick(main_w.qc_main_widget.pushButton_connect_scanner, QtCore.Qt.LeftButton)
        assert not main_w.qc_main_widget.comboBox_ports_scanner.isEnabled()

        # test frequency apply
        with qtbot.waitSignal(signal=main_w.qc_main_widget.widget_changed_frequency.signal_change_frequency,
                              timeout=3 * 1000):
            assert True
        assert re.match(r'EU\s\d{3}\.\d{2,3}\s\d{3}\.\d{2,3}',
                        main_w.qc_main_widget.label_frequency_device.text()).group(0)

        # scan device with qr scanner && test default setup table
        main_w.qc_main_widget.handler_qr_code(db_handler.devices[dev_type][0])
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 0).text() == db_handler.devices[dev_type][0]
        assert not main_w.qc_main_widget.view_obj[5].cellWidget(0, 1).check_box_grade_defect.isChecked()
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 1).push_btn_select_defect.isEnabled()
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 1).defects_popup.get_defects_return() == []

        # add grade defects
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[5].cellWidget(0, 1).push_btn_select_defect,
                         QtCore.Qt.LeftButton)
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[5].cellWidget(0, 1).defects_popup.push_btn_select_defect,
                         QtCore.Qt.LeftButton)
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[5].cellWidget(0, 1).check_box_grade_defect,
                         QtCore.Qt.LeftButton)

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[5].cellWidget(0, 2).push_btn_connect_device,
                         QtCore.Qt.LeftButton)

        socket.messages_heap.append(server.send_alarm("09", "08", (dev_id, dev_type_num)))
        assert not main_w.qc_main_widget.view_obj[5].cellWidget(0, 2).push_btn_connect_device.isEnabled()
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 2).styleSheet() == ColorResult.success.value

        # test check tamper alarms
        assert not main_w.qc_main_widget.view_obj[5].cellWidget(0, 3).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm(
                "07", "01", (dev_id, dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 07" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 3).label_off.text() == f'ВИКЛ: {i + 1}/2'

            socket.messages_heap.append(server.send_alarm(
                "01", "00", (dev_id, dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 01" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 3).label_on.text() == f'ВКЛ: {i + 1}/2'

        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 3).label_on.styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 3).label_off.styleSheet() == ColorResult.success.value

        # test check reed switch alarms
        assert not main_w.qc_main_widget.view_obj[5].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm(
                "04", "21", (dev_id, dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 04" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 4).text() == f'{i + i + 1}'

            socket.messages_heap.append(server.send_alarm(
                "00", "20", (dev_id, dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 00" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 4).text() == f'{i + i + 2}'

            if int(main_w.qc_main_widget.view_obj[5].cellWidget(0, 4).text()) >= 2:
                assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        # test check extra contact alarms
        assert not main_w.qc_main_widget.view_obj[5].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm(
                "04", "23", (dev_id, dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 04" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 5).text() == f'{i + i + 1}'

            socket.messages_heap.append(server.send_alarm(
                "00", "22", (dev_id, dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 00" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 5).text() == f'{i + i + 2}'

            if int(main_w.qc_main_widget.view_obj[5].cellWidget(0, 5).text()) >= 2:
                assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        # test check batt lvl update
        assert not main_w.qc_main_widget.view_obj[5].cellWidget(0, 6).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update(
            (dev_id, dev_type_num), ("05", "0064")
        ))
        # time.sleep(1)
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 6).label_status.text() == '100'
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 6).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 7).isEnabled()
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 8).isEnabled()
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 8).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 8).text() == 'Зареєструвати'
        # time.sleep(2)

        # test success register device
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[5].cellWidget(0, 8),
                         QtCore.Qt.LeftButton)

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[5].cellWidget(0, 8) is None, timeout=5 * 1000)
        socket.messages_heap.append(server.send_alarm(
            "09", "09", (dev_id, dev_type_num)
        ))
        socket.messages_heap.append(server.update_delete_device(
            (dev_id, dev_type_num)
        ))
        qtbot.waitUntil(lambda: "19 04" not in str(socket.messages_heap), timeout=5 * 1000)
        _, grade, _, info, defects, time_reg, success, operator, qr = \
            database.get_stat_device_after_success_reg(qr=db_handler.devices[dev_type][0])

        assert grade == '1'
        assert __version__ in info
        assert defects == '"Сколи;Прожоги"'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 1
        assert operator == 'armen'
        assert qr == db_handler.devices[dev_type][0]

        database.delete_device_from_db()

    def test_try_register_with_return_defects_that_was_chose_after_func_test(self, qtbot, mock_main_w_scanner_ports,
                                                                             mock_socket_csa, mock_qr_scanner_main_w,
                                                                             mock_return_defect, mock_you_sure):

        dev_type = "door"
        dev_id = db_handler.devices[dev_type][0][:6]
        dev_type_num = db_handler.devices[dev_type][0][-3:-1]
        main_w, server, socket = mock_socket_csa
        socket.dev = (dev_id, dev_type_num)
        qtbot.addWidget(main_w.qc_main_widget)

        # add device in DB
        database = db_handler.DBHandler(KeyDatabase.get_keys_production(), dev_type)
        database.delete_device_from_db()
        database.insert_device_in_db()
        database.insert_device_prog()
        database.insert_device_assembler()
        database.insert_device_long_test()

        qtbot.mouseClick(main_w.qc_main_widget.pushButton_connect_scanner, QtCore.Qt.LeftButton)
        assert not main_w.qc_main_widget.comboBox_ports_scanner.isEnabled()

        # test frequency apply
        with qtbot.waitSignal(signal=main_w.qc_main_widget.widget_changed_frequency.signal_change_frequency,
                              timeout=3 * 1000):
            assert True
        assert re.match(r'EU\s\d{3}\.\d{2,3}\s\d{3}\.\d{2,3}',
                        main_w.qc_main_widget.label_frequency_device.text()).group(0)

        # scan device with qr scanner && test default setup table
        main_w.qc_main_widget.handler_qr_code(db_handler.devices[dev_type][0])
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 0).text() == db_handler.devices[dev_type][0]
        assert not main_w.qc_main_widget.view_obj[5].cellWidget(0, 1).check_box_grade_defect.isChecked()
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 1).push_btn_select_defect.isEnabled()

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[5].cellWidget(0, 2).push_btn_connect_device,
                         QtCore.Qt.LeftButton)

        socket.messages_heap.append(server.send_alarm("09", "08", (dev_id, dev_type_num)))
        assert not main_w.qc_main_widget.view_obj[5].cellWidget(0, 2).push_btn_connect_device.isEnabled()
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 2).styleSheet() == ColorResult.success.value

        # test check tamper alarms
        assert not main_w.qc_main_widget.view_obj[5].cellWidget(0, 3).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm(
                "07", "01", (dev_id, dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 07" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 3).label_off.text() == f'ВИКЛ: {i + 1}/2'

            socket.messages_heap.append(server.send_alarm(
                "01", "00", (dev_id, dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 01" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 3).label_on.text() == f'ВКЛ: {i + 1}/2'

        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 3).label_on.styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 3).label_off.styleSheet() == ColorResult.success.value

        # test check reed switch alarms
        assert not main_w.qc_main_widget.view_obj[5].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm(
                "04", "21", (dev_id, dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 04" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 4).text() == f'{i + i + 1}'

            socket.messages_heap.append(server.send_alarm(
                "00", "20", (dev_id, dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 00" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 4).text() == f'{i + i + 2}'

            if int(main_w.qc_main_widget.view_obj[5].cellWidget(0, 4).text()) >= 2:
                assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        # test check extra contact alarms
        assert not main_w.qc_main_widget.view_obj[5].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm(
                "04", "23", (dev_id, dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 04" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 5).text() == f'{i + i + 1}'

            socket.messages_heap.append(server.send_alarm(
                "00", "22", (dev_id, dev_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 00" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 5).text() == f'{i + i + 2}'

            if int(main_w.qc_main_widget.view_obj[5].cellWidget(0, 5).text()) >= 2:
                assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        # test check batt lvl update
        assert not main_w.qc_main_widget.view_obj[5].cellWidget(0, 6).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update(
            (dev_id, dev_type_num), ("05", "0064")
        ))
        # time.sleep(1)
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 6).label_status.text() == '100'
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 6).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 7).isEnabled()
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 8).isEnabled()
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 8).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 8).text() == 'Зареєструвати'
        # time.sleep(2)

        # add return defects
        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[5].cellWidget(0, 8).text() == 'Зареєструвати')
        main_w.qc_main_widget.view_obj[5].cellWidget(0, 1).defects_popup.get_defects_return = mock_return_defect
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[5].cellWidget(0, 1).push_btn_select_defect,
                         QtCore.Qt.LeftButton)
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[5].cellWidget(0, 1).defects_popup.push_btn_select_defect,
                         QtCore.Qt.LeftButton)

        assert main_w.qc_main_widget.view_obj[5].cellWidget(0, 8).text() == 'Повернути'

        database.delete_device_from_db()

    def test_fail_with_return_defects(self, qtbot, mock_socket_csa, mock_qr_scanner_main_w, mock_you_sure, mock_defects_return):
        dev_type = "door"
        dev_id = db_handler.devices[dev_type][0][:6]
        dev_type_num = db_handler.devices[dev_type][0][-3:-1]
        main_w, server, socket = mock_socket_csa
        server.obj_settings_answer = server.object_add_timeout_answer
        qtbot.addWidget(main_w.qc_main_widget)

        # add device in DB
        database = db_handler.DBHandler(KeyDatabase.get_keys_production(), dev_type)
        database.delete_device_from_db()
        database.insert_device_in_db()
        database.insert_device_prog()
        database.insert_device_assembler()
        database.insert_device_long_test()

        # test frequency apply
        with qtbot.waitSignal(signal=main_w.qc_main_widget.widget_changed_frequency.signal_change_frequency,
                              timeout=3 * 1000):
            assert True

        # scan device with qr scanner
        main_w.qc_main_widget.handler_qr_code(db_handler.devices[dev_type][0])

        # add return defects
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[5].cellWidget(0, 1).push_btn_select_defect,
                         QtCore.Qt.LeftButton)
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[5].cellWidget(0, 1).defects_popup.push_btn_select_defect,
                         QtCore.Qt.LeftButton)

        qtbot.waitUntil(lambda: not main_w.qc_main_widget.view_obj[5].cellWidget(0, 1).check_box_grade_defect.isEnabled(),
                        timeout=3 * 1000)

        # try send device to repair
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[5].cellWidget(0, 8),
                         QtCore.Qt.LeftButton)

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[5].cellWidget(0, 8) is None, timeout=5 * 1000)
        socket.messages_heap.append(server.send_alarm(
            "09", "09", (dev_id, dev_type_num)
        ))
        socket.messages_heap.append(server.update_delete_device(
            (dev_id, dev_type_num)
        ))
        qtbot.waitUntil(lambda: "19 04" not in str(socket.messages_heap), timeout=5 * 1000)

        info_qc, info_repair = database.get_stat_device_after_fail_reg(qr=db_handler.devices[dev_type][0])
        _, grade, _, info, defects, time_reg, success, operator, qr = info_qc
        _, from_stage, reason, income_time, initial_problem, what_done, comment, outcome_time, \
            status, dev_type_db, income_operator, outcome_operator, qr_repair, repair_type, *_ = info_repair

        # check info in dev_qc
        assert grade is None
        assert __version__ in info
        assert defects == '"Відсутній QR на платі;Відсутність батарейки/акумулятора"'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 0
        assert operator == 'armen'
        assert qr == db_handler.devices[dev_type][0]

        # check info in dev_repair
        assert from_stage == "QC"
        assert reason == 'Відсутній QR на платі;Відсутність батарейки/акумулятора'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         income_time.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert initial_problem is None
        assert what_done is None
        assert comment is None
        assert status == 'Відправлено у ремонт'
        assert dev_type_db == 1
        assert income_operator == 'armen'
        assert outcome_operator is None
        assert qr_repair == db_handler.devices[dev_type][0]
        assert repair_type is None

        database.delete_device_from_db()
