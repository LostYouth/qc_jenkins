import os
from ftplib import FTP

SERVER = dict(
    host="localhost",
    user="tester",
    passwd="12345",
    port=2121
)


class Ftp:

    def __init__(self):
        self.current_path = "log_server/logs/"
        self.ftp = None
        self.connect_ftp()

    def connect_ftp(self):
        self.ftp = FTP()
        self.ftp.connect(SERVER['host'], SERVER['port'])
        self.ftp.login(user=SERVER['user'], passwd=SERVER['passwd'])

    def set_curr_path(self, direct, ip):
        self.current_path += '{direct}/{ip}/'.format(direct=direct, ip=ip)

    def upload_photo(self, dev_id):
        with open(f"photos/img_{dev_id}_ir_2019-09-26_20-32-17.jpg", "rb") as photo1:
            self.ftp.storbinary('STOR ' + "img_112246_ir_2019-09-26_20-32-17.jpg", photo1)

        with open(f"photos/img_{dev_id}_photo_2019-09-09_13-09-16.jpg", "rb") as photo2:
            self.ftp.storbinary('STOR ' + "img_112246_photo_2019-09-09_13-09-16.jpg", photo2)

    @property
    def get_current_path(self):
        return os.getcwd()

    def get_files(self):
        return os.listdir(os.getcwd())

    @property
    def show_curr_path(self):
        return self.current_path

    def __enter__(self):
        self.connect_ftp()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.ftp.close()

    def download_file(self, path2file, local_file, curr_dir):
        local_p = os.path.join(local_file)
        with open(local_p, 'wb') as local_f:
            def callback(data):
                local_f.write(data)
            self.ftp.retrbinary("RETR {}".format(curr_dir + path2file), callback)

    def show_last_logs(self):
        self.ftp.cwd(self.show_curr_path)
        return self.ftp.nlst()[-10:]

    def retr_list(self):
        lis = self.ftp.nlst()
        return lis

    def exit(self):
        self.ftp.close()
