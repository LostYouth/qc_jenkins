-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: 10.10.52.125    Database: production
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=157 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can view log entry',1,'view_logentry'),(5,'Can add permission',2,'add_permission'),(6,'Can change permission',2,'change_permission'),(7,'Can delete permission',2,'delete_permission'),(8,'Can view permission',2,'view_permission'),(9,'Can add group',3,'add_group'),(10,'Can change group',3,'change_group'),(11,'Can delete group',3,'delete_group'),(12,'Can view group',3,'view_group'),(13,'Can add user',4,'add_user'),(14,'Can change user',4,'change_user'),(15,'Can delete user',4,'delete_user'),(16,'Can view user',4,'view_user'),(17,'Can add content type',5,'add_contenttype'),(18,'Can change content type',5,'change_contenttype'),(19,'Can delete content type',5,'delete_contenttype'),(20,'Can view content type',5,'view_contenttype'),(21,'Can add session',6,'add_session'),(22,'Can change session',6,'change_session'),(23,'Can delete session',6,'delete_session'),(24,'Can view session',6,'view_session'),(25,'Can add central assembling',7,'add_centralassembling'),(26,'Can change central assembling',7,'change_centralassembling'),(27,'Can delete central assembling',7,'delete_centralassembling'),(28,'Can view central assembling',7,'view_centralassembling'),(29,'Can add central id resource',8,'add_centralidresource'),(30,'Can change central id resource',8,'change_centralidresource'),(31,'Can delete central id resource',8,'delete_centralidresource'),(32,'Can view central id resource',8,'view_centralidresource'),(33,'Can add central long test',9,'add_centrallongtest'),(34,'Can change central long test',9,'change_centrallongtest'),(35,'Can delete central long test',9,'delete_centrallongtest'),(36,'Can view central long test',9,'view_centrallongtest'),(37,'Can add central pack',10,'add_centralpack'),(38,'Can change central pack',10,'change_centralpack'),(39,'Can delete central pack',10,'delete_centralpack'),(40,'Can view central pack',10,'view_centralpack'),(41,'Can add central prog',11,'add_centralprog'),(42,'Can change central prog',11,'change_centralprog'),(43,'Can delete central prog',11,'delete_centralprog'),(44,'Can view central prog',11,'view_centralprog'),(45,'Can add central qc',12,'add_centralqc'),(46,'Can change central qc',12,'change_centralqc'),(47,'Can delete central qc',12,'delete_centralqc'),(48,'Can view central qc',12,'view_centralqc'),(49,'Can add central register',13,'add_centralregister'),(50,'Can change central register',13,'change_centralregister'),(51,'Can delete central register',13,'delete_centralregister'),(52,'Can view central register',13,'view_centralregister'),(53,'Can add central repair',14,'add_centralrepair'),(54,'Can change central repair',14,'change_centralrepair'),(55,'Can delete central repair',14,'delete_centralrepair'),(56,'Can view central repair',14,'view_centralrepair'),(57,'Can add dev assembling',15,'add_devassembling'),(58,'Can change dev assembling',15,'change_devassembling'),(59,'Can delete dev assembling',15,'delete_devassembling'),(60,'Can view dev assembling',15,'view_devassembling'),(61,'Can add dev calibration',16,'add_devcalibration'),(62,'Can change dev calibration',16,'change_devcalibration'),(63,'Can delete dev calibration',16,'delete_devcalibration'),(64,'Can view dev calibration',16,'view_devcalibration'),(65,'Can add dev long test',17,'add_devlongtest'),(66,'Can change dev long test',17,'change_devlongtest'),(67,'Can delete dev long test',17,'delete_devlongtest'),(68,'Can view dev long test',17,'view_devlongtest'),(69,'Can add dev pack',18,'add_devpack'),(70,'Can change dev pack',18,'change_devpack'),(71,'Can delete dev pack',18,'delete_devpack'),(72,'Can view dev pack',18,'view_devpack'),(73,'Can add dev prog',19,'add_devprog'),(74,'Can change dev prog',19,'change_devprog'),(75,'Can delete dev prog',19,'delete_devprog'),(76,'Can view dev prog',19,'view_devprog'),(77,'Can add dev qc',20,'add_devqc'),(78,'Can change dev qc',20,'change_devqc'),(79,'Can delete dev qc',20,'delete_devqc'),(80,'Can view dev qc',20,'view_devqc'),(81,'Can add dev register',21,'add_devregister'),(82,'Can change dev register',21,'change_devregister'),(83,'Can delete dev register',21,'delete_devregister'),(84,'Can view dev register',21,'view_devregister'),(85,'Can add dev repair',22,'add_devrepair'),(86,'Can change dev repair',22,'change_devrepair'),(87,'Can delete dev repair',22,'delete_devrepair'),(88,'Can view dev repair',22,'view_devrepair'),(89,'Can add dev test room',23,'add_devtestroom'),(90,'Can change dev test room',23,'change_devtestroom'),(91,'Can delete dev test room',23,'delete_devtestroom'),(92,'Can view dev test room',23,'view_devtestroom'),(93,'Can add firmware',24,'add_firmware'),(94,'Can change firmware',24,'change_firmware'),(95,'Can delete firmware',24,'delete_firmware'),(96,'Can view firmware',24,'view_firmware'),(97,'Can add general boards',25,'add_generalboards'),(98,'Can change general boards',25,'change_generalboards'),(99,'Can delete general boards',25,'delete_generalboards'),(100,'Can view general boards',25,'view_generalboards'),(101,'Can add general meta',26,'add_generalmeta'),(102,'Can change general meta',26,'change_generalmeta'),(103,'Can delete general meta',26,'delete_generalmeta'),(104,'Can view general meta',26,'view_generalmeta'),(105,'Can add hub meta',27,'add_hubmeta'),(106,'Can change hub meta',27,'change_hubmeta'),(107,'Can delete hub meta',27,'delete_hubmeta'),(108,'Can view hub meta',27,'view_hubmeta'),(109,'Can add mac resource',28,'add_macresource'),(110,'Can change mac resource',28,'change_macresource'),(111,'Can delete mac resource',28,'delete_macresource'),(112,'Can view mac resource',28,'view_macresource'),(113,'Can add prod operators',29,'add_prodoperators'),(114,'Can change prod operators',29,'change_prodoperators'),(115,'Can delete prod operators',29,'delete_prodoperators'),(116,'Can view prod operators',29,'view_prodoperators'),(117,'Can add prod options',30,'add_prodoptions'),(118,'Can change prod options',30,'change_prodoptions'),(119,'Can delete prod options',30,'delete_prodoptions'),(120,'Can view prod options',30,'view_prodoptions'),(121,'Can add prod statistic',31,'add_prodstatistic'),(122,'Can change prod statistic',31,'change_prodstatistic'),(123,'Can delete prod statistic',31,'delete_prodstatistic'),(124,'Can view prod statistic',31,'view_prodstatistic'),(125,'Can add prod transactions',32,'add_prodtransactions'),(126,'Can change prod transactions',32,'change_prodtransactions'),(127,'Can delete prod transactions',32,'delete_prodtransactions'),(128,'Can view prod transactions',32,'view_prodtransactions'),(129,'Can add qr register',33,'add_qrregister'),(130,'Can change qr register',33,'change_qrregister'),(131,'Can delete qr register',33,'delete_qrregister'),(132,'Can view qr register',33,'view_qrregister'),(133,'Can add res generate',34,'add_resgenerate'),(134,'Can change res generate',34,'change_resgenerate'),(135,'Can delete res generate',34,'delete_resgenerate'),(136,'Can view res generate',34,'view_resgenerate'),(137,'Can add soft version',35,'add_softversion'),(138,'Can change soft version',35,'change_softversion'),(139,'Can delete soft version',35,'delete_softversion'),(140,'Can view soft version',35,'view_softversion'),(141,'Can add stand register',36,'add_standregister'),(142,'Can change stand register',36,'change_standregister'),(143,'Can delete stand register',36,'delete_standregister'),(144,'Can view stand register',36,'view_standregister'),(145,'Can add stand repair',37,'add_standrepair'),(146,'Can change stand repair',37,'change_standrepair'),(147,'Can delete stand repair',37,'delete_standrepair'),(148,'Can view stand repair',37,'view_standrepair'),(149,'Can add kit register',38,'add_kitregister'),(150,'Can change kit register',38,'change_kitregister'),(151,'Can delete kit register',38,'delete_kitregister'),(152,'Can view kit register',38,'view_kitregister'),(153,'Can add step',39,'add_step'),(154,'Can change step',39,'change_step'),(155,'Can delete step',39,'delete_step'),(156,'Can view step',39,'view_step');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `central_assembling`
--

DROP TABLE IF EXISTS `central_assembling`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `central_assembling` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` datetime(6) NOT NULL,
  `success` tinyint(1) NOT NULL,
  `operator` varchar(32) NOT NULL,
  `qr` varchar(32) NOT NULL,
  `info` json NOT NULL,
  PRIMARY KEY (`id`),
  KEY `central_assembling_operator_c4ee6c3e_fk_prod_operators_login` (`operator`),
  KEY `central_assembling_qr_8be2fe36_fk_central_register_qr` (`qr`),
  CONSTRAINT `central_assembling_operator_c4ee6c3e_fk_prod_operators_login` FOREIGN KEY (`operator`) REFERENCES `prod_operators` (`login`),
  CONSTRAINT `central_assembling_qr_8be2fe36_fk_central_register_qr` FOREIGN KEY (`qr`) REFERENCES `central_register` (`qr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `central_assembling`
--

LOCK TABLES `central_assembling` WRITE;
/*!40000 ALTER TABLE `central_assembling` DISABLE KEYS */;
/*!40000 ALTER TABLE `central_assembling` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `insert_into_central_assembling` AFTER INSERT ON `central_assembling` FOR EACH ROW BEGIN UPDATE central_register SET info_assembling = NEW.id WHERE NEW.qr = central_register.qr; UPDATE central_register SET info_long_test = null WHERE NEW.qr = central_register.qr; UPDATE central_register SET info_qc = null WHERE NEW.qr = central_register.qr; UPDATE central_register SET info_pack = null WHERE NEW.qr = central_register.qr; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `update_central_assembling_last_step` AFTER INSERT ON `central_assembling` FOR EACH ROW BEGIN IF NEW.success = 1 THEN UPDATE central_register SET last_step = 'assembling' WHERE NEW.qr = central_register.qr; END IF; IF NEW.success = 1 THEN UPDATE central_register SET last_step_change_time = NEW.time WHERE NEW.qr = central_register.qr; END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `central_long_test`
--

DROP TABLE IF EXISTS `central_long_test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `central_long_test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `info` json NOT NULL,
  `time` datetime(6) NOT NULL,
  `success` tinyint(1) NOT NULL,
  `operator` varchar(32) NOT NULL,
  `qr` varchar(32) NOT NULL,
  `defects` json DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `central_long_test_operator_07b05619_fk_prod_operators_login` (`operator`),
  KEY `central_long_test_qr_4b27ee3d_fk_central_register_qr` (`qr`),
  CONSTRAINT `central_long_test_operator_07b05619_fk_prod_operators_login` FOREIGN KEY (`operator`) REFERENCES `prod_operators` (`login`),
  CONSTRAINT `central_long_test_qr_4b27ee3d_fk_central_register_qr` FOREIGN KEY (`qr`) REFERENCES `central_register` (`qr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `central_long_test`
--

LOCK TABLES `central_long_test` WRITE;
/*!40000 ALTER TABLE `central_long_test` DISABLE KEYS */;
/*!40000 ALTER TABLE `central_long_test` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `insert_into_central_long_test` AFTER INSERT ON `central_long_test` FOR EACH ROW BEGIN UPDATE central_register SET info_long_test = NEW.id WHERE NEW.qr = central_register.qr; UPDATE central_register SET info_qc = null WHERE NEW.qr = central_register.qr; UPDATE central_register SET info_pack = null WHERE NEW.qr = central_register.qr; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `update_central_long_test_last_step` AFTER INSERT ON `central_long_test` FOR EACH ROW BEGIN IF NEW.success = 1 THEN UPDATE central_register SET last_step = 'long_test' WHERE NEW.qr = central_register.qr; END IF; IF NEW.success = 1 THEN UPDATE central_register SET last_step_change_time = NEW.time WHERE NEW.qr = central_register.qr; END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `central_pack`
--

DROP TABLE IF EXISTS `central_pack`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `central_pack` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` datetime(6) NOT NULL,
  `success` tinyint(1) NOT NULL,
  `operator` varchar(32) NOT NULL,
  `qr` varchar(32) NOT NULL,
  `info` json NOT NULL,
  PRIMARY KEY (`id`),
  KEY `central_pack_operator_8bec8f13_fk_prod_operators_login` (`operator`),
  KEY `central_pack_qr_a9021d9e_fk_central_register_qr` (`qr`),
  CONSTRAINT `central_pack_operator_8bec8f13_fk_prod_operators_login` FOREIGN KEY (`operator`) REFERENCES `prod_operators` (`login`),
  CONSTRAINT `central_pack_qr_a9021d9e_fk_central_register_qr` FOREIGN KEY (`qr`) REFERENCES `central_register` (`qr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `central_pack`
--

LOCK TABLES `central_pack` WRITE;
/*!40000 ALTER TABLE `central_pack` DISABLE KEYS */;
/*!40000 ALTER TABLE `central_pack` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `insert_into_central_pack` AFTER INSERT ON `central_pack` FOR EACH ROW BEGIN UPDATE central_register SET info_pack = NEW.id WHERE NEW.qr = central_register.qr; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `update_central_pack_last_step` AFTER INSERT ON `central_pack` FOR EACH ROW BEGIN IF NEW.success = 1 THEN UPDATE central_register SET last_step = 'pack' WHERE NEW.qr = central_register.qr; END IF; IF NEW.success = 1 THEN UPDATE central_register SET last_step_change_time = NEW.time WHERE NEW.qr = central_register.qr; END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `central_prog`
--

DROP TABLE IF EXISTS `central_prog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `central_prog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `info` json NOT NULL,
  `time` datetime(6) NOT NULL,
  `success` tinyint(1) NOT NULL,
  `operator` varchar(32) NOT NULL,
  `qr` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `central_prog_operator_462107fe_fk_prod_operators_login` (`operator`),
  KEY `central_prog_qr_f2725ca8_fk_central_register_qr` (`qr`),
  CONSTRAINT `central_prog_operator_462107fe_fk_prod_operators_login` FOREIGN KEY (`operator`) REFERENCES `prod_operators` (`login`),
  CONSTRAINT `central_prog_qr_f2725ca8_fk_central_register_qr` FOREIGN KEY (`qr`) REFERENCES `central_register` (`qr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `central_prog`
--

LOCK TABLES `central_prog` WRITE;
/*!40000 ALTER TABLE `central_prog` DISABLE KEYS */;
/*!40000 ALTER TABLE `central_prog` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `insert_into_central_prog` AFTER INSERT ON `central_prog` FOR EACH ROW BEGIN UPDATE central_register SET info_prog = NEW.id WHERE NEW.qr = central_register.qr; UPDATE central_register SET info_assembling = null WHERE NEW.qr = central_register.qr; UPDATE central_register SET info_long_test = null WHERE NEW.qr = central_register.qr; UPDATE central_register SET info_qc = null WHERE NEW.qr = central_register.qr; UPDATE central_register SET info_pack = null WHERE NEW.qr = central_register.qr; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `update_central_prog_last_step` AFTER INSERT ON `central_prog` FOR EACH ROW BEGIN IF NEW.success = 1 THEN UPDATE central_register SET last_step = 'prog' WHERE NEW.qr = central_register.qr; END IF; IF NEW.success = 1 THEN UPDATE central_register SET last_step_change_time = NEW.time WHERE NEW.qr = central_register.qr; END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `central_qc`
--

DROP TABLE IF EXISTS `central_qc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `central_qc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grade` varchar(32) DEFAULT NULL,
  `comment` varchar(64) DEFAULT NULL,
  `info` json NOT NULL,
  `defects` json DEFAULT NULL,
  `time` datetime(6) NOT NULL,
  `success` tinyint(1) NOT NULL,
  `operator` varchar(32) NOT NULL,
  `qr` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `central_qc_operator_be49f88b_fk_prod_operators_login` (`operator`),
  KEY `central_qc_qr_efc5d959_fk_central_register_qr` (`qr`),
  CONSTRAINT `central_qc_operator_be49f88b_fk_prod_operators_login` FOREIGN KEY (`operator`) REFERENCES `prod_operators` (`login`),
  CONSTRAINT `central_qc_qr_efc5d959_fk_central_register_qr` FOREIGN KEY (`qr`) REFERENCES `central_register` (`qr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `central_qc`
--

LOCK TABLES `central_qc` WRITE;
/*!40000 ALTER TABLE `central_qc` DISABLE KEYS */;
/*!40000 ALTER TABLE `central_qc` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `insert_into_central_qc` AFTER INSERT ON `central_qc` FOR EACH ROW BEGIN UPDATE central_register SET info_qc = NEW.id WHERE NEW.qr = central_register.qr; UPDATE central_register SET info_pack = null WHERE NEW.qr = central_register.qr; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `insert_into_central_production_time` AFTER INSERT ON `central_qc` FOR EACH ROW BEGIN DECLARE existing datetime(6); set existing = (select production_time from central_register where central_register.qr = NEW.qr); IF NEW.success = 1 and existing is null THEN UPDATE central_register SET production_time = NEW.time WHERE NEW.qr = central_register.qr; END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `update_central_qc_last_step` AFTER INSERT ON `central_qc` FOR EACH ROW BEGIN IF NEW.success = 1 THEN UPDATE central_register SET last_step = 'qc' WHERE NEW.qr = central_register.qr; END IF; IF NEW.success = 1 THEN UPDATE central_register SET last_step_change_time = NEW.time WHERE NEW.qr = central_register.qr; END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `central_register`
--

DROP TABLE IF EXISTS `central_register`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `central_register` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qr` varchar(32) NOT NULL,
  `region` varchar(8) NOT NULL,
  `country` varchar(32) DEFAULT NULL,
  `info_prog` int(11) DEFAULT NULL,
  `info_assembling` int(11) DEFAULT NULL,
  `info_long_test` int(11) DEFAULT NULL,
  `info_qc` int(11) DEFAULT NULL,
  `info_repair` int(11) DEFAULT NULL,
  `info_pack` int(11) DEFAULT NULL,
  `times_restored` int(11) DEFAULT NULL,
  `comment` varchar(64) DEFAULT NULL,
  `production_time` datetime(6) DEFAULT NULL,
  `central_id` varchar(32) NOT NULL,
  `factory_const` json NOT NULL,
  `board_name` varchar(64) NOT NULL,
  `dev_type` int(11) NOT NULL,
  `hub_subtype` int(11) DEFAULT NULL,
  `color` varchar(15) DEFAULT NULL,
  `kit_in_old_db` tinyint(1) DEFAULT NULL,
  `kit_id` int(11) DEFAULT NULL,
  `chip_id` varchar(24) DEFAULT NULL,
  `imei` varchar(24) DEFAULT NULL,
  `last_step` varchar(32) DEFAULT NULL,
  `last_step_change_time` datetime(6) DEFAULT NULL,
  `pro_account_email` varchar(254) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `qr` (`qr`),
  UNIQUE KEY `central_id` (`central_id`),
  UNIQUE KEY `chip_id` (`chip_id`),
  UNIQUE KEY `imei` (`imei`),
  KEY `central_register_board_name_e341a008_fk_general_b` (`board_name`),
  KEY `central_register_dev_type_2347f3c1_fk_general_meta_type_index` (`dev_type`),
  KEY `central_register_hub_subtype_03850da9_fk_hub_meta_subtype_index` (`hub_subtype`),
  KEY `central_register_kit_id_d517b612_fk_kit_register_id` (`kit_id`),
  KEY `central_register_last_step_d78b6ad8_fk_step_name` (`last_step`),
  CONSTRAINT `central_register_board_name_e341a008_fk_general_b` FOREIGN KEY (`board_name`) REFERENCES `general_boards` (`board_name`),
  CONSTRAINT `central_register_dev_type_2347f3c1_fk_general_meta_type_index` FOREIGN KEY (`dev_type`) REFERENCES `general_meta` (`type_index`),
  CONSTRAINT `central_register_hub_subtype_03850da9_fk_hub_meta_subtype_index` FOREIGN KEY (`hub_subtype`) REFERENCES `hub_meta` (`subtype_index`),
  CONSTRAINT `central_register_kit_id_d517b612_fk_kit_register_id` FOREIGN KEY (`kit_id`) REFERENCES `kit_register` (`id`),
  CONSTRAINT `central_register_last_step_d78b6ad8_fk_step_name` FOREIGN KEY (`last_step`) REFERENCES `step` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `central_register`
--

LOCK TABLES `central_register` WRITE;
/*!40000 ALTER TABLE `central_register` DISABLE KEYS */;
/*!40000 ALTER TABLE `central_register` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `central_repair`
--

DROP TABLE IF EXISTS `central_repair`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `central_repair` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_stage` varchar(150) NOT NULL,
  `reason` varchar(256) NOT NULL,
  `income_time` datetime(6) NOT NULL,
  `initial_problem` varchar(256) DEFAULT NULL,
  `what_done` varchar(256) DEFAULT NULL,
  `comment` varchar(256) DEFAULT NULL,
  `outcome_time` datetime(6) DEFAULT NULL,
  `status` varchar(256) NOT NULL,
  `dev_type` int(11) NOT NULL,
  `income_operator` varchar(32) NOT NULL,
  `outcome_operator` varchar(32) DEFAULT NULL,
  `qr` varchar(32) NOT NULL,
  `repair_type` varchar(16) DEFAULT NULL,
  `visual_operator` varchar(30) DEFAULT NULL,
  `visual_time` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `central_repair_dev_type_866b3788_fk_general_meta_type_index` (`dev_type`),
  KEY `central_repair_income_operator_130eb6fc_fk_prod_operators_login` (`income_operator`),
  KEY `central_repair_outcome_operator_46425bb9_fk_prod_operators_login` (`outcome_operator`),
  KEY `central_repair_qr_6f55cdb0_fk_central_register_qr` (`qr`),
  CONSTRAINT `central_repair_dev_type_866b3788_fk_general_meta_type_index` FOREIGN KEY (`dev_type`) REFERENCES `general_meta` (`type_index`),
  CONSTRAINT `central_repair_income_operator_130eb6fc_fk_prod_operators_login` FOREIGN KEY (`income_operator`) REFERENCES `prod_operators` (`login`),
  CONSTRAINT `central_repair_outcome_operator_46425bb9_fk_prod_operators_login` FOREIGN KEY (`outcome_operator`) REFERENCES `prod_operators` (`login`),
  CONSTRAINT `central_repair_qr_6f55cdb0_fk_central_register_qr` FOREIGN KEY (`qr`) REFERENCES `central_register` (`qr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `central_repair`
--

LOCK TABLES `central_repair` WRITE;
/*!40000 ALTER TABLE `central_repair` DISABLE KEYS */;
/*!40000 ALTER TABLE `central_repair` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `insert_into_central_repair` AFTER INSERT ON `central_repair` FOR EACH ROW BEGIN UPDATE central_register SET info_repair = NEW.id WHERE NEW.qr = central_register.qr; UPDATE central_register SET info_prog = null WHERE NEW.qr = central_register.qr; UPDATE central_register SET info_assembling = null WHERE NEW.qr = central_register.qr; UPDATE central_register SET info_long_test = null WHERE NEW.qr = central_register.qr; UPDATE central_register SET info_qc = null WHERE NEW.qr = central_register.qr; UPDATE central_register SET info_pack = null WHERE NEW.qr = central_register.qr; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `update_central_repair_last_step` AFTER INSERT ON `central_repair` FOR EACH ROW BEGIN UPDATE central_register SET last_step = 'repair' WHERE NEW.qr = central_register.qr; UPDATE central_register SET last_step_change_time = NEW.income_time WHERE NEW.qr = central_register.qr; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `centralidresource`
--

DROP TABLE IF EXISTS `centralidresource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centralidresource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `central_id` varchar(12) NOT NULL,
  `block` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `centralidresource_central_id_4d80a772_uniq` (`central_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centralidresource`
--

LOCK TABLES `centralidresource` WRITE;
/*!40000 ALTER TABLE `centralidresource` DISABLE KEYS */;
/*!40000 ALTER TABLE `centralidresource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dev_assembling`
--

DROP TABLE IF EXISTS `dev_assembling`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dev_assembling` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` datetime(6) NOT NULL,
  `success` tinyint(1) NOT NULL,
  `operator` varchar(32) NOT NULL,
  `qr` varchar(32) NOT NULL,
  `info` json NOT NULL,
  PRIMARY KEY (`id`),
  KEY `dev_assembling_operator_1431f823_fk_prod_operators_login` (`operator`),
  KEY `dev_assembling_qr_edda15a6_fk_dev_register_qr` (`qr`),
  CONSTRAINT `dev_assembling_operator_1431f823_fk_prod_operators_login` FOREIGN KEY (`operator`) REFERENCES `prod_operators` (`login`),
  CONSTRAINT `dev_assembling_qr_edda15a6_fk_dev_register_qr` FOREIGN KEY (`qr`) REFERENCES `dev_register` (`qr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dev_assembling`
--

LOCK TABLES `dev_assembling` WRITE;
/*!40000 ALTER TABLE `dev_assembling` DISABLE KEYS */;
/*!40000 ALTER TABLE `dev_assembling` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `insert_into_dev_assembling` AFTER INSERT ON `dev_assembling` FOR EACH ROW BEGIN UPDATE dev_register SET info_assembling = NEW.id WHERE NEW.qr = dev_register.qr; UPDATE dev_register SET info_calibration = null WHERE NEW.qr = dev_register.qr; UPDATE dev_register SET info_long_test = null WHERE NEW.qr = dev_register.qr; UPDATE dev_register SET info_test_room = null WHERE NEW.qr = dev_register.qr; UPDATE dev_register SET info_qc = null WHERE NEW.qr = dev_register.qr; UPDATE dev_register SET info_pack = null WHERE NEW.qr = dev_register.qr; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `update_dev_assembling_last_step` AFTER INSERT ON `dev_assembling` FOR EACH ROW BEGIN IF NEW.success = 1 THEN UPDATE dev_register SET last_step = 'assembling' WHERE NEW.qr = dev_register.qr; END IF; IF NEW.success = 1 THEN UPDATE dev_register SET last_step_change_time = NEW.time WHERE NEW.qr = dev_register.qr; END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `dev_calibration`
--

DROP TABLE IF EXISTS `dev_calibration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dev_calibration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `info` json NOT NULL,
  `time` datetime(6) NOT NULL,
  `success` tinyint(1) NOT NULL,
  `operator` varchar(32) NOT NULL,
  `qr` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `dev_calibration_operator_dc065fad_fk_prod_operators_login` (`operator`),
  KEY `dev_calibration_qr_d185212e_fk_dev_register_qr` (`qr`),
  CONSTRAINT `dev_calibration_operator_dc065fad_fk_prod_operators_login` FOREIGN KEY (`operator`) REFERENCES `prod_operators` (`login`),
  CONSTRAINT `dev_calibration_qr_d185212e_fk_dev_register_qr` FOREIGN KEY (`qr`) REFERENCES `dev_register` (`qr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dev_calibration`
--

LOCK TABLES `dev_calibration` WRITE;
/*!40000 ALTER TABLE `dev_calibration` DISABLE KEYS */;
/*!40000 ALTER TABLE `dev_calibration` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `insert_into_dev_calibration` AFTER INSERT ON `dev_calibration` FOR EACH ROW BEGIN UPDATE dev_register SET info_calibration = NEW.id WHERE NEW.qr = dev_register.qr; UPDATE dev_register SET info_long_test = null WHERE NEW.qr = dev_register.qr; UPDATE dev_register SET info_test_room = null WHERE NEW.qr = dev_register.qr; UPDATE dev_register SET info_qc = null WHERE NEW.qr = dev_register.qr; UPDATE dev_register SET info_pack = null WHERE NEW.qr = dev_register.qr; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `update_dev_calibration_last_step` AFTER INSERT ON `dev_calibration` FOR EACH ROW BEGIN IF NEW.success = 1 THEN UPDATE dev_register SET last_step = 'calibration' WHERE NEW.qr = dev_register.qr; END IF; IF NEW.success = 1 THEN UPDATE dev_register SET last_step_change_time = NEW.time WHERE NEW.qr = dev_register.qr; END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `dev_long_test`
--

DROP TABLE IF EXISTS `dev_long_test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dev_long_test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `info` json NOT NULL,
  `time` datetime(6) NOT NULL,
  `success` tinyint(1) NOT NULL,
  `operator` varchar(32) NOT NULL,
  `qr` varchar(32) NOT NULL,
  `defects` json DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `dev_long_test_operator_13b0da64_fk_prod_operators_login` (`operator`),
  KEY `dev_long_test_qr_cf142df2_fk_dev_register_qr` (`qr`),
  CONSTRAINT `dev_long_test_operator_13b0da64_fk_prod_operators_login` FOREIGN KEY (`operator`) REFERENCES `prod_operators` (`login`),
  CONSTRAINT `dev_long_test_qr_cf142df2_fk_dev_register_qr` FOREIGN KEY (`qr`) REFERENCES `dev_register` (`qr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dev_long_test`
--

LOCK TABLES `dev_long_test` WRITE;
/*!40000 ALTER TABLE `dev_long_test` DISABLE KEYS */;
/*!40000 ALTER TABLE `dev_long_test` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `insert_into_dev_long_test` AFTER INSERT ON `dev_long_test` FOR EACH ROW BEGIN UPDATE dev_register SET info_long_test = NEW.id WHERE NEW.qr = dev_register.qr; UPDATE dev_register SET info_test_room = null WHERE NEW.qr = dev_register.qr; UPDATE dev_register SET info_qc = null WHERE NEW.qr = dev_register.qr; UPDATE dev_register SET info_pack = null WHERE NEW.qr = dev_register.qr; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `update_dev_long_test_last_step` AFTER INSERT ON `dev_long_test` FOR EACH ROW BEGIN IF NEW.success = 1 THEN UPDATE dev_register SET last_step = 'long_test' WHERE NEW.qr = dev_register.qr; END IF; IF NEW.success = 1 THEN UPDATE dev_register SET last_step_change_time = NEW.time WHERE NEW.qr = dev_register.qr; END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `dev_pack`
--

DROP TABLE IF EXISTS `dev_pack`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dev_pack` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` datetime(6) NOT NULL,
  `success` tinyint(1) NOT NULL,
  `operator` varchar(32) NOT NULL,
  `qr` varchar(32) NOT NULL,
  `info` json NOT NULL,
  PRIMARY KEY (`id`),
  KEY `dev_pack_operator_aafd702f_fk_prod_operators_login` (`operator`),
  KEY `dev_pack_qr_36941084_fk_dev_register_qr` (`qr`),
  CONSTRAINT `dev_pack_operator_aafd702f_fk_prod_operators_login` FOREIGN KEY (`operator`) REFERENCES `prod_operators` (`login`),
  CONSTRAINT `dev_pack_qr_36941084_fk_dev_register_qr` FOREIGN KEY (`qr`) REFERENCES `dev_register` (`qr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dev_pack`
--

LOCK TABLES `dev_pack` WRITE;
/*!40000 ALTER TABLE `dev_pack` DISABLE KEYS */;
/*!40000 ALTER TABLE `dev_pack` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `insert_into_dev_pack` AFTER INSERT ON `dev_pack` FOR EACH ROW BEGIN UPDATE dev_register SET info_pack = NEW.id WHERE NEW.qr = dev_register.qr; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `update_dev_pack_last_step` AFTER INSERT ON `dev_pack` FOR EACH ROW BEGIN IF NEW.success = 1 THEN UPDATE dev_register SET last_step = 'pack' WHERE NEW.qr = dev_register.qr; END IF; IF NEW.success = 1 THEN UPDATE dev_register SET last_step_change_time = NEW.time WHERE NEW.qr = dev_register.qr; END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `dev_prog`
--

DROP TABLE IF EXISTS `dev_prog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dev_prog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `info` json NOT NULL,
  `time` datetime(6) NOT NULL,
  `success` tinyint(1) NOT NULL,
  `operator` varchar(32) NOT NULL,
  `qr` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `dev_prog_operator_1184f37a_fk_prod_operators_login` (`operator`),
  KEY `dev_prog_qr_66c8aefc_fk_dev_register_qr` (`qr`),
  CONSTRAINT `dev_prog_operator_1184f37a_fk_prod_operators_login` FOREIGN KEY (`operator`) REFERENCES `prod_operators` (`login`),
  CONSTRAINT `dev_prog_qr_66c8aefc_fk_dev_register_qr` FOREIGN KEY (`qr`) REFERENCES `dev_register` (`qr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dev_prog`
--

LOCK TABLES `dev_prog` WRITE;
/*!40000 ALTER TABLE `dev_prog` DISABLE KEYS */;
/*!40000 ALTER TABLE `dev_prog` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `insert_into_dev_prog` AFTER INSERT ON `dev_prog` FOR EACH ROW BEGIN UPDATE dev_register SET info_prog = NEW.id WHERE NEW.qr = dev_register.qr; UPDATE dev_register SET info_assembling = null WHERE NEW.qr = dev_register.qr; UPDATE dev_register SET info_calibration = null WHERE NEW.qr = dev_register.qr; UPDATE dev_register SET info_long_test = null WHERE NEW.qr = dev_register.qr; UPDATE dev_register SET info_test_room = null WHERE NEW.qr = dev_register.qr; UPDATE dev_register SET info_qc = null WHERE NEW.qr = dev_register.qr; UPDATE dev_register SET info_pack = null WHERE NEW.qr = dev_register.qr; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `update_dev_prog_last_step` AFTER INSERT ON `dev_prog` FOR EACH ROW BEGIN IF NEW.success = 1 THEN UPDATE dev_register SET last_step = 'prog' WHERE NEW.qr = dev_register.qr; END IF; IF NEW.success = 1 THEN UPDATE dev_register SET last_step_change_time = NEW.time WHERE NEW.qr = dev_register.qr; END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `dev_qc`
--

DROP TABLE IF EXISTS `dev_qc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dev_qc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grade` varchar(32) DEFAULT NULL,
  `comment` varchar(64) DEFAULT NULL,
  `info` json NOT NULL,
  `defects` json DEFAULT NULL,
  `time` datetime(6) NOT NULL,
  `success` tinyint(1) NOT NULL,
  `operator` varchar(32) NOT NULL,
  `qr` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `dev_qc_operator_8bcf112f_fk_prod_operators_login` (`operator`),
  KEY `dev_qc_qr_eeacc971_fk_dev_register_qr` (`qr`),
  CONSTRAINT `dev_qc_operator_8bcf112f_fk_prod_operators_login` FOREIGN KEY (`operator`) REFERENCES `prod_operators` (`login`),
  CONSTRAINT `dev_qc_qr_eeacc971_fk_dev_register_qr` FOREIGN KEY (`qr`) REFERENCES `dev_register` (`qr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dev_qc`
--

LOCK TABLES `dev_qc` WRITE;
/*!40000 ALTER TABLE `dev_qc` DISABLE KEYS */;
/*!40000 ALTER TABLE `dev_qc` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `insert_into_dev_qc` AFTER INSERT ON `dev_qc` FOR EACH ROW BEGIN UPDATE dev_register SET info_qc = NEW.id WHERE NEW.qr = dev_register.qr; UPDATE dev_register SET info_pack = null WHERE NEW.qr = dev_register.qr; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `insert_into_dev_production_time` AFTER INSERT ON `dev_qc` FOR EACH ROW BEGIN DECLARE existing datetime(6); set existing = (select production_time from dev_register where dev_register.qr = NEW.qr); IF NEW.success = 1 and existing is null THEN UPDATE dev_register SET production_time = NEW.time WHERE NEW.qr = dev_register.qr; END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `update_dev_qc_last_step` AFTER INSERT ON `dev_qc` FOR EACH ROW BEGIN IF NEW.success = 1 THEN UPDATE dev_register SET last_step = 'qc' WHERE NEW.qr = dev_register.qr; END IF; IF NEW.success = 1 THEN UPDATE dev_register SET last_step_change_time = NEW.time WHERE NEW.qr = dev_register.qr; END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `dev_register`
--

DROP TABLE IF EXISTS `dev_register`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dev_register` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qr` varchar(32) NOT NULL,
  `region` varchar(8) NOT NULL,
  `country` varchar(32) DEFAULT NULL,
  `info_prog` int(11) DEFAULT NULL,
  `info_assembling` int(11) DEFAULT NULL,
  `info_long_test` int(11) DEFAULT NULL,
  `info_qc` int(11) DEFAULT NULL,
  `info_repair` int(11) DEFAULT NULL,
  `info_pack` int(11) DEFAULT NULL,
  `times_restored` int(11) DEFAULT NULL,
  `comment` varchar(64) DEFAULT NULL,
  `production_time` datetime(6) DEFAULT NULL,
  `dev_id` varchar(32) NOT NULL,
  `info_calibration` int(11) DEFAULT NULL,
  `info_test_room` int(11) DEFAULT NULL,
  `board_name` varchar(64) NOT NULL,
  `dev_type` int(11) NOT NULL,
  `color` varchar(15) DEFAULT NULL,
  `kit_in_old_db` tinyint(1) DEFAULT NULL,
  `kit_id` int(11) DEFAULT NULL,
  `chip_id` varchar(24) DEFAULT NULL,
  `last_step` varchar(32) DEFAULT NULL,
  `last_step_change_time` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `qr` (`qr`),
  UNIQUE KEY `dev_id` (`dev_id`),
  UNIQUE KEY `chip_id` (`chip_id`),
  KEY `dev_register_board_name_7d26a635_fk_general_boards_board_name` (`board_name`),
  KEY `dev_register_dev_type_81a504c6_fk_general_meta_type_index` (`dev_type`),
  KEY `dev_register_kit_id_efdbb904_fk_kit_register_id` (`kit_id`),
  KEY `dev_register_last_step_8fa94d8b_fk_step_name` (`last_step`),
  CONSTRAINT `dev_register_board_name_7d26a635_fk_general_boards_board_name` FOREIGN KEY (`board_name`) REFERENCES `general_boards` (`board_name`),
  CONSTRAINT `dev_register_dev_type_81a504c6_fk_general_meta_type_index` FOREIGN KEY (`dev_type`) REFERENCES `general_meta` (`type_index`),
  CONSTRAINT `dev_register_kit_id_efdbb904_fk_kit_register_id` FOREIGN KEY (`kit_id`) REFERENCES `kit_register` (`id`),
  CONSTRAINT `dev_register_last_step_8fa94d8b_fk_step_name` FOREIGN KEY (`last_step`) REFERENCES `step` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dev_register`
--

LOCK TABLES `dev_register` WRITE;
/*!40000 ALTER TABLE `dev_register` DISABLE KEYS */;
/*!40000 ALTER TABLE `dev_register` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dev_repair`
--

DROP TABLE IF EXISTS `dev_repair`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dev_repair` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_stage` varchar(150) NOT NULL,
  `reason` varchar(256) NOT NULL,
  `income_time` datetime(6) NOT NULL,
  `initial_problem` varchar(256) DEFAULT NULL,
  `what_done` varchar(256) DEFAULT NULL,
  `comment` varchar(256) DEFAULT NULL,
  `outcome_time` datetime(6) DEFAULT NULL,
  `status` varchar(256) NOT NULL,
  `dev_type` int(11) NOT NULL,
  `income_operator` varchar(32) NOT NULL,
  `outcome_operator` varchar(32) DEFAULT NULL,
  `qr` varchar(32) NOT NULL,
  `repair_type` varchar(16) DEFAULT NULL,
  `visual_operator` varchar(30) DEFAULT NULL,
  `visual_time` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `dev_repair_dev_type_91d08e14_fk_general_meta_type_index` (`dev_type`),
  KEY `dev_repair_income_operator_33320ea0_fk_prod_operators_login` (`income_operator`),
  KEY `dev_repair_outcome_operator_d876afe8_fk_prod_operators_login` (`outcome_operator`),
  KEY `dev_repair_qr_ac8fa91d_fk_dev_register_qr` (`qr`),
  CONSTRAINT `dev_repair_dev_type_91d08e14_fk_general_meta_type_index` FOREIGN KEY (`dev_type`) REFERENCES `general_meta` (`type_index`),
  CONSTRAINT `dev_repair_income_operator_33320ea0_fk_prod_operators_login` FOREIGN KEY (`income_operator`) REFERENCES `prod_operators` (`login`),
  CONSTRAINT `dev_repair_outcome_operator_d876afe8_fk_prod_operators_login` FOREIGN KEY (`outcome_operator`) REFERENCES `prod_operators` (`login`),
  CONSTRAINT `dev_repair_qr_ac8fa91d_fk_dev_register_qr` FOREIGN KEY (`qr`) REFERENCES `dev_register` (`qr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dev_repair`
--

LOCK TABLES `dev_repair` WRITE;
/*!40000 ALTER TABLE `dev_repair` DISABLE KEYS */;
/*!40000 ALTER TABLE `dev_repair` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `insert_into_dev_repair` AFTER INSERT ON `dev_repair` FOR EACH ROW BEGIN UPDATE dev_register SET info_repair = NEW.id WHERE NEW.qr = dev_register.qr; UPDATE dev_register SET info_prog = null WHERE NEW.qr = dev_register.qr; UPDATE dev_register SET info_assembling = null WHERE NEW.qr = dev_register.qr; UPDATE dev_register SET info_calibration = null WHERE NEW.qr = dev_register.qr; UPDATE dev_register SET info_long_test = null WHERE NEW.qr = dev_register.qr; UPDATE dev_register SET info_test_room = null WHERE NEW.qr = dev_register.qr; UPDATE dev_register SET info_qc = null WHERE NEW.qr = dev_register.qr; UPDATE dev_register SET info_pack = null WHERE NEW.qr = dev_register.qr; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `update_dev_repair_last_step` AFTER INSERT ON `dev_repair` FOR EACH ROW BEGIN UPDATE dev_register SET last_step = 'repair' WHERE NEW.qr = dev_register.qr; UPDATE dev_register SET last_step_change_time = NEW.income_time WHERE NEW.qr = dev_register.qr; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `dev_test_room`
--

DROP TABLE IF EXISTS `dev_test_room`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dev_test_room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `info` json NOT NULL,
  `time` datetime(6) NOT NULL,
  `success` tinyint(1) NOT NULL,
  `operator` varchar(32) NOT NULL,
  `qr` varchar(32) NOT NULL,
  `defects` json DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `dev_test_room_operator_eb191654_fk_prod_operators_login` (`operator`),
  KEY `dev_test_room_qr_cccc243f_fk_dev_register_qr` (`qr`),
  CONSTRAINT `dev_test_room_operator_eb191654_fk_prod_operators_login` FOREIGN KEY (`operator`) REFERENCES `prod_operators` (`login`),
  CONSTRAINT `dev_test_room_qr_cccc243f_fk_dev_register_qr` FOREIGN KEY (`qr`) REFERENCES `dev_register` (`qr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dev_test_room`
--

LOCK TABLES `dev_test_room` WRITE;
/*!40000 ALTER TABLE `dev_test_room` DISABLE KEYS */;
/*!40000 ALTER TABLE `dev_test_room` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `insert_into_dev_test_room` AFTER INSERT ON `dev_test_room` FOR EACH ROW BEGIN UPDATE dev_register SET info_test_room = NEW.id WHERE NEW.qr = dev_register.qr; UPDATE dev_register SET info_qc = null WHERE NEW.qr = dev_register.qr; UPDATE dev_register SET info_pack = null WHERE NEW.qr = dev_register.qr; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `update_dev_test_room_last_step` AFTER INSERT ON `dev_test_room` FOR EACH ROW BEGIN IF NEW.success = 1 THEN UPDATE dev_register SET last_step = 'test_room' WHERE NEW.qr = dev_register.qr; END IF; IF NEW.success = 1 THEN UPDATE dev_register SET last_step_change_time = NEW.time WHERE NEW.qr = dev_register.qr; END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'admin','logentry'),(7,'ajax_prod','centralassembling'),(8,'ajax_prod','centralidresource'),(9,'ajax_prod','centrallongtest'),(10,'ajax_prod','centralpack'),(11,'ajax_prod','centralprog'),(12,'ajax_prod','centralqc'),(13,'ajax_prod','centralregister'),(14,'ajax_prod','centralrepair'),(15,'ajax_prod','devassembling'),(16,'ajax_prod','devcalibration'),(17,'ajax_prod','devlongtest'),(18,'ajax_prod','devpack'),(19,'ajax_prod','devprog'),(20,'ajax_prod','devqc'),(21,'ajax_prod','devregister'),(22,'ajax_prod','devrepair'),(23,'ajax_prod','devtestroom'),(24,'ajax_prod','firmware'),(25,'ajax_prod','generalboards'),(26,'ajax_prod','generalmeta'),(27,'ajax_prod','hubmeta'),(38,'ajax_prod','kitregister'),(28,'ajax_prod','macresource'),(29,'ajax_prod','prodoperators'),(30,'ajax_prod','prodoptions'),(31,'ajax_prod','prodstatistic'),(32,'ajax_prod','prodtransactions'),(33,'ajax_prod','qrregister'),(34,'ajax_prod','resgenerate'),(35,'ajax_prod','softversion'),(36,'ajax_prod','standregister'),(37,'ajax_prod','standrepair'),(39,'ajax_prod','step'),(3,'auth','group'),(2,'auth','permission'),(4,'auth','user'),(5,'contenttypes','contenttype'),(6,'sessions','session');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2019-11-08 12:01:16.449005'),(2,'auth','0001_initial','2019-11-08 12:01:19.234130'),(3,'admin','0001_initial','2019-11-08 12:01:19.886167'),(4,'admin','0002_logentry_remove_auto_add','2019-11-08 12:01:19.927150'),(5,'admin','0003_logentry_add_action_flag_choices','2019-11-08 12:01:19.964265'),(6,'ajax_prod','0001_initial','2019-11-08 12:01:40.671796'),(7,'ajax_prod','0002_add_triggers','2019-11-08 12:01:41.482040'),(8,'ajax_prod','0003_auto_20190307_1531','2019-11-08 12:01:41.712975'),(9,'ajax_prod','0004_auto_20190318_1434','2019-11-08 12:01:42.025194'),(10,'ajax_prod','0005_auto_20190419_1252','2019-11-08 12:01:44.427838'),(11,'ajax_prod','0006_auto_20190614_1347','2019-11-08 12:01:44.664133'),(12,'ajax_prod','0007_auto_20190614_1351','2019-11-08 12:01:44.905268'),(13,'ajax_prod','0008_auto_20190729_1208','2019-11-08 12:01:46.044832'),(14,'ajax_prod','0009_auto_20190801_1241','2019-11-08 12:01:46.379216'),(15,'ajax_prod','0010_auto_20190806_1333','2019-11-08 12:01:46.463045'),(16,'ajax_prod','0011_auto_20190820_0812','2019-11-08 12:01:46.771245'),(17,'ajax_prod','0012_auto_20190909_1054','2019-11-08 12:01:46.853209'),(18,'ajax_prod','0013_kitregister_info','2019-11-08 12:01:46.990939'),(19,'ajax_prod','0014_auto_20190923_0935','2019-11-08 12:01:48.609663'),(20,'ajax_prod','0015_auto_20190923_1436','2019-11-08 12:01:49.309374'),(21,'ajax_prod','0016_auto_20191108_1108','2019-11-08 12:01:49.653498'),(22,'contenttypes','0002_remove_content_type_name','2019-11-08 12:01:50.154934'),(23,'auth','0002_alter_permission_name_max_length','2019-11-08 12:01:50.457215'),(24,'auth','0003_alter_user_email_max_length','2019-11-08 12:01:50.550076'),(25,'auth','0004_alter_user_username_opts','2019-11-08 12:01:50.621982'),(26,'auth','0005_alter_user_last_login_null','2019-11-08 12:01:50.860715'),(27,'auth','0006_require_contenttypes_0002','2019-11-08 12:01:50.881645'),(28,'auth','0007_alter_validators_add_error_messages','2019-11-08 12:01:50.924857'),(29,'auth','0008_alter_user_username_max_length','2019-11-08 12:01:51.228161'),(30,'auth','0009_alter_user_last_name_max_length','2019-11-08 12:01:51.535962'),(31,'sessions','0001_initial','2019-11-08 12:01:51.741049');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `firmware`
--

DROP TABLE IF EXISTS `firmware`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `firmware` (
  `fw_index` int(11) NOT NULL AUTO_INCREMENT,
  `version` varchar(32) NOT NULL,
  `id_address` varchar(32) NOT NULL,
  `time` datetime(6) NOT NULL,
  `fw` longblob NOT NULL,
  `region` varchar(8) NOT NULL,
  `fw_type` varchar(10) DEFAULT NULL,
  `board_id` int(11) NOT NULL,
  `uploader` varchar(32) NOT NULL,
  PRIMARY KEY (`fw_index`),
  KEY `firmware_board_id_212f1360_fk_general_boards_id` (`board_id`),
  KEY `firmware_uploader_7bc44a5f_fk_prod_operators_login` (`uploader`),
  CONSTRAINT `firmware_board_id_212f1360_fk_general_boards_id` FOREIGN KEY (`board_id`) REFERENCES `general_boards` (`id`),
  CONSTRAINT `firmware_uploader_7bc44a5f_fk_prod_operators_login` FOREIGN KEY (`uploader`) REFERENCES `prod_operators` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `firmware`
--

LOCK TABLES `firmware` WRITE;
/*!40000 ALTER TABLE `firmware` DISABLE KEYS */;
/*!40000 ALTER TABLE `firmware` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `general_boards`
--

DROP TABLE IF EXISTS `general_boards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `general_boards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `board_name` varchar(64) NOT NULL,
  `board_type` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `board_name` (`board_name`),
  UNIQUE KEY `general_boards_board_type_board_name_5819fe88_uniq` (`board_type`,`board_name`),
  CONSTRAINT `general_boards_board_type_12f84039_fk_general_meta_type_index` FOREIGN KEY (`board_type`) REFERENCES `general_meta` (`type_index`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `general_boards`
--

LOCK TABLES `general_boards` WRITE;
/*!40000 ALTER TABLE `general_boards` DISABLE KEYS */;
INSERT INTO `general_boards` VALUES (2,'door',1),(3,'door_v9',1),(17,'motion',2),(6,'fire_protect',3),(8,'glass',4),(16,'leaks',5),(19,'mpc',6),(22,'range',7),(1,'combi',8),(7,'fire_protect_plus',9),(14,'key_pad',10),(33,'key_pad_calibration',10),(15,'key_pad_v10',10),(25,'space',11),(71,'panic_button',12),(35,'motion_cam_v7',13),(18,'motion_plus',14),(4,'door_plus',15),(5,'door_plus_v9',15),(26,'transmitter',17),(23,'relay',18),(20,'mpo',19),(32,'mpo_pir',19),(30,'street_siren',20),(36,'street_siren_v11',20),(9,'home_siren_v3',21),(10,'home_siren_v4',21),(24,'socket',30),(31,'socket_rfm_mcu',30),(28,'wall_switch',31),(34,'calibrator',33),(11,'hub',33),(12,'hub_plus',33),(13,'hub_yavir',33),(72,'hub2',33),(73,'hub_yavir_plus',33),(29,'utb',253),(27,'uart_bridge',254),(21,'oc_bridge',255);
/*!40000 ALTER TABLE `general_boards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `general_meta`
--

DROP TABLE IF EXISTS `general_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `general_meta` (
  `type_index` int(11) NOT NULL,
  `name_form` varchar(64) NOT NULL,
  `name_full` varchar(64) NOT NULL,
  `longtest_viable` int(11) NOT NULL,
  `stand_viable` int(11) NOT NULL,
  PRIMARY KEY (`type_index`),
  UNIQUE KEY `name_form` (`name_form`),
  UNIQUE KEY `name_full` (`name_full`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `general_meta`
--

LOCK TABLES `general_meta` WRITE;
/*!40000 ALTER TABLE `general_meta` DISABLE KEYS */;
INSERT INTO `general_meta` VALUES (1,'DoorProtect','DoorProtect',1,0),(2,'MotionProtect','MotionProtect',1,1),(3,'FireProtect','FireProtect',1,0),(4,'GlassProtect','GlassProtect',1,0),(5,'LeaksProtect','LeaksProtect',0,0),(6,'MotionProtectCurtain','MotionProtectCurtain',1,1),(7,'RangeExtender','RangeExtender',0,0),(8,'CombiProtect','CombiProtect',1,1),(9,'FireProtectPlus','FireProtectPlus',1,0),(10,'Keypad','Keypad',0,0),(11,'SpaceControl','SpaceControl',0,0),(12,'Button','PanicButton',0,0),(13,'MotionCam','MotionCam',1,1),(14,'MotionProtectPlus','MotionProtectPlus',1,1),(15,'DoorProtectPlus','DoorProtectPlus',1,0),(17,'Transmitter','Transmitter',1,0),(18,'Relay','Relay',0,0),(19,'MotionProtectOutdoor','MotionProtectOutdoor',1,1),(20,'StreetSiren','StreetSiren',0,0),(21,'HomeSiren','HomeSiren',0,0),(30,'Socket','Socket',0,0),(31,'WallSwitch','WallSwitch',0,0),(33,'Hub','Hub',0,0),(253,'UTB','UTB',0,0),(254,'uartBridge','uartBridge',0,0),(255,'ocBridge','ocBridge',0,0);
/*!40000 ALTER TABLE `general_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hub_meta`
--

DROP TABLE IF EXISTS `hub_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hub_meta` (
  `subtype_index` int(11) NOT NULL,
  `name_form` varchar(45) NOT NULL,
  `name_full` varchar(45) NOT NULL,
  PRIMARY KEY (`subtype_index`),
  UNIQUE KEY `name_form` (`name_form`),
  UNIQUE KEY `name_full` (`name_full`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hub_meta`
--

LOCK TABLES `hub_meta` WRITE;
/*!40000 ALTER TABLE `hub_meta` DISABLE KEYS */;
INSERT INTO `hub_meta` VALUES (1,'Hub','Hub'),(2,'HubPlus','HubPlus'),(3,'HubYavir','HubYavir'),(4,'HubYavirPlus','HubYavirPlus'),(5,'Hub2','Hub2'),(6,'Hub2Plus','Hub2Plus'),(253,'Calibrator','Calibrator'),(254,'uartBridge','uartBridge'),(255,'ocBridge','ocBridge');
/*!40000 ALTER TABLE `hub_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kit_register`
--

DROP TABLE IF EXISTS `kit_register`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kit_register` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(64) NOT NULL,
  `model_number` varchar(32) NOT NULL,
  `bar_code` varchar(13) NOT NULL,
  `time` datetime(6) NOT NULL,
  `operator` varchar(32) NOT NULL,
  `info` json DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `kit_register_operator_01d7d976_fk_prod_operators_login` (`operator`),
  CONSTRAINT `kit_register_operator_01d7d976_fk_prod_operators_login` FOREIGN KEY (`operator`) REFERENCES `prod_operators` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kit_register`
--

LOCK TABLES `kit_register` WRITE;
/*!40000 ALTER TABLE `kit_register` DISABLE KEYS */;
/*!40000 ALTER TABLE `kit_register` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `macresource`
--

DROP TABLE IF EXISTS `macresource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `macresource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mac` varchar(12) NOT NULL,
  `block` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `macresource_mac_1fd67152_uniq` (`mac`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `macresource`
--

LOCK TABLES `macresource` WRITE;
/*!40000 ALTER TABLE `macresource` DISABLE KEYS */;
/*!40000 ALTER TABLE `macresource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prod_operators`
--

DROP TABLE IF EXISTS `prod_operators`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prod_operators` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(32) NOT NULL,
  `name` varchar(32) NOT NULL,
  `surname` varchar(32) NOT NULL,
  `password` varchar(256) NOT NULL,
  `role` json NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`)
) ENGINE=InnoDB AUTO_INCREMENT=169 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prod_operators`
--

LOCK TABLES `prod_operators` WRITE;
/*!40000 ALTER TABLE `prod_operators` DISABLE KEYS */;
INSERT INTO `prod_operators` VALUES (168,'3ema208','Р.В.','Недобитко','$5$rounds=535000$BVlvNAmD20yTyJF6$2u7wHPSlxxt6AdskWuLSjCeZbkya51/dGVCZlkXcs/0','{\"qc\": 1, \"packing\": 1, \"shipyard\": 1, \"long_test\": 1, \"test_room\": 1, \"assembling\": 1, \"repair_tool\": 1}',1);
/*!40000 ALTER TABLE `prod_operators` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prod_options`
--

DROP TABLE IF EXISTS `prod_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prod_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_name` varchar(32) NOT NULL,
  `options` json NOT NULL,
  `time` datetime(6) NOT NULL,
  `operator` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `prod_options_operator_bb96d0d7_fk_prod_operators_login` (`operator`),
  CONSTRAINT `prod_options_operator_bb96d0d7_fk_prod_operators_login` FOREIGN KEY (`operator`) REFERENCES `prod_operators` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prod_options`
--

LOCK TABLES `prod_options` WRITE;
/*!40000 ALTER TABLE `prod_options` DISABLE KEYS */;
/*!40000 ALTER TABLE `prod_options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prod_statistic`
--

DROP TABLE IF EXISTS `prod_statistic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prod_statistic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `statistic` json NOT NULL,
  `time` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prod_statistic`
--

LOCK TABLES `prod_statistic` WRITE;
/*!40000 ALTER TABLE `prod_statistic` DISABLE KEYS */;
/*!40000 ALTER TABLE `prod_statistic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prod_transactions`
--

DROP TABLE IF EXISTS `prod_transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prod_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_step` varchar(32) NOT NULL,
  `to_step` varchar(32) NOT NULL,
  `dev_type` int(11) NOT NULL,
  `operator` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `prod_transactions_dev_type_45e4a0e8_fk_general_meta_type_index` (`dev_type`),
  KEY `prod_transactions_operator_655baa72_fk_prod_operators_login` (`operator`),
  CONSTRAINT `prod_transactions_dev_type_45e4a0e8_fk_general_meta_type_index` FOREIGN KEY (`dev_type`) REFERENCES `general_meta` (`type_index`),
  CONSTRAINT `prod_transactions_operator_655baa72_fk_prod_operators_login` FOREIGN KEY (`operator`) REFERENCES `prod_operators` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prod_transactions`
--

LOCK TABLES `prod_transactions` WRITE;
/*!40000 ALTER TABLE `prod_transactions` DISABLE KEYS */;
/*!40000 ALTER TABLE `prod_transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qr_register`
--

DROP TABLE IF EXISTS `qr_register`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qr_register` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start_qr_sensor` varchar(32) NOT NULL,
  `start_qr_hub` varchar(32) NOT NULL,
  `start_qr_power` varchar(32) NOT NULL,
  `time` datetime(6) NOT NULL,
  `operator` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `qr_register_operator_df8728ff_fk_prod_operators_login` (`operator`),
  CONSTRAINT `qr_register_operator_df8728ff_fk_prod_operators_login` FOREIGN KEY (`operator`) REFERENCES `prod_operators` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qr_register`
--

LOCK TABLES `qr_register` WRITE;
/*!40000 ALTER TABLE `qr_register` DISABLE KEYS */;
/*!40000 ALTER TABLE `qr_register` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `res_generate`
--

DROP TABLE IF EXISTS `res_generate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `res_generate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `value` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `res_generate`
--

LOCK TABLES `res_generate` WRITE;
/*!40000 ALTER TABLE `res_generate` DISABLE KEYS */;
/*!40000 ALTER TABLE `res_generate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `soft_version`
--

DROP TABLE IF EXISTS `soft_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `soft_version` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(170) NOT NULL,
  `version` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `soft_version`
--

LOCK TABLES `soft_version` WRITE;
/*!40000 ALTER TABLE `soft_version` DISABLE KEYS */;
INSERT INTO `soft_version` VALUES (1,'futureHubVersion','207102'),(2,'futureRuHubVersion','207103'),(3,'futureHubPlusVersion','207104'),(4,'futureRuHubPlusVersion','207105'),(5,'futureHub2Version','207106'),(6,'futureRuHub2Version','207107'),(7,'futureHubYavirVersion','207108'),(8,'futureHubYavirPlusVersion','207109');
/*!40000 ALTER TABLE `soft_version` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stand_register`
--

DROP TABLE IF EXISTS `stand_register`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stand_register` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stand_id` varchar(32) NOT NULL,
  `qr` varchar(32) DEFAULT NULL,
  `mac_eth` varchar(64) NOT NULL,
  `first_calibration_t` datetime(6) NOT NULL,
  `last_calibration_t` datetime(6) NOT NULL,
  `calibration_result` int(11) DEFAULT NULL,
  `calibration_count` int(11) DEFAULT NULL,
  `info_repair` int(11) DEFAULT NULL,
  `stand_info` json NOT NULL,
  `operator` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `stand_id` (`stand_id`),
  UNIQUE KEY `qr` (`qr`),
  KEY `stand_register_operator_ef14f294_fk_prod_operators_login` (`operator`),
  CONSTRAINT `stand_register_operator_ef14f294_fk_prod_operators_login` FOREIGN KEY (`operator`) REFERENCES `prod_operators` (`login`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stand_register`
--

LOCK TABLES `stand_register` WRITE;
/*!40000 ALTER TABLE `stand_register` DISABLE KEYS */;
INSERT INTO `stand_register` VALUES (1,'123',NULL,'fafafafafafafafa','2019-11-08 12:05:10.000000','2019-11-08 12:05:10.000000',NULL,NULL,NULL,'{}','3ema208'),(2,'124',NULL,'fffafafafafafafa','2019-11-08 12:05:16.000000','2019-11-08 12:05:16.000000',NULL,NULL,NULL,'{}','3ema208');
/*!40000 ALTER TABLE `stand_register` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stand_repair`
--

DROP TABLE IF EXISTS `stand_repair`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stand_repair` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_stage` varchar(150) NOT NULL,
  `reason` varchar(256) NOT NULL,
  `income_time` datetime(6) NOT NULL,
  `initial_problem` varchar(256) DEFAULT NULL,
  `what_done` varchar(256) DEFAULT NULL,
  `comment` varchar(256) DEFAULT NULL,
  `outcome_time` datetime(6) DEFAULT NULL,
  `status` varchar(256) NOT NULL,
  `income_operator` varchar(32) NOT NULL,
  `outcome_operator` varchar(32) DEFAULT NULL,
  `qr` varchar(32) NOT NULL,
  `repair_type` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `stand_repair_income_operator_c905306c_fk_prod_operators_login` (`income_operator`),
  KEY `stand_repair_outcome_operator_f5330ca1_fk_prod_operators_login` (`outcome_operator`),
  KEY `stand_repair_qr_65eeebf0_fk_stand_register_qr` (`qr`),
  CONSTRAINT `stand_repair_income_operator_c905306c_fk_prod_operators_login` FOREIGN KEY (`income_operator`) REFERENCES `prod_operators` (`login`),
  CONSTRAINT `stand_repair_outcome_operator_f5330ca1_fk_prod_operators_login` FOREIGN KEY (`outcome_operator`) REFERENCES `prod_operators` (`login`),
  CONSTRAINT `stand_repair_qr_65eeebf0_fk_stand_register_qr` FOREIGN KEY (`qr`) REFERENCES `stand_register` (`qr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stand_repair`
--

LOCK TABLES `stand_repair` WRITE;
/*!40000 ALTER TABLE `stand_repair` DISABLE KEYS */;
/*!40000 ALTER TABLE `stand_repair` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `insert_into_stand_repair` AFTER INSERT ON `stand_repair` FOR EACH ROW BEGIN UPDATE stand_register SET info_repair = NEW.id WHERE NEW.qr = stand_register.qr; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `step`
--

DROP TABLE IF EXISTS `step`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `step` (
  `name` varchar(32) NOT NULL,
  `number` int(11) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `step`
--

LOCK TABLES `step` WRITE;
/*!40000 ALTER TABLE `step` DISABLE KEYS */;
INSERT INTO `step` VALUES ('assembling',3),('calibration',4),('long_test',5),('pack',8),('prog',2),('qc',7),('repair',1),('test_room',6);
/*!40000 ALTER TABLE `step` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-08 14:58:04
