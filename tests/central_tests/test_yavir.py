import re
from PyQt5 import QtCore
from app_setting import AppSetting
from qc.enum_color import ColorResult
from tests.central_tests.abstract_test_view_hub import AbstractTestViewHub
from tests.utils import db_handler
from __version__ import __version__


class TestYavir(AbstractTestViewHub):
    AppSetting.DEBUG = True

    hub_type = 'hub_yavir'
    hub_id = db_handler.hubs[hub_type][0][:9].replace("-", '')
    hub_type_num = "21"

    database = db_handler.DBHandlerHub(hub_type)

    def test_success_hub_reg(self, qtbot, mock_socket_csa, mock_sim_ok):
        main_w, server, socket = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_hub_last_steps(database=self.database, programming=True, assembling=True)

        self.wait_freq_apply(qtbot, main_w)

        self.scan_and_test_default_setup(main_w, view_obj_num=2)

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 2).push_btn_connect, QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='2b')

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[2].cellWidget(0, 2).push_btn_connect.styleSheet() == ColorResult.success.value)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 2).push_btn_connect.text() == 'Додано'

        qtbot.waitUntil(lambda: "19 09" not in str(socket.messages_heap))
        socket.messages_heap.append(server.update_hub_subtype(
            (self.hub_id, self.hub_type_num), "03"
        ))

        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is not None, timeout=5*1000)

        socket.check_message_exists(message_type='07',
                                    message_key=None,
                                    message_payload=['27', '27000001', '06', '01'])

        socket.check_message_exists(message_type='07',
                                    message_key=None,
                                    message_payload=['26', '26000001', '0a', '02', '0c', '01', '04', '00000001'])

        socket.check_message_exists(message_type='07',
                                    message_key=None,
                                    message_payload=['26', '26000002', '0a', '02', '0c', '01', '04', '00000001'])

        socket.check_message_exists(message_type='07',
                                    message_key=None,
                                    message_payload=['26', '26000003', '0a', '02', '0c', '01', '04', '00000001'])

        socket.check_message_exists(message_type='07',
                                    message_key=None,
                                    message_payload=['26', '26000004', '0a', '02', '0c', '01', '04', '00000001'])

        # test check color
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).label_status.text() == 'Невідомо'
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("49", "01")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap))
        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).styleSheet() == ColorResult.success.value, timeout=5*1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).label_status.text() == 'white'

        # test check tamper alarms
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm_hub(
                "07", "05", (self.hub_id, self.hub_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 07" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_on.text() == f'ВКЛ: {i + 1}/2'

            socket.messages_heap.append(server.send_alarm_hub(
                "01", "04", (self.hub_id, self.hub_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 01" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_off.text() == f'ВИКЛ: {i + 1}/2'

        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_on.styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_off.styleSheet() == ColorResult.success.value

        # test active channels
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("48", "05")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 5).label_status.text() == "Eth/Gsm"
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        # test active zones alarms
        for num_zone in range(4):
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[f"2600000{num_zone+1}"].text() == "False"
            assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[f"2600000{num_zone+1}"].styleSheet() == ColorResult.success.value

        for num in range(4):
            socket.messages_heap.append(server.wire_input_alarm(
                (f"2600000{num+1}", '26', self.hub_id)
            ))
            qtbot.wait_until(lambda: "08 00" not in str(socket.messages_heap), timeout=3 * 1000)
            qtbot.wait_until(lambda: main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[f"2600000{num+1}"].text() == "True", timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[f"2600000{num+1}"].text() == "True"

        # test sim slots
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_sim_one.styleSheet() == ColorResult.success.value
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_second_sim.styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.update_sim_slot1_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_sim_one.styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.update_sim_slot2_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_second_sim.styleSheet() == ColorResult.success.value

        # test led arm
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).styleSheet() == ColorResult.success.value

        yavir = main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id)
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).push_btn_changed,
                         QtCore.Qt.LeftButton)
        socket.messages_heap.append(server.update_state_hub_arm(self.hub_id))
        assert socket.check_message_exists(message_type='04', message_key='00')
        qtbot.wait_until(lambda: yavir.get_parameters("08") == '01')

        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).push_btn_changed,
                         QtCore.Qt.LeftButton)
        socket.messages_heap.append(server.update_state_hub_disarm(self.hub_id))
        assert socket.check_message_exists(message_type='04', message_key='02')
        qtbot.wait_until(lambda: yavir.get_parameters("08") == '00')

        main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).check_box.setChecked(True)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).styleSheet() == ColorResult.success.value

        # test siren led
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 9).styleSheet() == ColorResult.success.value

        main_w.qc_main_widget.view_obj[2].cellWidget(0, 9).check_box_select.setChecked(True)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 9).styleSheet() == ColorResult.success.value

        # test RS485 led
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 10).styleSheet() == ColorResult.success.value

        main_w.qc_main_widget.view_obj[2].cellWidget(0, 10).check_box_select.setChecked(True)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 10).styleSheet() == ColorResult.success.value

        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 11).isEnabled()
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 12).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 12).text() == 'Зареєструвати'

        # test success register device
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 12),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='07')
        assert socket.check_message_exists(message_type='03', message_key='0a')
        assert socket.check_message_exists(message_type='0c', message_key='01')

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[2].cellWidget(0, 12) is None, timeout=5 * 1000)

        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is None, timeout=5*1000)
        info_qc, csa_info, info_register = self.database.get_stat_hub_after_success_reg(qr=db_handler.hubs[self.hub_type][0])

        _, grade, _, info, defects, time_reg, success, operator, qr = info_qc
        _, hub_id, _, persisted_settings, firm_version, mandatory_update, fw_protection, fw_group, hub_type = csa_info
        country, pro_acc = info_register

        assert grade == '0'
        assert __version__ in info
        assert defects == '""'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 1
        assert operator == 'armen'
        assert qr == db_handler.hubs[self.hub_type][0]

        assert hub_id == self.hub_id.upper()
        assert persisted_settings is None
        assert firm_version == 207108
        assert mandatory_update is None
        assert fw_protection is None
        assert fw_group == 1715
        assert hub_type == int(db_handler.hubs[self.hub_type][0][-3])

        assert country is None
        assert pro_acc is None

    def test_scan_hub_without_prog(self, qtbot, mock_socket_csa):
        self.scan_without_prog(qtbot, mock_socket_csa, self.database)

    def test_scan_hub_without_assembling(self, qtbot, mock_socket_csa):
        self.scan_without_assembling(qtbot, mock_socket_csa, self.database)

    def test_hub_with_qc_passed(self, qtbot, mock_socket_csa, mock_retry_test_popup_true):
        main_w, *_ = mock_socket_csa
        self.scan_hub_with_qc_passed(qtbot, main_w, self.database)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 1) is not None

    def test_success_hub_reg_with_grade_defects(self, qtbot, mock_socket_csa, mock_defects_grade, mock_sim_ok):
        main_w, server, socket = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_hub_last_steps(database=self.database, programming=True, assembling=True)

        self.wait_freq_apply(qtbot, main_w)

        self.scan_and_test_default_setup(main_w, view_obj_num=2)

        self.add_grade_defects(qtbot=qtbot, main_w=main_w, view_obj_num=2)

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 2).push_btn_connect, QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='2b')

        qtbot.waitUntil(
            lambda: main_w.qc_main_widget.view_obj[2].cellWidget(0, 2).push_btn_connect.styleSheet() == ColorResult.success.value)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 2).push_btn_connect.text() == 'Додано'

        
        
        qtbot.waitUntil(lambda: "19 09" not in str(socket.messages_heap))
        socket.messages_heap.append(server.update_hub_subtype(
            (self.hub_id, self.hub_type_num), "03"
        ))

        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is not None, timeout=5 * 1000)

        # test check color
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).label_status.text() == 'Невідомо'
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("49", "01")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap))
        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).styleSheet() == ColorResult.success.value,
                        timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).label_status.text() == 'white'

        # test check tamper alarms
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm_hub(
                "07", "05", (self.hub_id, self.hub_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 07" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_on.text() == f'ВКЛ: {i + 1}/2'

            socket.messages_heap.append(server.send_alarm_hub(
                "01", "04", (self.hub_id, self.hub_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 01" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_off.text() == f'ВИКЛ: {i + 1}/2'

        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_on.styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_off.styleSheet() == ColorResult.success.value

        # test active channels
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("48", "05")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 5).label_status.text() == "Eth/Gsm"
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        # test active zones alarms
        for num_zone in range(4):
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[f"2600000{num_zone + 1}"].text() == "False"
            assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[
                           f"2600000{num_zone + 1}"].styleSheet() == ColorResult.success.value

        for num in range(4):
            socket.messages_heap.append(server.wire_input_alarm(
                (f"2600000{num + 1}", '26', self.hub_id)
            ))
            qtbot.wait_until(lambda: "08 00" not in str(socket.messages_heap), timeout=3 * 1000)
            qtbot.wait_until(
                lambda: main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[f"2600000{num + 1}"].text() == "True",
                timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[f"2600000{num + 1}"].text() == "True"

        # test sim slots
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_sim_one.styleSheet() == ColorResult.success.value
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_second_sim.styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.update_sim_slot1_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_sim_one.styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.update_sim_slot2_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_second_sim.styleSheet() == ColorResult.success.value

        # test led arm
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).styleSheet() == ColorResult.success.value

        yavir = main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id)
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).push_btn_changed,
                         QtCore.Qt.LeftButton)
        socket.messages_heap.append(server.update_state_hub_arm(self.hub_id))
        assert socket.check_message_exists(message_type='04', message_key='00')
        qtbot.wait_until(lambda: yavir.get_parameters("08") == '01')

        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).push_btn_changed,
                         QtCore.Qt.LeftButton)
        socket.messages_heap.append(server.update_state_hub_disarm(self.hub_id))
        assert socket.check_message_exists(message_type='04', message_key='02')
        qtbot.wait_until(lambda: yavir.get_parameters("08") == '00')

        main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).check_box.setChecked(True)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).styleSheet() == ColorResult.success.value

        # test siren led
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 9).styleSheet() == ColorResult.success.value

        main_w.qc_main_widget.view_obj[2].cellWidget(0, 9).check_box_select.setChecked(True)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 9).styleSheet() == ColorResult.success.value

        # test RS485 led
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 10).styleSheet() == ColorResult.success.value

        main_w.qc_main_widget.view_obj[2].cellWidget(0, 10).check_box_select.setChecked(True)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 10).styleSheet() == ColorResult.success.value

        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 11).isEnabled()
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 12).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 12).text() == 'Зареєструвати'

        # test success register device
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 12),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='07')
        assert socket.check_message_exists(message_type='03', message_key='0a')
        assert socket.check_message_exists(message_type='0c', message_key='01')

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[2].cellWidget(0, 12) is None, timeout=5 * 1000)
        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is None, timeout=5 * 1000)

        info_qc, csa_info, info_register = self.database.get_stat_hub_after_success_reg(qr=db_handler.hubs[self.hub_type][0])

        _, grade, _, info, defects, time_reg, success, operator, qr = info_qc
        _, hub_id, _, persisted_settings, firm_version, mandatory_update, fw_protection, fw_group, hub_type = csa_info
        country, pro_acc = info_register

        assert grade == '1'
        assert __version__ in info
        assert defects == '"Сколи;Прожоги"'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 1
        assert operator == 'armen'
        assert qr == db_handler.hubs[self.hub_type][0]

        assert hub_id == self.hub_id.upper()
        assert persisted_settings is None
        assert firm_version == 207108
        assert mandatory_update is None
        assert fw_protection is None
        assert fw_group == 1715
        assert hub_type == int(db_handler.hubs[self.hub_type][0][-3])

        assert country is None
        assert pro_acc is None

    def test_tamper_fail(self, qtbot, mock_socket_csa, mock_you_sure_central):
        main_w, server, socket = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_hub_last_steps(database=self.database, programming=True, assembling=True)

        self.wait_freq_apply(qtbot, main_w)

        self.scan_and_test_default_setup(main_w, view_obj_num=2)

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 2).push_btn_connect, QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='2b')

        qtbot.waitUntil(
            lambda: main_w.qc_main_widget.view_obj[2].cellWidget(0, 2).push_btn_connect.styleSheet() == ColorResult.success.value)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 2).push_btn_connect.text() == 'Додано'

        
        
        qtbot.waitUntil(lambda: "19 09" not in str(socket.messages_heap))
        socket.messages_heap.append(server.update_hub_subtype(
            (self.hub_id, self.hub_type_num), "03"
        ))

        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is not None, timeout=5 * 1000)

        socket.check_message_exists(message_type='07',
                                    message_key=None,
                                    message_payload=['27', '27000001', '06', '01'])

        socket.check_message_exists(message_type='07',
                                    message_key=None,
                                    message_payload=['26', '26000001', '0a', '02', '0c', '01', '04', '00000001'])

        socket.check_message_exists(message_type='07',
                                    message_key=None,
                                    message_payload=['26', '26000002', '0a', '02', '0c', '01', '04', '00000001'])

        socket.check_message_exists(message_type='07',
                                    message_key=None,
                                    message_payload=['26', '26000003', '0a', '02', '0c', '01', '04', '00000001'])

        socket.check_message_exists(message_type='07',
                                    message_key=None,
                                    message_payload=['26', '26000004', '0a', '02', '0c', '01', '04', '00000001'])

        # test check color
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).label_status.text() == 'Невідомо'
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("49", "01")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap))
        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).styleSheet() == ColorResult.success.value,
                        timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).label_status.text() == 'white'

        # test active channels
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("48", "05")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 5).label_status.text() == "Eth/Gsm"
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        # test active zones alarms
        for num_zone in range(4):
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[f"2600000{num_zone + 1}"].text() == "False"
            assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[
                           f"2600000{num_zone + 1}"].styleSheet() == ColorResult.success.value

        for num in range(4):
            socket.messages_heap.append(server.wire_input_alarm(
                (f"2600000{num + 1}", '26', self.hub_id)
            ))
            qtbot.wait_until(lambda: "08 00" not in str(socket.messages_heap), timeout=3 * 1000)
            qtbot.wait_until(
                lambda: main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[f"2600000{num + 1}"].text() == "True",
                timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[f"2600000{num + 1}"].text() == "True"

        # test sim slots
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_sim_one.styleSheet() == ColorResult.success.value
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_second_sim.styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.update_sim_slot1_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_sim_one.styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.update_sim_slot2_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_second_sim.styleSheet() == ColorResult.success.value

        # test led arm
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).styleSheet() == ColorResult.success.value

        yavir = main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id)
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).push_btn_changed,
                         QtCore.Qt.LeftButton)
        socket.messages_heap.append(server.update_state_hub_arm(self.hub_id))
        assert socket.check_message_exists(message_type='04', message_key='00')
        qtbot.wait_until(lambda: yavir.get_parameters("08") == '01')

        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).push_btn_changed,
                         QtCore.Qt.LeftButton)
        socket.messages_heap.append(server.update_state_hub_disarm(self.hub_id))
        assert socket.check_message_exists(message_type='04', message_key='02')
        qtbot.wait_until(lambda: yavir.get_parameters("08") == '00')

        main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).check_box.setChecked(True)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).styleSheet() == ColorResult.success.value

        # test siren led
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 9).styleSheet() == ColorResult.success.value

        main_w.qc_main_widget.view_obj[2].cellWidget(0, 9).check_box_select.setChecked(True)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 9).styleSheet() == ColorResult.success.value

        # test RS485 led
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 10).styleSheet() == ColorResult.success.value

        main_w.qc_main_widget.view_obj[2].cellWidget(0, 10).check_box_select.setChecked(True)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 10).styleSheet() == ColorResult.success.value

        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 11).isEnabled()
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 12).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 12).text() == 'Повернути'

        # test success register device
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 12),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='07')
        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is None, timeout=5 * 1000)

        info_qc, info_repair, csa_info = self.database.get_stat_hub_after_fail_reg(qr=db_handler.hubs[self.hub_type][0])

        _, grade, _, info, defects, time_reg, success, operator, qr = info_qc
        _, from_stage, reason, income_time, initial_problem, what_done, comment, outcome_time, \
            status, dev_type_db, income_operator, outcome_operator, qr_repair, repair_type, *_ = info_repair
        _, hub_id, _, persisted_settings, firm_version, mandatory_update, fw_protection, fw_group, hub_type = csa_info

        # check info in central_qc
        assert grade is None
        assert __version__ in info
        assert defects == '""'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 0
        assert operator == 'armen'
        assert qr == db_handler.hubs[self.hub_type][0]

        # check info in csa.hubs
        assert hub_id == self.hub_id.upper()
        assert persisted_settings is None
        assert firm_version == 208001
        assert mandatory_update is None
        assert fw_protection is None
        assert fw_group == 1
        assert hub_type == int(db_handler.hubs[self.hub_type][0][-3])

        # check info in central_repair
        assert from_stage == "QC"
        assert reason == ";Тампер"
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         income_time.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert initial_problem is None
        assert what_done is None
        assert comment is None
        assert status == 'Відправлено у ремонт'
        assert dev_type_db == 33
        assert income_operator == 'armen'
        assert outcome_operator is None
        assert qr_repair == db_handler.hubs[self.hub_type][0]
        assert repair_type is None

    def test_color_check_fail(self, qtbot, mock_socket_csa, mock_you_sure_central):
        main_w, server, socket = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_hub_last_steps(database=self.database, programming=True, assembling=True)

        self.wait_freq_apply(qtbot, main_w)

        self.scan_and_test_default_setup(main_w, view_obj_num=2)

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 2).push_btn_connect, QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='2b')

        qtbot.waitUntil(
            lambda: main_w.qc_main_widget.view_obj[2].cellWidget(0, 2).push_btn_connect.styleSheet() == ColorResult.success.value)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 2).push_btn_connect.text() == 'Додано'

        
        
        qtbot.waitUntil(lambda: "19 09" not in str(socket.messages_heap))
        socket.messages_heap.append(server.update_hub_subtype(
            (self.hub_id, self.hub_type_num), "03"
        ))

        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is not None, timeout=5 * 1000)

        # test check color
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).label_status.text() == 'Невідомо'
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("49", "02")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap))
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).label_status.text() == 'black'

        # test check tamper alarms
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm_hub(
                "07", "05", (self.hub_id, self.hub_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 07" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_on.text() == f'ВКЛ: {i + 1}/2'

            socket.messages_heap.append(server.send_alarm_hub(
                "01", "04", (self.hub_id, self.hub_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 01" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_off.text() == f'ВИКЛ: {i + 1}/2'

        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_on.styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_off.styleSheet() == ColorResult.success.value

        # test active channels
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("48", "05")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 5).label_status.text() == "Eth/Gsm"
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        # test active zones alarms
        for num_zone in range(4):
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[f"2600000{num_zone + 1}"].text() == "False"
            assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[
                           f"2600000{num_zone + 1}"].styleSheet() == ColorResult.success.value

        for num in range(4):
            socket.messages_heap.append(server.wire_input_alarm(
                (f"2600000{num + 1}", '26', self.hub_id)
            ))
            qtbot.wait_until(lambda: "08 00" not in str(socket.messages_heap), timeout=3 * 1000)
            qtbot.wait_until(
                lambda: main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[f"2600000{num + 1}"].text() == "True",
                timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[f"2600000{num + 1}"].text() == "True"

        # test sim slots
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_sim_one.styleSheet() == ColorResult.success.value
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_second_sim.styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.update_sim_slot1_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_sim_one.styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.update_sim_slot2_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_second_sim.styleSheet() == ColorResult.success.value

        # test led arm
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).styleSheet() == ColorResult.success.value

        yavir = main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id)
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).push_btn_changed,
                         QtCore.Qt.LeftButton)
        socket.messages_heap.append(server.update_state_hub_arm(self.hub_id))
        assert socket.check_message_exists(message_type='04', message_key='00')
        qtbot.wait_until(lambda: yavir.get_parameters("08") == '01')

        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).push_btn_changed,
                         QtCore.Qt.LeftButton)
        socket.messages_heap.append(server.update_state_hub_disarm(self.hub_id))
        assert socket.check_message_exists(message_type='04', message_key='02')
        qtbot.wait_until(lambda: yavir.get_parameters("08") == '00')

        main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).check_box.setChecked(True)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).styleSheet() == ColorResult.success.value

        # test siren led
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 9).styleSheet() == ColorResult.success.value

        main_w.qc_main_widget.view_obj[2].cellWidget(0, 9).check_box_select.setChecked(True)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 9).styleSheet() == ColorResult.success.value

        # test RS485 led
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 10).styleSheet() == ColorResult.success.value

        main_w.qc_main_widget.view_obj[2].cellWidget(0, 10).check_box_select.setChecked(True)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 10).styleSheet() == ColorResult.success.value

        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 11).isEnabled()
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 12).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 12).text() == 'Повернути'

        # test success register device
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 12),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='07')
        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is None, timeout=5 * 1000)

        info_qc, info_repair, csa_info = self.database.get_stat_hub_after_fail_reg(qr=db_handler.hubs[self.hub_type][0])

        _, grade, _, info, defects, time_reg, success, operator, qr = info_qc
        _, from_stage, reason, income_time, initial_problem, what_done, comment, outcome_time, \
            status, dev_type_db, income_operator, outcome_operator, qr_repair, repair_type, *_ = info_repair
        _, hub_id, _, persisted_settings, firm_version, mandatory_update, fw_protection, fw_group, hub_type = csa_info

        # check info in central_qc
        assert grade is None
        assert __version__ in info
        assert defects == '""'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 0
        assert operator == 'armen'
        assert qr == db_handler.hubs[self.hub_type][0]

        # check info in csa.hubs
        assert hub_id == self.hub_id.upper()
        assert persisted_settings is None
        assert firm_version == 208001
        assert mandatory_update is None
        assert fw_protection is None
        assert fw_group == 1
        assert hub_type == int(db_handler.hubs[self.hub_type][0][-3])

        # check info in central_repair
        assert from_stage == "QC"
        assert reason == ";Колір"
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         income_time.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert initial_problem is None
        assert what_done is None
        assert comment is None
        assert status == 'Відправлено у ремонт'
        assert dev_type_db == 33
        assert income_operator == 'armen'
        assert outcome_operator is None
        assert qr_repair == db_handler.hubs[self.hub_type][0]
        assert repair_type is None

    def test_zones_fail(self, qtbot, mock_socket_csa, mock_you_sure_central):
        main_w, server, socket = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_hub_last_steps(database=self.database, programming=True, assembling=True)

        self.wait_freq_apply(qtbot, main_w)

        self.scan_and_test_default_setup(main_w, view_obj_num=2)

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 2).push_btn_connect, QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='2b')

        qtbot.waitUntil(
            lambda: main_w.qc_main_widget.view_obj[2].cellWidget(0, 2).push_btn_connect.styleSheet() == ColorResult.success.value)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 2).push_btn_connect.text() == 'Додано'

        
        
        qtbot.waitUntil(lambda: "19 09" not in str(socket.messages_heap))
        socket.messages_heap.append(server.update_hub_subtype(
            (self.hub_id, self.hub_type_num), "03"
        ))

        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is not None, timeout=5 * 1000)

        # test check color
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).label_status.text() == 'Невідомо'
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("49", "01")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap))
        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).styleSheet() == ColorResult.success.value,
                        timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).label_status.text() == 'white'

        # test check tamper alarms
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm_hub(
                "07", "05", (self.hub_id, self.hub_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 07" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_on.text() == f'ВКЛ: {i + 1}/2'

            socket.messages_heap.append(server.send_alarm_hub(
                "01", "04", (self.hub_id, self.hub_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 01" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_off.text() == f'ВИКЛ: {i + 1}/2'

        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_on.styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_off.styleSheet() == ColorResult.success.value

        # test active channels
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("48", "05")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 5).label_status.text() == "Eth/Gsm"
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        # test active zones alarms
        for num_zone in range(4):
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[f"2600000{num_zone + 1}"].text() == "False"
            assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[
                           f"2600000{num_zone + 1}"].styleSheet() == ColorResult.success.value

        for num in range(2):
            socket.messages_heap.append(server.wire_input_alarm(
                (f"2600000{num + 1}", '26', self.hub_id)
            ))
            qtbot.wait_until(lambda: "08 00" not in str(socket.messages_heap), timeout=3 * 1000)

        # test sim slots
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_sim_one.styleSheet() == ColorResult.success.value
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_second_sim.styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.update_sim_slot1_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_sim_one.styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.update_sim_slot2_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_second_sim.styleSheet() == ColorResult.success.value

        # test led arm
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).styleSheet() == ColorResult.success.value

        yavir = main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id)
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).push_btn_changed,
                         QtCore.Qt.LeftButton)
        socket.messages_heap.append(server.update_state_hub_arm(self.hub_id))
        assert socket.check_message_exists(message_type='04', message_key='00')
        qtbot.wait_until(lambda: yavir.get_parameters("08") == '01')

        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).push_btn_changed,
                         QtCore.Qt.LeftButton)
        socket.messages_heap.append(server.update_state_hub_disarm(self.hub_id))
        assert socket.check_message_exists(message_type='04', message_key='02')
        qtbot.wait_until(lambda: yavir.get_parameters("08") == '00')

        main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).check_box.setChecked(True)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).styleSheet() == ColorResult.success.value

        # test siren led
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 9).styleSheet() == ColorResult.success.value

        main_w.qc_main_widget.view_obj[2].cellWidget(0, 9).check_box_select.setChecked(True)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 9).styleSheet() == ColorResult.success.value

        # test RS485 led
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 10).styleSheet() == ColorResult.success.value

        main_w.qc_main_widget.view_obj[2].cellWidget(0, 10).check_box_select.setChecked(True)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 10).styleSheet() == ColorResult.success.value

        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 11).isEnabled()
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 12).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 12).text() == 'Повернути'

        # test success register device
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 12),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='07')
        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is None, timeout=5 * 1000)

        info_qc, info_repair, csa_info = self.database.get_stat_hub_after_fail_reg(qr=db_handler.hubs[self.hub_type][0])

        _, grade, _, info, defects, time_reg, success, operator, qr = info_qc
        _, from_stage, reason, income_time, initial_problem, what_done, comment, outcome_time, \
            status, dev_type_db, income_operator, outcome_operator, qr_repair, repair_type, *_ = info_repair
        _, hub_id, _, persisted_settings, firm_version, mandatory_update, fw_protection, fw_group, hub_type = csa_info

        # check info in central_qc
        assert grade is None
        assert __version__ in info
        assert defects == '""'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 0
        assert operator == 'armen'
        assert qr == db_handler.hubs[self.hub_type][0]

        # check info in csa.hubs
        assert hub_id == self.hub_id.upper()
        assert persisted_settings is None
        assert firm_version == 208001
        assert mandatory_update is None
        assert fw_protection is None
        assert fw_group == 1
        assert hub_type == int(db_handler.hubs[self.hub_type][0][-3])

        # check info in central_repair
        assert from_stage == "QC"
        assert reason == ";Активні зони"
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         income_time.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert initial_problem is None
        assert what_done is None
        assert comment is None
        assert status == 'Відправлено у ремонт'
        assert dev_type_db == 33
        assert income_operator == 'armen'
        assert outcome_operator is None
        assert qr_repair == db_handler.hubs[self.hub_type][0]
        assert repair_type is None

    def test_led_arm_fail(self, qtbot, mock_socket_csa, mock_you_sure_central):
        main_w, server, socket = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_hub_last_steps(database=self.database, programming=True, assembling=True)

        self.wait_freq_apply(qtbot, main_w)

        self.scan_and_test_default_setup(main_w, view_obj_num=2)

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 2).push_btn_connect, QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='2b')

        qtbot.waitUntil(
            lambda: main_w.qc_main_widget.view_obj[2].cellWidget(0, 2).push_btn_connect.styleSheet() == ColorResult.success.value)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 2).push_btn_connect.text() == 'Додано'

        
        
        qtbot.waitUntil(lambda: "19 09" not in str(socket.messages_heap))
        socket.messages_heap.append(server.update_hub_subtype(
            (self.hub_id, self.hub_type_num), "03"
        ))

        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is not None, timeout=5 * 1000)

        # test check color
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).label_status.text() == 'Невідомо'
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("49", "01")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap))
        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).styleSheet() == ColorResult.success.value,
                        timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).label_status.text() == 'white'

        # test check tamper alarms
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm_hub(
                "07", "05", (self.hub_id, self.hub_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 07" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_on.text() == f'ВКЛ: {i + 1}/2'

            socket.messages_heap.append(server.send_alarm_hub(
                "01", "04", (self.hub_id, self.hub_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 01" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_off.text() == f'ВИКЛ: {i + 1}/2'

        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_on.styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_off.styleSheet() == ColorResult.success.value

        # test active channels
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("48", "05")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 5).label_status.text() == "Eth/Gsm"
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        # test active zones alarms
        for num_zone in range(4):
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[f"2600000{num_zone + 1}"].text() == "False"
            assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[
                           f"2600000{num_zone + 1}"].styleSheet() == ColorResult.success.value

        for num in range(4):
            socket.messages_heap.append(server.wire_input_alarm(
                (f"2600000{num + 1}", '26', self.hub_id)
            ))
            qtbot.wait_until(lambda: "08 00" not in str(socket.messages_heap), timeout=3 * 1000)
            qtbot.wait_until(
                lambda: main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[f"2600000{num + 1}"].text() == "True",
                timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[f"2600000{num + 1}"].text() == "True"

        # test sim slots
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_sim_one.styleSheet() == ColorResult.success.value
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_second_sim.styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.update_sim_slot1_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_sim_one.styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.update_sim_slot2_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_second_sim.styleSheet() == ColorResult.success.value

        # test siren led
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 9).styleSheet() == ColorResult.success.value

        main_w.qc_main_widget.view_obj[2].cellWidget(0, 9).check_box_select.setChecked(True)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 9).styleSheet() == ColorResult.success.value

        # test RS485 led
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 10).styleSheet() == ColorResult.success.value

        main_w.qc_main_widget.view_obj[2].cellWidget(0, 10).check_box_select.setChecked(True)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 10).styleSheet() == ColorResult.success.value

        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 11).isEnabled()
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 12).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 12).text() == 'Повернути'

        # test success register device
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 12),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='07')
        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is None, timeout=5 * 1000)

        info_qc, info_repair, csa_info = self.database.get_stat_hub_after_fail_reg(qr=db_handler.hubs[self.hub_type][0])

        _, grade, _, info, defects, time_reg, success, operator, qr = info_qc
        _, from_stage, reason, income_time, initial_problem, what_done, comment, outcome_time, \
        status, dev_type_db, income_operator, outcome_operator, qr_repair, repair_type, *_ = info_repair
        _, hub_id, _, persisted_settings, firm_version, mandatory_update, fw_protection, fw_group, hub_type = csa_info

        # check info in central_qc
        assert grade is None
        assert __version__ in info
        assert defects == '""'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 0
        assert operator == 'armen'
        assert qr == db_handler.hubs[self.hub_type][0]

        # check info in csa.hubs
        assert hub_id == self.hub_id.upper()
        assert persisted_settings is None
        assert firm_version == 208001
        assert mandatory_update is None
        assert fw_protection is None
        assert fw_group == 1
        assert hub_type == int(db_handler.hubs[self.hub_type][0][-3])

        # check info in central_repair
        assert from_stage == "QC"
        assert reason == ";Світлодіод статусу хаба"
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         income_time.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert initial_problem is None
        assert what_done is None
        assert comment is None
        assert status == 'Відправлено у ремонт'
        assert dev_type_db == 33
        assert income_operator == 'armen'
        assert outcome_operator is None
        assert qr_repair == db_handler.hubs[self.hub_type][0]
        assert repair_type is None

    def test_sim_slot1_fail(self, qtbot, mock_socket_csa, mock_you_sure_central):
        main_w, server, socket = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_hub_last_steps(database=self.database, programming=True, assembling=True)

        self.wait_freq_apply(qtbot, main_w)

        self.scan_and_test_default_setup(main_w, view_obj_num=2)

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 2).push_btn_connect, QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='2b')

        qtbot.waitUntil(
            lambda: main_w.qc_main_widget.view_obj[2].cellWidget(0, 2).push_btn_connect.styleSheet() == ColorResult.success.value)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 2).push_btn_connect.text() == 'Додано'

        
        
        qtbot.waitUntil(lambda: "19 09" not in str(socket.messages_heap))
        socket.messages_heap.append(server.update_hub_subtype(
            (self.hub_id, self.hub_type_num), "03"
        ))

        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is not None, timeout=5 * 1000)

        # test check color
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).label_status.text() == 'Невідомо'
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("49", "01")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap))
        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).styleSheet() == ColorResult.success.value,
                        timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).label_status.text() == 'white'

        # test check tamper alarms
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm_hub(
                "07", "05", (self.hub_id, self.hub_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 07" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_on.text() == f'ВКЛ: {i + 1}/2'

            socket.messages_heap.append(server.send_alarm_hub(
                "01", "04", (self.hub_id, self.hub_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 01" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_off.text() == f'ВИКЛ: {i + 1}/2'

        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_on.styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_off.styleSheet() == ColorResult.success.value

        # test active channels
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("48", "05")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 5).label_status.text() == "Eth/Gsm"
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        # test active zones alarms
        for num_zone in range(4):
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[f"2600000{num_zone + 1}"].text() == "False"
            assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[
                           f"2600000{num_zone + 1}"].styleSheet() == ColorResult.success.value

        for num in range(4):
            socket.messages_heap.append(server.wire_input_alarm(
                (f"2600000{num + 1}", '26', self.hub_id)
            ))
            qtbot.wait_until(lambda: "08 00" not in str(socket.messages_heap), timeout=3 * 1000)
            qtbot.wait_until(
                lambda: main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[f"2600000{num + 1}"].text() == "True",
                timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[f"2600000{num + 1}"].text() == "True"

        # test sim slots
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_sim_one.styleSheet() == ColorResult.success.value
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_second_sim.styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.update_sim_slot2_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_second_sim.styleSheet() == ColorResult.success.value

        # test led arm
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).styleSheet() == ColorResult.success.value

        yavir = main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id)
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).push_btn_changed,
                         QtCore.Qt.LeftButton)
        socket.messages_heap.append(server.update_state_hub_arm(self.hub_id))
        assert socket.check_message_exists(message_type='04', message_key='00')
        qtbot.wait_until(lambda: yavir.get_parameters("08") == '01')

        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).push_btn_changed,
                         QtCore.Qt.LeftButton)
        socket.messages_heap.append(server.update_state_hub_disarm(self.hub_id))
        assert socket.check_message_exists(message_type='04', message_key='02')
        qtbot.wait_until(lambda: yavir.get_parameters("08") == '00')

        main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).check_box.setChecked(True)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).styleSheet() == ColorResult.success.value

        # test siren led
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 9).styleSheet() == ColorResult.success.value

        main_w.qc_main_widget.view_obj[2].cellWidget(0, 9).check_box_select.setChecked(True)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 9).styleSheet() == ColorResult.success.value

        # test RS485 led
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 10).styleSheet() == ColorResult.success.value

        main_w.qc_main_widget.view_obj[2].cellWidget(0, 10).check_box_select.setChecked(True)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 10).styleSheet() == ColorResult.success.value

        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 11).isEnabled()
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 12).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 12).text() == 'Повернути'

        # test success register device
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 12),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='07')
        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is None, timeout=5 * 1000)

        info_qc, info_repair, csa_info = self.database.get_stat_hub_after_fail_reg(qr=db_handler.hubs[self.hub_type][0])

        _, grade, _, info, defects, time_reg, success, operator, qr = info_qc
        _, from_stage, reason, income_time, initial_problem, what_done, comment, outcome_time, \
            status, dev_type_db, income_operator, outcome_operator, qr_repair, repair_type, *_ = info_repair
        _, hub_id, _, persisted_settings, firm_version, mandatory_update, fw_protection, fw_group, hub_type = csa_info

        # check info in central_qc
        assert grade is None
        assert __version__ in info
        assert defects == '""'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 0
        assert operator == 'armen'
        assert qr == db_handler.hubs[self.hub_type][0]

        # check info in csa.hubs
        assert hub_id == self.hub_id.upper()
        assert persisted_settings is None
        assert firm_version == 208001
        assert mandatory_update is None
        assert fw_protection is None
        assert fw_group == 1
        assert hub_type == int(db_handler.hubs[self.hub_type][0][-3])

        # check info in central_repair
        assert from_stage == "QC"
        assert reason == ";Сім холдер"
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         income_time.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert initial_problem is None
        assert what_done is None
        assert comment is None
        assert status == 'Відправлено у ремонт'
        assert dev_type_db == 33
        assert income_operator == 'armen'
        assert outcome_operator is None
        assert qr_repair == db_handler.hubs[self.hub_type][0]
        assert repair_type is None

    def test_sim_slot2_fail(self, qtbot, mock_socket_csa, mock_you_sure_central):
        main_w, server, socket = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_hub_last_steps(database=self.database, programming=True, assembling=True)

        self.wait_freq_apply(qtbot, main_w)

        self.scan_and_test_default_setup(main_w, view_obj_num=2)

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 2).push_btn_connect, QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='2b')

        qtbot.waitUntil(
            lambda: main_w.qc_main_widget.view_obj[2].cellWidget(0, 2).push_btn_connect.styleSheet() == ColorResult.success.value)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 2).push_btn_connect.text() == 'Додано'

        
        
        qtbot.waitUntil(lambda: "19 09" not in str(socket.messages_heap))
        socket.messages_heap.append(server.update_hub_subtype(
            (self.hub_id, self.hub_type_num), "03"
        ))

        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is not None, timeout=5 * 1000)

        # test check color
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).label_status.text() == 'Невідомо'
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("49", "01")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap))
        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).styleSheet() == ColorResult.success.value,
                        timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).label_status.text() == 'white'

        # test check tamper alarms
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm_hub(
                "07", "05", (self.hub_id, self.hub_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 07" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_on.text() == f'ВКЛ: {i + 1}/2'

            socket.messages_heap.append(server.send_alarm_hub(
                "01", "04", (self.hub_id, self.hub_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 01" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_off.text() == f'ВИКЛ: {i + 1}/2'

        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_on.styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_off.styleSheet() == ColorResult.success.value

        # test active channels
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("48", "05")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 5).label_status.text() == "Eth/Gsm"
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        # test active zones alarms
        for num_zone in range(4):
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[f"2600000{num_zone + 1}"].text() == "False"
            assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[
                           f"2600000{num_zone + 1}"].styleSheet() == ColorResult.success.value

        for num in range(4):
            socket.messages_heap.append(server.wire_input_alarm(
                (f"2600000{num + 1}", '26', self.hub_id)
            ))
            qtbot.wait_until(lambda: "08 00" not in str(socket.messages_heap), timeout=3 * 1000)
            qtbot.wait_until(
                lambda: main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[f"2600000{num + 1}"].text() == "True",
                timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[f"2600000{num + 1}"].text() == "True"

        # test sim slots
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_sim_one.styleSheet() == ColorResult.success.value
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_second_sim.styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.update_sim_slot1_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_sim_one.styleSheet() == ColorResult.success.value

        # test led arm
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).styleSheet() == ColorResult.success.value

        yavir = main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id)
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).push_btn_changed,
                         QtCore.Qt.LeftButton)
        socket.messages_heap.append(server.update_state_hub_arm(self.hub_id))
        assert socket.check_message_exists(message_type='04', message_key='00')
        qtbot.wait_until(lambda: yavir.get_parameters("08") == '01')

        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).push_btn_changed,
                         QtCore.Qt.LeftButton)
        socket.messages_heap.append(server.update_state_hub_disarm(self.hub_id))
        assert socket.check_message_exists(message_type='04', message_key='02')
        qtbot.wait_until(lambda: yavir.get_parameters("08") == '00')

        main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).check_box.setChecked(True)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).styleSheet() == ColorResult.success.value

        # test siren led
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 9).styleSheet() == ColorResult.success.value

        main_w.qc_main_widget.view_obj[2].cellWidget(0, 9).check_box_select.setChecked(True)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 9).styleSheet() == ColorResult.success.value

        # test RS485 led
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 10).styleSheet() == ColorResult.success.value

        main_w.qc_main_widget.view_obj[2].cellWidget(0, 10).check_box_select.setChecked(True)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 10).styleSheet() == ColorResult.success.value

        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 11).isEnabled()
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 12).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 12).text() == 'Повернути'

        # test success register device
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 12),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='07')
        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is None, timeout=5 * 1000)

        info_qc, info_repair, csa_info = self.database.get_stat_hub_after_fail_reg(qr=db_handler.hubs[self.hub_type][0])

        _, grade, _, info, defects, time_reg, success, operator, qr = info_qc
        _, from_stage, reason, income_time, initial_problem, what_done, comment, outcome_time, \
        status, dev_type_db, income_operator, outcome_operator, qr_repair, repair_type, *_ = info_repair
        _, hub_id, _, persisted_settings, firm_version, mandatory_update, fw_protection, fw_group, hub_type = csa_info

        # check info in central_qc
        assert grade is None
        assert __version__ in info
        assert defects == '""'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 0
        assert operator == 'armen'
        assert qr == db_handler.hubs[self.hub_type][0]

        # check info in csa.hubs
        assert hub_id == self.hub_id.upper()
        assert persisted_settings is None
        assert firm_version == 208001
        assert mandatory_update is None
        assert fw_protection is None
        assert fw_group == 1
        assert hub_type == int(db_handler.hubs[self.hub_type][0][-3])

        # check info in central_repair
        assert from_stage == "QC"
        assert reason == ";Сім холдер"
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         income_time.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert initial_problem is None
        assert what_done is None
        assert comment is None
        assert status == 'Відправлено у ремонт'
        assert dev_type_db == 33
        assert income_operator == 'armen'
        assert outcome_operator is None
        assert qr_repair == db_handler.hubs[self.hub_type][0]
        assert repair_type is None

    def test_channels_fail_only_eth(self, qtbot, mock_socket_csa, mock_you_sure_central):
        main_w, server, socket = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_hub_last_steps(database=self.database, programming=True, assembling=True)

        self.wait_freq_apply(qtbot, main_w)

        self.scan_and_test_default_setup(main_w, view_obj_num=2)

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 2).push_btn_connect, QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='2b')

        qtbot.waitUntil(
            lambda: main_w.qc_main_widget.view_obj[2].cellWidget(0, 2).push_btn_connect.styleSheet() == ColorResult.success.value)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 2).push_btn_connect.text() == 'Додано'

        
        
        qtbot.waitUntil(lambda: "19 09" not in str(socket.messages_heap))
        socket.messages_heap.append(server.update_hub_subtype(
            (self.hub_id, self.hub_type_num), "03"
        ))

        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is not None, timeout=5 * 1000)

        # test check color
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).label_status.text() == 'Невідомо'
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("49", "01")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap))
        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).styleSheet() == ColorResult.success.value,
                        timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).label_status.text() == 'white'

        # test check tamper alarms
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm_hub(
                "07", "05", (self.hub_id, self.hub_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 07" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_on.text() == f'ВКЛ: {i + 1}/2'

            socket.messages_heap.append(server.send_alarm_hub(
                "01", "04", (self.hub_id, self.hub_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 01" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_off.text() == f'ВИКЛ: {i + 1}/2'

        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_on.styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_off.styleSheet() == ColorResult.success.value

        # test active channels
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("48", "01")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 5).label_status.text() == "Eth"
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        # test active zones alarms
        for num_zone in range(4):
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[f"2600000{num_zone + 1}"].text() == "False"
            assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[
                           f"2600000{num_zone + 1}"].styleSheet() == ColorResult.success.value

        for num in range(4):
            socket.messages_heap.append(server.wire_input_alarm(
                (f"2600000{num + 1}", '26', self.hub_id)
            ))
            qtbot.wait_until(lambda: "08 00" not in str(socket.messages_heap), timeout=3 * 1000)
            qtbot.wait_until(
                lambda: main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[f"2600000{num + 1}"].text() == "True",
                timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[f"2600000{num + 1}"].text() == "True"

        # test sim slots
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_sim_one.styleSheet() == ColorResult.success.value
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_second_sim.styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.update_sim_slot1_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_sim_one.styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.update_sim_slot2_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_second_sim.styleSheet() == ColorResult.success.value

        # test led arm
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).styleSheet() == ColorResult.success.value

        yavir = main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id)
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).push_btn_changed,
                         QtCore.Qt.LeftButton)
        socket.messages_heap.append(server.update_state_hub_arm(self.hub_id))
        assert socket.check_message_exists(message_type='04', message_key='00')
        qtbot.wait_until(lambda: yavir.get_parameters("08") == '01')

        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).push_btn_changed,
                         QtCore.Qt.LeftButton)
        socket.messages_heap.append(server.update_state_hub_disarm(self.hub_id))
        assert socket.check_message_exists(message_type='04', message_key='02')
        qtbot.wait_until(lambda: yavir.get_parameters("08") == '00')

        main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).check_box.setChecked(True)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).styleSheet() == ColorResult.success.value

        # test siren led
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 9).styleSheet() == ColorResult.success.value

        main_w.qc_main_widget.view_obj[2].cellWidget(0, 9).check_box_select.setChecked(True)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 9).styleSheet() == ColorResult.success.value

        # test RS485 led
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 10).styleSheet() == ColorResult.success.value

        main_w.qc_main_widget.view_obj[2].cellWidget(0, 10).check_box_select.setChecked(True)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 10).styleSheet() == ColorResult.success.value

        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 11).isEnabled()
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 12).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 12).text() == 'Повернути'

        # test success register device
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 12),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='07')
        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is None, timeout=5 * 1000)

        info_qc, info_repair, csa_info = self.database.get_stat_hub_after_fail_reg(qr=db_handler.hubs[self.hub_type][0])

        _, grade, _, info, defects, time_reg, success, operator, qr = info_qc
        _, from_stage, reason, income_time, initial_problem, what_done, comment, outcome_time, \
            status, dev_type_db, income_operator, outcome_operator, qr_repair, repair_type, *_ = info_repair
        _, hub_id, _, persisted_settings, firm_version, mandatory_update, fw_protection, fw_group, hub_type = csa_info

        # check info in central_qc
        assert grade is None
        assert __version__ in info
        assert defects == '""'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 0
        assert operator == 'armen'
        assert qr == db_handler.hubs[self.hub_type][0]

        # check info in csa.hubs
        assert hub_id == self.hub_id.upper()
        assert persisted_settings is None
        assert firm_version == 208001
        assert mandatory_update is None
        assert fw_protection is None
        assert fw_group == 1
        assert hub_type == int(db_handler.hubs[self.hub_type][0][-3])

        # check info in central_repair
        assert from_stage == "QC"
        assert reason == ";Канали звязку"
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         income_time.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert initial_problem is None
        assert what_done is None
        assert comment is None
        assert status == 'Відправлено у ремонт'
        assert dev_type_db == 33
        assert income_operator == 'armen'
        assert outcome_operator is None
        assert qr_repair == db_handler.hubs[self.hub_type][0]
        assert repair_type is None

    def test_channels_fail_only_gsm(self, qtbot, mock_socket_csa, mock_you_sure_central):
        main_w, server, socket = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_hub_last_steps(database=self.database, programming=True, assembling=True)

        self.wait_freq_apply(qtbot, main_w)

        self.scan_and_test_default_setup(main_w, view_obj_num=2)

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 2).push_btn_connect, QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='2b')

        qtbot.waitUntil(
            lambda: main_w.qc_main_widget.view_obj[2].cellWidget(0, 2).push_btn_connect.styleSheet() == ColorResult.success.value)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 2).push_btn_connect.text() == 'Додано'

        
        
        qtbot.waitUntil(lambda: "19 09" not in str(socket.messages_heap))
        socket.messages_heap.append(server.update_hub_subtype(
            (self.hub_id, self.hub_type_num), "03"
        ))

        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is not None, timeout=5 * 1000)

        # test check color
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).label_status.text() == 'Невідомо'
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("49", "01")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap))
        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).styleSheet() == ColorResult.success.value,
                        timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).label_status.text() == 'white'

        # test check tamper alarms
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm_hub(
                "07", "05", (self.hub_id, self.hub_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 07" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_on.text() == f'ВКЛ: {i + 1}/2'

            socket.messages_heap.append(server.send_alarm_hub(
                "01", "04", (self.hub_id, self.hub_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 01" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_off.text() == f'ВИКЛ: {i + 1}/2'

        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_on.styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_off.styleSheet() == ColorResult.success.value

        # test active channels
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("48", "04")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 5).label_status.text() == "Gsm"
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        # test active zones alarms
        for num_zone in range(4):
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[f"2600000{num_zone + 1}"].text() == "False"
            assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[
                           f"2600000{num_zone + 1}"].styleSheet() == ColorResult.success.value

        for num in range(4):
            socket.messages_heap.append(server.wire_input_alarm(
                (f"2600000{num + 1}", '26', self.hub_id)
            ))
            qtbot.wait_until(lambda: "08 00" not in str(socket.messages_heap), timeout=3 * 1000)
            qtbot.wait_until(
                lambda: main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[f"2600000{num + 1}"].text() == "True",
                timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[f"2600000{num + 1}"].text() == "True"

        # test sim slots
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_sim_one.styleSheet() == ColorResult.success.value
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_second_sim.styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.update_sim_slot1_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_sim_one.styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.update_sim_slot2_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_second_sim.styleSheet() == ColorResult.success.value

        # test led arm
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).styleSheet() == ColorResult.success.value

        yavir = main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id)
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).push_btn_changed,
                         QtCore.Qt.LeftButton)
        socket.messages_heap.append(server.update_state_hub_arm(self.hub_id))
        assert socket.check_message_exists(message_type='04', message_key='00')
        qtbot.wait_until(lambda: yavir.get_parameters("08") == '01')

        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).push_btn_changed,
                         QtCore.Qt.LeftButton)
        socket.messages_heap.append(server.update_state_hub_disarm(self.hub_id))
        assert socket.check_message_exists(message_type='04', message_key='02')
        qtbot.wait_until(lambda: yavir.get_parameters("08") == '00')

        main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).check_box.setChecked(True)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).styleSheet() == ColorResult.success.value

        # test siren led
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 9).styleSheet() == ColorResult.success.value

        main_w.qc_main_widget.view_obj[2].cellWidget(0, 9).check_box_select.setChecked(True)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 9).styleSheet() == ColorResult.success.value

        # test RS485 led
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 10).styleSheet() == ColorResult.success.value

        main_w.qc_main_widget.view_obj[2].cellWidget(0, 10).check_box_select.setChecked(True)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 10).styleSheet() == ColorResult.success.value

        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 11).isEnabled()
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 12).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 12).text() == 'Повернути'

        # test success register device
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 12),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='07')
        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is None, timeout=5 * 1000)

        info_qc, info_repair, csa_info = self.database.get_stat_hub_after_fail_reg(qr=db_handler.hubs[self.hub_type][0])

        _, grade, _, info, defects, time_reg, success, operator, qr = info_qc
        _, from_stage, reason, income_time, initial_problem, what_done, comment, outcome_time, \
            status, dev_type_db, income_operator, outcome_operator, qr_repair, repair_type, *_ = info_repair
        _, hub_id, _, persisted_settings, firm_version, mandatory_update, fw_protection, fw_group, hub_type = csa_info

        # check info in central_qc
        assert grade is None
        assert __version__ in info
        assert defects == '""'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 0
        assert operator == 'armen'
        assert qr == db_handler.hubs[self.hub_type][0]

        # check info in csa.hubs
        assert hub_id == self.hub_id.upper()
        assert persisted_settings is None
        assert firm_version == 208001
        assert mandatory_update is None
        assert fw_protection is None
        assert fw_group == 1
        assert hub_type == int(db_handler.hubs[self.hub_type][0][-3])

        # check info in central_repair
        assert from_stage == "QC"
        assert reason == ";Канали звязку"
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         income_time.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert initial_problem is None
        assert what_done is None
        assert comment is None
        assert status == 'Відправлено у ремонт'
        assert dev_type_db == 33
        assert income_operator == 'armen'
        assert outcome_operator is None
        assert qr_repair == db_handler.hubs[self.hub_type][0]
        assert repair_type is None

    def test_siren_fail(self, qtbot, mock_socket_csa, mock_you_sure_central):
        main_w, server, socket = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_hub_last_steps(database=self.database, programming=True, assembling=True)

        self.wait_freq_apply(qtbot, main_w)

        self.scan_and_test_default_setup(main_w, view_obj_num=2)

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 2).push_btn_connect, QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='2b')

        qtbot.waitUntil(
            lambda: main_w.qc_main_widget.view_obj[2].cellWidget(0, 2).push_btn_connect.styleSheet() == ColorResult.success.value)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 2).push_btn_connect.text() == 'Додано'

        
        
        qtbot.waitUntil(lambda: "19 09" not in str(socket.messages_heap))
        socket.messages_heap.append(server.update_hub_subtype(
            (self.hub_id, self.hub_type_num), "03"
        ))

        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is not None, timeout=5 * 1000)

        # test check color
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).label_status.text() == 'Невідомо'
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("49", "01")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap))
        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).styleSheet() == ColorResult.success.value,
                        timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).label_status.text() == 'white'

        # test check tamper alarms
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm_hub(
                "07", "05", (self.hub_id, self.hub_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 07" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_on.text() == f'ВКЛ: {i + 1}/2'

            socket.messages_heap.append(server.send_alarm_hub(
                "01", "04", (self.hub_id, self.hub_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 01" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_off.text() == f'ВИКЛ: {i + 1}/2'

        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_on.styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_off.styleSheet() == ColorResult.success.value

        # test active channels
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("48", "05")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 5).label_status.text() == "Eth/Gsm"
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        # test active zones alarms
        for num_zone in range(4):
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[f"2600000{num_zone + 1}"].text() == "False"
            assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[
                           f"2600000{num_zone + 1}"].styleSheet() == ColorResult.success.value

        for num in range(4):
            socket.messages_heap.append(server.wire_input_alarm(
                (f"2600000{num + 1}", '26', self.hub_id)
            ))
            qtbot.wait_until(lambda: "08 00" not in str(socket.messages_heap), timeout=3 * 1000)
            qtbot.wait_until(
                lambda: main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[f"2600000{num + 1}"].text() == "True",
                timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[f"2600000{num + 1}"].text() == "True"

        # test sim slots
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_sim_one.styleSheet() == ColorResult.success.value
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_second_sim.styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.update_sim_slot1_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_sim_one.styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.update_sim_slot2_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_second_sim.styleSheet() == ColorResult.success.value

        # test led arm
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).styleSheet() == ColorResult.success.value

        yavir = main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id)
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).push_btn_changed,
                         QtCore.Qt.LeftButton)
        socket.messages_heap.append(server.update_state_hub_arm(self.hub_id))
        assert socket.check_message_exists(message_type='04', message_key='00')
        qtbot.wait_until(lambda: yavir.get_parameters("08") == '01')

        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).push_btn_changed,
                         QtCore.Qt.LeftButton)
        socket.messages_heap.append(server.update_state_hub_disarm(self.hub_id))
        assert socket.check_message_exists(message_type='04', message_key='02')
        qtbot.wait_until(lambda: yavir.get_parameters("08") == '00')

        main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).check_box.setChecked(True)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).styleSheet() == ColorResult.success.value

        # test RS485 led
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 10).styleSheet() == ColorResult.success.value

        main_w.qc_main_widget.view_obj[2].cellWidget(0, 10).check_box_select.setChecked(True)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 10).styleSheet() == ColorResult.success.value

        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 11).isEnabled()
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 12).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 12).text() == 'Повернути'

        # test success register device
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 12),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='07')
        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is None, timeout=5 * 1000)

        info_qc, info_repair, csa_info = self.database.get_stat_hub_after_fail_reg(qr=db_handler.hubs[self.hub_type][0])

        _, grade, _, info, defects, time_reg, success, operator, qr = info_qc
        _, from_stage, reason, income_time, initial_problem, what_done, comment, outcome_time, \
            status, dev_type_db, income_operator, outcome_operator, qr_repair, repair_type, *_ = info_repair
        _, hub_id, _, persisted_settings, firm_version, mandatory_update, fw_protection, fw_group, hub_type = csa_info

        # check info in central_qc
        assert grade is None
        assert __version__ in info
        assert defects == '""'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 0
        assert operator == 'armen'
        assert qr == db_handler.hubs[self.hub_type][0]

        # check info in csa.hubs
        assert hub_id == self.hub_id.upper()
        assert persisted_settings is None
        assert firm_version == 208001
        assert mandatory_update is None
        assert fw_protection is None
        assert fw_group == 1
        assert hub_type == int(db_handler.hubs[self.hub_type][0][-3])

        # check info in central_repair
        assert from_stage == "QC"
        assert reason == ";Світлодіод роботи сирени"
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         income_time.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert initial_problem is None
        assert what_done is None
        assert comment is None
        assert status == 'Відправлено у ремонт'
        assert dev_type_db == 33
        assert income_operator == 'armen'
        assert outcome_operator is None
        assert qr_repair == db_handler.hubs[self.hub_type][0]
        assert repair_type is None

    def test_rs485_fail(self, qtbot, mock_socket_csa, mock_you_sure_central):
        main_w, server, socket = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_hub_last_steps(database=self.database, programming=True, assembling=True)

        self.wait_freq_apply(qtbot, main_w)

        self.scan_and_test_default_setup(main_w, view_obj_num=2)

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 2).push_btn_connect, QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='2b')

        qtbot.waitUntil(
            lambda: main_w.qc_main_widget.view_obj[2].cellWidget(0, 2).push_btn_connect.styleSheet() == ColorResult.success.value)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 2).push_btn_connect.text() == 'Додано'

        
        
        qtbot.waitUntil(lambda: "19 09" not in str(socket.messages_heap))
        socket.messages_heap.append(server.update_hub_subtype(
            (self.hub_id, self.hub_type_num), "03"
        ))

        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is not None, timeout=5 * 1000)

        # test check color
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).label_status.text() == 'Невідомо'
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).styleSheet() == ColorResult.success.value
        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("49", "01")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap))
        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).styleSheet() == ColorResult.success.value,
                        timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 3).label_status.text() == 'white'

        # test check tamper alarms
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).styleSheet() == ColorResult.success.value

        for i in range(2):
            socket.messages_heap.append(server.send_alarm_hub(
                "07", "05", (self.hub_id, self.hub_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 07" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_on.text() == f'ВКЛ: {i + 1}/2'

            socket.messages_heap.append(server.send_alarm_hub(
                "01", "04", (self.hub_id, self.hub_type_num)
            ))
            # time.sleep(1)
            qtbot.waitUntil(lambda: "08 01" not in str(socket.messages_heap), timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_off.text() == f'ВИКЛ: {i + 1}/2'

        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_on.styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 4).label_off.styleSheet() == ColorResult.success.value

        # test active channels
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.send_update_hub(
            (self.hub_id, self.hub_type_num), ("48", "05")
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 5).label_status.text() == "Eth/Gsm"
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 5).styleSheet() == ColorResult.success.value

        # test active zones alarms
        for num_zone in range(4):
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[f"2600000{num_zone + 1}"].text() == "False"
            assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[
                           f"2600000{num_zone + 1}"].styleSheet() == ColorResult.success.value

        for num in range(4):
            socket.messages_heap.append(server.wire_input_alarm(
                (f"2600000{num + 1}", '26', self.hub_id)
            ))
            qtbot.wait_until(lambda: "08 00" not in str(socket.messages_heap), timeout=3 * 1000)
            qtbot.wait_until(
                lambda: main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[f"2600000{num + 1}"].text() == "True",
                timeout=5 * 1000)
            assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 6).relations_label_sensor_id[f"2600000{num + 1}"].text() == "True"

        # test sim slots
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_sim_one.styleSheet() == ColorResult.success.value
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_second_sim.styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.update_sim_slot1_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_sim_one.styleSheet() == ColorResult.success.value

        socket.messages_heap.append(server.update_sim_slot2_active_hub(
            (self.hub_id, self.hub_type_num)
        ))
        qtbot.waitUntil(lambda: "19 0b" not in str(socket.messages_heap), timeout=5 * 1000)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 7).label_second_sim.styleSheet() == ColorResult.success.value

        # test led arm
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).styleSheet() == ColorResult.success.value

        yavir = main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id)
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).push_btn_changed,
                         QtCore.Qt.LeftButton)
        socket.messages_heap.append(server.update_state_hub_arm(self.hub_id))
        assert socket.check_message_exists(message_type='04', message_key='00')
        qtbot.wait_until(lambda: yavir.get_parameters("08") == '01')

        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).push_btn_changed,
                         QtCore.Qt.LeftButton)
        socket.messages_heap.append(server.update_state_hub_disarm(self.hub_id))
        assert socket.check_message_exists(message_type='04', message_key='02')
        qtbot.wait_until(lambda: yavir.get_parameters("08") == '00')

        main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).check_box.setChecked(True)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 8).styleSheet() == ColorResult.success.value

        # test siren led
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 9).styleSheet() == ColorResult.success.value

        main_w.qc_main_widget.view_obj[2].cellWidget(0, 9).check_box_select.setChecked(True)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 9).styleSheet() == ColorResult.success.value

        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 11).isEnabled()
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 12).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 12).text() == 'Повернути'

        # test success register device
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 12),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='07')
        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is None, timeout=5 * 1000)

        info_qc, info_repair, csa_info = self.database.get_stat_hub_after_fail_reg(qr=db_handler.hubs[self.hub_type][0])

        _, grade, _, info, defects, time_reg, success, operator, qr = info_qc
        _, from_stage, reason, income_time, initial_problem, what_done, comment, outcome_time, \
            status, dev_type_db, income_operator, outcome_operator, qr_repair, repair_type, *_ = info_repair
        _, hub_id, _, persisted_settings, firm_version, mandatory_update, fw_protection, fw_group, hub_type = csa_info

        # check info in central_qc
        assert grade is None
        assert __version__ in info
        assert defects == '""'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 0
        assert operator == 'armen'
        assert qr == db_handler.hubs[self.hub_type][0]

        # check info in csa.hubs
        assert hub_id == self.hub_id.upper()
        assert persisted_settings is None
        assert firm_version == 208001
        assert mandatory_update is None
        assert fw_protection is None
        assert fw_group == 1
        assert hub_type == int(db_handler.hubs[self.hub_type][0][-3])

        # check info in central_repair
        assert from_stage == "QC"
        assert reason == ";RS485"
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         income_time.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert initial_problem is None
        assert what_done is None
        assert comment is None
        assert status == 'Відправлено у ремонт'
        assert dev_type_db == 33
        assert income_operator == 'armen'
        assert outcome_operator is None
        assert qr_repair == db_handler.hubs[self.hub_type][0]
        assert repair_type is None

    def test_all_func_fail(self, qtbot, mock_socket_csa, mock_you_sure_central):
        main_w, server, socket = mock_socket_csa
        qtbot.addWidget(main_w.qc_main_widget)

        self.add_hub_last_steps(database=self.database, programming=True, assembling=True)

        self.wait_freq_apply(qtbot, main_w)

        self.scan_and_test_default_setup(main_w, view_obj_num=2)

        # test click button start reg process
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 2).push_btn_connect, QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='2b')

        qtbot.waitUntil(lambda: main_w.qc_main_widget.view_obj[2].cellWidget(0,
                                                                             2).push_btn_connect.styleSheet() == ColorResult.success.value)
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 2).push_btn_connect.text() == 'Додано'

        qtbot.waitUntil(lambda: "19 09" not in str(socket.messages_heap))

        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is not None, timeout=5*1000)

        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 11).isEnabled()
        assert not main_w.qc_main_widget.view_obj[2].cellWidget(0, 12).styleSheet() == ColorResult.success.value
        assert main_w.qc_main_widget.view_obj[2].cellWidget(0, 12).text() == 'Повернути'

        # test register device
        qtbot.mouseClick(main_w.qc_main_widget.view_obj[2].cellWidget(0, 12),
                         QtCore.Qt.LeftButton)
        assert socket.check_message_exists(message_type='11', message_key='07')
        qtbot.waitUntil(lambda: main_w.qc_main_widget.csa_client.get_hub_obj(self.hub_id) is None, timeout=5 * 1000)

        info_qc, info_repair, csa_info = self.database.get_stat_hub_after_fail_reg(qr=db_handler.hubs[self.hub_type][0])

        _, grade, _, info, defects, time_reg, success, operator, qr = info_qc
        _, from_stage, reason, income_time, initial_problem, what_done, comment, outcome_time, \
            status, dev_type_db, income_operator, outcome_operator, qr_repair, repair_type, *_ = info_repair
        _, hub_id, _, persisted_settings, firm_version, mandatory_update, fw_protection, fw_group, hub_type = csa_info

        # check info in central_qc
        assert grade is None
        assert __version__ in info
        assert defects == '""'
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         time_reg.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert success == 0
        assert operator == 'armen'
        assert qr == db_handler.hubs[self.hub_type][0]

        # check info in csa.hubs
        assert hub_id == self.hub_id.upper()
        assert persisted_settings is None
        assert firm_version == 208001
        assert mandatory_update is None
        assert fw_protection is None
        assert fw_group == 1
        assert hub_type == int(db_handler.hubs[self.hub_type][0][-3])

        # check info in central_repair
        assert from_stage == "QC"
        assert "RS485" in reason
        assert "Канали звязку" in reason
        assert "Світлодіод статусу хаба" in reason
        assert "Світлодіод роботи сирени" in reason
        assert "Активні зони" in reason
        assert "Колір" in reason
        assert "Тампер" in reason
        assert "Сім холдер" in reason
        assert re.search(r'\d{4}[-]\d{2}[-]\d{2}\s\d{2}[:]\d{2}[:]\d{2}.\d{6}',
                         income_time.strftime("%Y-%m-%d %H:%M:%S.%f")).group(0)
        assert initial_problem is None
        assert what_done is None
        assert comment is None
        assert status == 'Відправлено у ремонт'
        assert dev_type_db == 33
        assert income_operator == 'armen'
        assert outcome_operator is None
        assert qr_repair == db_handler.hubs[self.hub_type][0]
        assert repair_type is None

