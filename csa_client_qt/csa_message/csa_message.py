from csa_client_qt.csa_message.utils import *
from binascii import hexlify, unhexlify


def create_csa_message(bytes_data: bytearray):
    bytes_data = remove_service_bytes(bytes_data)
    result, data_bytes = check_crc(bytes_data)
    if result:
        return CsaMessage()
    dencrypt_message_data = decrypt_message(bytes(bytes_data))
    m = CsaMessage()
    m.init_with_data(bytes(dencrypt_message_data))
    return m


class CsaMessage:
    def __init__(self,
                 sender='',
                 receiver='',
                 message_number='',
                 message_flag='',
                 message_type='',
                 message_key='',
                 payload=list()):

        self._sender = sender
        self._receiver = receiver
        self._message_number = message_number
        self._link = None
        self._message_flag = message_flag
        self._message_type = message_type
        self._message_key = message_key
        if not payload:
            self.payload = []

    @property
    def receiver(self):
        return self._receiver

    @property
    def sender(self):
        return self._sender

    @property
    def link(self):
        return self._link

    @property
    def message_type(self):
        return self._message_type

    @property
    def message_key(self):
        return self._message_key

    def set_message_number(self, message_number: int):
        self._message_number = "{:06x}".format(message_number)

    def set_message_link(self, link: int):
        self._link = "{:02x}".format(link)

    def init_with_data(self, income_data: bytes):
        self._sender = bytes.hex(income_data[:4])
        self._receiver = bytes.hex(income_data[4:8])
        self._message_number = bytes.hex(income_data[8:11])
        self._link = "{:02x}".format(int(income_data[11]))
        self._message_flag = "{:02x}".format(int(income_data[12]))
        self._message_type = "{:02x}".format(int(income_data[13]))
        payload_list = split_payload(bytearray(income_data[14:]))
        p = prepare_message(payload_list)
        self._message_key = p[0]
        self.payload = p[1:]

    def init_manual(self, sender=None, receiver=None, message_type=None, message_key=None, payload=[]):
        self._sender = sender
        self._receiver = receiver
        self._message_type = message_type
        self._message_key = message_key
        self.payload = payload

    def get_payload(self):
        return self.payload

    @property
    def message_flag(self):
        return int(self._message_flag, 16)

    def __bool__(self):
        if not self.message_type and not self.message_key:
            return False
        return True

    def __repr__(self):
        return "{_sender} {_receiver} {_message_number} {_link} {_message_flag} {_message_type} {_message_key} {payload}".format(**self.__dict__)

    def __bytes__(self):
        return self.convert_csa_message2bytes()

    def convert_csa_message2bytes(self) -> bytes:
        message_head = "{_sender}{_receiver}{_message_number}{_link}{_message_flag}{_message_type}".format(**self.__dict__)
        message_head = bytearray(bytes.fromhex(message_head))
        message_body = self._prepare_payload()
        message = message_head + message_body
        while len(message) % 16 != 0:
            message.append(0xaa)
        return bytes(message)

    def _prepare_payload(self) -> bytearray:
        _payload = bytearray()
        if self.message_key:
            self.payload.insert(0, self.message_key)

        for args in self.payload:
            _payload.append(0x05)
            for byte_p_chunk in bytes.fromhex(args):
                if byte_p_chunk == 0x05:
                    _payload.append(0x06)
                    _payload.append(0x35)
                elif byte_p_chunk == 0x06:
                    _payload.append(0x06)
                    _payload.append(0x36)
                else:
                    _payload.append(byte_p_chunk)
        _payload.append(0x05)
        return _payload
