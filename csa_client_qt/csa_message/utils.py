from Crypto.Cipher import AES

from csa_client_qt.security import *


def remove_service_bytes(data_bytes: bytearray):
    """ if byte 0x04 sub 0x30"""
    out_mesage_data = bytearray()
    while data_bytes:
        b = data_bytes.pop(0)
        if b == 0x04:
            out_mesage_data.append(data_bytes.pop(0) - 0x30)
        else:
            out_mesage_data.append(b)
    return out_mesage_data


def split_payload(payload: bytearray) -> list:
    payload_list = list()
    chunk_payload = bytearray()
    for b in payload:
        if b == 0x05:
            if chunk_payload:
                payload_list.append(chunk_payload[:])
            chunk_payload.clear()
        else:
            chunk_payload.append(b)
    return payload_list


def prepare_message(payload_message: list):
    """ Sub service bytes """
    payload = list()
    for chunk_payload in payload_message:
        temp_ = bytearray()
        index = 0
        while index < len(chunk_payload):
            b1 = chunk_payload[index]
            if b1 == 0x06:
                b2 = chunk_payload[index+1]
                if b2 == 0x35 or b2 == 0x36:
                    index += 2
                    temp_.append(b2-0x30)
                    continue
            index += 1
            temp_.append(b1)
        payload.append(bytearray.hex(temp_))
    return payload


def insert_service_byte(data: bytearray) -> bytes:
    out_data = bytearray()
    out_data.append(0x02)
    for byte_data in data:
        if byte_data == 0x02 or byte_data == 0x03 or byte_data == 0x04:
            out_data.append(0x04)
            out_data.append(byte_data + 0x30)
        else:
            out_data.append(byte_data)
    out_data.append(0x03)
    return bytes(out_data)


def check_crc(data_bytes: bytearray):
    """return bool"""
    b1 = data_bytes.pop()
    b2 = data_bytes.pop()
    crc_wait = b1 | (b2 << 8)
    crc = CRC_START
    for bt in data_bytes:
        crc1 = (crc << 8) & 0xFFFF
        index = ((crc >> 8) ^ bt) & 0xFF
        crc = (crc1 ^ CRC16Table[index]) & 0xFFFF
    return not crc == crc_wait, data_bytes


def add_crc(data_bytes: bytearray):
    crc = CRC_START
    for bt in data_bytes:
        crc1 = (crc << 8) & 0xFFFF
        index = ((crc >> 8) ^ bt) & 0xFF
        crc = (crc1 ^ CRC16Table[index]) & 0xFFFF
    data_bytes.append((crc >> 8) & 0xFF)
    data_bytes.append(crc & 0xFF)


def encrypt_message(data: bytes) -> bytes:
    cryptor = AES.new(AES_KEY, AES.MODE_CBC, AES_IV)
    return cryptor.encrypt(data)


def decrypt_message(data: bytes) -> bytes:
    cryptor = AES.new(AES_KEY, AES.MODE_CBC, AES_IV)
    return cryptor.decrypt(data)
