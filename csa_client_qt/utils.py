
def auth_compute(data):
    """ Compute auth response """
    byte1 = int(data[:2], 16)
    byte2 = int(data[2:4], 16)
    answer1 = byte1 ^ byte2
    answer2 = ((byte1 & byte2) << 2) & 0xff
    return "{:02x}{:02x}".format(answer1, answer2)
