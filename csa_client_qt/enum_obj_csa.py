from enum import Enum, unique


@unique
class HubSubType(Enum):
    Unknow = '00'
    Hub = '01'
    HubPlus = '02'
    HubYavir = '03'
    HubYavirPlus = '04'
    Hub2 = '05'
    Hub2Plus = '06'


@unique
class ObjectsCsa(Enum):
    Hub = '21'
    User = '22'
    Group = '23'
    Room = '24'
    Camera = '25'

    DoorProtect = '01'
    MotionProtect = '02'
    FireProtect = '03'
    GlassProtect = '04'
    LeaksProtect = '05'
    MotionProtectCurtain = '06'
    RangeExtender = '07'
    CombiProtect = '08'
    FireProtectPlus = '09'
    Keypad = '0a'
    SpaceControl = '0b'
    Button = '0c'
    MotionCam = '0d'
    MotionProtectPlus = '0e'
    DoorProtectPlus = '0f'
    UniversalDevice = '10'
    Transmitter = '11'
    Socket = '1e'
    WallSwitch = '1f'
    Relay = '12'
    MotionProtectOutdoor = '13'
    StreetSiren = '14'
    HomeSiren = '15'
    StoveguardSens = '16'
    StoveguardSwitch = '17'

    WireInput = '26'
    WireSiren = '27'
    YavirAccessControl = '28'
