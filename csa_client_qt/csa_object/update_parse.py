
def update_parse(data: tuple, delimiter='fefe'):
    temp_list = list()
    for d in data:
        if d == delimiter:
            yield temp_list
            temp_list.clear()
        else:
            temp_list.append(d)
    if temp_list:
        yield temp_list
