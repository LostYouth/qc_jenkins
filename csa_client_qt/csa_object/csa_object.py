import binascii
import logging
from collections import deque

from PyQt5.QtCore import pyqtSignal, QObject

from app_setting import AppSetting
from csa_client_qt.enum_params import SERVER_ID, Wifi, PartialRegistration, WriteParameter, Security, DevCommand, \
    SecuredActions, Logs, UserRegistration, NetWorkSettings, Answer, HubConsole
from csa_client_qt.enum_obj_csa import ObjectsCsa, HubSubType
from csa_client_qt.csa_message import CsaMessage
from csa_client_qt.qt_socket_csa import SocketCsa
from csa_client_qt.enum_params import Updates, MessageType, Alarms, DeviceRegister
from .update_parse import update_parse
from csa_client_qt.enum_obj_csa import ObjectsCsa

logger = logging.getLogger(AppSetting.LOGGER_NAME)


class CsaObject(QObject):
    signal_update_data = pyqtSignal(str)
    signal_alarm_object = pyqtSignal(str, str)

    def __init__(self, socket: SocketCsa, object_type, object_id):
        super(CsaObject, self).__init__()
        self._socket = socket

        self._object_type = object_type
        self._object_id = object_id

        self._parameters_object = dict()

        self._alarms = deque(maxlen=25)

    @property
    def type(self):
        return self._object_type

    @property
    def id(self):
        return self._object_id

    def set_parameters(self, *args):
        logger.debug("Set param {}".format(repr(args)))
        for i in range(0, len(args), 2):
            num_param, val_param = args[i], args[i+1]
            self._parameters_object[num_param] = val_param
        self.signal_update_data.emit(self.id)

    def get_parameters(self, num_param: str) -> str:
        return self._parameters_object.get(num_param, '')

    def append_alarm(self, alarm: str):
        self._alarms.append(alarm)
        self.signal_alarm_object.emit(self.id, alarm)

    def __repr__(self):
        return "ObjectCSA TYPE: {} ID:{}".format(self.type, self.id)

    def __str__(self):
        obj = ObjectsCsa.Hub
        for _obj in ObjectsCsa:
            if _obj.value == self.type:
                obj = _obj
                break
        return "{} {}".format(obj.name, self.id)


class CsaObjectHub(CsaObject):
    signal_append_object = pyqtSignal(object)
    signal_remove_object = pyqtSignal(str)

    def __init__(self, socket: SocketCsa, object_type, object_id):
        super(CsaObjectHub, self).__init__(socket, object_type, object_id)

        self._role_user = ''

        self._csa_object = []

        self.settings_id = ''

        self.__settings_message_heap = []
        self.__status_message_heap = []

        self.request_full_setting()
        self.request_full_status()

        self._work_hub_for_qc = False

    @property
    def work_hub_qc(self):
        return self._work_hub_for_qc

    @work_hub_qc.setter
    def work_hub_qc(self, val: bool):
        self._work_hub_for_qc = val

    def force_check_settings(self):
        m = CsaMessage()
        m.init_manual(
            sender=self._socket.client_id,
            receiver=self.id,
            message_type=MessageType.Updates.value,
            message_key=Updates.ForceCheck.value,
            payload=[]
        )
        self._socket._write_message(m, timeout_ack=2)

    def request_full_setting(self):
        m = CsaMessage()
        m.init_manual(
            sender=self._socket.client_id,
            receiver=self.id,
            message_type=MessageType.Updates.value,
            message_key=Updates.RequestFullsettings.value,
            payload=[]
        )
        self._socket._write_message(m, timeout_ack=2)

    def request_full_status(self):
        m = CsaMessage()
        m.init_manual(
            sender=self._socket.client_id,
            receiver=self.id,
            message_type=MessageType.Updates.value,
            message_key=Updates.RequestFullStatus.value,
            payload=[]
        )
        self._socket._write_message(m, timeout_ack=2)

    @property
    def role_user(self):
        return self._role_user

    @role_user.setter
    def role_user(self, role: str):
        self._role_user = role

    @property
    def settings_id(self):
        return self._settings_id

    @settings_id.setter
    def settings_id(self, sett):
        self._settings_id = sett
        logger.debug("Update settingsId hub({}) SettingsID: {}".format(self.id, self._settings_id))

    @property
    def sub_type_hub(self):
        hub_sub_type = self.get_parameters('75')
        try:
            return HubSubType(hub_sub_type)
        except ValueError:
            return HubSubType.Unknow

    def append_object_csa(self, obj_type, object_id) -> CsaObject:
        if self.get_object(obj_type, object_id) is not None:
            return self.get_object(obj_type, object_id)

        csa_obj = CsaObject(self._socket, obj_type, object_id)
        self._csa_object.append(csa_obj)
        self.signal_append_object.emit(csa_obj)
        logger.debug("Create ObjCsa hub({}) type: {} id: {}".format(self.id, obj_type, object_id))
        return csa_obj

    def get_object(self, object_type, object_id) -> CsaObject:
        for obj in self._csa_object:
            if obj.type == object_type and obj.id == object_id:
                logger.debug("Get ObjCsa hub({}) type: {} id: {}".format(self.id, object_type, object_id))
                return obj
        # logger.warning("GetObj Not found! type:{} id:{} all_obj: {}".format(object_type, object_id, self._csa_object))

    def get_objects(self, object_type: ObjectsCsa):
        return list(filter(lambda obj: obj.type == object_type.value, self._csa_object))

    def remove_csa_obj(self, obj_type, obj_id):
        for obj in self._csa_object:
            if obj.type == obj_type and obj.id == obj_id:
                logger.debug("Remove ObjCsa hub({}) type: {} id: {}".format(self.id, obj_type, obj_id))
                self._csa_object.remove(obj)
                self.signal_remove_object.emit(obj_id)
                return
        logger.warning("RemoveCsaObj Not found! type:{} id:{} all_obj: {}".format(obj_type, obj_id, self._csa_object))

    def _delete_device_not_exists_settings(self, list_obj: list):
        exists_obj = set((obj.type, obj.id) for obj in self._csa_object)
        logger.debug("Sync obj exists in side client and objects in settings update")
        for obj_type, obj_id in exists_obj:
            if not (obj_type, obj_id) in list_obj:
                self.remove_csa_obj(obj_type, obj_id)

    def _update_object_parameters(self, obj_type: str, obj_id: str, parameters: tuple, create_obj: bool = False):
        if obj_type == self.type and self.id == obj_id:
            self.set_parameters(*parameters)
        else:
            obj = self.get_object(obj_type, obj_id)
            if not obj and not create_obj:
                logger.warning("Object not found while parse {} {}".format(obj_type, obj_id))
                logger.warning("All object {}".format(self._csa_object))
                return
            elif not obj:
                obj = self.append_object_csa(obj_type, obj_id)
            obj.set_parameters(*parameters)

    def update_handler(self, message: CsaMessage):
        if message.message_key == Updates.CurrentSettingsId.value:
            self._settings_id, *_ = message.payload
        elif message.message_key == Updates.SettingsIdUpdate.value:
            self.settings_id, *_ = message.payload
        elif message.message_key == Updates.ObjectRemove.value:
            settings_id, *payload = message.payload
            self.settings_id = settings_id
            for obj_type, obj_id in update_parse(payload):
                self.remove_csa_obj(obj_type, obj_id)
        elif message.message_key == Updates.SettingsBody.value:
            settings_id, pack_num, is_final_pack, *payload = message.payload
            self.settings_id = settings_id
            self.__settings_message_heap.append(payload)
            if is_final_pack != '01':
                return
            if int(pack_num, 16) != len(self.__settings_message_heap):
                self.__settings_message_heap.clear()
                self.request_full_setting()
                self.request_full_setting()
                return
            list_check_exists_obj = []
            while self.__settings_message_heap:
                data = self.__settings_message_heap.pop(0)
                for obj_type, obj_id, *parameter in update_parse(data):
                    self._update_object_parameters(obj_type, obj_id, parameter, create_obj=True)
                    list_check_exists_obj.append((obj_type, obj_id))

            self._delete_device_not_exists_settings(list_check_exists_obj)

        elif message.message_key == Updates.ObjectAdded.value:
            settings_id, *payload = message.payload
            for obj_type, obj_id, *data in update_parse(payload):
                obj = self.append_object_csa(obj_type, obj_id)
                obj.set_parameters(*data)
        elif message.message_key == Updates.NoData:
            # Force check settings command for server check settings id in side Hub!
            self.force_check_settings()
        elif message.message_key == Updates.StatusBody.value:
            settings_id, pack_number, is_final_pack, *payload = message.payload
            self.settings_id = settings_id
            self.__status_message_heap.append(payload)
            if is_final_pack != '01':
                logger.debug("Pack isn't final {} {}".format(message.message_type, message.message_key))
                return

            if int(pack_number, 16) != len(self.__status_message_heap):
                self.__status_message_heap.clear()
                self.request_full_setting()
                self.request_full_status()
                return

            while self.__status_message_heap:
                data = self.__status_message_heap.pop(0)
                for obj_type, obj_id, *parameter in update_parse(data):
                    self._update_object_parameters(obj_type, obj_id, parameter)
        elif message.message_key == Updates.UpdateStatus.value:
            for obj_type, obj_id, *parameter in update_parse(message.payload):
                self._update_object_parameters(obj_type, obj_id, parameter)
        elif message.message_key == Updates.UpdateSettings.value:
            settings_id, *payload = message.payload
            for obj_type, obj_id, *data in update_parse(payload):
                self._update_object_parameters(obj_type, obj_id, data)
        else:
            logger.warning("Message ignore not parse")
            logger.warning("MESS: {}".format(message))

    def alarm_handler(self, message: CsaMessage):
        message.payload.insert(0, message.message_key)
        for alarm_type, obj_type, obj_id, alarm_number, *_ in update_parse(message.payload):
            if obj_type == '21' and obj_id == self.id:
                self.append_alarm(alarm_number)
            else:
                obj = self.get_object(obj_type, obj_id)
                if obj:
                    obj.append_alarm(alarm_number)

    def add_room_hub(self, room_name):
        room_name = binascii.hexlify(room_name.encode()).decode()
        m = CsaMessage()
        m.init_manual(
            sender=self._socket.client_id,
            receiver=self.id,
            message_type=MessageType.PartialRegistration.value,
            message_key=PartialRegistration.RegisterRoom.value,
            payload=[room_name]
        )
        answer = self._socket.send_message(m, timeout_wait_answer=4)
        if answer.message_type == MessageType.PartialRegistration.value and PartialRegistration.ExistsRoomWithItParams.value:
            type_obj, object_id, *params, _ = answer.payload
            room_obj = self.append_object_csa(type_obj, object_id)
            room_obj.set_parameters(*params)
        return answer

    def remove_room_hub(self, room_id):
        m = CsaMessage()
        m.init_manual(
            sender=self._socket.client_id,
            receiver=self.id,
            message_type=MessageType.PartialRegistration.value,
            message_key=PartialRegistration.DeleteRoom.value,
            payload=["{:0>8}".format(room_id)]
        )
        return self._socket.send_message(m, timeout_wait_answer=30)

    def scan_wifi_network(self):
        m = CsaMessage()
        m.init_manual(
            sender=self._socket.client_id,
            receiver=self.id,
            message_type=MessageType.Wifi.value,
            message_key=Wifi.ScanNetworks.value,
            payload=[]
        )
        return self._socket.send_message(message=m, timeout_wait_answer=180)

    def delete_device(self, device_id):
        m = CsaMessage()
        m.init_manual(
            sender=self._socket.client_id,
            receiver=self.id,
            message_type=MessageType.DeviceRegister.value,
            message_key=DeviceRegister.DeleteDevice.value,
            payload=["{:0>8}".format(device_id)]
        )
        return self._socket.send_message(m, timeout_wait_answer=10)

    def start_search_device1(self, dev_id, dev_type, room_id, device_name, device_color):
        dev_name = binascii.hexlify(device_name.encode()).decode()
        m = CsaMessage()
        m.init_manual(
            sender=self._socket.client_id,
            receiver=self.id,
            message_type=MessageType.DeviceRegister.value,
            message_key=DeviceRegister.RequestRegisterDevice.value,
            payload=[
                "{:0>8}".format(dev_id),
                "{:02x}".format(int(room_id, 16)),
                dev_name,
                "{:0>2}".format(device_color),
                dev_type,
                '00'
            ]
        )
        return self._socket.send_message(m, timeout_wait_answer=5)

    def cancel_registration_device(self):
        m = CsaMessage()
        m.init_manual(
            sender=self._socket.client_id,
            receiver=self.id,
            message_type=MessageType.DeviceRegister.value,
            message_key=DeviceRegister.CancelRegistrationDev.value,
            payload=[]
        )
        return self._socket.send_message(m, timeout_wait_answer=5)

    def start_search_device2(self, dev_id, dev_type, room_id, group_id, device_name, device_color):
        dev_name = binascii.hexlify(device_name.encode()).decode()
        m = CsaMessage()
        m.init_manual(
            sender=self._socket.client_id,
            receiver=self.id,
            message_type=MessageType.DeviceRegister.value,
            message_key=DeviceRegister.RequestRegistrationDevice.value,
            payload=[dev_id, '01', room_id, '02', group_id, '03', dev_name, '04', device_color, '05', dev_type]
        )
        return self._socket.send_message(message=m, timeout_wait_answer=5)

    def write_param(self, obj_type, obj_id, payload: list):
        m = CsaMessage()
        m.init_manual(
            sender=self._socket.client_id,
            receiver=self.id,
            message_type=MessageType.WriteParameter.value,
            payload=[obj_type, "{:0>8}".format(obj_id), *payload]
        )
        return self._socket.send_message(m, timeout_wait_answer=60)

    def arm(self, ignore_alarms=True):
        m = CsaMessage()
        m.init_manual(
            sender=self._socket.client_id,
            receiver=self.id,
            message_type=MessageType.Security.value,
            message_key=Security.Arm.value,
            payload=["{:02x}".format(ignore_alarms)]
        )
        return self._socket.send_message(m, timeout_wait_answer=10)

    def disarm(self):
        m = CsaMessage()
        m.init_manual(
            sender=self._socket.client_id,
            receiver=self.id,
            message_type=MessageType.Security.value,
            message_key=Security.Disarm.value,
            payload=['{:08x}'.format(1)]
        )
        return self._socket.send_message(m, timeout_wait_answer=10)

    def send_device_command(self, dev_type, dev_id, command_number, adding_params=None):
        payload = [dev_type, "{:0>8}".format(dev_id), command_number]
        if adding_params is not None:
            payload += adding_params

        m = CsaMessage()
        m.init_manual(
            sender=self._socket.client_id,
            receiver=self.id,
            message_type=MessageType.DevCommands.value,
            message_key=DevCommand.DeviceCommand.value,
            payload=payload,
        )
        return self._socket.send_message(m, timeout_wait_answer=3)

    def add_group(self, group_name):
        m = CsaMessage()
        m.init_manual(
            sender=self._socket.client_id,
            receiver=self.id,
            message_type=MessageType.PartialRegistration.value,
            message_key=PartialRegistration.RegisterGroup.value,
            payload=[binascii.hexlify(group_name.encode()).decode()]
        )
        return self._socket.send_message(m, timeout_wait_answer=4)

    def change_frequency(self, freq):
        pass

    def change_frequency2(self, master_key: str,  freq1: int, freq2: int):
        m = CsaMessage()
        m.init_manual(
            sender=self._socket.client_id,
            receiver=self.id,
            message_type=MessageType.SecuredActions.value,
            message_key=SecuredActions.SetFrequencyHub2.value,
            payload=[master_key, "{:08x}".format(freq1), "{:08x}".format(freq2)]
        )
        return self._socket.send_message(m, timeout_wait_answer=10)

    def factory_reset(self, master_key):
        m = CsaMessage()
        m.init_manual(
            sender=self._socket.client_id,
            receiver=self.id,
            message_type=MessageType.SecuredActions.value,
            message_key=SecuredActions.DropSettings.value,
            payload=[master_key]
        )
        return self._socket.send_message(m, timeout_wait_answer=10)

    def drop_logs(self):
        m = CsaMessage()
        m.init_manual(
            sender=self._socket.client_id,
            receiver=self.id,
            message_type=MessageType.Logs.value,
            message_key=Logs.DropLogs.value,
            payload=[]
        )
        return self._socket.send_message(m, timeout_wait_answer=22)

    def share_hub(self, emails: list, is_pro_user: bool):
        div = '23'
        email = "01" + div + div.join([binascii.hexlify(email.encode()).decode() for email in emails])
        ajax = binascii.hexlify("Ajax".encode()).decode()
        m = CsaMessage()
        m.init_manual(
            sender=self._socket.client_id,
            receiver=self.id,
            message_type=MessageType.UserRegistration.value,
            message_key=UserRegistration.ShareHubNew.value,
            payload=['01', ajax, '02', email, '03', "{:02x}".format(is_pro_user)]
        )
        return self._socket.send_message(message=m, timeout_wait_answer=5)

    def set_gsm_settings(self, gsm_apn, gsm_username, apn_password, sim_slot=None):
        gsm_apn_converted = binascii.hexlify(gsm_apn.encode()).decode()
        gsm_username_converted = binascii.hexlify(gsm_username.encode()).decode()
        gsm_password_converted = binascii.hexlify(apn_password.encode()).decode()
        p = [gsm_apn_converted, gsm_username_converted, gsm_password_converted]
        if sim_slot:
            p.append(sim_slot)
        m = CsaMessage()
        m.init_manual(
            sender=self._socket.client_id,
            receiver=self.id,
            message_type=MessageType.NetWorkSettings.value,
            message_key=NetWorkSettings.GsmSettings.value,
            payload=p
        )
        answer = self._socket.send_message(m, timeout_wait_answer=220)
        if answer.message_type == MessageType.Answer.value and answer.message_key == Answer.Delivered.value:
            return self._socket.create_task_wait_messa('', answer.link, timeout_wait_message=200)
        return answer

    def send_command2console(self, command: str):
        command = binascii.hexlify(command.encode()).decode()

        m = CsaMessage()
        m.init_manual(
            sender=self._socket.client_id,
            receiver=self.id,
            message_type=MessageType.HubConsole.value,
            message_key=HubConsole.ToHub.value,
            payload=[command]
        )

        return self._socket.send_message(m, timeout_wait_answer=25)
