from csa_client_qt.csa_message import CsaMessage
from PyQt5.QtCore import QObject, pyqtSignal


class TaskWaitMessage(QObject):

    signal_close_task = pyqtSignal()

    def __init__(self, message_number, link):
        super(TaskWaitMessage, self).__init__()
        self._message_number = message_number
        self._link = link

        self._result = None

    def get_link_wait(self):
        return self._link

    def done(self):
        if self._result:
            return True
        return False

    def get_result(self):
        return self._result

    def set_result(self, result: CsaMessage):
        if self._result is None:
            self._result = result
        self.signal_close_task.emit()

    def check_message(self, message: CsaMessage):
        pass


class TaskWaitMessageACK(TaskWaitMessage):
    def check_message(self, message: CsaMessage):
        if self._link == message.link and self._message_number == message.payload[0]:
            return True
        return False


class TaskWaitMessageExpected(TaskWaitMessage):
    def check_message(self, message: CsaMessage):
        if self._link == message.link:
            return True
        return False
