import binascii
import logging

from PyQt5 import QtCore

from app_setting import AppSetting
from csa_client_qt.enum_params import MessageType, UserRegistration
from csa_client_qt.qt_socket_csa import SocketCsa
from csa_client_qt.csa_message.csa_message import CsaMessage
from csa_client_qt.enum_params import SERVER_ID, RELEASE
from csa_client_qt.utils import auth_compute
from csa_client_qt.csa_object.csa_object import CsaObjectHub

logger = logging.getLogger(AppSetting.LOGGER_NAME)


def generate_hosts(hosts):
    while True:
        for host in hosts:
            h, p = host
            yield h, p


class CsaClient(QtCore.QObject):
    signal_status_client = QtCore.pyqtSignal(bool)
    signal_error_connect_socket = QtCore.pyqtSignal()
    signal_added_hub = QtCore.pyqtSignal(CsaObjectHub)
    signal_delete_hub = QtCore.pyqtSignal(str)

    def __init__(self, client_email='', client_id=SERVER_ID, client_pass='', host=RELEASE):
        super(CsaClient, self).__init__()
        self.client_email = client_email
        self.client_id = client_id
        self.client_password = binascii.hexlify(client_pass.encode()).decode()

        self._push_token_connect = ''

        self._socket = SocketCsa(self.client_id)
        self._socket.connected.connect(self.slot_connected)
        self._socket.disconnected.connect(self.slot_disconnected)
        self._socket.error.connect(self.slot_error_socket)

        self.timer_handler_message = QtCore.QTimer()
        self.timer_handler_message.setInterval(120)             # Stop in the world handler message's
        self.timer_handler_message.timeout.connect(self.handler_message)

        self.hosts = generate_hosts(host)

        self.connect()

        self._hubs = list()

    @property
    def id(self):
        return self.client_id

    @id.setter
    def id(self, client_id):
        self.client_id = client_id
        self._socket.client_id = client_id

    def add_hub2client(self, hub_id, master_key, name_hub, role='01'):
        message_add2hub = CsaMessage()
        message_add2hub.init_manual(
            sender=self.id,
            receiver=hub_id,
            message_type=MessageType.UserRegistration.value,
            message_key=UserRegistration.ClaimHubByMasterKey.value,
            payload=['01', master_key, '02', role, '03', binascii.hexlify(name_hub.encode()).decode()]
        )
        return self._socket.send_message(message_add2hub, timeout_wait_answer=15)

    def del_hub2client(self, hub_id):
        message_del_hub = CsaMessage()
        message_del_hub.init_manual(
            sender=self.id,
            receiver=hub_id,
            message_type=MessageType.UserRegistration.value,
            message_key=UserRegistration.DetachUser.value,
            payload=[self.id, hub_id]
        )
        return self._socket.send_message(message_del_hub, timeout_wait_answer=10)

    def append_hub_obj(self, hub_id) -> CsaObjectHub:
        hub_obj = self.get_hub_obj(hub_id)
        if hub_obj is not None:
            return hub_obj
        hub_obj = CsaObjectHub(self._socket, '21', hub_id)
        self._hubs.append(hub_obj)
        logger.debug("Add HubObj {}".format(hub_id))
        self.signal_added_hub.emit(hub_obj)
        return hub_obj

    def get_hub_obj(self, hub_id) -> CsaObjectHub:
        for hub_obj in self._hubs:
            if hub_obj.id == hub_id:
                logger.debug("Get HubObj {}".format(hub_id))
                return hub_obj

    def remove_hub_obj(self, hub_id: str):
        for hub_obj in self._hubs:
            if hub_obj.id == hub_id:
                self._delete_message_in_heap(hub_id)        # special delete all message with sender == hub_id
                self._hubs.remove(hub_obj)
                logger.debug("Remove hub obj {}".format(hub_obj))
                self.signal_delete_hub.emit(hub_id)
                return
        # Todo fix AUT-2075
        logger.warning("Cant delete hub Model, NotFound!")

    def _delete_message_in_heap(self, sender: str):
        """
        Delete all message senders
        :param sender:
        :return:
        """
        for mess in self._socket.messages_heap:
            if mess.sender == sender:
                self._socket.messages_heap.remove(mess)

    def connect(self):
        """return state connect"""
        host, port = next(self.hosts)
        self._socket.connectToHost(host, port)
        return self._socket.waitForConnected()

    def reconnect(self):
        host, port = next(self.hosts)
        self._socket.connectToHost(host, port)

    def _reset_object(self):
        self._socket.message_number = 0
        self._socket.link = 0

    def hand_shake(self):
        message_connect = CsaMessage()
        message_connect.init_manual(
            sender=self.client_id,
            receiver=SERVER_ID,
            message_type='11',
            message_key='06',
            payload=[self.client_password])
        message_auth_request = self._socket.send_message(message_connect)
        if not message_auth_request.message_type:
            self._socket.close()
            return
        auth_data = message_auth_request.payload[0]
        auth_data_responce = auth_compute(auth_data)
        message_responce_auth = CsaMessage()
        message_responce_auth.init_manual(
            sender=self.client_id,
            receiver=SERVER_ID,
            message_type='15',
            message_key='01',
            payload=[auth_data_responce]
        )
        self._socket._write_message(message_responce_auth, timeout_ack=2)
        self.timer_handler_message.start()

    def connect_with_email(self, profi=False):
        message = CsaMessage()
        message.init_manual(
            sender=SERVER_ID,
            receiver=SERVER_ID,
            message_type='11',
            message_key='05',
            payload=[binascii.hexlify(self.client_email.encode()).decode(), '{:02x}'.format(profi)]
        )
        auth_request = self._socket.send_message(message, timeout_wait_answer=10)
        if not auth_request.message_type:
            self._socket.close()
            return
        auth_data = auth_request.payload[0]
        auth_data_responce = auth_compute(auth_data)
        message_responce_auth = CsaMessage()
        message_responce_auth.init_manual(
            sender=self.client_id,
            receiver=SERVER_ID,
            message_type='15',
            message_key='01',
            payload=[auth_data_responce]
        )
        answer = self._socket.send_message(message_responce_auth, timeout_wait_answer=5)
        if not answer.message_type:
            return
        self.id = answer.payload[0]
        self.hand_shake()

    def slot_connected(self):
        self._reset_object()
        self._socket.start_send_ping()
        self.connect_with_email()

    def slot_disconnected(self):
        logger.debug("Call signal disconnect")
        self._socket.stop_send_ping()
        logger.debug("Stop send ping")
        self.timer_handler_message.stop()
        logger.debug("Stop timer handler message")
        self._socket.cancel_all_task()
        logger.debug("Cancel all tasks")
        QtCore.QTimer.singleShot(5000, self.reconnect)
        logger.debug("Register singleShot reconnect when disconnect")
        self.signal_status_client.emit(False)

    def slot_error_socket(self, error):
        logger.warning("Error Socket {}".format(error))
        self.signal_status_client.emit(False)
        self.signal_error_connect_socket.emit()

    def _wait_message(self, link, timeout):
        return self._socket.create_task_wait_messa('', link, timeout_wait_message=timeout)

    def _cancel_wait_message(self, link):
        return self._socket._cancel_task_by_link(link)

    def _cancel_task_wait(self):
        pass

    def handler_message(self):
        while self._socket.messages_heap:
            message = self._socket.messages_heap.popleft()
            if message.message_type == MessageType.UserRegistration.value:
                self.handler_user_registration(message)
                self.signal_status_client.emit(True)
            elif message.message_type == MessageType.Updates.value:
                self.handler_updates(message)
            elif message.message_type == MessageType.Alarm.value:
                self.handler_alarms(message)

    def handler_updates(self, message: CsaMessage):
        hub_id = message.sender
        hub_obj = self.get_hub_obj(hub_id)
        if hub_obj is None:
            # TODO fix
            logger.warning("Hub obj not found while parse Update {}".format(hub_id))
            return
        hub_obj.update_handler(message)

    def handler_user_registration(self, message: CsaMessage):
        if message.message_key == UserRegistration.Connected.value:
            push_token_client, *hubs_roles = message.payload
            self._parse_connected(hubs_roles)
            self._push_token_connect = push_token_client
        elif message.message_key == UserRegistration.DetachOk.value:
            hub_id = message.sender
            client_id, *_ = message.payload
            self.remove_hub_obj(hub_id)
        elif message.message_key == UserRegistration.ClaimedWithId.value:
            hub_id, user_role, *_ = message.payload
            hub_obj = self.append_hub_obj(hub_id)
            hub_obj.role_user = user_role

    def _parse_connected(self, payload_connected):
        for i in range(0, len(payload_connected), 2):
            hub_id, user_role = payload_connected[i], payload_connected[i+1]
            hub_obj = self.append_hub_obj(hub_id)
            hub_obj.role_user = user_role

    def handler_alarms(self, message: CsaMessage):
        hub_id = message.sender
        hub_obj = self.get_hub_obj(hub_id)
        if hub_id is None:
            logger.error("Hub obj {} not found handler alarm {}".format(hub_id, message))
            return
        hub_obj.alarm_handler(message)

