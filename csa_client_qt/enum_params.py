from enum import IntEnum, unique, Enum


SERVER_ID = '00000000'

RELEASE = [
    ("178.62.38.104", 80),
    ("52.57.175.225", 80),
    ("52.57.131.150", 80),
    ("178.62.38.104", 2020),
    ("52.57.175.225", 2020),
    ("52.57.131.150", 2020),
]

DEVELOP = [
    ("46.101.2.130", 80),
    ("178.62.122.122", 80),
# 46.101.39.41:80
]


@unique
class DeviceEnums(Enum):
    DoorProtect = '01'
    MotionProtect = '02'
    FireProtect = '03'
    GlassProtect = '04'
    LeaksProtect = '05'
    MotionProtectCurtain = '06'
    RangeExtender = '07'
    CombiProtect = '08'
    FireProtectPlus = '09'
    KeyPad = '0a'
    SpaceControl = '0b'
    Button = '0c'
    MotionCam = '0d'
    MotionProtectPlus = '0e'
    DoorProtectPlus = '0f'
    UniversalDevice = '10'
    Transmitter = '11'
    Relay = '12'
    MotionProtectOutdoor = '13'
    StreetSiren = '14'
    HomeSiren = '15'
    StoveGuardSensor = '16'
    StoveGuardSwitch = '17'
    Socket = '1e'
    WallSwitch = '1f'
    Hub = '21'
    WireInput = '26'
    WireSiren = '27'


@unique
class MessageType(Enum):
    EditProfile = '02'
    Logs = '03'
    Security = '04'
    DeviceRegister = '05'
    DevCommands = '06'
    WriteParameter = '07'
    Alarm = '08'
    HubRegistration = '09'
    Wifi = '0a'
    NetWorkSettings = '0b'
    SecuredActions = '0c'
    Ping = '0d'
    PartialRegistration = '0e'
    SimBalance = '0f'
    AdmControl = '10'
    UserRegistration = '11'
    Camera = '12'
    Authentification = '15'
    Answer = '16'
    Connection = '18'
    Updates = '19'
    GroupRegistrations = '1a'
    Images = '1b'
    Firmware = '1c'
    HubService = '1d'
    HubConsole = '1e'


@unique
class HubConsole(Enum):
    ToHub = '00'
    FromHub = '01'


@unique
class UserRegistration(Enum):
    RegisterNewUser = '00'
    ClaimHubGuest = '01'
    GuestClaimOk = '02'
    UserId = '03'
    ShareHub = '04'
    RemindId = '05'
    ConnectUser = '06'
    DetachUser = '07'
    ClaimError = '08'
    LoginExists = '09'
    PhoneExists = '0a'
    RegisterWithId = '0b'
    ClaimedWithId = '0c'
    AlreadyClaimed = '0d'
    NotFound = '0e'
    Connected = '0f'
    ClaimHubMaster = '10'
    MasterKeyValid = '11'
    MasterKeyInvalid = '12'
    MasterKeyInUser = '13'
    GetUserData = '14'
    UserData = '15'
    NotUnique = '16'
    ShortSessionAccepted = '17'
    DetachOk = '18'
    ProfileChanges = '19'
    UserList = '1a'
    ConfirmRegToken = '1b'
    ReaskRegToken = '1c'
    AccountNotConfirmed = '1d'
    PurgePush = '1e'
    AddPush = '1f'
    SettingsSessionReq = '20'
    SettingsSessionStarted = '21'
    ForgotPassword = '22'
    NewPassword = '23'
    GetInvitations = '24'
    HubInvitations = '25'
    ConnectUserByToken = '26'
    RevokeInvite = '27'
    BadMail = '28'
    BadPhone = '29'
    UserListNew = '2a'
    ClaimHubByMasterKey = '2b'
    ProfiRequestHubAccess = '2c'
    ProfiHubAccessResponce = '2d'
    ConnectProfi = '2e'
    RegisterNewProfi = '2f'
    RequestUserList = '30'
    ShareHubNew = '31'
    ChangeUserRole = '32'
    ChangeUserPermissions = '33'
    WrongOrInactiveRequestId = '34'
    PullMessedAlarms = '35'
    RegisterNewUserNew = '36'
    ProfiHubAccessResponseResult = '37'
    GetHubList = '38'
    HubsList = '39'
    GetHubsListChunked = '3a'
    HubListChunk = '3b'
    ValidateUserToken = '3c'
    GetUserRoleForHub = '3d'


@unique
class Updates(Enum):
    RequestSettingsId = '00'
    CurrentSettingsId = '01'
    SettingsIdUpdate = '02'
    RequestFullsettings = '03'
    ObjectRemove = '04'
    SettingsBody = '05'
    ObjectAdded = '06'
    RequestFullStatus = '07'
    NoData = '08'
    StatusBody = '09'
    SetNStat = '0a'
    UpdateStatus = '0b'
    UpdateSettings = '0c'
    ForceCheck = '0d'
    GetFullData = '0e'
    EndFullData = '0f'
    ReadHubParams = '10'
    HubParams = '11'
    BreakUpdate = '12'
    SubscribeUpdates = '13'
    UnSubscribeUpdates = '14'
    Customselection = '15'
    CustomSelectionResult = '16'
    BulkSubscribeUpdates = '17'
    BulkUnSubscriveUpdates = '18'
    BulkReadHubParams = '19'
    HubSettingsSearch = '1a'
    HubSettingsSearchResult = '1b'
    HubSettingsSearchCompressedResult = '1c'
    HubMetadataSearchBySetring = '1d'
    HubMetaSearchResultChunk = '1e'


@unique
class Answer(Enum):
    Ack = '00'
    Delivered = '01'
    DeliveredCommandPerform = '02'
    DeliveredCommandNotPerform = '03'
    ModeFinish = '04'
    FailInsufissientAccess = '05'
    FailedUnknowCommand = '06'
    UndeliveredReceiverOffline = '07'
    UndeliveredWrongReceiver = '08'
    DeliveredWasAlreadyPerform = '09'
    FailedWrongParameters = '0a'
    FailedWrongMessageType = '0b'
    TransportException = '0c'
    RequestDelivered = '0d'
    ServerError = '0e'
    HubBusy = '0f'
    HubError = '10'
    WrongState = '11'
    ObjectLimit = '12'

    FailedNotEmpty = '14'
    FailedObjectNotFound = '15'
    HubBlockedByServiceProvider = '16'


@unique
class Alarms(Enum):
    Alarm = '00'
    Malfunction = '01'
    Security = '02'
    CommonEvent = '03'
    AlarmEvent = '04'
    MalfEvent = '07'
    UserEvent = '08'
    ServiceEvent = '09'


@unique
class Wifi(Enum):
    ScanNetworks = '00'
    Networks = '01'
    SelectWifiWork = '02'
    JoinError = '03'
    JoinSuccess = '04'
    WifiSettingAdv = '05'
    ForgetNewWork = '06'


@unique
class DeviceRegister(Enum):
    RequestRegisterDevice = '00'
    SearchStartDevice = '01'
    FinalRegisterDevice = '02'
    TimeOutRegisterDevice = '03'
    ErrorWhileRegDevice = '04'
    DeleteDevice = '05'
    CancelRegistrationDev = '06'
    RegistrationStep = '07'
    RequestRegistrationDevice = '08'


@unique
class PartialRegistration(Enum):
    RegisterRoom = '00'
    DeleteRoom = '01'
    ExistsRoomWithItParams = '02'
    RegisterGroup = '03'
    DeleteGroup = '04'
    ExistsGroupWithItParams = '05'


@unique
class WriteParameter(Enum):
    KeyWriteParam = '00'


@unique
class Security(Enum):
    Arm = '00'
    PerimetralArm = '01'
    Disarm = '02'
    ArmFailedSensorsState = '07'
    SelectiveArm = '08'
    SelectiveDisarm = '09'
    PerimentralDisarm = '0c'


@unique
class DevCommand(Enum):
    DeviceCommand = '00'


@unique
class Logs(Enum):
    GetLogs = '00'
    GetLogsByRoom = '01'
    GetLogsByDevice = '02'
    ResponseLogsByRoom = '03'
    ResponseLogsByDevice = '04'
    LogsResponse = '05'
    LogsNotResult = '06'
    LastLogs = '07'
    LogNotFound = '08'
    UnreadedEvents = '09'
    DropLogs = '0a'
    LogDropped = '0b'


@unique
class SecuredActions(Enum):
    SetFrequency = '00'
    DropSettings = '01'
    RestartHub = '02'
    SetFrequencyHub2 = '03'
    ActiveSimSlot = '04'


@unique
class NetWorkSettings(Enum):
    EthSettings = '00'
    WifiSettings = '01'
    GsmSettings = '02'
    TurnEth = '03'
    NetworkSetOk = '04'
    NetworkSetErr = '05'
    TurnWifi = '06'
    TurnGsm = '07'
