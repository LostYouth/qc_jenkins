import random
import logging
from collections import deque
from functools import partial

from PyQt5 import QtCore
from PyQt5.QtCore import QTimer

from PyQt5.QtNetwork import QTcpSocket

from app_setting import AppSetting
# package CSA
from csa_client_qt.enum_params import SERVER_ID
from csa_client_qt.csa_message.csa_message import CsaMessage, create_csa_message
from csa_client_qt.csa_message.utils import encrypt_message, add_crc, insert_service_byte
from csa_client_qt.csa_task import TaskWaitMessageExpected, TaskWaitMessageACK

logger = logging.getLogger(AppSetting.LOGGER_NAME)


class SocketCsa(QTcpSocket):

    def __init__(self, client_id):
        super(SocketCsa, self).__init__()

        self.client_id = client_id

        self.link = 0
        self.message_number = 0
        self._create_default_params()

        self._time_ping_period = 30
        self.timer_ping = QTimer()
        self.timer_ping.setInterval(self._time_ping_period * 1000)
        # Check send ping
        self.timer_ping.timeout.connect(self._check_send_ping)
        self._default_timeout_wait_ack = 2
        self.timer = QTimer()

        self.readyRead.connect(self._read_data)

        self.messages_heap = deque(maxlen=100)

        self._task_ack_message_wait = deque(maxlen=25)
        self._task_message_wait = deque(maxlen=25)

        self._stream_bytes = bytes()

    def start_send_ping(self):
        self.timer_ping.start()

    def stop_send_ping(self):
        self.timer_ping.stop()

    def _create_default_params(self):
        self.link = 0
        self.message_number = 0

    def get_message_number(self) -> int:
        self.message_number += 1
        return self.message_number

    def get_link(self) -> int:
        self.link += 1
        if self.link == 255:
            self.link = random.randint(1, 254)
        return self.link

    def _read_data(self):
        while not self.atEnd():
            data = self.read(1)
            self._stream_bytes += data
        self._check_message_exists()

    def _check_message_exists(self):
        message_bytearray = bytearray()
        message_need_ack = []

        for byte_data in self._stream_bytes:
            if byte_data == 0x02:
                message_bytearray.clear()
            elif byte_data == 0x03:
                m = create_csa_message(message_bytearray)
                logger.debug("<< {}".format(repr(m)))
                self.message_number = int(m._message_number, 16)
                if m.message_type == '16' and m.message_key == '00':
                    self._check_message_expected_message_ack(m)

                elif m.message_type == '05' and m.message_key == '07':
                    # IGNORE THIS MESSAGE SUPPORT REGISTER DEVICE FAMILY WALLSWITCH
                    self.send_ack_message(m)
                else:
                    self._check_message_expected_message(m)
                    self.messages_heap.append(m)
                    # not send ping
                    # after create message send ack message
                    message_need_ack.append(m)
                message_bytearray.clear()
            else:
                message_bytearray.append(byte_data)
        self._stream_bytes = message_bytearray
        while message_need_ack:
            m = message_need_ack.pop(0)
            self.send_ack_message(m)

    def _check_message_expected_message_ack(self, message: CsaMessage):
        for task in list(self._task_ack_message_wait):
            if task.check_message(message):
                task.set_result(message)

    def _check_message_expected_message(self, message):
        for task in list(self._task_message_wait):
            if task.check_message(message):
                task.set_result(message)

    def send_message(self, message: CsaMessage, timeout_wait_answer=30):
        link_expected = self.get_link()
        task_expected_message = TaskWaitMessageExpected('', "{:0>02x}".format(link_expected))

        self._task_message_wait.append(task_expected_message)
        self.timer.singleShot(
            timeout_wait_answer * 1000, partial(self._cancel_task_message_expected, task_expected_message)
        )

        ack = self._write_message(message, self._default_timeout_wait_ack, link=link_expected)
        if not ack.link:
            return ack
        return self._wait_answer_message(task_expected_message)

    def create_task_wait_messa(self, message_number, link, timeout_wait_message):
        task_expected_message = TaskWaitMessageExpected(message_number, link)
        self._task_message_wait.append(task_expected_message)
        self.timer.singleShot(timeout_wait_message * 1000, partial(self._cancel_task_message_expected, task_expected_message))
        return self._wait_answer_message(task_expected_message)

    def cancel_all_task(self):
        while self._task_message_wait:
            task = self._task_message_wait.popleft()
            task.set_result(CsaMessage())
        while self._task_ack_message_wait:
            task = self._task_ack_message_wait.popleft()
            task.set_result(CsaMessage())

    def _cancel_task_by_link(self, link):
        for task in list(self._task_message_wait):
            logger.debug("Cancel task by link {}".format(link))
            if task.get_link_wait() == link:
                task.set_result(CsaMessage())
                self._task_message_wait.remove(task)
                return True
        return False

    def _cancel_task_ack(self, task: TaskWaitMessageACK):
        task.set_result(CsaMessage())

    def _cancel_task_message_expected(self, task: TaskWaitMessageExpected):
        task.set_result(CsaMessage())

    def _write_message(self, message_csa: CsaMessage, timeout_ack, link=None):
        if not self.isOpen():
            return CsaMessage()

        message_flag = 0
        if not link:
            link = self.get_link()
        message_csa.set_message_link(link)

        message_csa.set_message_number(self.get_message_number())
        message_csa._message_flag = "{:02x}".format(message_flag)

        task_ack = TaskWaitMessageACK(message_csa._message_number, message_csa.link)
        self.timer.singleShot(timeout_ack * 1000, partial(self._cancel_task_ack, task_ack))
        self._task_ack_message_wait.append(task_ack)
        self.__write_message(message_csa)
        result_wait_ack = self._wait_answer_message(task_ack)
        if not result_wait_ack:
            self.close()
        return result_wait_ack

    def __write_message(self, message_csa: CsaMessage):
        logger.debug(">> {}".format(message_csa))
        bytes_message = bytes(message_csa)
        encrypt_mess = bytearray(encrypt_message(bytes_message))
        add_crc(encrypt_mess)
        w_mess = insert_service_byte(encrypt_mess)
        self.writeData(w_mess)
        self.flush()

    def _wait_answer_message(self, task):
        loop = QtCore.QEventLoop()
        task.signal_close_task.connect(loop.quit)
        loop.exec_()
        # while not task.done():
        #     QApplication.processEvents()
        return task.get_result()

    def _check_send_ping(self):
        self.send_ping()
        logger.debug("Send ping message and set temp ping var 0")

    def send_ping(self):
        ping_message = CsaMessage()
        ping_message.init_manual(
            sender=self.client_id,
            receiver=SERVER_ID,
            message_type='0d',
            payload=[]
        )
        self._write_message(ping_message, timeout_ack=1)

    def send_ack_message(self, message: CsaMessage):
        ack_message = CsaMessage()
        ack_message.init_manual(
            sender=self.client_id,
            receiver=SERVER_ID,
            message_type='16',
            message_key='00',
            payload=[message._message_number]
        )
        ack_message.set_message_link(self.get_link())
        ack_message.set_message_number(self.get_message_number())
        ack_message._message_flag = '00'
        self.__write_message(ack_message)
