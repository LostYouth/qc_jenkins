from .telegram_api import TelegramBot


def setup_telegram(*args):
    TelegramBot.set_cred(*args)
