import requests
import logging

from app_setting import AppSetting

logger = logging.getLogger(AppSetting.LOGGER_NAME)


class TelegramBot:
    bot_cred = ''
    chat_id = -1
    _url = ''

    @classmethod
    def set_cred(cls, bot_cred: str, chat_id: int):
        cls.bot_cred = bot_cred
        cls.chat_id = chat_id
        cls._url = 'https://api.telegram.org/bot{}/'.format(bot_cred)

    @classmethod
    def get_updates_json(cls):
        reque = "{}{}".format(cls._url, "getUpdates")
        res = requests.get(reque)
        logger.debug("Response getUpdates {}".format(res.json()))

    @classmethod
    def send_message(cls, message_text):
        url = "{}{}".format(cls._url, 'sendMessage')
        response = requests.post(url=url, data={'chat_id': cls.chat_id, 'text': "{}".format(message_text)})
        response = response.json()
        logger.debug("Response sendMessage {}".format(response))

    @classmethod
    def send_file(cls, file_path):
        url = "{}{}".format(cls._url, 'sendDocument')
        with open(file_path, 'rb') as file:
            responce = requests.post(url=url, data={'chat_id': cls.chat_id}, files={'document': file}, timeout=30)
            logger.debug(responce)
